;;; semantic/wisent/asm.wy -- Semantic LALR grammar for ASM

;; Copyright (C) 2002-2012 Free Software Foundation, Inc.
;;
;; Author: Anand Tamariya <atamariya@gmail.com>
;; Created: 24 Mar 2023

;; Keywords: syntax

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

%package wisent-asm-wy
%provide semantic/wisent/asm-wy

%languagemode  asm-mode

%case-insensitive

;; The default start symbol
%start compilation_unit
;; Alternate entry points
;;    - Needed by partial re-parse
%start include_declaration
%start label_declaration

;; -----------------------------
;; Block & Parenthesis terminals
;; -----------------------------
%type  <block>       ;;syntax "\\s(\\|\\s)" matchdatatype block

%token <block>       PAREN_BLOCK "(LPAREN RPAREN)"
%token <block>       BRACE_BLOCK "(LBRACE RBRACE)"
%token <block>       BRACK_BLOCK "(LBRACK RBRACK)"

%token <open-paren>  LPAREN      "("
%token <close-paren> RPAREN      ")"
%token <open-paren>  LBRACE      "{"
%token <close-paren> RBRACE      "}"
%token <open-paren>  LBRACK      "["
%token <close-paren> RBRACK      "]"

;; ------------------
;; Operator terminals
;; ------------------
%type  <punctuation> ;;syntax "\\(\\s.\\|\\s$\\|\\s'\\)+" matchdatatype string

%token <punctuation> NOTEQ       "<>"
%token <punctuation> PERCENT     "%"
%token <punctuation> MODEQ       "%="
%token <punctuation> AND         "&"
%token <punctuation> ANDAND      "&&"
%token <punctuation> ANDEQ       "&="
%token <punctuation> MULT        "*"
%token <punctuation> MULTEQ      "*="
%token <punctuation> PLUS        "+"
%token <punctuation> PLUSPLUS    "++"
%token <punctuation> PLUSEQ      "+="
%token <punctuation> COMMA       ","
%token <punctuation> MINUS       "-"
;;%token <punctuation> MINUSMINUS  "--"
%token <punctuation> MINUSEQ     "-="
%token <punctuation> DOT         "."
%token <punctuation> DIV         "/"
%token <punctuation> DIVEQ       "/="
%token <punctuation> COLON       ":"
%token <punctuation> SEMICOLON   ";"
%token <punctuation> LT          "<"
%token <punctuation> LSHIFT      "<<"
%token <punctuation> LSHIFTEQ    "<<="
%token <punctuation> LTEQ        "<="
%token <punctuation> EQ          "="
%token <punctuation> EQEQ        "=="
%token <punctuation> GT          ">"
%token <punctuation> GTEQ        ">="
%token <punctuation> QUESTION    "?"
%token <punctuation> XOR         "^"
%token <punctuation> XOREQ       "^="
%token <punctuation> OR          "|"
%token <punctuation> OREQ        "|="
%token <punctuation> OROR        "||"
%token <punctuation> COMP        "~"

;; -----------------
;; Literal terminals
;; -----------------
%type  <symbol>      ;;syntax "\\(\\sw\\|\\s_\\)+"
%token <symbol>      IDENTIFIER

%type  <string>      ;;syntax "\\s\"" matchdatatype sexp
%token <string>      STRING_LITERAL

%type  <number>      ;;syntax semantic-lex-number-expression
%token <number>      NUMBER_LITERAL

%type <unicode>      syntax "\\\\u[0-9a-f][0-9a-f][0-9a-f][0-9a-f]"
%token <unicode>     unicodecharacter

;; -----------------
;; Keyword terminals
;; -----------------

;; Generate a keyword analyzer
%type  <keyword> ;;syntax "\\(\\sw\\|\\s_\\)+" matchdatatype keyword

%keyword ADD      "add"
%keyword ALIGN    "align"
%keyword CALL     "call"
%keyword CLI      "cli"
%keyword DIV      "div"
%keyword EXTERN   "extern"
%keyword GLOBAL   "global"
%keyword INCLUDE  "include"
%keyword JMP      "jmp"
%keyword JNC      "jnc"
%keyword JNZ      "jnz"
%keyword JPE      "jpe"
%keyword JPO      "jpo"
%keyword LOAD     "load"
%keyword MOV      "mov"
%keyword NOP      "nop"
%keyword PUSH     "push"
%keyword RET      "ret"
%keyword RETI     "reti"
%keyword SECTION  "section"
%keyword STI      "sti"
%keyword TEST     "test"
%keyword XOR      "xor"

%%
;; ------------
;; LALR Grammar
;; ------------

;; This grammar is not designed to fully parse correct ASM syntax.  It
;; is optimized to work in an interactive environment to extract tokens
;; (tags) needed by Semantic.  In some cases a syntax not allowed by
;; the ASM language specification will be accepted by this grammar.
;; By default, the return type is (NAME START.END) for the first rule.

compilation_unit
  : include_declaration
  | label_declaration
  ;

include_declaration
  : IDENTIFIER STRING_LITERAL
    (INCLUDE-TAG (replace-regexp-in-string "\"" "" $2 nil 'literal) nil)
  ;

label_declaration
  : IDENTIFIER COLON
    (TAG $1 'label nil nil)
  ;

%%
;; Define the lexer for this grammar
(define-lex wisent-asm-lexer
  "Lexical analyzer that handles ASM buffers.
It ignores whitespaces, newlines and comments."
  semantic-lex-ignore-whitespace
  semantic-lex-ignore-newline
  semantic-lex-ignore-comments
  ;;;; Auto-generated analyzers.
  wisent-asm-wy--<number>-regexp-analyzer
  wisent-asm-wy--<string>-sexp-analyzer
  ;; Must detect keywords before other symbols
  wisent-asm-wy--<keyword>-keyword-analyzer
  wisent-asm-wy--<symbol>-regexp-analyzer
  wisent-asm-wy--<punctuation>-string-analyzer
  wisent-asm-wy--<block>-block-analyzer
  ;; In theory, Unicode chars should be turned into normal chars
  ;; and then combined into regular ascii keywords and text.  This
  ;; analyzer just keeps these things from making the lexer go boom.
  wisent-asm-wy--<unicode>-regexp-analyzer
  ;;;;
  semantic-lex-default-action)

;;; semantic/wisent/asm.wy ends here
