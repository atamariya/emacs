;;; latex.srt --- SRecode templates for latex-mode

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

set mode "latex-mode"

set comment_start  "%"
set comment_end    "%"
set comment_prefix "%"

context file

template empty :file :user :time
----
{{>:filecomment}}

\documentclass{article}

\title{ {{^}} }
\author{ {{AUTHOR}} }
\date{ {{MONTHNAME}} {{YEAR}} }

\begin{document}
\section{Introduction}

\end{document}
----


;; end
