;; java.srt

;; Copyright (C) 2009-2020 Free Software Foundation, Inc.

;; Author: Eric M. Ludlam <zappo@gnu.org>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;; Function section is repeated in declaration and classsection
;; contexts. Declaration context is used when class tag is inserted
;; along with function tags. classdecl context is used when only
;; function tag is inserted in pre-existing class.

set mode "java-mode"
set escape_start "{{"
set escape_end "}}"

context file

set comment_start  "/**"
set comment_end    " */"
set comment_prefix " *"

template empty :file :user :time :java :indent
"Fill out an empty file."
sectiondictionary "CLASSSECTION"
set NAME macro "FILENAME_AS_CLASS"
----
{{>:filecomment}}

{{>CLASSSECTION:declaration:class}}

----
bind "e"

template empty-main :file :user :time :java :indent
"Fill out an empty file with a class having a static main method"
sectiondictionary "CLASSSECTION"
set NAME macro "FILENAME_AS_CLASS"
----
{{>:filecomment}}

{{<CLASSSECTION:declaration:class}}
public static void main(String args[]) {
       {{^}}
}
{{/CLASSSECTION}}
----
bind "l"

context declaration

template package :java :indent :blank
"Template to declare a package."
----
package {{CURRENT_PACKAGE}};
----

template import :java :indent :blank
"Template to import a package."
----
{{#IMPORTS}}{{#GROUP}}
import {{IMPORT}};{{/GROUP}}
{{/IMPORTS}}
----
bind "i"


;;; Semantic Tag support
;;
template class :indent :blank :java
"Insert a Java class with the expectation of it being used by a tag inserter.
Override this to affect applications, or the outer class structure for
the user-facing template."
----
{{>:declaration:package}}

{{>:declaration:javadoc-class}}
{{>:declaration:class-signature}} {
   {{^}}
}
----
bind "c"

template class-signature
"Note: There's an extra space at the end."
----
public {{?TYPE}} {{?NAME}}{{#PARENTS}}
{{#FIRST}}extends {{NAME}}{{/FIRST}}{{#NOTFIRST}}, {{NAME}}{{/NOTFIRST}}{{/PARENTS}}{{#INTERFACES}}{{#FIRST}}
implements {{NAME}}{{/FIRST}}{{#NOTFIRST}}, {{NAME}}{{/NOTFIRST}}{{/INTERFACES}}
----

template interface :indent :blank :java
"Insert a Java interface with the expectation of it being used by a tag inserter."
----
{{>:declaration:class}}
----

template function :indent :blank
"Used in class."
----
{{>:misc:fn-signature}} {
{{CODE}}{{^}}
}
----
bind "m"

template function-prototype :indent :blank
"Used in interface."
----
{{>:misc:fn-signature}};
----

context misc

template arglist
"Insert an argument list for a function.
@todo - Support smart CR in a buffer for not too long lines."
----
({{#ARGS}}{{TYPE}} {{NAME}}{{#NOTLAST}}, {{/NOTLAST}}{{/ARGS}})
----

template funcall
"Insert an argument list for a function.
@todo - Support smart CR in a buffer for not too long lines."
----
{{NAME}}({{#ARGS}}{{NAME}}{{#NOTLAST}}, {{/NOTLAST}}{{/ARGS}});
----

template fn-signature
----
{{PROTECTION}} {{?TYPE}} {{?NAME}}{{>:misc:arglist}}
----

context classdecl

template function :indent :blank
"Used in class."
----
{{>:misc:fn-signature}} {
{{CODE}}{{^}}
}
----
bind "m"

template function-prototype :indent :blank
"Used in interface."
----
{{>:misc:fn-signature}};
----

template variable :indent :blank
"Insert a variable declaration."
----
{{?TYPE}} {{?NAME}}{{#HAVEDEFAULT}} = {{DEFAULT}}{{/HAVEDEFAULT}};
----
bind "v"

;;; Java Doc Comments
;;
context classdecl

prompt GROUPNAME "Name of declaration group: "

template javadoc-function-group-start :indent :blank
----
/**
 * {{?GROUPNAME}}
 * @{
 */

----

template javadoc-function-group-end :indent :blank
----
/**
 * @}
 */

----

context declaration

template javadoc-class :indent :blank :time :user :tag
----
/**
 * {{DOC}}{{^}}
 *
 * Created: {{DATE}}
 *
 * @author {{AUTHOR}}
 * @version
 * @since
 */
----

template javadoc-function :indent :blank :tag
----
/**
 * {{DOC}}{{^}}
 * {{#ARGS}}
 * @param {{?NAME}} - {{DOC}}{{/ARGS}}
 * @return {{TYPE}}{{#THROWS}}
 * @exception {{NAME}} - {{EXDOC}}{{/THROWS}}
 */
----

template javadoc-variable-same-line
----
/**< {{DOC}}{{^}} */
----

template javadoc-section-comment :blank :indent
"Insert a comment that separates sections of an Emacs Lisp file."
----

/** {{?TITLE}}
 *
 * {{^}}
 */

----


;; end
