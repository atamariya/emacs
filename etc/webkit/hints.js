var __WKViewHints = Object.freeze((function(){
  'use strict';
  var hints = [];

  function addHint(elem, hintText) {
    let bounding = elem.getBoundingClientRect();
    if (bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth) &&
        (elem.offsetWidth > 0 || elem.offsetHeight > 0 || elem.getClientRects().length > 0) &&
        hints.every(
          function (other) {
            let other_bounding = other.getBoundingClientRect();
            return !(Math.abs(other_bounding.top - bounding.top) < 5
                     && Math.abs(other_bounding.left - bounding.left) < 5)
          })
       ){
      let hint = document.createElement('div');
      hint.setAttribute('webkitviewhint', 'hint');
      hint.style.left = bounding.left + 'px';
      hint.style.top = bounding.top + 'px';
      if (elem.nodeName == "INPUT"
	  || elem.nodeName == "BUTTON"
	  || elem.nodeName == "TEXTAREA") {
	    // Add a container div
	    let parent = elem.parentNode;
	    parent.removeChild(elem);

	    let container = document.createElement('span');
	    container.className = "_hint";
	    container.appendChild(elem);
	    container.appendChild(hint);
	    parent.appendChild(container);
	} else
	    elem.appendChild(hint);
      hint.appendChild(document.createTextNode(hintText));
      hints.push(hint);
    }
  }

  return {
    init: function(hintKeys) {
      let N = hintKeys.length;
      let tags = 'button, input, [href], select, textarea, [tabindex]:not([tabindex="-1"])';
      let elems = document.querySelectorAll(tags);
      let hintPadLen = Math.ceil(Math.log(elems.length)/Math.log(N));
      let idxToHintText = function (idx) {
        return idx.toString(N).padStart(hintPadLen, '0').split('').map(
          digit => hintKeys.charAt(parseInt(digit, N))).join('');};

      elems.forEach((elem, idx) => addHint(elem, idxToHintText(idx)));

      return hints.length;
    },
      reset : function() {
          hints.forEach(hint => hint.parentNode.removeChild(hint));
          hints = [];
          return -1;
      },
      update: function(key) {
      if (!key) this.reset();

      let newHints = hints.filter(hint => hint.innerText.startsWith(key));
      if (newHints.length > 1){
        hints.forEach(function (hint) {
          if (!hint.innerText.startsWith(key))
            hint.parentNode.removeChild(hint);
        });
        newHints.forEach(function (hint) {
          hint.innerText = hint.innerText.substring(1)
        });
        hints = newHints;
        return hints.length;
      }
      else if (newHints.length == 1){
        let selected = newHints[0].parentNode;
	  if (selected.className == "_hint") {
	      // Retrieve element from container div
	      selected = selected.children[0];
	  }
        console.log(selected);
        hints.forEach(hint => hint.parentNode.removeChild(hint));
        hints = [];
        selected.focus();
        selected.click();
        return 1;
      }
      else {
        hints.forEach(hint => hint.parentNode.removeChild(hint));
        hints = [];
        return -1;
      }
    },
  };
})());

function findactiveelement(doc) {
  doc = doc || document;
//alert(doc.activeElement.value);
   if(doc.activeElement.value != undefined){
      return doc.activeElement;
   }else{
        // recurse over the child documents:
        var frames = doc.getElementsByTagName('frame');
        for (var i = 0; i < frames.length; i++)
        {
                var d = frames[i].contentDocument;
                 var rv = findactiveelement(d);
                 if(rv != undefined){
                    return rv;
                 }
        }
    }
    return undefined;
};

var domain_script = {};
var blocklist = ["getvideo_info", "pagead"];
var open = XMLHttpRequest.prototype.open;
XMLHttpRequest.prototype.open = function() {
  var url = arguments[1];
  if (!checkUrl(url, this)) {
    // console.log("Blocking: " + url);
    // this.abort();
  } else {
    // console.log("Allowing: " + url);
    open.apply(this, arguments)
  }
};

navigator.sendBeacon = function(url){
  checkUrl(url);
  return true;
};

function checkUrl (url, req) {
  // Don't block the URL BEFORE checking for useful params
  // console.log("Blocklist: " + blocklist);
  // console.log("Testing: " + url);

  // Apply filter
  var pageUrl = new URL(window.location.href);
  // Next url may be CDN URL
  var urlObj = new URL(url, window.location.href);
  var host = pageUrl.host;
  // console.log("pl: " + urlObj.pathname);
  // console.log("pl: " + domain_script[host]);
  url = domain_script[host] ? domain_script[host](urlObj) : url;
  // console.log("After filter: " + url);

  // Apply blocklist
  var result = url && blocklist.find(e => url.match(e));
  return (url && !result);
}

domain_script["www.google.com"] = function(url) {
  // console.log("Check response " + url.pathname);
  if (url.pathname == "/url") {
    // Skip click tracking
    var redirect_url = window.location.searchParams.get("url");
    // console.log("Overriding response " + URI.decode(redirect_url));
    url = URI.decode(redirect_url);
  }
  return url;
}

// Local Variables:
// js-indent-level: 2
// End:
