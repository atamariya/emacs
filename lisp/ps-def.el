;;; ps-def.el --- Emacs definitions for ps-print -*- lexical-binding: t -*-

;; Copyright (C) 2007-2020 Free Software Foundation, Inc.

;; Author: Vinicius Jose Latorre <viniciusjl.gnu@gmail.com>
;;	Kenichi Handa <handa@gnu.org> (multi-byte characters)
;; Keywords: wp, print, PostScript
;; X-URL: http://www.emacswiki.org/cgi-bin/wiki/ViniciusJoseLatorre
;; Package: ps-print

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; See ps-print.el for documentation.

;;; Code:
(require 'descr-text)

(declare-function ps-plot-with-face "ps-print" (from to face))
(declare-function ps-plot-string    "ps-print" (string))

(defvar ps-bold-faces)                  ; in ps-print.el
(defvar ps-italic-faces)




;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Emacs Definitions


(defun ps-mark-active-p ()
  mark-active)


(defun ps-face-foreground-name (face)
  (face-foreground face nil t))


(defun ps-face-background-name (face)
  (face-background face nil t))


(defalias 'ps-frame-parameter 'frame-parameter)

;; Return t if the device (which can be changed during an emacs session) can
;; handle colors.  This function is not yet implemented for GNU emacs.
(defun ps-color-device ()
  (if (fboundp 'color-values)
      (funcall 'color-values "Green")
    t))


(defun ps-color-values (x-color)
  (cond
   ((fboundp 'color-values)
    (funcall 'color-values x-color))
   ((fboundp 'x-color-values)
    (funcall 'x-color-values x-color))
   (t
    (error "No available function to determine X color values"))))


(defun ps-face-bold-p (face)
  (or (face-bold-p face)
      (memq face ps-bold-faces)))


(defun ps-face-italic-p (face)
  (or (face-italic-p face)
      (memq face ps-italic-faces)))


(defun ps-face-strikeout-p (face)
  (eq (face-attribute face :strike-through) t))


(defun ps-face-overline-p (face)
  (eq (face-attribute face :overline) t))


(defun ps-face-box-p (face)
  (not (memq (face-attribute face :box) '(nil unspecified))))


;; Emacs understands the %f format; we'll use it to limit color RGB values
;; to three decimals to cut down some on the size of the PostScript output.
(defvar ps-color-format "%0.3f %0.3f %0.3f")
(defvar ps-float-format "%0.3f ")


(defun ps-font-name (font)
  "PS font name for FONT object."
  (let* ((info (font-info font))
         (file (aref info 12)))
    (or (font-get font :postscriptname)
        (file-name-base file))))

(defvar ps--cmap nil
  "Char table containing (FONT CODE (GLYPH INDICES)) for a
  character. Glyph indices are indices in PS font.")

(defvar ps--adjustment nil
  "X adjustment for composite glyphs.")

(defun ps-generate-postscript-with-faces1 (from to)
  ;; Generate some PostScript.
  (let ((face 'default)
	(position to)
	;; Emacs
	(property-change from)
	(overlay-change from)
        (font-change from)
        family char-info glyph glyphs chars
        fonts font-index font-glyphs font-chars
        pos next ltr
        components
        )
    (if (eq major-mode 'image-mode)
        (setq ps--cmap nil)
      ;; This is unnecessary in image-mode
    ;; Generate a map containing font-family and a list of (font index (glyph indices))
    ;; from that family used in the doc for unicode char
    (goto-char from)
    (setq ps--cmap (make-char-table 'cmap))
    (setq ps--adjustment (make-char-table 'adjustment))
    (while (and (not (eobp))
                (or (> (char-after) 255)
                    (and (re-search-forward "[^[:ascii:]]" to t)
                         (goto-char (match-beginning 0))))
                )
      (if (composite-char-p (point))
          (progn
            (setq char-info (find-composition (point))
                  pos (nth 0 char-info)
                  next (nth 1 char-info)
                  components (nth 2 char-info)
                  family (ps-font-name (lgstring-font components)))
            )
        (setq char-info (internal-char-font (point))
              pos (point)
              next (1+ (point))
              components nil
              glyph (cdr char-info)
              family (ps-font-name (car char-info))))

      (unless (assoc family fonts)
        (setq font-index (length fonts))
        (push (list family font-index (aref char-script-table (char-after))) fonts)
        (push (cons font-index nil) font-chars)
        (push (cons font-index nil) font-glyphs))
      (setq font-index (nth 1 (assoc family fonts)))

      (setq chars  nil)
      (setq glyphs (cdr (assoc font-index font-glyphs)))
      (if components
          ;; Loop for composites
          (dotimes (i (lgstring-glyph-len components))
            (setq glyph (lglyph-code (lgstring-glyph components i)))
            (unless (memq glyph glyphs)
              (push glyph glyphs))
            (push (1- (length (memq glyph glyphs))) chars)
            (if (= i 0)
                (aset ps--adjustment pos
                      (lglyph-adjustment (lgstring-glyph components i))))
            )
        ;; For non-composites
        (unless (memq glyph glyphs)
          (push glyph glyphs))
        (push (1- (length (memq glyph glyphs))) chars))
      (setcdr (assoc font-index font-glyphs) glyphs)

      ;; (message "%s %s" pos (reverse chars))
      (aset ps--cmap pos (cons font-index (reverse chars)))
      ;; (message "%s" char-info)
      (goto-char next)
      )
    ;; (pp fonts)
    ;; (pp font-chars)
    ;; (pp font-glyphs)

    ;; Use font-glyphs to generate new PS font
    (dolist (e font-glyphs)
      (let* ((font-index (car e))
             (font (pop fonts))
             (script (nth 2 font))
             (encoding-str nil)
             (map-str nil)
             (cmap-str nil)
             (i 0)
             (glyphs (cdr e)))
        (when (memq script '(han kana))
          ;; CIDFont for CJK script
          (mapc (lambda (k)
                  (setq cmap-str (concat cmap-str
                                         (format "<%02x> <%02x> %d\n" i i k))
                        i (1+ i)))
                (nreverse glyphs))
          (setq i (1- i))
          (ps-output (format "
/CIDInit /ProcSet findresource begin
12 dict begin
begincmap /CIDSystemInfo <<
/Registry (Adobe) /Ordering (Identity) /Supplement 0 >> def
/CMapName /CM def
/CMapType 1 def
1 begincodespacerange
<%02x> <%02x>
endcodespacerange

%d begincidrange
%s
endcidrange
endcmap CMapName currentdict /CMap defineresource
pop end end
/F%d /CM [/%s] composefont pop
"
                             0 i (1+ i) cmap-str font-index (car font))))

        (unless (memq script '(han kana))
          ;; Type42 font
          (mapc (lambda (k)
                  (setq encoding-str (concat encoding-str (format "/g%d " k))
                        map-str (concat map-str (format "/g%d %d\n" k k))))
                (nreverse glyphs))
          ;; (pp encoding-str)
          ;; (pp map-str)
          (ps-output (format "/E [%s] def\n" encoding-str))
          (ps-output (format "/C <<%s>> def\n" map-str))
          (ps-output (format "/F%d C E /%s DeriveFont\n" font-index (car font)))
          ))))

    (while (and (goto-char from)
                (char-after)
                (< from to))

      (and (< property-change to)  ; Don't search for property change
					; unless previous search succeeded.
	   (setq property-change (next-property-change from nil to)))
      (and (< overlay-change to)   ; Don't search for overlay change
					; unless previous search succeeded.
	   (setq overlay-change (next-overlay-change from)))
      (and (or (null font-change) (< font-change to))  ; Don't search for property change
					; unless previous search succeeded.
           ;; Range of characters sharing same font
           ;; Whitespace uses default font usually in between mixed font families.
           ;; Hence must be treated separately.
           (setq font-change (or (if (= (syntax-class (syntax-after from)) 0)
                                     (+ from (skip-chars-forward "[ \t\n]")))
                                 (when (eq (char-charset (char-after)) 'ascii)
                                   (and (re-search-forward "[[:multibyte:][:blank:]]" nil t)
                                        (1- (point))))
                                 (when (eq (char-charset (char-after)) 'unicode)
                                   (and (re-search-forward "[[:ascii:][:blank:]]" nil t)
                                        (1- (point))))
                                 ))
           )
      (setq position
            (if (eq major-mode 'image-mode)
                to
            (if (null font-change)
                (min property-change overlay-change)
              (min property-change overlay-change font-change)))
            ltr (get-char-code-property (char-after from) 'bidi-class)
            )
      (if (composite-char-p from)
          (setq char-info (find-composition from)
                position (nth 1 char-info)))
      ;; (message "%s %s %s %s %s" from property-change overlay-change font-change ltr)
      (setq face
	    (cond ((invisible-p from)
		   'emacs--invisible--face)
		  ((get-char-property from 'face))
		  (t 'default)))
      (ps-plot-with-face from position face (not (memq ltr '(R AL))))
      (setq from position)
      )))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'ps-def)

;;; ps-def.el ends here
