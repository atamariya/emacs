;;; mgit.el --- Meta git wrapper

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: git

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'magit)

(defvar git--tracked nil)
(make-variable-buffer-local 'git--tracked)

(defvar git--untracked nil)
(make-variable-buffer-local 'git--untracked)

(defvar git--untracked-stage nil)
(make-variable-buffer-local 'git--untracked-stage)

(defvar git--untracked-unstage nil)
(make-variable-buffer-local 'git--untracked-unstage)

(defvar mgit-keymap
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map magit-status-mode-map)
    (define-key map "g" 'git-status)
    (define-key map "s" 'git--stage)
    (define-key map "u" 'git--unstage)
    (define-key map "k" 'git--discard)
    map))

(defun git--exec (str &optional buf)
  (let* ((status-buf (or buf (generate-new-buffer " *temp*")))
         (status nil)
	 (inhibit-read-only t))
    (with-current-buffer status-buf
      (setq status
            (apply #'call-process "git" nil status-buf nil
	           (split-string str)))
      (unless (zerop status)
        (message "git %s" str)
        (user-error (buffer-string)))
      (when (and (not buf)
                 (> (point) 1))
        (delete-char -1)
        (buffer-string))
      )))

(defun git--toplevel ()
 (git--exec "rev-parse --show-toplevel"))

;;;###autoload
(defun git-status (&optional prefix)
  (interactive "P")
  (let* (;;(status-buf (magit-get-mode-buffer 'magit-status-mode))
	 (default-directory (git--toplevel))
         (file (buffer-file-name))
         (status-buf (get-buffer-create (concat "mgit: " default-directory)))
	 (inhibit-read-only t)
         (inhibit-modification-hooks t)
         cmd files tracked untracked start pos)
    (if file
        (setq file (replace-regexp-in-string (concat default-directory "/")
                                             "" file)))
    (set-buffer status-buf)
    (pop-to-buffer status-buf)
    (setq pos (point))
    ;; (font-lock-mode t)

    ;; Get files
    (erase-buffer)
    (when (or prefix
              (not git--tracked)
              (and git--tracked file
                   (not (member file git--tracked))))
      (message "here")
      (git--exec "status --porcelain" status-buf)
      (goto-char (point-min))
      (let (line)
        (while (not (eobp))
	  (setq line (buffer-substring-no-properties
		      (+ (point) 3)
		      (line-end-position)))
          (if (looking-at-p (regexp-quote "??"))
              (push line untracked)
            (push line tracked))
	  (forward-line 1))
        (setq git--untracked-stage nil)
        (setq git--untracked-unstage nil)
        (setq git--tracked (nreverse tracked))
        (setq git--untracked (nreverse untracked)))
      (erase-buffer))

    (setq tracked (append git--tracked git--untracked-stage))
    (setq untracked (sort (append git--untracked git--untracked-unstage) 'equal))
    (magit-insert-section (magit-root-section)
      (magit-insert-status-headers)

      (magit-insert-section (untracked)
        (magit-insert-heading "Untracked files:")
        (unless untracked (magit-cancel-section))
        (dolist (file untracked)
          (unless (member file git--untracked-stage)
          (magit-insert-section (file file)
            (insert (propertize file 'font-lock-face 'magit-filename) ?\n))
          )))

      (magit-insert-section (unstaged)
	(magit-insert-heading "Unstaged files:")
        (setq start (point))
	(setq cmd (concat "diff --no-prefix  -- "
                          (mapconcat 'identity tracked " ")))
	(git--exec cmd status-buf)
        (when (= start (point)) (magit-cancel-section))
	(save-excursion
	  (goto-char start)
	  (magit-diff-wash-diffs nil))
	)

      (magit-insert-section (staged)
	(magit-insert-heading "Staged files:")
        (setq start (point))
	(setq cmd (concat "diff --no-prefix --staged -- "
                          (mapconcat 'identity tracked " ")))
	(git--exec cmd status-buf)
        (when (= start (point)) (magit-cancel-section))
        (delete-char -1)
	(save-excursion
	  (goto-char start)
	  (magit-diff-wash-diffs nil))
	)

      ;; magit-status-sections-hook
      ;; (magit-insert-stashes)
      ;; (magit-insert-unpushed-to-upstream-or-recent)
      )

    (goto-char pos)
    (use-local-map mgit-keymap)
    ))

(defun git--apply-hunk (sections &optional args)
  (let* ((parent (oref (car sections) parent))
         (header (oref parent header))
         (patch (mapconcat #'identity
                           (magit-apply--adjust-hunk-new-starts
                            (mapcar #'magit-apply--section-content sections))
                           ""))
         (file (make-temp-file "patch-"))
         ;; (file (expand-file-name "~/patch.txt"))
         )
    (when (string-match "^diff --cc" (oref parent value))
      (user-error "Cannot un-/stage resolution hunks.  Stage the whole file"))
    (with-temp-file file
      (insert header patch))
    (git--exec (concat "apply -p0 " args " " file))
    ))

(defun git--apply-file (sections &optional args)
  (let* (revert source)
    (mapc (lambda (it)
            (if (setq source (oref it source))
                (git--exec (format "mv %s %s" (oref it value) source))
              (push (oref it value) revert)))
          sections)
    (if revert
        (git--exec (concat args " "
                           (mapconcat 'identity revert " "))))
    ))

(defun git--discard-hunk (sections &optional args)
  (let* ((parent (oref (car sections) parent))
         (header (oref parent header))
         (patch (mapconcat #'identity
                           (magit-apply--adjust-hunk-new-starts
                            (mapcar #'magit-apply--section-content sections))
                           ""))
         ;; (file (make-temp-file "patch-"))
         (file (expand-file-name "~/patch.txt"))
         )
    (when (string-match "^diff --cc" (oref parent value))
      (user-error "Cannot un-/stage resolution hunks.  Stage the whole file"))
    (with-temp-file file
      (insert header patch))
    (git--exec (concat "apply " args " " file))
    ))

(defun git--stage (&optional intent)
  "Add the change at point to the staging area.
With a prefix argument, INTENT, and an untracked file (or files)
at point, stage the file but not its content."
  (interactive "P")
  (let ((it (magit-apply--get-selection))
        (diff-type (magit-diff-type))
        (diff-scope (magit-diff-scope))
        (major-mode 'magit-status-mode))
    (let (files)
      (when (and (eq diff-type 'untracked)
                 (member diff-scope '(file files list)))
        (when (eq diff-scope 'file)
          (push it files))
        (mapc (lambda (it)
                (push (oref it value) git--untracked-stage))
              files)
        (git--apply-file files "add")))
    (pcase (list ;(oref it type)
                 (magit-diff-type)
                   (magit-diff-scope)
                   (magit-apply--ignore-whitespace-p it diff-type diff-scope))
        ;; (`(untracked     ,_  ,_) (magit-stage-untracked intent))
        ;; (`(unstaged  region  ,_) (magit-apply-region it "--cached"))
        (`(unstaged    hunk  ,_)  (git--apply-hunk (list it) "--cached"))
        (`(unstaged    hunks ,_)  (git--apply-hunk it "--cached"))
        ;; (`(unstaged   hunks  ,_) (magit-apply-hunks  it "--cached"))
        (`(unstaged      file   ,_) (git--apply-file (list it) "add"))
        (`(unstaged      files  ,_) (git--apply-file it "add"))
        (`(unstaged      list   ,_) (git--apply-file it "add"))
        ;; ('(unstaged    file   t) (magit-apply-diff   it "--cached"))
        ;; ('(unstaged   files   t) (magit-apply-diffs  it "--cached"))
        ;; ('(unstaged    list   t) (magit-apply-diffs  it "--cached"))
        ;; ('(unstaged    file nil) (magit-stage-1 "-u" (list (oref it value))))
        ;; ('(unstaged   files nil) (magit-stage-1 "-u" (magit-region-values nil t)))
        ;; ('(unstaged    list nil) (magit-stage-modified))
        (`(staged        ,_  ,_) (user-error "Already staged"))
        (`(committed     ,_  ,_) (user-error "Cannot stage committed changes"))
        (`(undefined     ,_  ,_) (user-error "Cannot stage this change")))
    ;; (call-interactively #'magit-stage-file)
    (git-status)
    ))

(defun git--unstage ()
  "Remove the change at point from the staging area."
  (interactive)
  (let ((it (magit-apply--get-selection))
        (diff-type (magit-diff-type))
        (diff-scope (magit-diff-scope))
        (major-mode 'magit-status-mode))
    (let (files)
      (when (and (eq diff-type 'staged)
                 (member diff-scope '(file files list)))
        (when (eq diff-scope 'file)
          (push it files))
        (mapc (lambda (it)
                (cond ((member (oref it value) git--untracked-stage)
                       (setq git--untracked-stage
                             (delete (oref it value) git--untracked-stage)))
                      ((member (oref it value) git--tracked))
                      (t (push (oref it value) git--untracked-unstage))
                  ))
              files)
        ))
    (pcase (list (magit-diff-type)
                 (magit-diff-scope)
                 (magit-apply--ignore-whitespace-p it diff-type diff-scope))
      (`(untracked     ,_  ,_) (user-error "Cannot unstage untracked changes"))
      ;; (`(unstaged    file  ,_) (magit-unstage-intent (list (oref it value))))
      ;; (`(unstaged   files  ,_) (magit-unstage-intent (magit-region-values nil t)))
      (`(unstaged      ,_  ,_) (user-error "Already unstaged"))
      ;; (`(staged    region  ,_) (magit-apply-region it "--reverse" "--cached"))
      (`(staged      hunk  ,_) (git--apply-hunk (list it) "--reverse --cached"))
      (`(staged      hunks ,_) (git--apply-hunk it "--reverse --cached"))
      ;; (`(staged     hunks  ,_) (magit-apply-hunks  it "--reverse" "--cached"))
      (`(staged      file   ,_) (git--apply-file (list it) "restore --staged"))
      (`(staged      files  ,_) (git--apply-file it "restore --staged"))
      (`(staged      list   ,_) (git--apply-file it "restore --staged"))
      ;; ('(staged      file   t) (magit-apply-diff   it "--reverse" "--cached"))
      ;; ('(staged     files   t) (magit-apply-diffs  it "--reverse" "--cached"))
      ;; ('(staged      list   t) (magit-apply-diffs  it "--reverse" "--cached"))
      ;; ('(staged      file nil) (magit-unstage-1 (list (oref it value))))
      ;; ('(staged     files nil) (magit-unstage-1 (magit-region-values nil t)))
      ;; ('(staged      list nil) (magit-unstage-all))
      (`(committed     ,_  ,_) (if magit-unstage-committed
                                   (magit-reverse-in-index)
                                 (user-error "Cannot unstage committed changes")))
      (`(undefined     ,_  ,_) (user-error "Cannot unstage this change")))
    (git-status)
    ))

(defun git--discard ()
  "Remove the change at point.

On a hunk or file with unresolved conflicts prompt which side to
keep (while discarding the other).  If point is within the text
of a side, then keep that side without prompting."
  (interactive)
  (magit-confirm 'discard "Discard")
  (let* ((it (magit-apply--get-selection))
         (major-mode 'magit-status-mode)
         (status (magit-diff-type))
         (hunk-arg (concat (if (eq status 'staged)
                               "--cached ")
                           "--reverse "))
         (file-arg (concat "restore "
                           (if (eq status 'staged)
                               "--staged "))))
    (pcase (list (magit-diff-type) (magit-diff-scope))
      (`(committed ,_) (user-error "Cannot discard committed changes"))
      (`(undefined ,_) (user-error "Cannot discard this change"))
      ;; (`(,_    region) (magit-discard-region it))
      ;; (`(,_      hunk) (magit-discard-hunk   it))
      ;; (`(,_     hunks) (magit-discard-hunks  it))
      ;; (`(,_      file) (magit-discard-file   it))
      ;; (`(,_     files) (magit-discard-files  it))
      ;; (`(,_      list) (magit-discard-files  it)))
      (`(,_      hunk)  (git--apply-hunk (list it) hunk-arg))
      (`(,_      hunks) (git--apply-hunk it hunk-arg))
      (`(,_      file)  (git--apply-file (list it) file-arg))
      (`(,_      files) (git--apply-file it file-arg))
      (`(,_      list)  (git--apply-file it file-arg))
      )
    (git-status)
    ))

(provide 'mgit)
