;;; tailor.el --- create tailoring pattern

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: tailor, pattern

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)

(defun tailor--bust (svg multiplier)
  (let* ((cmds nil)
         (body-length    72)
	 (chest          56)
	 (waist          56)
	 (shoulder       48)
         (sleeve-length  21)
         (arm-hole       26.5)
         (neck-back      2)
         (neck-front     4)
         (neck-opening   17)
         (sleeve-opening 19)
         ;; ang in radians
         (ang (acos (/ (- chest shoulder) arm-hole)))
         (h (* arm-hole (sin ang)))
         )
    (setq chest (/ chest 2.0)
          waist (/ waist 2.0)
          shoulder (/ shoulder 2.0)
          neck-opening (/ neck-opening 2.0)
          )
    (setq body-length    (* multiplier body-length   )
          chest          (* multiplier chest         )
          waist          (* multiplier waist         )
          shoulder       (* multiplier shoulder      )
          sleeve-length  (* multiplier sleeve-length )
          arm-hole       (* multiplier arm-hole      )
          neck-back      (* multiplier neck-back     )
          neck-front     (* multiplier neck-front    )
          neck-opening   (* multiplier neck-opening  )
          sleeve-opening (* multiplier sleeve-opening)
          h (* multiplier h))

    (setq cmds
          (list
           `(moveto (0 . ,(+ neck-back h)))
           `(lineto ((,(- chest waist) . ,body-length)))
           `(horizontal-lineto ,waist)
           `(vertical-lineto ,neck-front)
           ;; Neck lines
           `(smooth-curveto ((,(- chest neck-opening) . ,neck-front)
                             (,(- chest neck-opening) . 0)))
           `(lineto ((,(- chest shoulder) . ,neck-back)))
           `(smooth-curveto ((,(- chest shoulder) . ,(+ neck-back h))
                             (0 . ,(+ neck-back h))))
           ))
    (svg-path svg cmds :id "front" :fill "none" :stroke-color "black")
    (setq cmds
          (list
           `(moveto (0 . ,(+ neck-back h)))
           `(lineto ((,(- chest waist) . ,body-length)))
           `(horizontal-lineto ,waist)
           `(vertical-lineto ,neck-back)
           ;; Neck lines
           `(smooth-curveto ((,(- chest neck-opening) . ,neck-back)
                             (,(- chest neck-opening) . 0)))
           `(lineto ((,(- chest shoulder) . ,neck-back)))
           `(smooth-curveto ((,(- chest shoulder) . ,(+ neck-back h))
                             (0 . ,(+ neck-back h))))
           ))
    (svg-path svg cmds :id "back" :fill "none" :stroke-color "black")
    (setq ;ang (/ 3.14 6)
          ;; ang (/ 3.14 35)
          ang 0
          cmds
          (list
           ;; `(moveto (,(- chest shoulder) . ,neck-back))
           `(moveto (0 . ,(+ neck-back h)))
           `(lineto ((,(+ (* -1 sleeve-length (cos ang))
                          (* sleeve-opening (sin ang))
                          (- chest shoulder))
                      . ,(+ neck-back
                            (* sleeve-length (sin ang))
                            (* sleeve-opening (cos ang))))))
           `(lineto ((,(+ (* -1 sleeve-length (cos ang))
                          (- chest shoulder))
                      . ,(+ neck-back
                            (* sleeve-length (sin ang))))))
           `(lineto ((,(- chest shoulder) . ,neck-back)))
           `(smooth-curveto ((,(- chest shoulder) . ,(+ neck-back h))
                             (0 . ,(+ neck-back h))))
           ))
    (svg-path svg cmds :id "sleeve" :fill "none" :stroke-color "black"
              :transform (format "translate(-%d)" (* 3 multiplier))
              )
    ))

;;;###autoload
(defun tailor (&optional prefix)
  "Create a tailoring pattern (an SVG image)."
  (interactive "P")
  (let* ((buf (get-buffer-create "*Tailor*"))
         (svg (svg-create 400 400))
         (multiplier 2)
         (bbox nil)
         (stroke-width 0.5)
         (margin 10))
    (pop-to-buffer buf)

    ;; (widget-create 'text "body-length")
    (when prefix
      ;; Unit conversion
      ;; 1mm stroke width
      (setq multiplier
            (pcase (read-string "Unit: " nil nil '("cm" "in"))
              ("cm" (/ 96 2.54))
              ("in" 96))
            stroke-width (/ 96 25.4)
            margin (* margin multiplier)))
    (setq margin (* 2 multiplier))
    (dom-set-attribute svg 'stroke-width stroke-width)
    (tailor--bust svg multiplier)

    ;; (message "%s" (svg-bbox svg))
    (setq bbox (svg-bbox svg))
    (setf (nth 0 bbox) (- (nth 0 bbox) margin))
    (setf (nth 1 bbox) (- (nth 1 bbox) margin))
    (setf (nth 2 bbox) (+ (nth 2 bbox) margin))
    (canvas--zoom svg bbox)

    (with-current-buffer buf
      (setq inhibit-read-only t)
      (erase-buffer)
      ;; (canvas-mode nil svg)
      ;; (insert-image (svg-image svg))
      (svg-print svg)
      (image-mode)

      ;; (widget-setup)
      ;; (use-local-map widget-keymap)
      )
    ))

(provide 'tailor)
