;;; game-match.el --- Game for matching names with images

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: game-match, pattern

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)

(defvar game-match--dir nil)
(defvar game-match--size nil)

(defun game-match--prepare (names)
  (let* ((w (window-pixel-width))
         (h (window-pixel-height))
         (svg (svg-create w h))
         (padding (/ w 100))
         (i 0)
         (orig names)
         (order (number-sequence 0 (length names)))
         x y w1 h1 name j)
    ;; Randomize order of names
    (dotimes (i (length names))
      (setq j (random (length names)))
      (psetf (elt order j) (elt order i)
             (elt order i) (elt order j)))
    ;; (message "%s" order)
    (setq w1 (- (/ w 2) (* 2 padding))
          h1 (- (/ h (length names)) (* 2 padding))
          x 0 y 0 j 0)
    (while names
      (setq name (capitalize (file-name-base (car names))))
      (svg-text svg name :id (number-to-string i) :x (/ w1 5)
                :y (+ y (/ h1 2))
                :name (car names)
                :type "source"
                :font-size (/ h1 5))
      ;; (svg-rectangle svg (+ (/ w 2) padding) (+ y padding) w1 h1
      ;;                :id (number-to-string (1+ i))
      ;;                :fill "none"
      ;;                :stroke "black"
      ;;                :name (car names)
      ;;                :type "target"
      ;;                ;; :viewPort
      ;;                )
      (svg-embed-href svg (nth (nth j order) orig)
                      :id (number-to-string (+ i 2))
                      :width w1
                      :height h1
                      :x (+ (/ w 2) padding)
                      :y (+ y padding))
      (setq i (+ i 3)
            j (1+ j)
            y (+ y h1 (* 2 padding))
            names (cdr names))
      (setq game-match--size (- i 1))
      )
    svg))

(defun game-match--select-mode (key svg)
  (let (target desktop)
    (unless (memq key '(?u ?m ?g ?G ?R ?\M-w ?> escape ?\C-x ?A))
      ;; All operations which don't use canvas--undo-marker should
      ;; deselect objects
      (canvas--deselect))

    (setq canvas--mode
          (pcase key
            ;; Object operations
            (?u (unless (string= (dom-attr (last (dom-children svg)) 'id)
                                 (number-to-string game-match--size))
                  (canvas--undo svg)))
            ('escape (when (eq canvas--mode 'move)
                       (canvas--move-cancel))
                     (when (eq canvas--mode 'place)
                       (canvas--undo svg))
                     (canvas--deselect)
                     (svg-possibly-update-image svg)
                     nil)

            ;; Retain mode even if mouse moves outside the image
            (_ 'line)
            ))
    key))

;;;###autoload
(defun game-match (&optional prefix)
  "Create a game-matching pattern (an SVG image)."
  (interactive "P")
  (let* ((dir (if (or prefix (not game-match--dir))
                  (setq game-match--dir (read-string "Directory: " game-match--dir))
                game-match--dir))
         (buf (find-file-noselect (concat dir "/.temp.svg")))
         (names (directory-files dir nil "^[^.].*"))
         (canvas--stroke-width 10)
         (canvas--outline-style 'solid)
         (canvas-plugin-fn 'game-match--select-mode)
         ;; (canvas-act-in-region-fn 'game-match--act-in-region)
         ;; (canvas--hide-prompt t)
         svg)

    (pop-to-buffer buf)
    (setq svg (game-match--prepare names)
          canvas--svg (list svg)
          canvas--id game-match--size)

    (with-current-buffer buf
      (setq inhibit-read-only t)
      (erase-buffer)
      ;; (canvas-mode nil svg)
      ;; (insert-image (svg-image svg))
      ;; (image-mode)
      (svg-insert-image svg)
      )
    (ewp-crop-image-1 svg)
    ))

(provide 'game-match)
