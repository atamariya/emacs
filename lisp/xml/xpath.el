;;; xpath.el --- XPATH API using xmllint

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: xml, xpath

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 're-builder)

(defvar xpath--buffer nil)
(make-variable-buffer-local 'xpath--buffer)

(defvar xpath--update-fn 'xpath--do-update)
(make-variable-buffer-local 'xpath--update-fn)

;;;###autoload
(defun json-resolve (path json &optional raw)
  (interactive "sPath: ")
  (let* ((out (get-buffer-create " *out*"))
         (tokens (split-string path "\\."))
         res tok i)
    (while (and tokens (not (string-empty-p (car tokens))))
      ;; (message "%s" json)
      (setq tok (car tokens)
            i nil)
      (if (string-match "\\(.*\\)\\[\\(.*\\)\\]" tok)
          (setq tok (match-string 1 (car tokens))
                i   (match-string 2 (car tokens))))
      (if (and (not (string-empty-p tok))
               (hash-table-p json))
          (setq json (gethash tok json)))
      (setq tokens (cdr tokens))
      (when (and i
                 (setq i (string-to-number i)))
        (if (hash-table-p json)
            (progn
              (setq res (sort (hash-table-keys json) 'string<))
              ;; (message "Key %d: %s" i (nth i res))
              (setq json (gethash (nth i res) json)))
          (setq json (aref json i))))
      )
    ;; (message "%s" json)
    (if raw
        (setq res json)
    (cond ((stringp json)
           (setq res (format "\"%s\"" json)))
          ((numberp json)
           (setq res (format "%s" json)))
          ((eq json :null)
           (setq res "null"))
          ((eq json t)
           (setq res "true"))
          ((eq json :false)
           (setq res "false"))
          (t
           (with-current-buffer out
             (erase-buffer)
             (json-insert json)
             (json-pretty-print (point-min) (point-max))
             (setq res (buffer-string))))))
    ;; (message "%s" res)
    res))

(defun json--do-update ()
  "Update matches in the target window."
  (reb-assert-buffer-in-window)
  (let* ((xpath (buffer-substring (save-excursion
                                    (beginning-of-line)
                                    (point))
                                  (save-excursion
                                    (end-of-line)
                                    (point))))
         res json)
    (with-current-buffer reb-target-buffer
      (goto-char (point-min))
      (setq json (json-parse-buffer))
      (setq res (json-resolve xpath json))
      (if res
          (progn
            (unless reb-overlays
              (push (make-overlay (point-min) (point-max)) reb-overlays))
            (move-overlay (car reb-overlays) (point-min) (point-max))
            (overlay-put (car reb-overlays) 'display res))
        (setq reb-regexp (regexp-quote (car (last (split-string xpath "\\.")))))
        ))

    (unless res
      (reb-update-overlays))
    ))

;;;###autoload
(defun xpath-resolve (xpath)
  (interactive "sXpath: ")
  (let* ((out (get-buffer-create " *out*"))
          res status)
    (save-excursion
      (with-current-buffer out (erase-buffer))
      (setq status
            (call-process-region (point-min) (point-max) "xmllint" nil out nil
                                 "--xpath" xpath "-"))
      (setq res (with-current-buffer out (buffer-string)))
      ;; (message "%s %s" status res)
      (unless (= status 0)
        (setq res nil))
      res)))

(defun xpath--do-update ()
  "Update matches in the target window."
  (reb-assert-buffer-in-window)
  (let* ((xpath (buffer-substring (save-excursion
                                    (beginning-of-line)
                                    (point))
                                  (save-excursion
                                    (end-of-line)
                                    (point))))
         (res nil))
    (with-current-buffer reb-target-buffer
      (setq res (xpath-resolve xpath))
      (if res
          (progn
            (unless reb-overlays
              (push (make-overlay (point-min) (point-max)) reb-overlays))
            (move-overlay (car reb-overlays) (point-min) (point-max))
            (overlay-put (car reb-overlays) 'display res))
        (setq reb-regexp (regexp-quote (car (last (split-string xpath "/")))))
        ))

    (unless res
      (reb-update-overlays))
    ))

(defun xpath--auto-update (_beg _end _lenold &optional force)
  "Called from `after-update-functions' to update the display.
BEG, END and LENOLD are passed in from the hook."
  (funcall xpath--update-fn))

;;;###autoload
(defun xpath-builder ()
  "Construct an xpath interactively.
This command makes the current buffer the \"target\" buffer of
the xpath builder.  It displays a buffer named \"*XPATH-Builder*\"
in another window, initially containing an empty xpath.

As you edit the xpath in the \"*XPATH-Builder*\" buffer, the
results are updated in the target buffer."
  (interactive)
  (xpath--init "XPATH Builder" 'xpath--do-update))

;;;###autoload
(defun json-path-builder ()
  "Construct a JSON path interactively.
This command makes the current buffer the \"target\" buffer of
the JSON path builder.  It displays a buffer named \"*JSON PATH-Builder*\"
in another window, initially containing an empty JSON path.

As you edit the JSON path in the \"*JSON PATH-Builder*\" buffer, the
results are updated in the target buffer."
  (interactive)
  (xpath--init "JSON Path Builder" 'json--do-update))

(defun xpath--init (name fn)
  (if (and (eq (buffer-name) xpath--buffer))
      (message "Already in the %s" name)
    (when reb-target-buffer
      (reb-delete-overlays))
    (setq reb-target-buffer (current-buffer)
          reb-target-window (selected-window))
    (setq xpath--buffer (get-buffer-create (format "*%s*" name)))
    (select-window (or (get-buffer-window xpath--buffer)
		       (progn
			 (setq reb-window-config (current-window-configuration))
			 (split-window (selected-window) (- (window-height) 4)))))
    (switch-to-buffer xpath--buffer)
    (setq xpath--update-fn fn)
    (add-hook 'after-change-functions 'xpath--auto-update nil t)
    ;; At least make the overlays go away if the buffer is killed
    (add-hook 'kill-buffer-hook 'reb-kill-buffer nil t)
    ))

(provide 'xpath)
