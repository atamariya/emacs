;;; webkit-ace.el --- ace for webkit dynamic module -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Akira Kyle

;; Author: Akira Kyle <akira@akirakyle.com>
;; URL: https://github.com/akirakyle/emacs-webkit
;; Version: 0.1
;; Package-Requires: ((emacs "28.0") (webkit "0.1"))

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation; either version 3, or (at your
;; option) any later version.
;;
;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; See README.org

;;; Code:

(declare-function xwidget-webkit-current-session "xwidget")
(declare-function xwidget-webkit-add-user-style "xwidget")
(declare-function xwidget-webkit-add-user-script "xwidget")

(defun webkit--file-to-string (filename)
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

(defconst webkit--base (concat data-directory "webkit/"))

(defvar webkit--ace-init-flag nil)
(defvar webkit--hints-script nil)
(defvar webkit--hints-style  nil)

(defcustom webkit-ace-chars "asdfghjkl"
  "Link hint characters."
  :type 'string
  :group 'webkit)

(defun webkit-ace--callback (msg)
  ;; (message "webkit-ace--callback %s" msg)
  (let ((length msg)
        (key nil))
    (when (> length 1)
      (setq key (read-key "Type hint key"))
      (if (eq key ?\C-m)
          (xwidget-webkit-execute-script
           (xwidget-webkit-current-session)
           "__WKViewHints.update('');")
        (xwidget-webkit-execute-script
         (xwidget-webkit-current-session)
         (format "__WKViewHints.update('%c');" key)
         'webkit-ace--callback)
        ))))

(defun webkit-ace ()
  "Start a webkit ace jump."
  (interactive)
  (webkit--ace-init)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   (format "__WKViewHints.init('%s');" webkit-ace-chars)
   'webkit-ace--callback))

(defun webkit--ace-init ()
  "Insert user script and styles."
  (unless webkit--ace-init-flag
    (setq webkit--hints-script (webkit--file-to-string
                                (expand-file-name "hints.js" webkit--base)))
    (setq webkit--hints-style  (webkit--file-to-string
                                (expand-file-name "hints.css" webkit--base)))
    (when (file-exists-p xwidget-webkit-block-file)
    (with-temp-buffer
      (insert-file-contents
       (expand-file-name xwidget-webkit-block-file))
      (setq xwidget-webkit-block-list
            (split-string (buffer-string)))))
    (setq webkit--ace-init-flag t))

  (xwidget-webkit-remove-user-content (xwidget-webkit-current-session))
  (xwidget-webkit-add-user-style
   (xwidget-webkit-current-session)
   webkit--hints-style)
  (xwidget-webkit-add-user-script
   (xwidget-webkit-current-session)
   webkit--hints-script)
  (xwidget-webkit-add-user-script
   (xwidget-webkit-current-session)
   (format "blocklist = [%s]" (mapconcat #'(lambda (a)
                                             (format "\"%s\"" a))
                                         xwidget-webkit-block-list ", "))))

(provide 'webkit-ace)
