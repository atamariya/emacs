;;; lgtv.el --- control LG TV (WebOS)

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: tv, lg

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;; (eval-after-load 'websocket
  (require  'websocket)
  ;; )

(defvar lgtv--commands
  '("api/getServiceList"
    "audio/setMute"
    "audio/getStatus"
    "audio/getVolume"
    "audio/setVolume"
    "audio/volumeUp"
    "audio/volumeDown"

    "com.webos.applicationManager/getForegroundAppInfo"
    "com.webos.applicationManager/launch"
    "com.webos.applicationManager/listLaunchPoints"
    "com.webos.service.appstatus/getAppStatus"
    "com.webos.service.ime/sendEnterKey"
    "com.webos.service.ime/deleteCharacters"
    "com.webos.service.tv.display/set3DOn"
    "com.webos.service.tv.display/set3DOff"
    "com.webos.service.update/getCurrentSWInformation"

    "media.controls/play"
    "media.controls/stop"
    "media.controls/pause"
    "media.controls/rewind"
    "media.controls/fastForward"
    "media.viewer/close"

    "system/turnOff"
    "system.notifications/createToast"
    "system.launcher/close"
    "system.launcher/getAppState"
    "system.launcher/launch"
    "system.launcher/open"

    "tv/channelDown"
    "tv/channelUp"
    "tv/getChannelList"
    "tv/getChannelProgramInfo"
    "tv/getCurrentChannel"
    "tv/getExternalInputList"
    "tv/openChannel"
    "tv/switchInput"

    "webapp/closeWebApp"
    ))

(defvar lgtv--connection nil)
(defvar lgtv--client-key nil)
(defvar lgtv--command-stack nil)
(defvar lgtv--pairing-json (concat data-directory "tv/pairing.json"))

(defun lgtv--message-cb (ws frame)
  "Send commands from stack to the end point."
  (if frame
      (message "msg %s %s" lgtv--client-key (websocket-frame-text frame)))
  (cond ((and (not lgtv--client-key)
	      (string-match-p "client-key" (websocket-frame-text frame)))
	 (with-current-buffer (find-file-noselect
			       (concat user-emacs-directory
				       "tv/" lgtv--connection))
	   (erase-buffer)
	   (insert (websocket-frame-text frame))
	   (goto-char (point-min))
	   (search-forward "client-key")
	   (forward-char 3)
	   (set-mark (point))
	   (forward-sexp)
	   (setq lgtv--client-key
		 (buffer-substring-no-properties (region-beginning)
						 (region-end)))
	   (erase-buffer)
	   (insert lgtv--client-key)
	   (save-buffer)
	   (lgtv--register ws)))
	((string-match-p "pairingType" (websocket-frame-text frame))
	 ;; Wait for user prompt
	 )
	(t
	 (if lgtv--command-stack
	     (websocket-send-text ws (pop lgtv--command-stack))
	   (websocket-close ws)))
	))

(defun lgtv--message (cmd)
  "Push commands on a stack."
  (setq lgtv--command-stack
	(if lgtv--command-stack
	    (append lgtv--command-stack (list cmd))
	  (list cmd))))

(defun lgtv--execute (cmd &optional args type)
  (let* ((host lgtv--connection)
	 (ws (websocket-open
	      (format "ws://%s:3000" host)
	      :on-message #'lgtv--message-cb
	      ))
	 (fmt-reg "{ \"id\": \"%s\", \"type\": \"%s\", \"payload\": %s }")
	 (fmt-cmd (concat "{ \"id\": \"%s\", \"type\": \"%s\", \"uri\": \"%s\","
			  "\"payload\": %s }"))
	 (json nil))
    (lgtv--register ws)

    (setq json (format fmt-cmd "0c8b18a10002" (or type "request")
		       (if cmd (format "ssap://%s" cmd))
		       args
		       ))
    ;; (setq json
	  ;; "{\"id\":\"0c8b19a10002\",\"type\":\"request\",\"uri\":\"ssap://system.notifications/createToast\",\"payload\":{\"message\":\"TV is being controlled by Emacs!\"}}")
    (lgtv--message json)
    ))

(defun lgtv--register (ws)
  (let ((fmt-reg "{ \"id\": \"%s\", \"type\": \"%s\", \"payload\": %s }")
	(keyfile nil)
	(json nil))
    ;; Read client key from file
    (when (and (null lgtv--client-key)
	       (file-exists-p
		(setq keyfile
		      (concat user-emacs-directory
			      "tv/" lgtv--connection))))
      (with-temp-buffer
	(insert-file-contents keyfile)
	(setq lgtv--client-key
	      (buffer-substring-no-properties (point-min)
					      (point-max)))
	))

    (if lgtv--client-key
	(setq json (format
		    fmt-reg "0c8b18a10001" "register"
		    ;; (with-current-buffer (get-buffer-create "reg.json")
		    ;;   (buffer-substring-no-properties (point-min) (point-max)))
		    (with-current-buffer (get-buffer-create "*LG REG*")
		      (erase-buffer)
		      (insert-file-contents lgtv--pairing-json)

		      (re-search-forward "\n")
		      (insert (format "  \"client-key\": \"%s\",\n"
				      lgtv--client-key))
		      (buffer-substring-no-properties (point-min) (point-max)))
		    ))

      (setq json (format fmt-reg "0c8b18a10000" "register" "{}")))
    (websocket-send-text ws json)))

;;;###autoload
(defun lgtv-command (prefix)
  (interactive "p")
  (if (or (/= prefix 1) (null lgtv--connection))
    (setq lgtv--connection (read-string "Host: " lgtv--connection)))
  (unless lgtv--connection
    (error "Invalid host"))

  (let* ((b (get-buffer-create "*LGTV*"))
	 (cur (current-buffer))
	 cmd args)
    (set-window-buffer nil b)
    (with-current-buffer b
      (erase-buffer)
      (insert (format "LGTV connected at %s" lgtv--connection))
      )

    (setq cmd (completing-read "Command: " lgtv--commands)
	  args (read-string "Parameters (JSON): "))
    (unless (string-empty-p cmd)
      (lgtv--execute cmd (if (string-empty-p args) "{}" args)))
    (set-window-buffer nil cur)
    ))

(defvar wfd-wpa-cli "/usr/sbin/wpa_cli")
(defvar wfd--refresh-timer nil)
(defvar wfd--connection nil)

(defun wfd--refresh (ifname)
  (let* ((peers nil)
         (wpa_cli wfd-wpa-cli)
         ;; (ifname (read-string "Interface: " "p2p-dev-wlp2s0"))
         (buf (get-buffer-create "WFD"))
         name)
    ;; (pop-to-buffer-same-window buf)
    (with-current-buffer buf
    (setq inhibit-modification-hooks t)
    (erase-buffer)
    (widget-setup)
    (use-local-map widget-keymap)
    (insert "Click on a device to connect\n\n")

    ;; (shell-command (concat wpa_cli " p2p_find"))
    ;; (sleep-for 5)

    (setq peers (split-string
                 (shell-command-to-string (concat wpa_cli " p2p_peers"))
                 "\n"))
    (insert (pop peers))
    (newline)
    (while peers
      (unless (string-empty-p (car peers))
        (with-temp-buffer
          (insert
           (shell-command-to-string (concat wpa_cli " p2p_peer " (car peers))))
          (goto-char 1)
          (re-search-forward "device_name=" nil t)
          (set-mark (point))
          (end-of-line)
          (setq name (buffer-substring (region-beginning) (region-end))))
        (widget-create 'push-button
                       :format "%[%v%]\n"
                       :ifname ifname
                       :peer (car peers)
                       :notify (lambda (w _e _s)
                                 (wfd--connect (widget-get w :ifname)
                                               (widget-get w :peer)
                                               ))
                       name))
      (setq peers (cdr peers)))

    (insert "\n\n")
    (widget-create 'push-button
                   :format "%[%v%]\n"
                   :ifname ifname
                   :notify (lambda (w _e _s)
                             (wfd--quit (widget-get w :ifname)))
                   "Quit")
    )))

(defun wfd--quit (ifname)
  (let* ((wpa_cli wfd-wpa-cli)
         )
    (when wfd--refresh-timer
      (cancel-timer wfd--refresh-timer))
    (shell-command (concat wpa_cli " p2p_cancel"))
    (if wfd--connection
        (shell-command (format "nmcli dev disconnect %s" ifname)))
    (kill-buffer)
    ))

(defun wfd--connect (ifname peer)
  (let* ((wpa_cli wfd-wpa-cli)
         (name (format "'Wi-Fi P2P Peer %s'" peer))
         out)
    (with-temp-buffer
      ;; Check if the connection exists
      (insert
       (shell-command-to-string
        (format "nmcli con show %s" name)))
      (goto-char 1)
      (when (looking-at "Error")
        ;; Create a new connection
        (erase-buffer)
        ;; (message
        ;;  (format "nmcli con add con-name %s type wifi-p2p ifname %s ipv4.method auto peer %s wifi-p2p.wfd-ies 00000600901C4400C8 autoconnect no"
        ;;          name ifname peer))
        (insert
         (shell-command-to-string
          (format "nmcli con add con-name %s type wifi-p2p ifname %s ipv4.method auto peer %s wifi-p2p.wfd-ies 00000600901C4400C8 autoconnect no"
                  name ifname peer)))
        ))
    (shell-command (format "nmcli dev connect %s" ifname))
    (setq wfd--connection t)
    nil))

;;;###autoload
(defun wfd ()
  "Start a Miracast screen mirroring session."
  (interactive)
  (let* ((wpa_cli wfd-wpa-cli)
         (ifname (read-string "Interface: " "p2p-dev-wlp2s0"))
         (buf (get-buffer-create "WFD")))
    (pop-to-buffer-same-window buf)
    (when wfd--refresh-timer
      (cancel-timer wfd--refresh-timer))
    (shell-command (concat wpa_cli " p2p_find"))
    (wfd--refresh ifname)
    (setq wfd--connection nil
          wfd--refresh-timer
          (run-with-idle-timer 5 t 'wfd--refresh ifname))
    ))

(provide 'tv)
