;;; oauth2.el --- OAuth 2.0 Authorization Protocol  -*- lexical-binding:t -*-

;; Copyright (C) 2011-2021 Free Software Foundation, Inc

;; Author: Julien Danjou <julien@danjou.info>
;; Version: 0.16
;; Keywords: comm
;; Package-Requires: ((cl-lib "0.5") (nadvice "0.3"))

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Implementation of the OAuth 2.0 draft.
;;
;; The main entry point is `oauth2-auth-and-store' which will return a token
;; structure.  This token structure can be then used with
;; `oauth2-url-retrieve-synchronously' or `oauth2-url-retrieve' to retrieve
;; any data that need OAuth authentication to be accessed.
;;
;; If the token needs to be refreshed, the code handles it automatically and
;; store the new value of the access token.

;;; Code:

(eval-when-compile (require 'cl-lib))
(require 'plstore)
(require 'json)
(require 'url-http)

(defvar url-http-data)
(defvar url-http-method)
(defvar url-http-extra-headers)
(defvar url-callback-arguments)
(defvar url-callback-function)

(defvar oauth2-user-agent-string "Emacs oauth2"
  "Unique user agent string to avoid hitting request limit.")

(defvar oauth2-service-conf
  '((twitter "https://twitter.com/i/oauth2/authorize"
             "https://api.twitter.com/2/oauth2/token")
    (reddit "https://www.reddit.com/api/v1/authorize"
            "https://www.reddit.com/api/v1/access_token")
    (tumblr "https://www.tumblr.com/oauth2/authorize"
            "https://api.tumblr.com/v2/oauth2/token")
    (google "https://accounts.google.com/o/oauth2/auth"
            "https://oauth2.googleapis.com/token")
    (facebook "https://www.facebook.com/v15.0/dialog/oauth"
              "https://graph.facebook.com/v15.0/oauth/access_token")
    (instagram "https://api.instagram.com/oauth/authorize"
              "https://api.instagram.com/oauth/access_token")
    (amazon "https://www.amazon.com/ap/oa"
            "https://api.amazon.com/auth/o2/token")
    )
  "List of (service auth-url token-url)")

(defvar oauth2-provider-conf
  ;; (service . (client-id secret-key redirect-url))
  nil
  )

(defgroup oauth2 nil
  "OAuth 2.0 Authorization Protocol."
  :group 'comm
  :link '(url-link :tag "Savannah" "http://git.savannah.gnu.org/cgit/emacs/elpa.git/tree/?h=externals/oauth2")
  :link '(url-link :tag "ELPA" "https://elpa.gnu.org/packages/oauth2.html"))

(defvar code-verifier (base64-encode-string (format "%32s" (random))))

(defun oauth2-request-authorization (auth-url client-id &optional scope state redirect-uri)
  "Request OAuth authorization at AUTH-URL by launching `browse-url'.
CLIENT-ID is the client id provided by the provider.
It returns the code provided by the service."
  (browse-url (concat auth-url
                      (if (string-match-p "\?" auth-url) "&" "?")
                      "client_id=" (url-hexify-string client-id)
                      "&response_type=code"
		      "&duration=permanent"
                      "&code_challenge=" code-verifier
                      "&code_challenge_method=plain"
                      "&redirect_uri=" (url-hexify-string (or redirect-uri "urn:ietf:wg:oauth:2.0:oob"))
                      (if scope (concat "&scope=" (url-hexify-string scope)) "")
                      (if state (concat "&state=" (url-hexify-string state)) "")))
  (read-string "Enter the code your browser displayed: "))

(defun oauth2-request-access-parse ()
  "Parse the result of an OAuth request."
  (goto-char (point-min))
  (when (search-forward-regexp "^$" nil t)
    (json-read)))

(defun oauth2-make-access-request (url data)
  "Make an access request to URL using DATA in POST."
  (let ((url-request-method "POST")
        (url-request-data data)
        (url-user-agent oauth2-user-agent-string)
        (url-request-extra-headers
	 (append url-request-extra-headers
		 `(("Content-Type" . "application/x-www-form-urlencoded"))
		 ))
	)
    (with-current-buffer (url-retrieve-synchronously url)
      (let ((data (oauth2-request-access-parse)))
        (kill-buffer (current-buffer))
        data))))

(cl-defstruct oauth2-token
  plstore
  plstore-id
  client-id
  client-secret
  access-token
  refresh-token
  token-url
  service
  access-response)

(defvar oauth2-token nil)
(defvar oauth2-service nil)

(defun oauth2--find-service (url)
  (let ((services oauth2-service-conf)
        res)
    (while (and (not res)
		services)
      (if (string= url (nth 2 (car services)))
          (setq res (nth 0 (car services))))
      (pop services))
    res))

(defun url-oauth2-auth (url &optional _prompt _overwrite _realm args)
  ;; (message "%s %s %s %s %s" url _prompt _overwrite _realm args)
  ;; (message "%s %s" oauth2-service oauth2-token)
  (when t ;(and oauth2-service oauth2-token)
    (let* ((attrs (url-attributes url))
           (token (or (cdr (assoc 'token attrs))
                      (make-oauth2-token)))
           (service (oauth2-token-service token))
           (token-url (oauth2-token-token-url token))
	   (client-id (oauth2-token-client-id token))
	   (client-secret (oauth2-token-client-secret token))
           )
      (if args (oauth2-refresh-access token))
      (setq token (oauth2-token-access-token token))
      (if (and token-url
               (string-match-p (url-filename url) token-url))
          (if (eq service 'reddit)
              (concat "Basic "
                      (base64-encode-string
                       (concat client-id ":" client-secret))))
        (if token
            (format "Bearer %s" token)
          (error "Token invalid or missing")))
      )))

(url-register-auth-scheme "oauth2" nil 8)

(defun oauth2-login-test ()
  (let* ((service 'tumblr)
         (oauth2-conf (assoc service oauth2-service-conf))
	 (conf (assoc service oauth2-provider-conf))
	 (client-id (nth 1 conf))
	 (client-secret (nth 2 conf))
	 (callback-url (nth 3 conf))
	 (auth-scope "write offline_access")
	 ;; (auth-scope "write")
         )
    (setq token (oauth2-auth
                 (nth 1 oauth2-conf)
	         (nth 2 oauth2-conf)
	         client-id client-secret
	         auth-scope "nil" callback-url)
          )))

(defun oauth2-request-access (token-url client-id client-secret code &optional redirect-uri)
  "Request OAuth access at TOKEN-URL.
The CODE should be obtained with `oauth2-request-authorization'.
Return an `oauth2-token' structure."
  (when code
    (let* ((url-request-extra-headers
            ;; Reddit needs auth header
            (if (string= token-url
                         (nth 2 (assoc 'reddit oauth2-service-conf)))
	        `(("Authorization" . ,(concat "Basic "
                                              (base64-encode-string
                                               (concat client-id ":" client-secret))))
	          )))
	   (result
           (oauth2-make-access-request
            token-url
            (concat
             "client_id=" client-id
	     (when client-secret
               (concat  "&client_secret=" client-secret))
             "&access_type=offline"
             "&code=" code
             "&code_verifier=" code-verifier
             "&redirect_uri=" (url-hexify-string (or redirect-uri "urn:ietf:wg:oauth:2.0:oob"))
             "&grant_type=authorization_code"
	     ))))
      (unless (assoc 'access_token result)
	(error "Authorization failed: %s" (cdr (assoc 'message result))))
      (make-oauth2-token :client-id client-id
                         :client-secret client-secret
                         :access-token (cdr (assoc 'access_token result))
                         :refresh-token (cdr (assoc 'refresh_token result))
                         :token-url token-url
                         :service (oauth2--find-service token-url)
                         :access-response result))))

;;;###autoload
(defun oauth2-refresh-access (token)
  "Refresh OAuth access TOKEN.
TOKEN should be obtained with `oauth2-request-access'."
  (let ((plstore (oauth2-token-plstore token))
        (refresh-token (oauth2-token-refresh-token token))
        (url-request-extra-headers
         ;; Reddit needs auth header
         (if (eq 'reddit (oauth2-token-service token))
	     `(("Authorization" . ,(concat "Basic "
                                           (base64-encode-string
                                            (concat (oauth2-token-client-id token)
                                                    ":"
                                                    (oauth2-token-client-secret token)))))
	       )))
        (result nil))
    ;; Refresh token can only be used once for twitter
    ;; (setf (oauth2-token-refresh-token token) nil)
    ;; (setf (oauth2-token-access-token token) nil)
    (if refresh-token
        (setq result
              (oauth2-make-access-request
               (oauth2-token-token-url token)
               (concat "client_id=" (oauth2-token-client-id token)
		       (when (oauth2-token-client-secret token)
                         ;; Public clients don't have secret
                         (concat "&client_secret="
                                 (oauth2-token-client-secret token)))
                       "&refresh_token=" refresh-token
                       "&grant_type=refresh_token")))
      (error "Token invalid. Generate new token."))

    (if (not (assoc 'access_token result))
        (error "Error: %s" result)
      (setf (oauth2-token-access-response token) result)
      (setf (oauth2-token-access-token token)
            (cdr (assoc 'access_token result)))
      (setf (oauth2-token-refresh-token token)
            (cdr (assoc 'refresh_token result))))

    (when plstore
      ;; If the token has a plstore, update it
      (plstore-put plstore (oauth2-token-plstore-id token)
                   nil `(:access-token
                         ,(oauth2-token-access-token token)
                         :refresh-token
                         ,(oauth2-token-refresh-token token)
                         :access-response
                         ,(oauth2-token-access-response token)
                         ))
      (plstore-save plstore)))
  token)

;;;###autoload
(defun oauth2-auth (auth-url token-url client-id client-secret &optional scope state redirect-uri)
  "Authenticate application via OAuth2."
  (oauth2-request-access
   token-url
   client-id
   client-secret
   (oauth2-request-authorization
    auth-url client-id scope state redirect-uri)
   redirect-uri))

(defcustom oauth2-token-file (concat user-emacs-directory "oauth2.plstore")
  "File path where store OAuth tokens."
  :group 'oauth2
  :type 'file)

(defun oauth2-compute-id (auth-url token-url scope)
  "Compute an unique id based on URLs.
This allows to store the token in an unique way."
  (secure-hash 'md5 (concat auth-url token-url scope)))

;;;###autoload
(defun oauth2-auth-and-store (auth-url token-url client-id client-secret &optional scope state redirect-uri)
  "Request access to a resource and store it using `plstore'."
  ;; We store a MD5 sum of all URL
  (let* ((plstore (plstore-open oauth2-token-file))
         (id (oauth2-compute-id client-id scope state))
         (plist (cdr (plstore-get plstore id))))
    ;; Check if we found something matching this access
    (if plist
        ;; We did, return the token object
        (make-oauth2-token :plstore plstore
                           :plstore-id id
                           :client-id client-id
                           :client-secret client-secret
                           :access-token (plist-get plist :access-token)
                           :refresh-token (plist-get plist :refresh-token)
                           :token-url token-url
                           :service (plist-get plist :service)
                           :access-response (plist-get plist :access-response))
      (let ((token (oauth2-auth auth-url token-url
                                client-id client-secret scope state redirect-uri)))
        ;; Set the plstore
        (setf (oauth2-token-plstore token) plstore)
        (setf (oauth2-token-plstore-id token) id)
        (plstore-put plstore id nil `(:access-token
                                      ,(oauth2-token-access-token token)
                                      :refresh-token
                                      ,(oauth2-token-refresh-token token)
                                      :access-response
                                      ,(oauth2-token-access-response token)))
        (if (oauth2-token-refresh-token token)
            ;; Don't store temporary token
            (plstore-save plstore))
        token))))

(defun oauth2-url-append-access-token (token url)
  "Append access token to URL."
  (concat url
          (if (string-match-p "\?" url) "&" "?")
          "access_token=" (oauth2-token-access-token token)))

(defvar oauth--url-advice nil)
(defvar oauth--token-data)

(defun oauth2-authz-bearer-header (token)
  "Return `Authoriztions: Bearer' header with TOKEN."
  (cons "Authorization" (format "Bearer %s" token)))

(defun oauth2-extra-headers (extra-headers)
  "Return EXTRA-HEADERS with `Authorization: Bearer' added."
  (unless (assoc "Authorization" extra-headers)
    ;; Don't corrupt the headers by adding an add auth header if
    ;; there's already one - happens when the token has expired
  (cons (oauth2-authz-bearer-header (oauth2-token-access-token (car oauth--token-data)))
        extra-headers)))


;; FIXME: We should change URL so that this can be done without an advice.
(defun oauth2--url-http-handle-authentication-hack (orig-fun &rest args)
  (if (not oauth--url-advice)
      (apply orig-fun args)
    (let ((url-request-method url-http-method)
          (url-request-data url-http-data)
          (url-request-extra-headers
           (oauth2-extra-headers url-http-extra-headers)))
      (oauth2-refresh-access (car oauth--token-data))
      (url-retrieve-internal (cdr oauth--token-data)
                             url-callback-function
                             url-callback-arguments)
      ;; This is to make `url' think it's done.
      (when (boundp 'success) (setq success t)) ;For URL library in Emacs<24.4.
      t)))                                      ;For URL library in Emacsâ‰¥24.4.
;; (advice-add 'url-http-handle-authentication :around
;;             #'oauth2--url-http-handle-authentication-hack)

;;;###autoload
(defun oauth2-url-retrieve-synchronously (token url &optional request-method request-data request-extra-headers)
  "Retrieve an URL synchronously using TOKEN to access it.
TOKEN can be obtained with `oauth2-auth'."
  (let* ((oauth--token-data (cons token url)))
    (let ((oauth--url-advice nil)         ;Activate our advice.
          (url-request-method request-method)
          (url-request-data request-data)
          (url-request-extra-headers
           ;; request-extra-headers
           (oauth2-extra-headers request-extra-headers)
          ))
      (setq url (url-generic-parse-url url))
      (setf (url-attributes url) `((token . ,token) (auth-scheme . oauth2)))
      (url-retrieve-synchronously url))))

;;;###autoload
(defun oauth2-url-retrieve (token url callback &optional
                                  cbargs
                                  request-method request-data request-extra-headers)
  "Retrieve an URL asynchronously using TOKEN to access it.
TOKEN can be obtained with `oauth2-auth'.  CALLBACK gets called with CBARGS
when finished.  See `url-retrieve'."
  ;; TODO add support for SILENT and INHIBIT-COOKIES.  How to handle this in `url-http-handle-authentication'.
  (let* ((oauth--token-data (cons token url)))
    (let ((oauth--url-advice nil)         ;Activate our advice.
          (url-request-method request-method)
          (url-request-data request-data)
          (url-request-extra-headers
           ;; request-extra-headers
           (oauth2-extra-headers request-extra-headers)
          ))
      (setq url (url-generic-parse-url url))
      (setf (url-attributes url) `((token . ,token) (auth-scheme . oauth2)))
      (url-retrieve url callback cbargs))))

(provide 'oauth2)

;;; oauth2.el ends here
