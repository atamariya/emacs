;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

(require 'svg)

(defconst task--base (concat data-directory "images/task/"))

(defvar task--desktops nil)
(defvar task--windows nil)
(defvar task--desktop-cur nil)
(defvar task--window-cur nil)
(defvar task--desktop-width nil)
(defvar task--desktop-height nil)
(defvar task--path "/tmp/task/")
(defvar task--grid nil)
(defvar task--fullscreen nil)
(defvar task--gradient nil)
(defvar task--gradient-xml "
<defs>
    <linearGradient id=\"gradient\" x1=\"0%\" y1=\"0%\" x2=\"0\" y2=\"100%\">
      <stop offset=\"0%\" style=\"stop-color:skyblue;\" />
      <stop offset=\"100%\" style=\"stop-color:seagreen;\" />
    </linearGradient>
</defs>
")

(defun task--generate-image ()
  (let* ((b (get-buffer-create "*task*"))
         (default-directory task--path)
         line name)
    (make-directory task--path t)
    (with-current-buffer b
      ;; Find desktops and active one
      (erase-buffer)

      (shell-command "wmctrl -d" b)
      (when (re-search-forward "[*]" nil t)
        (setq task--desktop-cur (int-to-string (1- (line-number-at-pos)))))
      (when (re-search-forward "\\([0-9]*\\)x\\([0-9]*\\)" nil t)
        (setq task--desktop-width (string-to-number (match-string 1))
              task--desktop-height (string-to-number (match-string 2))
              ))
      (goto-char (point-max))
      (delete-char -1)
      (setq task--desktops (split-string (buffer-string) "\n"))

      ;; Find windows
      (erase-buffer)
      (shell-command "wmctrl -lG" b)
      (goto-char (point-max))
      (delete-char -1)
      (setq task--windows (split-string (buffer-string) "\n"))

      ;; Find active window
      (erase-buffer)
      (shell-command "xdotool getwindowfocus" b)
      (goto-char (point-max))
      (delete-char -1)
      (setq task--window-cur (buffer-string))
      (call-process-shell-command
       (concat "wmctrl -i -b toggle,hidden -r " task--window-cur))
      )

    ;; Generate image of windows on active desktop
    (dolist (i task--windows)
      (setq line (split-string i))
      (if (and task--fullscreen
               (string= (nth 0 line)
                        (format "0x0%x" (string-to-number task--window-cur))))
          (delete i task--windows)
      (when (string= (nth 1 line) task--desktop-cur)
        ;; (message "wmctrl -ia %s && xwd -id %s -out %s.xwd"
        ;;                        (car line) (car line) (car line))
        (call-process-shell-command
         (format "wmctrl -ia %s && xwd -id %s -out %s.xwd -silent"
                 (car line) (car line) (car line))
         ))))

    ;; Generate image of workspaces
    (dotimes (i (length task--desktops))
      (setq name (concat "desktop" (int-to-string i)))
      (call-process-shell-command
       (concat "wmctrl -s " (int-to-string i)
               "&& xwd -root -out " name ".xwd -silent"))
      )
    (call-process-shell-command (concat "wmctrl -s " task--desktop-cur))

    ;; Restore active window
    ;; (call-process-shell-command (concat "wmctrl -i -a " task--window-cur))
    ))

(defun task--get-pos (n)
  "Return an empty position ((M . N) (X . Y)) for N in a (M . N) 2D grid."
  (let* ((total n)
         (m (floor (sqrt n)))
         (n (ceiling (* m (/ 3 2 1.0))))
         ;; (n (* m (/ (window-pixel-width) (window-pixel-height))))
         (pos nil))
    (if (< (* m n) total)
        (setq n (1+ n)))
    (unless (and (= m (length task--grid))
                 (= n (length (aref task--grid 0))))
      (setq task--grid (make-vector m nil))
      (dotimes (i m)
        (aset task--grid i (make-vector n nil))))
    ;; (message "%s %s" total task--grid)
    (while (progn
             (setq pos (cons (random m) (random n)))
             (aref (aref task--grid (car pos)) (cdr pos))))
    (cons (cons m n) pos)))

(defun task--view (svg x y w h &rest args)
  "Insert IMAGE into the SVG structure.
IMAGE should be a file name."
  (svg--append
   svg
   (dom-node
    'view
    `((viewBox . ,(format "%d %d %d %d" x y w h))
      ,@(svg--arguments svg args)))))

(defun task--buffer ()
  "Render an SVG."
  (let* ((b (find-file-literally (concat task--path "main.svg")))
         (desktops (length task--desktops))
         (default-directory task--path)
         (win (get-buffer-window b))
         (width_s task--desktop-width)
         (fringes (window-fringes win))
         (width_w (- (window-pixel-width win)
                     (nth 0 fringes)
                     (nth 1 fringes)
                     (window-scroll-bar-width win)))
         (height_w (- (window-pixel-height win)
                      (window-scroll-bar-height win)))
         (top (svg-create width_w height_w))
         (canvas-plugin-fn 'task--select-mode)
         (canvas-act-in-region-fn 'task--act-in-region)
         name img pos line gap m n x y w h wins svg
         w1 h1 scale ox oy offset aspect_o aspect)
    ;; SVG href works with files in the same directory
    (call-process-shell-command
     (concat "cp " task--base "add.svg " task--path))
    (with-current-buffer b
      (setq canvas--outline-style 'solid)
      (erase-buffer)

      ;; Parse effects
      (setq gap (* 0.05 width_w)
            ox (/ gap (1+ desktops))
            w (/ (- width_w gap) desktops)
            h (* 0.3 height_w)
            aspect_o (/ task--desktop-width task--desktop-height 1.0)
            aspect (/ w h 1.0))
      (unless task--gradient
        (setq task--gradient (svg-load-from-xml task--gradient-xml)))
      (svg--append top task--gradient)
      (svg-rectangle top 0 0 "100%" h
                     :id "background"
                     :fill "url(#gradient)" :opacity "0.6")

      (setq default-directory task--path
            w1 w
            h1 h
            svg top;(svg-node top 'svg :id "workspaces" :height h)
            )
      (if (> aspect aspect_o)
          ;; Use height
          (setq oy (* 0.05 height_w)
                h (- h1 (* 2 oy))
                w (* h aspect_o)
                gap (- width_w (* w desktops))
                ox (/ gap (1+ desktops))
                w1 (/ (- width_w gap) desktops)
                )
        (setq w (- w1 0)
              h (/ w aspect_o)
              oy (/ (- h1 h) 2)
              ))

      (setq pos ox)
      (dotimes (i desktops)
        (setq name (concat "desktop" (int-to-string i)))
        (call-process-shell-command
         (concat "ffmpeg -y -i " name ".xwd " name ".png"))
        ;; Embedding is too heavy
        ;; (svg-embed svg (concat task--path name ".png") "image/png" nil
        (svg-embed-href svg (concat name ".png")
                     :id name :x (* pos) :y (* oy)
                     :type "desktop"
                   :width (* w ) :height (* h))
        (if (= i (string-to-number task--desktop-cur))
            (svg-rectangle svg pos oy w h
                           :id "marker_active_desktop"
                           :fill "none"
                           :stroke-width 5
                           :stroke-linejoin "round"
                           :opacity 0.6
                           :stroke "red"))
        ;; (svg-rectangle svg pos oy w1 h1
        ;;                :id (concat name "_b")
        ;;                :fill "none"
        ;;                :stroke "black")
        ;; (message "%s" svg)
        (setq pos (+ pos w1 ox)))
      ;; (svg-embed-href svg (concat "add.svg")
      ;;              :id "add" :x pos :y 0
      ;;              :border 2
      ;;              :width w :height h)
      ;; (setq pos (+ pos w gap))
      ;; (dom-set-attribute svg 'viewBox (format "0 0 %s %s" pos h))

      ;; Calculate grid
      (setq task--grid nil)
      (setq svg top;(svg-node top 'svg :id "windows" :y h)
            )
      (dolist (i task--windows)
        (setq line (split-string i))
        (when (string= (nth 1 line) task--desktop-cur)
          (push i wins)))
      (when wins
      (setq pos (task--get-pos (length wins)))
      ;; (message "%s %s" pos task--grid)
      (setq m (caar pos)
            n (cdar pos)
            gap (* 0.02 width_w)
            ox (/ gap (1+ m))
            w1 (/ (- width_w 0) n)
            offset h1
            h1 (/ (- height_w h1) m)
            aspect (/ w1 h1 1.0)))

      ;; Place windows in grid
      (dolist (i wins)
        (setq line (split-string i))
        (when (string= (nth 1 line) task--desktop-cur)
          (setq pos (task--get-pos (length wins)))
          ;; (message "%s %s" pos task--grid)
          (setq x (cddr pos)
                y (cadr pos)
                w (* (string-to-number (nth 4 line)))
                h (* (string-to-number (nth 5 line)))
                aspect_o (/ w h 1.0)
                pos (cdr pos))
          (aset (aref task--grid (car pos)) (cdr pos) 1)

          (if (> aspect aspect_o)
              ;; Use height
              (setq oy (* 0.1 h1)
                    h (- h1 (* 2 oy))
                    w (* h aspect_o)
                    ox (* 0.5 (- w1 w))
                    oy (+ offset oy)
                    )
            (setq w (- w1 gap)
                  h (/ w aspect_o)
                  oy (+ offset (* 0.5 (- h1 h)))
                  ))
          ;; (message "%s %s %s %s %s" line w h aspect aspect_o)
          (setq name (car line))
          (call-process-shell-command
           (concat "ffmpeg -y -i " name ".xwd " name ".png"))
          ;; (svg-rectangle svg
          ;;              (+ (* x w1)) (+ (* y h1) offset) w1 h1
          ;;              :id (concat name "_b")
          ;;              :fill "none"
          ;;              :stroke "black")
          (svg-embed-href svg (concat name ".png")
                       :id name
                       :type "window"
                       :x (+ (* x w1) ox)
                       :y (+ (* y h1) oy)
                       :width w :height h)
          ;; (message "%s" svg)
          ))
      ;; (dom-set-attribute svg 'viewBox (format "0 0 %s %s"
      ;;                                         (+ width_w gap)
      ;;                                         (+ height_w gap)))
      ;; (message "%s" svg)
      (svg-insert-image top)
      (setq canvas--svg (list top))
      ;;       canvas--id 10)
      ;; (ewp-crop-image-1 top)
      ;; (svg-print top)
      ;; (write-file nil)
      top
      )

    ;; (set-window-buffer nil b)
    ))

(defun task--buffer1 ()
  "Insert in text buffer."
  (let* ((b (get-buffer-create "*task*"))
         (desktops (length task--desktops))
         (default-directory task--path)
         (width_s task--desktop-width)
         (width_w (window-pixel-width))
         name img pos line)
    (with-current-buffer b
      (erase-buffer)
      (insert "\n ")
      (forward-char -1)

      (dotimes (i desktops)
        (setq name (concat "desktop" (int-to-string i)))
        (call-process-shell-command (concat "ffmpeg -y -i " name ".xwd -vf scale=256:-1 "
                                            name ".png"))
        (setq img (create-image (concat task--path name ".png")))
        (insert-image img)
        ;; (insert-image-file (concat name ".png"))
        (insert "     ")
        (forward-char -1)
        )

      (insert "\n\n ")
      (forward-char -1)
      (dolist (i task--windows)
        (setq line (split-string i))
        (when (string= (nth 1 line) task--desktop-cur)
          (setq name (car line))
          (call-process-shell-command
           (concat "ffmpeg -y -i " name
                   ".xwd -vf scale=iw*320/" width_s ":-1 "
                   name ".png"))
          (setq img (create-image (concat task--path name ".png")))
          (insert-image img)
          ;; (message "%s" (image-size img :pixels))
          ;; (insert-image-file (concat name ".png"))
          (insert " ")
          (setq pos (or (car (posn-x-y (posn-at-point))) 0))
          (if (< (+ pos (car (image-size img :pixels))) width_w)
              (insert " ")
            (setq pos 0)
            (insert "\n\n "))
          (forward-char -1)
          ))
      ;; (center-line)
      )))

(defun task--select-mode (key svg)
  (let (target desktop)
    (unless (memq key '(?u ?m ?g ?G ?R ?\M-w ?> escape ?\C-x ?A))
      ;; All operations which don't use canvas--undo-marker should
      ;; deselect objects
      (canvas--deselect))

    (setq canvas--mode
          (pcase key
            ;; Object operations
            ('escape (when (eq canvas--mode 'move)
                       (canvas--move-cancel))
                     (when (eq canvas--mode 'place)
                       (canvas--undo svg))
                     (when (and (eq canvas--mode 'conn)
                                canvas--temp-points)
                       (svg-polyline svg canvas--temp-points
                                     :stroke-width canvas--stroke-width
                                     :id (number-to-string canvas--id)
                                     :stroke-color (foreground-color-at-point)
                                     :fill "none")
                       (setq canvas--temp-points nil
                             canvas--id (1+ canvas--id)))
                     (canvas--deselect)
                     (svg-possibly-update-image svg)
                     nil)
            ('double-mouse-1
             ;; (message "key1 %s %s" key canvas--nearest-objects)
             (setq target (car canvas--nearest-objects))
             (when target
               (cond
                ((eq (dom-tag target) 'g)
                 (mapc (lambda (a)
                         (call-process-shell-command
                          (format "wmctrl -ia %s" (dom-attr a 'id))))
                       (dom-children target)))

                ((string-match-p "desktop" (dom-attr target 'type))
                 ;; Refresh view for new desktop.
                 (setq canvas--undo-marker nil)
                 (setq desktop (substring (dom-attr target 'id) 7))
                 (unless (string= desktop task--desktop-cur)
                   (setq task--desktop-cur desktop)
                   (call-process-shell-command
                    (format "wmctrl -s %s" task--desktop-cur))
                   (task--generate-image))
                 (task--buffer))

                ((string-match-p "window" (dom-attr target 'type))
                 (call-process-shell-command
                  (format "wmctrl -ia %s" (dom-attr target 'id))))))

             canvas--mode)
            (?g (canvas--group svg canvas--undo-marker)
                nil)
            (?G (canvas--ungroup svg canvas--undo-marker)
                nil)
            (?m (when canvas--undo-marker
                  (when (and (= (length canvas--undo-marker) 1)
                             (eq (dom-tag (car canvas--undo-marker)) 'g))
                    (setq canvas--undo-marker
                          (dom-children (car canvas--undo-marker))))

                  (mapc #'canvas--move-init canvas--undo-marker)
                  'move))

            ;; Retain mode even if mouse moves outside the image
            (_ canvas--mode)
            ))
    key))

(defun task--act-in-region (svg area)
  (let* ((res (canvas--transform svg area))
         (x (nth 0 res))
         (y (nth 1 res))
         (w (nth 2 res))
         (h (nth 3 res))
         target win desktop flag)
  (pcase canvas--mode
    ((guard (eq canvas--mode nil))
     ;; Remove selection box
     (dom-remove-node svg (car (dom-by-id svg "_selection")))
     (canvas--select (or (svg--shapes-in-region svg (list x y (+ x w) (+ y h)))
                         canvas--nearest-objects))

     (setq target (car canvas--nearest-objects))
     (when (or (null target)
               (and (not (eq (dom-tag target) 'g))
                    (dom-attr target 'type)
                    (string-match-p "desktop" (dom-attr target 'type))))
       (setq canvas--undo-marker nil))

     ;; (message "%s" canvas--undo-marker)
     (svg-possibly-update-image svg))
    ('move
     ;; (message "move ended %s" canvas--undo-marker)
     (setq canvas--mode nil
           target (car canvas--nearest-objects))

     (when (and target
                (dom-attr target 'type)
                (string-match-p "desktop" (dom-attr target 'type)))
       ;; Move to new desktop.
       (setq desktop (substring (dom-attr target 'id) 7))
       (unless (string= desktop task--desktop-cur)
         (setq flag t)
         (mapc (lambda (a)
                 (setq win (dom-attr a 'id))
                 ;; (message "wmctrl -ir %s -t %s " win desktop)
                 (call-process-shell-command
                  (format "wmctrl -ir %s -t %s " win desktop))
                 (dom-remove-node svg a))
               canvas--undo-marker)))

     (unless flag
       ;; Unless target is new desktop, cancel move operation.
       (canvas--move-cancel))

     (canvas--deselect))
    (_ (setq canvas--id (1+ canvas--id))))))

;;;###autoload
(defun task-view (prefix)
  "Overview of workspaces and running applications."
  (interactive "P")
  (let* ((svg nil)
         (canvas-plugin-fn 'task--select-mode)
         (canvas-act-in-region-fn 'task--act-in-region)
         (canvas--hide-prompt t)
         )
    (when prefix
      (set-window-buffer nil (find-file-literally (concat task--path "main.svg")))
      (toggle-frame-fullscreen)
      (setq mode-line-format nil
            task--fullscreen t
            header-line-format nil)
      (menu-bar-mode -1)
      (tool-bar-mode -1)
      (scroll-bar-mode -1))
    (task--generate-image)
    (setq svg (task--buffer)
          canvas--svg (list svg)
          canvas--id (length (dom-children svg)))
    (ewp-crop-image-1 svg)
    (task-view-revert)))

;;;###autoload
(defun task-view-fullscreen ()
  (interactive)
  (task-view t))

;;;###autoload
(defun task-view-revert ()
  "Revert to normal mode."
  (interactive)
  (let* ()
    (when task--fullscreen
      ;; (toggle-frame-maximized)
      (save-buffers-kill-terminal t)
      )
    (setq mode-line-format t
          task--fullscreen nil
          header-line-format t)
    (menu-bar-mode 1)
    (tool-bar-mode 1)
    (scroll-bar-mode 1)
    ))

(provide 'task)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "task"
;; End:

;;; task.el ends here
