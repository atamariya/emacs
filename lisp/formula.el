;;; formula.el --- SVG formula creation functions -*- lexical-binding: t -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com> 2021
;; Keywords: image formula
;; Version: 1.0
;; Package-Requires: ((emacs "25"))

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)
(require 'face-remap)

(defun formula--fraction (svg)
  ;; A group can be recursively edited.
  (let* ((num (read-string "Numerator: "))
         ;; (denom "lum")
         (denom (read-string "Denominator: "))
         (id canvas--id)
         (size 20)
         (padding 5)
         (x 0)
         (node1 (dom-node 'g `((id . ,(number-to-string (+ id 1))))))
         (node2 (dom-node 'g `((id . ,(number-to-string (+ id 2)))))))
    (setq num (mapconcat 'string (string-to-list num) " ")
          denom (mapconcat 'string (string-to-list denom) " "))
    (svg-text node1 num :x x :y size
                      :font-size size
                      :stroke canvas--stroke-color
                      ;; :id (number-to-string (+ id 1))
                      )
    (svg-text node2 denom :x x :y size ;(* 2 (+ (* 0.8 size) padding))
                      :font-size size
                      :stroke canvas--stroke-color
                      ;; :id (number-to-string (+ id 3))
                      )

    ;; (mapc (lambda (a)
    ;;         (svg-text node1 a :x x :y size
    ;;                   :font-size size
    ;;                   :stroke canvas--stroke-color
    ;;                   ;; :id (number-to-string (+ id 1))
    ;;                   ))
    ;;       (split-string num))
    ;; (mapc (lambda (a)
    ;;         (svg-text node2 a :x x :y (* 2 (+ (* 0.8 size) padding))
    ;;                   :font-size size
    ;;                   :stroke canvas--stroke-color
    ;;                   ;; :id (number-to-string (+ id 3))
    ;;                   ))
    ;;       (split-string denom))
    (insert "1")
    (setq canvas--id (+ canvas--id 3))
    (formula--fraction-object svg node1 node2)
    (setq canvas--mode 'move)
    ))

(defun formula--fraction-object (svg num denom)
  ;; A fraction of group objects
  (let* ((id canvas--id)
         (size 0)
         (bbox1 (svg-bbox num))
         (bbox2 (svg-bbox denom))
         (len1 (- (nth 2 bbox1) (nth 0 bbox1)))
         (len2 (- (nth 2 bbox2) (nth 0 bbox2)))
         (len (max len1 len2))
         (padding 10)
         (x 0)
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "fraction")
    (dom-set-attribute num 'formula "num")
    (dom-set-attribute denom 'formula "denom")

    (if (< len1 len2)
        (setq x (* 0.5 (- len2 len1))))
    (dom-set-attribute num 'transform (format "translate(%s)" (+ x padding)))

    (setq size (- (nth 3 bbox1) (nth 1 bbox1)))
    (svg-line node 0 (+ size padding) len (+ size padding)
              :id (number-to-string (+ id 1))
              :stroke canvas--stroke-color)

    (setq x (if (> len1 len2)
                (* 0.5 (- len1 len2)) 0))
    (dom-set-attribute denom 'transform (format "translate(%s,%s)" x
                                                (+ padding (nth 3 bbox1))))

    (dom-append-child node num)
    (dom-append-child node denom)
    (dom-append-child svg node)
    (message "%s" (dom-by-id svg (number-to-string id)))
    (setq canvas--id (+ canvas--id 2)
          canvas--undo-marker (list node))
    ))

(defun formula--group (_svg)
  ;; A fraction of group objects
  (let* ((id canvas--id)
         (padding 10)
         (size 40)
         (start (save-excursion
                  (if (region-active-p) (goto-char (region-beginning)))
                  (1+ (current-column))))
         (x 0)
         (y 0)
         bbox w h
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "group")
    (dom-set-attribute content 'formula "content")

    (nconc content canvas--undo-marker)

    (setq bbox (svg-bbox content)
          x (* (1- start) size)
          w (- (nth 2 bbox) (nth 0 bbox))
          h (- (nth 3 bbox) (nth 1 bbox))
          y (- (nth 3 bbox) (* h 0.2))
          ;; h size
          )
    (message "%s" bbox)

    (svg-text node "(" :font-size h :x x :y y
              :id (number-to-string (+ id 2)))
    (svg-text node ")" :font-size h :x (+ x w ) :y y
              :id (number-to-string (+ id 3)))
    (dom-set-attribute content 'transform (format "translate(%s)" (+ padding)))
    ;; (dom-set-attribute node 'width w)

    (dom-append-child node content)
    ;; (dom-append-child svg node)
    ;; (insert "111")
    (message "%s" node)
    (setq canvas--id (+ canvas--id 4)
          canvas--undo-marker (list node))
    ))

(defun formula--root (_svg)
  ;; A root of group objects
  (let* ((id canvas--id)
         ;; (padding 10)
         (size 40)
         (start (save-excursion
                  (if (region-active-p) (goto-char (region-beginning)))
                  (1+ (current-column))))
         (x 0)
         (y 0)
         bbox w h tmp w1
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "group")
    (dom-set-attribute content 'formula "content")

    (nconc content canvas--undo-marker)

    (setq bbox (svg-bbox content)
          x (* (1- start) size)
          y (nth 1 bbox)
          w (- (nth 2 bbox) (nth 0 bbox))
          h (min (- (nth 3 bbox) (nth 1 bbox))
                 size)
          )
    (message "%s" bbox)

    (setq tmp
    (svg-text node "√" :font-size (* 1.5 h) :x x :y (nth 3 bbox)
              :id (number-to-string (+ id 2))))
    (setq bbox (svg-bbox tmp)
          x (+ (nth 2 bbox))
          y (nth 1 bbox)
          w1 (- (nth 2 bbox) (nth 0 bbox)))
    (message "%s" bbox)
    (svg-line node x y (+ x w) y
              :stroke canvas--stroke-color
              :id (number-to-string (+ id 3)))
    (dom-set-attribute content 'transform (format "translate(%s)" w1))

    (dom-append-child node content)
    (message "%s" node)
    (setq canvas--id (+ canvas--id 4)
          canvas--undo-marker (list node))
    ))

(defun formula--factorial (_svg)
  ;; A factorial of group objects
  (let* ((id canvas--id)
         (padding 5)
         (size 40)
         (start (save-excursion
                  (if (region-active-p) (goto-char (region-end)))
                  (1+ (current-column))))
         (x 0)
         (y 0)
         bbox w h
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "group")
    (dom-set-attribute content 'formula "content")

    (nconc content canvas--undo-marker)

    (setq bbox (svg-bbox content)
          x (* (1- start) size)
          y (nth 3 bbox)
          w (- (nth 2 bbox) (nth 0 bbox))
          ;; h (- (nth 3 bbox) (nth 1 bbox))
          h size
          )
    ;; (message "%s" bbox)

    (svg-text node "!" :font-size h :x (+ x w padding) :y y
              :id (number-to-string (+ id 3)))

    (dom-append-child node content)
    ;; (message "%s" node)
    (setq canvas--id (+ canvas--id 4)
          canvas--undo-marker (list node))
    ))

(defun formula--select (svg)
  (let* ((start (point))
         (end (1+ (point)))
         i id node prev)
    (if (region-active-p)
      (setq start (region-beginning) end (1+ (region-end))))
    (setq i start)

    (while (< i end)
      (if (setq id (get-text-property i 'formula))
          (setq i (next-single-property-change i 'formula))
        (setq id (format "^%s$" i)
              i (1+ i)))
      (setq node (car (dom-by-id svg id)))

      (when node
        (message "%s" node)
        (if (eq (dom-tag node) 'text)
            (progn
              (if (and (eq (dom-tag node) 'text)
                       (eq (dom-tag prev) 'text))
                  (setf (nth 2 prev) (concat (car (dom-children prev))
                                             (car (dom-children node))))
                (setq prev node)
                (push node canvas--undo-marker)
                ))
          (setq prev nil)
          (push node canvas--undo-marker))
        (dom-remove-node svg node)
        ))

    ;; (unless canvas--undo-marker
    ;;   (setq id (number-to-string (point))
    ;;         x (* (current-column) 40)
    ;;         y (* (line-number-at-pos) 100))
    ;;   (push (svg-text svg (buffer-substring-no-properties start end)
    ;;                   :id id :x x :y y :font-size 40)
    ;;         canvas--undo-marker))
    canvas--undo-marker))

(defun formula--exponent (_svg)
  (formula--vertical "Superscript / Exponent: " 1))

(defun formula--subscript (_svg)
  (formula--vertical "Subscript: " -1))

(defun formula--vertical (prompt valign)
  ;; Add exponent to group objects
  (let* ((id canvas--id)
         ;; (padding 5)
         (size 40)
         (start (save-excursion
                  (if (region-active-p) (goto-char (region-end)))
                  (1+ (current-column))))
         (x 0)
         (y 0)
         (exp (read-string prompt))
         ;; (exp "3")
         bbox w h
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "exponent")
    (dom-set-attribute content 'formula "base")

    (nconc content canvas--undo-marker)

    (setq bbox (svg-bbox content)
          x (* (1- start) size)
          y (if (> (signum valign) 0)
                (* 1.05 (nth 1 bbox))
              (* 1.05 (nth 3 bbox)))
          w (- (nth 2 bbox) (nth 0 bbox))
          ;; h (- (nth 3 bbox) (nth 1 bbox))
          h size
          )
    (message "%s" bbox)

    (svg-text node exp :font-size (/ h 2) :x (+ x w) :y y
              :id (number-to-string (+ id 2)))
    ;; (dom-set-attribute content 'transform (format "translate(%s)" (+ x padding)))
    ;; (dom-set-attribute node 'height (* 0.3 h))

    (dom-append-child node content)
    (message "%s" node)
    (setq canvas--id (+ canvas--id 4)
          canvas--undo-marker (list node))
    ))

(defun formula--vector (_svg)
  (formula--vertical-center "vector" 1))

(defun formula--above (_svg)
  (formula--vertical-center nil 1 "Symbol Above: "))

(defun formula--vertical-center (exp valign &optional prompt)
;; Add exponent to group objects ‸ ~ _ ⃗
  (let* ((id canvas--id)
         (padding 3)
         (size 40)
         (start (save-excursion
                  (if (region-active-p) (goto-char (region-beginning)))
                  (1+ (current-column))))
         (x 0)
         (y 0)
         (options '(("caret" . "‸") ("tilde" . "~") ("bar" . "_")
                    ("vector" . "→")))
         (exp (if prompt (completing-read prompt options) exp))
         ;; (exp "bar")
         bbox h w w1 tmp
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "exponent")
    (dom-set-attribute content 'formula "base")

    (nconc content canvas--undo-marker)

    (setq bbox (svg-bbox content)
          x (* (1- start) size)
          y (if (> (signum valign) 0) (nth 1 bbox)
                 (nth 3 bbox))
          w (- (nth 2 bbox) (nth 0 bbox))
          h (- (nth 3 bbox) (nth 1 bbox))
          )
    (setq y (+ y padding (if (member exp '("tilde" "vector")) 0 (/ h -2))))
    (message "%s" bbox)

    (setq tmp (svg-text node (cdr (assoc exp options))
                        :font-size size :x 0 :y y
                        :id (number-to-string (+ id 2))))
    (setq bbox (svg-bbox tmp)
          w1 (- (nth 2 bbox) (nth 0 bbox))
          )
    (message "%s" bbox)
    (dom-set-attribute tmp 'x (+ x (/ (- w w1) 2)))
    ;; (dom-set-attribute content 'transform (format "translate(%s)" (+ x padding)))

    (dom-append-child node content)
    (message "%s" node)
    (setq canvas--id (+ canvas--id 4)
          canvas--undo-marker (list node))
    ))

(defun formula--integral (_svg)
  ;; Add integral to group objects
  (let* ((id canvas--id)
         (padding 10)
         (size 40)
         (start (save-excursion
                  (if (region-active-p) (goto-char (region-beginning)))
                  (1+ (current-column))))
         (x 0)
         (y 200)
         ;; (exp (read-string "Exponent: "))
         (exp "dx")
         from to bbox w h
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "integral")
    (dom-set-attribute content 'formula "function")
    ;; (dom-set-attribute content 'formula "variable")
    ;; (dom-set-attribute content 'formula "interval-s")
    ;; (dom-set-attribute content 'formula "interval-e")

    (when canvas--undo-marker
      (nconc content canvas--undo-marker)

      (setq bbox (svg-bbox content)
            x (* (1- start) size)
            y (nth 1 bbox)
            w (- (nth 2 bbox) (nth 0 bbox))
            ;; h (- (nth 3 bbox) (nth 1 bbox))
            h size
            )
      (dom-set-attribute content 'transform (format "translate(%s)"
                                                    (+ (/ size 2))))
      (svg--append node content)
      )
    (setq from "0" to "n")
    ;; (setq from (read-string "From: "))
    ;; (if (string-empty-p from)
    ;;     (setq from nil)
    ;;   (setq to (read-string "To: ")))
    ;; (message "%s" bbox)

    (svg-text node "∫" :font-size (* 2 h) :x x :y (+ y (* h 0.7))
              :id (number-to-string (+ id 2)))
    (svg-text node exp :font-size h :x (+ x w (* 2.5 padding)) :y (nth 3 bbox)
              :id (number-to-string (+ id 3)))
    (when from
      (svg-text node to :font-size (/ h 2) :x (+ x (* 2.8 padding)) :y (+ y (* h -0.5))
                :id (number-to-string (+ id 4)))
      (svg-text node from :font-size (/ h 2) :x (+ x (* 1.5 padding)) :y (+ y (* h 1.2))
                :id (number-to-string (+ id 5))))
    ;; (dom-set-attribute node 'height (* 0.3 h))

    (message "%s" node)
    (setq canvas--id (+ canvas--id 6)
          canvas--undo-marker (list node))
    ))

(defun formula--sum (_svg)
  ;; Add sum
  (let* ((id canvas--id)
         (padding 10)
         (size 40)
         (x (* (current-column) size))
         (y (* 100 (line-number-at-pos)))
         (h size)
         var start end
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "integral")
    (dom-set-attribute content 'formula "function")
    ;; (dom-set-attribute content 'formula "variable")
    ;; (dom-set-attribute content 'formula "interval-s")
    ;; (dom-set-attribute content 'formula "interval-e")

    (when canvas--undo-marker
      (nconc content canvas--undo-marker)
      (dom-append-child node content))

    ;; (setq var "i" start 0 end "n")
    (setq var (read-string "Variable: " nil))
    (if (string-empty-p var)
        (setq var nil)
      (setq start (read-string "Exponent: " "0")
            end (read-string "Exponent: " "")))


    (svg-text node "Σ" :font-size (* 1.5 h) :x x :y (+ y (* size 0.3))
              :id (number-to-string (+ id 2)))
    (when var
      (svg-text node (format "%s = %s"  var start)
                :font-size (/ h 2.5) :x (+ x (/ size padding)) :y (+ y (* size 0.7))
                :id (number-to-string (+ id 3)))
      (svg-text node end :font-size (/ h 2) :x (+ x (* padding)) :y (+ y (* size -0.8))
                :id (number-to-string (+ id 4))))
    ;; (dom-set-attribute node 'height (* 0.3 h))

    ;; (message "%s" node)
    (insert "1")
    (setq canvas--id (+ canvas--id 5)
          canvas--undo-marker (list node))
    ))

(defun formula--limit (_svg)
  ;; Add limit
  (let* ((id canvas--id)
         (padding 10)
         (size 40)
         (x (* (current-column) size))
         (y (* 100 (line-number-at-pos)))
         var end bbox w
         (node (dom-node 'g `((id . ,(number-to-string id)))))
         (content (dom-node 'g `((id . ,(number-to-string (1+ id))))))
         )
    ;; Mark the objects
    (dom-set-attribute node 'formula "integral")
    (dom-set-attribute content 'formula "function")
    ;; (dom-set-attribute content 'formula "variable")
    ;; (dom-set-attribute content 'formula "interval-s")
    ;; (dom-set-attribute content 'formula "interval-e")

    ;; (setq var "i" start 0 end "∞")
    (setq var (read-string "Variable: " "n")
          end (completing-read "Tends to: " '("∞" "0")))


    (svg-text node "lim " :font-size (* 0.7 size) :x x :y (+ y )
              :id (number-to-string (+ id 2)))
    (svg-text node (format "%s→%s"  var end)
              :font-size (/ size 2) :x (+ x (/ size padding)) :y (+ y (* size 0.4))
              :id (number-to-string (+ id 3)))

    (when canvas--undo-marker
      (nconc content canvas--undo-marker)

      (setq bbox (svg-bbox node)
            w (- (nth 2 bbox) (nth 0 bbox))
            )
      (dom-set-attribute content 'transform (format "translate(%s)" w))
      (dom-append-child node content)
      )

    ;; (message "%s" node)
    (insert "1")
    (setq canvas--id (+ canvas--id 5)
          canvas--undo-marker (list node))
    ))

(defun formula--edit-start (svg)
  ;; Explode group and allow edit.
  (let* ((width (dom-attr svg 'width))
         (height (dom-attr svg 'height))
         (marker (dom-attr svg :image))
         (frag (car canvas--undo-marker))
         (image nil))
    (when (and frag (eq (dom-tag frag) 'g))
      (dom-set-attribute frag 'stroke-dasharray nil)
      (setq image (svg--extract-fragment svg frag width height t)
            canvas--dialog-object frag)
      ;; (message "1 %s" image)
      ;; (message "dialog 2 %s" canvas--dialog-object)

      (canvas--set image marker)
      (setq canvas--mode nil)
      ))
  )

(defun formula--edit-end (svg)
  ;; Align all nodes in a line and group them.
  (let* ((image svg)
         (h 0)
         (w 0)
         (size 0)
         (padding 10)
         (nodes nil)
         (x 0)
         ;; h1
         w1 font-size bbox)
    (when (> (length canvas--svg) 0)
      (pop canvas--svg))

    (unless (string= "fraction" (dom-attr canvas--dialog-object 'formula))
    ;; Find reference values
    (mapc (lambda (a)
            (unless (string-prefix-p "_" (dom-attr a 'id))
              (push a nodes)
              (setq bbox (svg-bbox a)
                    font-size (or (dom-attr a 'font-size) 0)
                    size (max size (if (stringp font-size)
                                       (string-to-number font-size)
                                     font-size))
                    h (max h (- (nth 3 bbox) (nth 1 bbox)))
                    w (+ w (- (nth 2 bbox) (nth 0 bbox))))))
          (dom-children image))
    ;; Update nodes based on reference values
    (setq nodes (nreverse nodes))
    (message "%s" nodes)
    (mapc (lambda (a)
            (dom-set-attribute a 'transform nil)
            (setq bbox (svg-bbox a)
                  ;; h1 (- (nth 3 bbox) (nth 1 bbox))
                  w1 (- (nth 2 bbox) (nth 0 bbox)))

            (if (eq (dom-tag a) 'text)
                (progn
                  (dom-set-attribute a 'x x)
                  (dom-set-attribute a 'y (/ (+ h size) 2))
                  (dom-set-attribute a 'font-size size)
                  )
              (dom-set-attribute a 'transform (format "translate(%s)" x))
              (dom-set-attribute a 'height h))

            ;; (message "%s %s %s" x h1 h)
            (setq x (+ x w1 padding)))
          nodes)

      (let* ((parent (dom-parent (car canvas--svg) canvas--dialog-object))
             num denom group
             (transform (dom-attr parent 'transform))
             (type (dom-attr canvas--dialog-object 'formula)))
        ;; Update current object
        (setq canvas--dialog-object `(g nil ,@nodes))

        ;; Find remaining parts from parent
        (pcase type ("num" (setq num canvas--dialog-object))
               ("denom" (setq denom canvas--dialog-object)))
        (dolist (a (dom-children parent))
          (if (and (null num) (string= (dom-attr a 'formula) "num"))
              (setq num a))
          (if (and (null denom) (string= (dom-attr a 'formula) "denom"))
              (setq denom a)))

        ;; Resize parent
        (setq group (formula--fraction-object (car canvas--svg) num denom))
        (dom-set-attribute group 'transform transform)
        (canvas--deselect)
        (delq parent (car canvas--svg))

        ;; (message "%s" canvas--dialog-object)
        ))
    (svg-possibly-update-image (car canvas--svg))
    ))

(defun formula-canvas-mode1 (key svg)
  (let* ((func (lookup-key global-map (vector key)))
         (i 0)
         (size 40)
         (x 0)
         (y 0)
         mkey
         node bbox w h node1 node2
         id point opts pos start end)
    (message "Before: %s %s" key func)
    (unless func
      (setq mkey (lookup-key local-function-key-map (vector key))
            func (if mkey (lookup-key global-map mkey))))
    (message "After: %s %s" key func)

    (with-current-buffer (get-buffer-create "*formula*")
      (setq i (current-column)
            id (number-to-string (point))
            x (* i size)
            y (* 100 (line-number-at-pos)))
      (when t;func
        (condition-case var
            (cond ((eq func 'self-insert-command)
                   ;; Add
                   (svg-text svg key :id id :x x :y y
                             :font-size size
                             :xml:space "preserve")
                   (funcall func 1 key)
                   (deactivate-mark)
                   (setq i (1+ i))
                   (if (eq key ?q)
                       (setq key nil)))
                  ((or (eq key 'backspace)
                       (eq func 'delete-forward-char)
                       (eq func 'delete-backward-char))
                   ;; Delete
                   ;; (if (> i 0) (setq i (1- i)))
                   ;; (mapc (lambda (a)
                   ;;         (dom-remove-node svg a))
                   ;;       (dom-by-id svg "_cursor"))
                   ;; (message "%s" (last (dom-children svg)))
                   (mapc (lambda (a)
                           (setq node a)
                           (if (string= (dom-attr a 'id)
                                        (number-to-string (point)))
                               (dom-remove-node svg a)))
                         (dom-children svg))
                   (unless (string= (dom-attr node 'id) "_grid")
                     ;; Don't remove grid
                     (dom-remove-node svg node))
                   (funcall func 1)
                   (setq x (* (1- (point)) size))
                   )
                  ((eq func 'Control-X-prefix)
                   ;; Quit
                   (setq key (read-key))
                   (setq func (lookup-key formula-canvas-map (vector key)))
                   (message "C-x: %s %s" key func)
                   (when func
                     (formula--select svg)
                     (funcall (nth 1 func) svg)
                     ))
                  ((or (eq key 'S-left)
                       (eq key 'S-right))
                   ;; Selection
                   (unless (region-active-p)
                     (push-mark)
                     (activate-mark))
                   (if (eq key 'S-left)
                       (goto-char (1- (point)))
                     (goto-char (1+ (point))))
                   (message "%s %s %s" (region-active-p) (region-beginning)
                            (region-end))
                   )
                  ((eq func 'newline)
                   (setq key nil)
                   (funcall func))
                  (func
                   ;; Otherwise
                   (setq x (- x size))
                   (deactivate-mark)
                   (funcall func 1)))
          (error (message (format "Error: %s" (car var)))))

        (when (setq node (car canvas--undo-marker))
          ;; (message "1 %s" node)
          ;; (dom-remove-node svg (car (dom-by-id svg id)))
          (if (region-active-p)
              (progn
                (setq bbox (svg-bbox node)
                      i (round (/ (nth 2 bbox) size)) )
                (if (> i (point-max))
                    (insert (make-string (- i (point-max)) "1")))
                (goto-char i)
                (put-text-property (region-beginning) i
                                   'formula (dom-attr node 'id)))
            (dom-set-attribute node 'id id))
          (svg--append svg node)
          (canvas--deselect)
          (canvas--move-init node)
          (setq pos (car canvas--temp-points))
          (if (eq canvas--mode 'move)
              (canvas--move-to node (+ x (car pos)) (+ y (cdr pos)(- size))))
          (setq canvas--mode nil)
          )

        ;; Move point forward
        (setq i (current-column)
              id (number-to-string (point))
              x (* i size)
              y (* 100 (line-number-at-pos)))

        ;; Cursor
        ;; (message "%s %s %s" (region-active-p) (region-beginning) (region-end))
        ;; (message "s %s" (dom-by-id svg (number-to-string
        ;;                                 (region-beginning))))
        ;; (message "e %s" (dom-by-id svg (number-to-string
        ;;                                        (region-end))))
        (setq w size h size); x (* (1- (point)) size))
        ;; (message "%s %s" x y)
        (when (region-active-p)
          (setq start (region-beginning)
                x (* (save-excursion
                       (if (region-active-p) (goto-char (region-beginning)))
                       (current-column))
                     size)
                end (region-end))
          (setq node1 (dom-by-id svg (format "^%s$" start)))
          (setq node2 (dom-by-id svg (format "^%s$" end)))
          (if (or node1 node2)
              (setq node (append '(g nil) node1 node2)
                    bbox (svg-bbox node)
                    w (max (- (nth 2 bbox) (nth 0 bbox))
                           (* (1+ (- end start)) size))
                    h (max (- (nth 3 bbox) (nth 1 bbox)) size))
            (setq w (* (1+ (- end start)) size) h size)))

        (svg-rectangle svg x (- y size) w h
                       :id "_cursor" :fill "black" :opacity "0.2")

        ;; Popup
        ;; (setq opts '("a" "b" "c" ))
        (if opts
            (formula-canvas-popup svg point (+ y size) opts))
        (svg-possibly-update-image svg)
        )
      ))
  key)

(defun formula-canvas-mode (key svg)
  (let* ((func (lookup-key global-map (vector key)))
         (i 0)
         (size 20)
         mkey win
         text x y id point pos start end opts)
    ;; (message "Before: %s %s" key func)
    (unless func
      (setq mkey (lookup-key local-function-key-map (vector key))
            func (lookup-key global-map mkey)))
    ;; (message "After: %s %s" key func)

    (with-current-buffer (get-buffer-create "a.txt")
      (when func
        (condition-case var
            (if (eq func 'self-insert-command)
                (funcall func 1 key)
              (funcall func 1))
          (error (message (format "Error: %s" (car var)))))

        (save-excursion
          (setq win (get-buffer-window))
          (goto-char (window-start win))
          ;; Need to keep popup at the end
          ;; (setq svg (subseq svg 0 2))
          (dom-remove-node svg (car (dom-by-id svg "_popup")))
          (while (< (point) (window-end win))
            (setq start (point)
                  end (progn (end-of-line) (point))
                  text (buffer-substring start end)
                  id (number-to-string i)
                  x 0
                  y (* (1+ i) size))
            (setq i (1+ i))
            (forward-line)
            ;; Text
            (svg-text svg text :id id :x x :y y :xml:space "preserve")))

        ;; Cursor
        (setq pos (posn-col-row (posn-at-point (point) win))
              point (* 4 (car pos))
              y (* size (cdr pos)))
        (svg-rectangle svg point y 5 size
                       :id "_cursor" :fill "black" :opacity "0.4")
        ;; Popup
        (setq opts '("a" "b" "c" ))
        (if opts
            (formula-canvas-popup svg point (+ y size) opts))
        (svg-possibly-update-image svg)
        )
      )))

(defun formula-canvas-popup (svg x y &optional opts)
  (let* ((w 100)
         (i 0)
         (padding 10)
         (selected 2)
         (size 20)
         (rows (min (length opts) 5))
         (parent (svg-group svg nil :id "_popup"))
         (a nil)
         (width (dom-attr svg 'width)))

    ;; Background: cornsilk, selection: "#add8e6"
    ;; Scrollbar- Background: wheat, selection: darkred
    (if (> (+ x w) width)
        (setq x (- width w)))
    (svg-rectangle parent x y w (* rows size)
                   :id "_bg" :fill "cornsilk" :stroke "red")

    (while (and opts (< i rows))
      (setq i (1+ i)
            a (car opts)
            opts (cdr opts))
      (if (= i selected)
          (svg-rectangle parent x (+ y (* (1- i) size)) w size
                         :id "_selected" :fill "#add8e6"))
      (svg-text parent a :id a :x (+ x padding) :y (+ y (* i size))))
    ))

;;;###autoload
(defun formula-mode ()
  (interactive)
  (let ((canvas-plugin-fn 'formula-canvas-mode1))
    (canvas-mode)))

(defvar formula-canvas-map
  (let* ((key-mode (make-sparse-keymap)))
    ;; mode function docstring
    (define-key key-mode "a" '(formula formula--above "Above"))
    (define-key key-mode "e" '(formula formula--exponent "Exponent")) ;; Superscript
    (define-key key-mode "f" '(formula formula--fraction "Fraction"))
    (define-key key-mode "F" '(formula formula--factorial "Factorial"))
    (define-key key-mode "g" '(formula formula--group "Group")) ;; () [] {}
    (define-key key-mode "i" '(formula formula--integral "Integral"))
    (define-key key-mode "l" '(formula formula--limit "Limit"))
    (define-key key-mode "m" '(formula formula--matrix "Matrix"))
    (define-key key-mode "M" '(formula formula--n-integral "Multiple Integrals"))
    (define-key key-mode "r" '(formula formula--root "Root"))
    (define-key key-mode "s" '(formula formula--subscript "Subscript"))
    (define-key key-mode "S" '(formula formula--sum "Sum")) ;; Sigma
    (define-key key-mode "v" '(formula formula--vector "Vector"))
    (define-key key-mode ">" '(formula formula--edit-start "Edit start"))
    (define-key key-mode "<" '(formula formula--edit-end "Edit exit"))
    key-mode)
  "Map to be used for canvas plugin.")

(defvar formula--overlay nil)
(defvar formula--inline-size nil)
(make-variable-buffer-local 'formula--overlay)

(defun formula--init-buffer ()
  (let* ((w (window-pixel-width))
         (h (window-pixel-height))
         (font-size 14)
         (slant (- (cos (* (/ float-pi 180) 32))))
         svg id anchor pos)
    (if formula--overlay
        (setq svg formula--overlay)

      ;; Turn off font lock to prevent loss of formatting properties
      (font-lock-mode -1)
      (put-text-property (point-min) (point-max) 'id "1")
      (setq svg (svg-create w h))
      ;; Outline for main window
      (svg-rectangle svg 0 0 w h :id (concat "shape" id) :fill "none"))
    (setq cursor-type nil)
    ;; (setq anchor (dom-node 'a
    ;;                        `((:id  . "_a")
    ;;                          (text . "title")
    ;;                          (href . "href"))))
    ;; (dom-append-child svg anchor)
    (goto-char (point-min))
    (while (and (setq pos (next-single-property-change (point) 'id nil (point-max)))
                (not (eobp)))
      (if (not (setq id (get-text-property (point) 'id)))
          (forward-char)
      ;; (when (eq (char-after pos) ?\n)
      ;;   (setq pos (1+ pos)))
      (narrow-to-region (point) pos)
      ;; (if (string= id "1")
      ;;     ;; Outline for main window
      ;;     (svg-rectangle svg 0 0 w h :id (concat "shape" id) :fill "none"))
      (formula--buffer-to-node svg id)
      (widen)
      (goto-char pos)))
    ;; (svg-rectangle svg 150 50 2 font-size :id "cursor" :fill "red"
    ;;                :transform  (format "matrix(1, 0.1, %.3f, 1, 0, 0)" slant)
    ;;                )
    (with-current-buffer (get-buffer-create "tmp.svg")
      (erase-buffer)
      (svg-print svg)
      (save-buffer)
      )
    (setq formula--overlay svg)
    ;; (put-text-property (point-min) (point-max) 'display (svg-image svg))
    (put-text-property (point-min) (point-max) 'keymap formula--mouse-keymap)
    svg))

(defun formula--buffer-to-node (svg id)
  (let* ((font-size 14)
         (dy 0)
         (blank-line-height 0)
         (pad (get-text-property (point) 'padding))
         text pos end family weight slant width fg bg)
    (setq text (svg-text svg nil
                         :id id
                         :style (concat "shape-inside: url(#shape" id ");"
                                        (if pad (format "shape-padding: %dpx;" pad))
                                        (format "font: %dpx;" font-size))
                         ;; :xml:space "preserve"
                         ))
    ;; Prepare tspan for each section with different props
    (goto-char (point-min))
    (while (not (eobp))
      (setq end (save-excursion (end-of-line) (point)))
      (while (and (setq pos (next-property-change (point) nil end))
                  (not (eolp)))
        (setq font-size (* 1 (aref (font-info (font-at (point))) 2))
              family (faces--attribute-at-point :family)
              slant (faces--attribute-at-point :slant)
              weight (faces--attribute-at-point :weight)
              width (faces--attribute-at-point :width)
              fg (foreground-color-at-point))
        (if text-scale-mode
            (facemenu-set-size font-size (point) pos))
        (svg-tspan text (concat (buffer-substring-no-properties (point) pos)
                                ;; (when (eq (char-after pos) ?\n)
                                ;;   (setq pos (1+ pos))
                                ;;   "\n")
                                )
                   :id (number-to-string (point))
                   :dy dy
                   :x (if (bolp) 0)
                   :style (concat
                           ;; (if (not (eq 'normal weight))
                           ;;     (format "font-weight: %s;" weight))
                           ;; (if (not (eq 'normal width))
                           ;;     (format "font-stretch: %s;" width))
                           (format "font: %s %dpx '%s'; fill: %s"
                                   (concat ""
                                           (if (not (eq 'normal slant))
                                               (format " %s" slant))
                                           (if (not (eq 'normal weight))
                                               (format " %s" weight))
                                           (if (not (eq 'normal width))
                                               (format " %s" width)))
                                   font-size family fg))
                   :xml:space "preserve")
        (setq blank-line-height (max blank-line-height (aref (font-info (font-at (point))) 3))
              dy 0)
        (goto-char pos))
      (when (eolp)
        (if (bolp)
            (setq dy (* 2 blank-line-height))
          (setq dy blank-line-height))
        (if (not (eobp)) (forward-char)))
      )))

(defun formula--after-change-function (_b _e _len)
  ""
  (let* ((pad (or facemenu--padding 0)))
    (put-text-property (point-min) (point-max)
                       'inline-size (- formula--inline-size pad pad))))

(defun formula--edit (width id)
  (let* ((buf (current-buffer))
         cur text pos)
    (put-text-property (point-min) (point-max) 'display nil)
    (goto-char (point-min))
    (while (and (setq pos (next-single-property-change (point) 'id nil (point-max))
                      cur (get-text-property (point) 'id))
                (not (string= id cur)))
      (goto-char pos))
    (if (not cur) (setq cur (point-max) pos (point-max)))
    (setq text (delete-and-extract-region (point) pos))

    (pop-to-buffer-same-window (get-buffer-create "*Edit Text*"))
    (erase-buffer)
    (text-scale-adjust 0)
    (if text (insert text))
    (setq cursor-type t)
    (setq formula--inline-size width)
    (formula--after-change-function nil nil nil)
    (add-hook 'after-change-functions 'formula--after-change-function
               nil t)
    (local-set-key (kbd "C-c C-c") (lambda ()
                                     (interactive)
                                     (remove-hook 'after-change-functions
                                                  'formula--after-change-function
                                                  t)
                                     (formula--exit-edit id buf)
                                     ))
    ))

(defun formula--exit-edit (id buf)
  (let* (text svg)
    (put-text-property (point-min) (point-max) 'id id)
    (with-current-buffer buf
      (setq svg formula--overlay))
    ;; (pp svg)
    (goto-char (point-min))
    (formula--buffer-to-node svg id)
    (setq text (buffer-string))
    (with-current-buffer buf
      (setq formula--overlay svg))
    ;; (pp svg)
    (with-current-buffer (get-buffer-create "tmp.svg")
      (let (inhibit-modification-hooks)
      (erase-buffer))
      (svg-print svg))
    (bury-buffer)
    ;; (switch-to-buffer buf)
    (pop-to-buffer-same-window buf)
    (insert text)
    (put-text-property (point-min) (point-max) 'display (svg-image svg :pointer 'arrow))
    (put-text-property (point-min) (point-max) 'keymap formula--mouse-keymap)
    ;; (svg-possibly-update-image svg)
    ))

(defun formula--exit-canvas ()
  (let* ((image (car canvas--svg)))
    (dom-remove-node image (car (dom-by-id image "_defs")))
    (svg-possibly-update-image image)
    (setq formula--overlay image)
    ))

(defun formula--mode-on-click (event)
  "Select shape."
  (interactive "e")
  (let* ((e (event-start event))
         (win (posn-window e))
         (image (posn-image e))
         xy hit o href title origin x y)
    ;; (message "%s" e)
    (when image
      ;; (select-window win)
      (with-selected-window win
        (setq xy (posn-object-x-y e)
              image (svg--animation-init (image-property image :data))))
      (setq origin (svg-parse-viewBox image)
            x (car origin)
            y (cadr origin))
      (canvas--adjacent-obj image (+ (car xy) x) (+ (cdr xy) y))
      (setq o canvas--nearest-objects)
      ;; (pp image)
      (pp xy)
      (pp o)

      )))

(defun formula--mode-on-double-click (event)
  "Edit text mode."
  (interactive "e")
  (let* ((e (event-start event))
         (win (posn-window e))
         (image (posn-image e))
         xy o origin x y style)
    ;; (message "%s" e)
    (when image
      ;; (select-window win)
      (with-selected-window win
        (setq xy (posn-object-x-y e)
              image formula--overlay ;(svg--animation-init (image-property image :data))
              ))
      ;; (setq formula--overlay image)
      (setq origin (svg-parse-viewBox image)
            x (car origin)
            y (cadr origin))
      (dolist (i (reverse (dom-children image)))
        (when (and (not o)
                   (not (eq (dom-tag i) 'text))
                   (not (string-match-p "_marker" (dom-attr i 'id)))
                   (svg--point-in-region (+ (car xy) x) (+ (cdr xy) y)
                                    (svg-bbox i)))
          (setq o i)))
      ;; (canvas--adjacent-obj image (+ (car xy) x) (+ (cdr xy) y))
      (when o ;(setq o (car canvas--nearest-objects))
        (let* ((id (dom-attr o 'id))
               text-id width bbox)
          (dolist (i (dom-by-tag image 'text))
            (setq style (dom-attr i 'style))
            (when (string-match (format "(#%s)" id) style)
              (setq text-id (dom-attr i 'id))
              ))
          (when (not text-id)
            ;; Shape with no text
            (dom-set-attribute o 'id (concat "shape" id))
            (setq text-id id))
          (setq bbox (svg-bbox o)
                width (- (nth 2 bbox) (nth 0 bbox)))
          (formula--edit width text-id)
        ))
      )))

;; (local-set-key [mouse-1] #'formula--mode-on-click)
;; (local-set-key [double-mouse-1] #'formula--mode-on-double-click)
(defvar formula--mouse-keymap (let ((map (make-sparse-keymap)))
                                (define-key map [mouse-1] #'formula--mode-on-click)
                                (define-key map [double-mouse-1] #'formula--mode-on-double-click)
                                map))
;; (put-text-property (point-min) (point-max) 'keymap formula--mouse-keymap)

;;;###autoload
(defun formula-draw ()
  (interactive)
  (let* (;(keymap formula--mouse-keymap)
         (canvas-exit-fn 'formula--exit-canvas)
         (svg (formula--init-buffer)))
    (canvas--set svg (point-marker))
    (ewp-crop-image-1 svg)
    ))

(provide 'formula)
