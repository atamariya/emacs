;;; pdf.el --- PDF utility

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: pdf

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;; Part 2: Linearization parameter dictionary
;; 43 0 obj
;; << /Linearized 1.0 % Version
;; /L 54567 % File length
;; /H [ 475 598 ] % Primary hint stream offset and length (part 5)
;; /O 45 % Object number of first page’s page object (part 6)
;; /E 5437 % Offset of end of first page
;; /N 11 % Number of pages in document
;; /T 52786 % Offset of first entry in main cross-reference table (part 11)
;; >>
;; endobj

;; Part 3: First-page cross-reference table and trailer
;; xref
;; 43 14
;; 0000000052 00000 n
;; 0000000392 00000 n
;; 0000001073 00000 n
;; ... Cross-reference entries for remaining objects in the first page ...
;; 0000000475 00000 n
;; trailer
;; << /Size 57 % Total number of cross-reference table entries in document
;; /Prev 52776 % Offset of main cross-reference table (part 11)
;; /Root 44 0 R % Indirect reference to catalogue (part 4)
;; ... Any other entries, such as Info and Encrypt ... % (part 9)
;; >>
;; % Dummy cross-reference table offset
;; startxref
;; 0
;; %% EOF

;; Part 4: First-page cross-reference stream
;; 11 0 obj % The cross-reference stream, at offset 4899
;; << /Type /XRef
;; /Index [2 10] % This stream contains entries for objects 2 through 11
;; /Size 100
;; /W [1 2 1] % The byte-widths of each field
;; /Filter /ASCIIHexDecode % For readability only
;; ...
;; >>
;; stream
;; 01 0E8A 0
;; 02 0002 00
;; 02 0002 01
;; 02 0002 02
;; 02 0002 03
;; 02 0002 04
;; 02 0002 05
;; 02 0002 06
;; 02 0002 07
;; 01 1323 0
;; endstream
;; endobj
;; % The entries above are for: object 2 (0x0E8A = 3722), object 3 (in object stream 2, index 0),
;; % object 4 (in object stream 2, index 1) ... object 10 (in object stream 2, index 7),
;; % object 11 (0x1323 = 4899).

(defvar pdf--startxref nil)
(make-variable-buffer-local 'pdf--startxref)

(defvar pdf--xref-table nil)
(make-variable-buffer-local 'pdf--xref-table)

(defvar pdf--stream-objects nil
  "Hash of objects in object streams.")
(make-variable-buffer-local 'pdf--stream-objects)

(defvar pdf--trailer nil)
(make-variable-buffer-local 'pdf--trailer)

(defvar pdf--total-pages nil)
(make-variable-buffer-local 'pdf--total-pages)

(defvar pdf--form nil
  "Alist of (field-id . val).")
(make-variable-buffer-local 'pdf--form)

(defun pdf-parse-dict ()
  (interactive)
  (let* (k v o from to used start end inner)
    (when
    (re-search-forward "<<" nil t)
    (save-excursion
      (setq used 1 start (point))
      (while (not (and (= used 0) (looking-at ">>")))
        (cond ((looking-at "<<")
               (forward-char 2)
               (setq used (1+ used)))
              ((looking-at ">>")
               (setq used (1- used))
               (if (/= used 0) (forward-char 2)))
              (t (forward-char)))
        (setq end (- (point) 0))))
    ;; (message "%s" end)
    (narrow-to-region start end)

    (setq used nil)
    (while (not (or (>= (point) end)
                    (eobp)))
      (cond ((looking-at "\\s(")
             (forward-sexp)
             (setq to (point)))
            ((looking-at "<<")
             (setq v (pdf-parse-dict)
                   inner t
                   used t
                   to (point))
             (forward-char 2)
             (narrow-to-region start end))
            (t
             (or (and (re-search-forward "[\r\n ]?/\\([_[:alnum:]]+\\)[ ]*" end t)
                      (setq to (match-beginning 0)))
                 (and (re-search-forward "[\r\n]" end t)
                      (setq to (1- (point))))
                 (and (or ;(re-search-forward "R" end t)
                          (skip-chars-forward "[ \\.[:alnum:]]"))
                      (setq to (point))))
             ))

      (if k
          (unless inner
            (setq v (if (= from to)
                        (setq used (concat "/" (match-string 1)))
                      (buffer-substring from to))))
        (setq k (match-string 1)
              from (point)))
      (when (and k v)
        ;; (message "%s %s" k v)
        (push (cons (substring-no-properties k)
                    (if (stringp v) (substring-no-properties v) v))
              o)
        (setq k nil v nil inner nil)
        (if used
            (setq used nil)
          (setq k (match-string 1)
                from (point))
          ))
      (skip-chars-forward "\r\n "))
    (widen)
    (nreverse o))))

(defun pdf-parse-obj (&optional pos n)
  (interactive)
  (let* (obj end stream start str id o table)
    (if pos (goto-char pos))
    (re-search-forward "\\([0-9]+\\) \\([0-9]+\\) obj" nil t)
    (setq id (match-string 0))
    (skip-chars-forward " \r\n")
    (save-excursion
      (setq end (- (re-search-forward "[\r\n]endobj" nil t) 7)))
    ;; (message "%s" end)

    (when end
      ;; Array
      (cond ((looking-at "\\[")
          (progn
            (setq start (point))
            (forward-sexp)
            (setq str (buffer-substring start (point))
                  obj (nreverse (pdf-get "str" `(("str" . ,str)))))
            (while (re-search-forward "\\([0-9]+\\) \\([0-9]+\\) R"
                                      (point) t)
              (push (cons "_id" (format "%s %s obj"
                                        (match-string 1)
                                        (match-string 2)))
                    obj))))
            ((looking-at "[[:alnum:]]")
             (setq obj (buffer-substring (point) end)))
            (t
    ;; Dictionary & stream
    (setq obj (pdf-parse-dict))
    (push (cons "_id" id) obj)
    ;; Decode stream
    (if (setq stream (pdf-decode (pdf-get "DecodeParms" obj) end))
        (push (cons "stream"
                    stream)
              obj))
    ;; Parse stream objects
    (when (string= "/ObjStm" (pdf-get "Type" obj))
      (unless pdf--stream-objects
        (setq pdf--stream-objects (make-hash-table)))

      (unless (gethash n pdf--stream-objects)
      (setq table pdf--stream-objects)
      (with-temp-buffer
      ;; (with-current-buffer "temp"
        ;; (erase-buffer)
        (insert stream)
        (goto-char (point-min))
        (setq start (string-to-number (pdf-get "First" obj))
              str (mapcar 'string-to-number
                          (split-string
                           (buffer-substring start
                                             (point))))
              start (+ start (nth 1 str)))
        (dotimes (i (/ (length str) 2))
          (goto-char (+ start (nth (1+ (* i 2)) str)))
          (setq o (pdf-parse-dict))
          (push (cons "_id" (format "%s 0 obj" (nth (* i 2) str))) o)
          ;; (message "%s %s" (nth (* i 2) str) o)
          (puthash (nth (* i 2) str) o table))
        ))

      (when (gethash n pdf--stream-objects)
        ;; Return a copy to prevent modification to the original
        (setq obj (copy-seq (gethash n pdf--stream-objects)))))
    ))
    ;; (message "%s" obj)
    (goto-char end)
    obj)))

(defun pdf--write-obj (obj &optional inner)
  (let* (id stream)
    (with-temp-buffer
      (mapc (lambda (a)
              (cond ((string= (car a) "_id")
                     (setq id (cdr a)))
                    ((string= (car a) "stream")
                     (setq stream (cdr a)))
                    ((not (and (car a) (cdr a))))
                    ((listp (cdr a))
                     (insert (format "/%s %s" (car a)
                                     (pdf--write-obj (cdr a) t))))
                    (t
                     (insert (format "/%s %s\n" (car a) (cdr a))))))
            obj)
      (insert ">>")
      (unless inner
        (insert "\n"))
      (if stream
          (insert "stream\n" stream "\nendstream\n"))
      (insert (if id "endobj" "") "\n")
      (goto-char (point-min))
      (unless inner
        (insert (or id "trailer")))
      (insert "\n<<\n")
      (buffer-string))
    ))

(defun pdf--get-width-sym (n)
  (nth n '(0 u8 u16 u24 u32)))

(defun pdf--put-xref (i o _v)
  "Add Object number, offset and version."
  (unless pdf--xref-table
    (setq pdf--xref-table (make-hash-table)))
  (unless (gethash i pdf--xref-table)
    (puthash i o pdf--xref-table)))

(defun pdf--get-xref (i v &optional object)
  "Retrieve object number and version."
  (let* ((o (if pdf--xref-table (gethash i pdf--xref-table))))
    (unless o
      (goto-char (point-min))
      (when (re-search-forward (format "[\r\n]%s %s %s" i v "obj")
                                       nil t)
        (forward-word -3)
        (setq o (point))
        (pdf--put-xref i o v)))
    (when o
      (if object (pdf-parse-obj o i) o))))

(defun pdf--generate-xref ()
  "Generate XREF table."
  (interactive)
  (let* ((table pdf--xref-table)
         (n (hash-table-count table))
         (keys (sort (hash-table-keys table) #'<))
         (k (car keys))
         (n 0)
         (i k))
    (with-temp-buffer
    (insert "xref\n")
    (while
        (progn
          (setq k (car keys)
                keys (cdr keys))
          (if (and k (= (+ i n) k))
              (setq n (1+ n))

            (insert (format "%s %s\n" i n))
            (dotimes (j n)
              (insert (format "%010d %05d n\r\n" (gethash (+ i j) table) 0)))
            (setq n 1 i k))
          i))
    (insert "\n")
    (buffer-string))
    ))

;;;###autoload
(defun pdf-fix-xref ()
  "Scan file and Generate XREF table."
  (interactive)
  (let* (start end i o v)
    (setq pdf--xref-table nil)
    (goto-char (point-min))
    (while (setq start (re-search-forward
                        "\\([0-9]+\\) \\([0-9]+\\) obj"
                        nil t))
      (setq o (- (point) (length (match-string 0)))
            i (string-to-number (match-string 1))
            v (string-to-number (match-string 2)))
      ;; (pdf-parse-obj)
      ;; (message "%s %s %s" i o v)
      (pdf--put-xref i o v))
    (pdf--generate-xref)
    ))

(defun pdf--get-page (pages i)
  (let* ((j 0)
         res n kids)
    (while (and pages (not res)
                (setq n (pdf-get "Count" (car pages))))
      (setq n (string-to-number n))
      (if (<= i (+ j n))
          (progn
            (if (string= "/Pages" (pdf-get "Type" (car pages)))
                (setq kids (pdf-get "Kids" (car pages))
                      res (pdf--get-page kids (- i j)))
              (setq res (car pages))))
        (setq pages (cdr pages)
              j (+ j n))
        ))
    (or res (nth (- i 1) pages))))

(defun pdf-get-popup (page)
  (let* (i popup obj w h x1 y1 x2 y2 rect)
    ;; Find page object
    (setq obj (pdf--get-page (pdf-get "Root.Pages.Kids") page))

    ;; Find popup annotations
    (setq obj (pdf-get "Annots" obj)
          i 0)
    (while obj
      (when (and ;(or (null n) (= i n))
                 (string= "/Popup" (pdf-get "Subtype" (car obj))))
        (setcdr (assoc "Open" (car obj)) "false")
                ;; (if n "true" "false"))
        ;; Position popup close to icon. Use defined dimensions.
        (setq rect (pdf-get "Rect" (car obj))
              w (- (string-to-number (nth 2 rect))
                   (string-to-number (nth 0 rect)))
              h (- (string-to-number (nth 3 rect))
                   (string-to-number (nth 1 rect))))
        (setq rect (pdf-get "Parent.Rect" (car obj)))
        (setcdr (assoc "Rect" (car obj))
                (format "[%s %0.3f %0.3f %s]"
                        (nth 0 rect)
                        (- (string-to-number (nth 3 rect)) h)
                        (+ (string-to-number (nth 0 rect)) w)
                        (nth 3 rect)
                        ))

        (push (car obj) popup))

    (when (and (string= "/Widget" (pdf-get "Subtype" (car obj))))
      (push (car obj) popup))

      (setq obj (cdr obj)))
    (nreverse popup)))

(defun pdf--fill-box (obj active index)
  (let* (ap n str rect v i id w h ch)
    (when (and (string= "/Widget" (pdf-get "Subtype" obj)))
      (setq id (pdf-get "_id" obj)
            v (or (cdr (assoc id pdf--form))
                  (pdf-get "V" obj)))
      (when (and (or (string= "/Tx" (pdf-get "FT" obj))
                     (setq ch (string= "/Ch" (pdf-get "FT" obj))))
               (setq n (pdf-get "AP" obj)
                     i index
                     ap (pdf-get "N" n)))
      (if v (setq v (pdf-decode-hex-value v)))
      ;; Change appearance to show a color filled box
      (setq rect (pdf-get "BBox" ap)
            w (string-to-number (nth 2 rect))
            h (string-to-number (nth 3 rect))
            str  (concat
                  "/Tx BMC q "
                  (if active
                      (format "1 w 0.8 0.8 1.0 rg %s %s %s %s re f* "
                              (nth 0 rect) (nth 1 rect) (nth 2 rect)
                              (nth 3 rect)))
                  (format "q 0 g BT /Helv 11 Tf 2 2 Td 1 Tc (%s) Tj "
                          (or v ""))
                  (if ch
                      (format "%s 2 Td /Symbol 11 Tf <DA> Tj " (- w h)))
                  "ET Q Q EMC"
                  ))
      (if v (setcdr (assoc "V" obj) (format "(%s)" v)))
      (setcdr (assoc "N" n) (format "%d 0 R" i))
      (setcdr (assoc "_id" ap) (format "%d 0 obj" i))
      (setcdr (assoc "Filter" ap) nil)
      (setcdr (assoc "stream" ap) str)
      (setcdr (assoc "Length" ap) (length str))
      )

     (when (string= "/Btn" (pdf-get "FT" obj))
       (setcdr (assoc "V" obj) v)
       (setcdr (assoc "AS" obj) v)
       ;; (message "%s %s" v obj)
       ))

    (when (and active
               (string= "/Popup" (pdf-get "Subtype" obj)))
      (setcdr (assoc "Open" obj) "true"))

    ap))

(defun pdf-decode-hex-value (v)
  "PDF values are encoded as either <FEFF0031> or (1)."
  (when (string-match "(\\(.*\\))" v)
    (setq v (match-string 1)))

  (when (string-match "<FEFF" v)
    (with-temp-buffer
      (insert v)
      (goto-char (point-min))
      (while (re-search-forward "\\(FE\\|FF\\|00\\|[<>]\\)" nil t)
        (replace-match ""))
      (setq v (decode-hex-string (buffer-string)))))
  v)

(defun pdf-show-popup (objects n)
  "Show (annotation popup) or highlight (field) nth object."
  (pdf--incremental-update objects n))

(defun pdf--incremental-update (objects n)
  (let* ((file (buffer-file-name))
         (tmpfile (expand-file-name "doc.pdf"
                                    (doc-view--current-cache-dir)))
         (trailer (copy-alist pdf--trailer))
         (size (string-to-number (pdf-get "Size" trailer)))
         start id i v popup obj ap)
    (when objects
      (setq objects (copy-tree objects)
            i 0)
      (when (assoc "Type" trailer)
        (setcdr (assoc "Type" trailer) nil)
        (setcdr (assoc "stream" trailer) nil))
      (if (assoc "Prev" trailer)
          (setcdr (assoc "Prev" trailer) pdf--startxref)
        (push (cons "Prev" pdf--startxref) trailer))

      ;; Populate appearance for form fields
      (dolist (o objects)
        (setq ap (pdf--fill-box o (= i n) size)
              i (1+ i))
        (when ap
          (setq size (1+ size))
          (push ap objects)))
      (setcdr (assoc "Size" trailer) (number-to-string size))
    (with-current-buffer (get-buffer-create "doc.pdf")
      (unless start
        (setq pdf--xref-table nil)
        (erase-buffer)
        (insert-file-contents file)
        (goto-char (point-max)))

      ;; Objects, xref, trailer, startxref
      (while objects
        (setq popup (car objects)
              ;; popup (pdf-get "Root.Pages.Kids[0].Kids[0].Kids[0].Annots[1]")
              id (cdr (assoc "_id" popup)))
      (string-match "\\([0-9]+\\) \\([0-9]+\\) obj" id)
      (setq i (string-to-number (match-string 1 id))
            v (string-to-number (match-string 2 id)))
      (pdf--put-xref i (1- (point)) v)
      (insert (pdf--write-obj popup))
      (setq objects (cdr objects)))

      (setq start (1- (point)))
      (insert (pdf--generate-xref))
      (insert (pdf--write-obj trailer))
      (insert "startxref\n")
      (insert (number-to-string start))
      (insert "\n%%EOF\n")
      (write-file tmpfile)
      ))
    tmpfile))

(defun pdf--gen-stream (width values)
  (let* ((specs `((type ,(pdf--get-width-sym (nth 0 width)))
                  (offset ,(pdf--get-width-sym (nth 1 width)))
                  (index ,(pdf--get-width-sym (nth 2 width))))))
    (bindat-pack specs values)
    ))

(defun pdf-parse-hint-table ()
  (interactive)
  ;; Linearized PDF
  (let* (obj)
    (goto-char (point-min))
    (setq obj (pdf-parse-obj))
    (when (assoc "Linearized" obj)
      (setq pdf--total-pages (cdr (assoc "N" obj)))
      ;; (message "%s" pdf--total-pages)
      (pdf-parse-xref-obj))))

(defun pdf-parse-xref-obj ()
  (interactive)
  (let* (start i n tmp obj v xref idx o
               j stream width entry specs len)
    (setq obj    (pdf-parse-obj)
          width  (mapcar #'string-to-number (pdf-get "W" obj))
          len    (apply #'+ width)
          j      0
          specs `((type ,(pdf--get-width-sym (nth 0 width)))
                  (offset ,(pdf--get-width-sym (nth 1 width)))
                  (index ,(pdf--get-width-sym (nth 2 width))))
          stream (cdr (assoc "stream" obj)))
          ;; stream [1 0 0 16 0])

    (unless pdf--trailer
      (setq pdf--trailer obj))

    ;; Unpack stream
    (setq idx (pdf-get "Index" obj))
    (while idx
      (setq i (string-to-number (car idx))
            n (+ i (string-to-number (cadr idx))))

    (while (and (< i n)
                (setq entry (bindat-unpack specs stream j)))
      ;; (message "%s %s" i entry)
      (pcase (alist-get 'type entry)
        ('1 (setq o (alist-get 'offset entry)
                  v (alist-get 'index entry))
            (pdf--put-xref i o v))
        ('2 (setq o (pdf--get-xref (alist-get 'offset entry) 0)
                  v (alist-get 'index entry))
            (pdf--put-xref i o v))
        )
      (setq i (1+ i)
            j (+ j len)))
    (setq idx (cddr idx)))
    obj))

(defun pdf-parse-xref-table ()
  (interactive)
  (let* (start i n tmp o v xref)
    (re-search-forward "xref")
    (skip-chars-forward "\r\n")
    (while (looking-at "[0-9]")
    (setq start (point))
    (re-search-forward "[\r\n]")
    (setq tmp (split-string (buffer-substring start (point)))
          i (string-to-number (nth 0 tmp))
          n (string-to-number (nth 1 tmp)))
    (dotimes (j n)
      (skip-chars-forward "\r\n")
      (setq start (point))
      (re-search-forward "[\r\n]")
      (setq tmp (split-string (buffer-substring start (point)))
            o (string-to-number (nth 0 tmp))
            v (string-to-number (nth 1 tmp)))
      (pdf--put-xref (+ i j) o v)))

    ;; Parse trailer which can be used to obtain Root
    (re-search-forward "trailer" nil t)
    (setq o (pdf-parse-dict))
    (unless pdf--trailer
      (setq pdf--trailer o))
    o))

(defun pdf-parse-xref-1 (start)
  ;; Parse xref table
  (let* (obj)
    (goto-char start)
    (skip-chars-forward "\r\n ")
    (setq obj (if (looking-at-p "xref")
                  (pdf-parse-xref-table)
                (pdf-parse-xref-obj)))

    (when (setq start (pdf-get "Prev" obj))
      (pdf-parse-xref-1 (string-to-number start)))))

(defun pdf-parse-xref ()
  (interactive)
  (condition-case err
  (let* (start)
    (setq pdf--xref-table nil
          pdf--trailer nil
          pdf--stream-objects nil)

    ;; Find xref table
    (goto-char (- (point-max) 50))
    (re-search-forward "startxref" nil t)
    (skip-chars-forward "\r\n ")
    (setq start (+ (point)))
    (forward-word)
    (setq start (string-to-number (buffer-substring start (point)))
          pdf--startxref start)
    ;; (message "%s" start)
    (pdf-parse-xref-1 start)
    )
  (error (message "No xref table")
    ;; Parse trailer when xref is missing
    (re-search-forward "trailer" nil t)
    (setq pdf--trailer (pdf-parse-dict))
    )))

(defun pdf--get-object (name obj)
  (unless (or obj pdf--trailer)
    (save-excursion
      (pdf-parse-xref)))
  (let* ((tmp name)
         id pos ids res i j v found)
    (if (string-match "\\(.*\\)\\[\\(.*\\)\\]" tmp)
        (setq name (match-string 1 tmp)
              i    (string-to-number (match-string 2 tmp))))
    (setq v (cdr (assoc name (or obj pdf--trailer))))
    (cond ((and (stringp v) (string-match "^\\[" v))
           (setq ids (split-string (substring v 1 -1))
                 j 0)
           (if (or (null (nth 2 ids))
                   (not (string= (nth 2 ids) "R")))
             (setq res ids
                   ids nil)
           (while ids
             (when (or (null i)
                       (= i j))
             (setq v (pdf--get-xref
                        (string-to-number (nth 0 ids))
                        (string-to-number (nth 1 ids))
                        t))
             (push v res)
             ;; (if pos
             ;;     (push (pdf-parse-obj pos) res)
             ;;   (goto-char (point-min))
             ;;   (search-forward (format "%s %s %s"
             ;;                           (nth 0 ids) (nth 1 ids) "obj"))
             ;;   (push (pdf-parse-obj) res))
             )
             (setq ids (cdddr ids)
                   j (1+ j)))
           (setq res (reverse res)))
           (if i (car res) res))
          ((and (stringp v) (string-match " R$" v))
           (setq ids (split-string v)
                 id (substring v 0 -1)
                 res (pdf--get-xref
                      (string-to-number (nth 0 ids))
                      (string-to-number (nth 1 ids))
                      t))
           ;; (if pos
           ;;     (setq res (pdf-parse-obj pos))
           ;;   (goto-char (point-min))
           ;;   (search-forward (concat id "obj"))
           ;;   (setq res (pdf-parse-obj)))
           ;; (setq res (reverse res))
           (if i (nth i res) res))
          (t v))
      ))

;;;###autoload
(defun pdf-total-pages ()
  "Number of pages in the PDF."
  (interactive)
  (let* ((start pdf--total-pages))
    ;; Root > Catalog > Pages/Count
    (unless (and start (not (buffer-modified-p)))
      (if (setq start (pdf-get "Root.Pages.Count"))
          (setq pdf--total-pages (string-to-number start)
                start pdf--total-pages)))
    (when (called-interactively-p 'any)
      (message "%s" start))
    (or start 1)))

;;;###autoload
(defun pdf-form ()
  "Number of pages in the PDF."
  (interactive)
  (let* (obj start fields k v i)
    ;; Root > AcroForm > Fields (/T and /V) (/AA.E|X)
    ;; Kids are annotation widgets
    (setq fields (pdf-get "Root.AcroForm.Fields"))
    ;; (message "%s" fields)
    (while fields
      (setq obj (car fields)
            fields (cdr fields)
            i (pdf-get "_id" obj)
            k (pdf-get "T" obj)
            v (pdf-get "V" obj))
      (message "%s %s %s" i k v)

      ;; scripts
      (setq obj (pdf-get "AA" obj))
      (when obj
        (setq k (pdf-get "E.JS" obj)
              v (pdf-get "X.JS" obj))
        (message "%s %s" k v)))
    ))

(defun pdf-get (name &optional obj)
  (interactive "sName: ")
  (let* ((parts (split-string name "\\."))
         (start nil))
    (dolist (part parts)
      (setq obj (pdf--get-object part obj))
      ;; (message "%s" obj)
      )
    obj))

(defun pdf-decode (&optional params end)
  "Decode compressed streams in PDF"
  (interactive)
  (let* (start stream)
    (when (re-search-forward "stream" end t)
      (skip-chars-forward "\r\n")
      (setq start (point))
      (re-search-forward "endstream" end t)
      (setq stream (buffer-substring-no-properties start (- (point) 10)))
      ;; (setq params '(("W" . "[1 3 1]")
      ;;                ("Predictor" . "12")
      ;;                ("Columns" . "5")))
      (with-temp-buffer
        (set-buffer-multibyte nil)
        (insert stream)
        (zlib-decompress-region (point-min) (point-max))
        (if params
            (setq stream
            (pdf--post-decode (point-min) (point-max)
                              (string-to-number (pdf-get "Columns" params))
                              (string-to-number (pdf-get "Predictor" params))))
        (setq stream (buffer-string))))
      ;; (message "%s %s" start stream)
      )))

(defun pdf--post-decode (start end cols predictor)
  ;; PNG Filter
  (let* ((len (- end start))
         (row 0)
         (res (make-vector (- len (/ len (+ cols 1))) 0))
         j b)
    (goto-char start)
    (while (< (point) end)
      (setq j (* row cols)
            predictor (char-after))
      ;; (message "%s %s" predictor row)
      (forward-char)
      (dotimes (i cols)
        (setq b (pcase predictor
                  ('0 (char-after))
                  ;; ('1 ;; Sub
                  ;;  )
                  ('2 ;; Up
                   (+ (aref res (+ (* (max 0 (1- row)) cols) i))
                      (char-after)))
                  ;; ('3 ;; Average
                  ;;  )
                  ;; ('4 ;; Paeth
                  ;;  )
                  ))
        (aset res (+ i j) b)
        (forward-char))
      (setq row (1+ row)))
    ;; (message "%s" res)
    res))

(provide 'pdf)
