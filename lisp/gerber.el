;;; gerber.el --- Gerber file creation functions -*- lexical-binding: t -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com> 2021
;; Keywords: eda
;; Version: 1.0
;; Package-Requires: ((emacs "25"))

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)

(defvar gerber-precision 4
  "Number of digits after decimal.")
(defvar gerber-uom "MM"
  "Unit of measurement (mm or in).")
(defvar gerber-aperture-width 1)

(defvar gerber--aperture-count 10)
(defvar gerber--font-cache nil)

;;;###autoload
(defun gerber (&optional file)
  (interactive)
  (let* ((file (or file "~/c.svg"))
         (svg (svg-load file))
         (layer nil)
         (font nil)
         (cache (make-char-table 'font))
         )
    (unless gerber--font-cache
      (setq file (car (find-file-read-args "SVG Font file: "
                                           (confirm-nonexistent-file-or-buffer)))
            font (svg-load file))
      ;; (setq font (svg-load "~/EMSAllure.svg"))
      (mapc (lambda (a)
              (aset cache (string-to-char (dom-attr a 'unicode)) a)
              ;; (message "%s " (string-to-char (dom-attr a 'unicode)))
              )
            (dom-by-tag font 'glyph))
      (setq gerber--font-cache cache))

    ;; (gerber--transform-origin svg)
    (gerber--export svg layer)
    ;; (pcb--3d svg)
    ))

(defun gerber--transform-origin (svg)
  "For WYSIWYG in gerber view, replace Y with -Y.
Possibly not required."
  (let* ((origin (dom-attr svg 'eda:origin))
         x y point bbox)
    (if origin
        (setq point (split-string origin ",")
              x (string-to-number (car point))
              y (string-to-number (cadr point)))
      (setq x 0 y (+ (dom-attr svg 'height))))
    (pp svg)
    (dolist (a (dom-attributes svg))
      (if (and (memq (car a) '(x y width height x1 y1 x2 y2 cx cy r rx ry))
               (stringp (cdr a))
               (not (string-match-p "%" (cdr a))))
          (setcdr a (string-to-number (cdr a))))

      (cond ((and ox (memq (car a) '(x x1 x2 cx)))
             (setcdr a (- (cdr a) ox)))
            ((and oy (memq (car a) '(y y1 y2 cy)))
             (setcdr a (- (cdr a) oy)))
            ))
    (dolist (node (dom-children svg))
      (unless (stringp node)
        (cond ((and ox (memq (car a) '(x x1 x2 cx)))
             (setcdr a (- (cdr a) ox)))
            ((and oy (memq (car a) '(y y1 y2 cy)))
             (setcdr a (- (cdr a) oy)))
            )))

    (pp svg)
  ))

(defun gerber--export (svg layer)
  (with-current-buffer (get-buffer-create "a.gbr")
      (erase-buffer)
      ;;(insert "%TF.FileFunction,Soldermask,Top*%\n")
      ;; 7 is the padding in co-ordinate format string
      (insert (format "%%FSLAX%d%dY%d%d*%%\n"
                      (- 7 gerber-precision) gerber-precision
                      (- 7 gerber-precision) gerber-precision))
      (insert (format "%%MO%s*%%\n" (upcase gerber-uom)))
      (insert "%LPD*%\n")
      (insert (format "%%ADD10C,%0.2f*%%\n" gerber-aperture-width))
      (insert "D10*\n")
      (setq gerber--aperture-count 11)
      (dolist (node (dom-children svg))
        (gerber--plot node nil layer)
        )
      (insert "M02*\n")
      (save-buffer)
      ))

(defun gerber--plot (node &optional transform layer)
  (let* ((mult (expt 10 gerber-precision))
         (fill (unless (string= (dom-attr node 'fill) "none") t))
         (pad (dom-attr node 'eda:pad))
         (l (dom-attr node 'eda:layer))
         (transform (concat transform " " (dom-attr node 'transform)))
         (i 0)
         x y w h r point)
    ;; Skip unrelated layer
    (when (or (eq (dom-tag node) 'g)
              (string= l layer))
    (pcase (dom-tag node)
      ('comment
       (insert (format "G04 %s*\n" (car (dom-children node)))))
      ('line
       (setq point (svg--apply-transform (dom-attr node 'x1) (dom-attr node 'y1)
                                         transform))
       (insert (format "X%07dY%07dD02* " (* (car point) mult) (* (cdr point) mult)))
       (setq point (svg--apply-transform (dom-attr node 'x2) (dom-attr node 'y2)
                                         transform))
       (insert (format "X%07dY%07dD01*\n" (* (car point) mult) (* (cdr point) mult)))
       )
      ('polyline
       (mapc (lambda (a)
                 (setq point (split-string a ",")
                       x (string-to-number (car point))
                       y (string-to-number (cadr point))
                       point (svg--apply-transform x y transform)
                       )
                 (if (> i 0)
                     (progn
                       (if (= (% i 4) 0)
                           (insert "\n"))
                       (insert (format "X%07dY%07dD01* "
                                       (* (car point) mult) (* (cdr point) mult))))
                   (insert (format "X%07dY%07dD02* "
                                   (* (car point) mult) (* (cdr point) mult)))
                   (setq i (1+ i))
                 ))
               (split-string (dom-attr node 'points)))
       (insert "\n")
       )
      ('circle
       ;; G01, G02, G03 linear, clockwise, anti-clockwise interpolation mode
       (setq x (dom-attr node 'cx)
             y (dom-attr node 'cy)
             r (dom-attr node 'r)
             point (svg--apply-transform x y transform)
             )
       (when pad
         (insert (format "%%ADD%dC,%0.3f*%%\n" gerber--aperture-count (* r 2)))
         (insert (format "D%d*\n" gerber--aperture-count))
         (setq gerber--aperture-count (1+ gerber--aperture-count))
         (insert (format "X%07dY%07dD03*\n" (* (car point) mult) (* (cdr point) mult)))
         (insert "D10*\n"))
       (unless pad
         (if fill
             (insert "G36*\n"))
         ;; Multi-quadrant mode. Set same start and end point.
         (insert "G75*\n")
         (insert "G03*\n")
         (insert (format "X%07dY%07dD02*\n" (* (+ r (car point)) mult)
                         (* (+ r (cdr point)) mult)))
         (insert (format "X%07dY%07dI%07dJ%07dD01*\n" (* (+ r (car point)) mult)
                         (* (+ r (cdr point)) mult)
                         (* r -1 mult) (* r -1 mult)))
         (insert "G01*\n")
         (if fill
             (insert "G37*\n"))
         )
      )
      ('rect
       (setq x (dom-attr node 'x)
             y (dom-attr node 'y)
             w (dom-attr node 'width)
             h (dom-attr node 'height)
             )
       (when (and (numberp x) (numberp y) (numberp w) (numberp h))
       (when pad
         (insert (format "%%ADD%dR,%0.3fX%0.3f*%%\n" gerber--aperture-count w h))
         (insert (format "D%d*\n" gerber--aperture-count))
         (setq gerber--aperture-count (1+ gerber--aperture-count))
         (insert (format "X%07dY%07dD03*\n" (* (car point) mult) (* (cdr point) mult)))
         (insert "D10*\n")
         )

       (unless pad
       (if fill
         (insert "G36*\n"))
       (setq point (svg--apply-transform x y transform))
       (insert (format "X%07dY%07dD02*\n" (* (car point) mult) (* (cdr point) mult)))
       (setq point (svg--apply-transform (+ x w) y transform))
       (insert (format "X%07dY%07dD01* " (* (car point) mult) (* (cdr point) mult)))
       (setq point (svg--apply-transform (+ x w) (+ y h)
                                         transform))
       (insert (format "X%07dY%07dD01* " (* (car point) mult) (* (cdr point) mult)))
       (setq point (svg--apply-transform x (+ y h) transform))
       (insert (format "X%07dY%07dD01* " (* (car point) mult) (* (cdr point) mult)))
       (setq point (svg--apply-transform x y transform))
       (insert (format "X%07dY%07dD01* " (* (car point) mult) (* (cdr point) mult)))
       (insert "\n")
       (if fill
         (insert "G37*\n"))
       )))
      ('polygon
       (when pad
         (insert (format "%%ADD%dR,%0.3fX%0.3f*%%\n" gerber--aperture-count w h))
         (insert (format "D%d*\n" gerber--aperture-count))
         (setq gerber--aperture-count (1+ gerber--aperture-count))
         (insert (format "X%07dY%07dD03*\n" (* (car point) mult) (* (cdr point) mult)))
         (insert "D10*\n")
         )

       (unless pad
         (if fill
             (insert "G36*\n"))
         (mapc (lambda (a)
                 ; (message "%s" a) *
                 (setq point (split-string a ",")
                       x (string-to-number (car point))
                       y (string-to-number (cadr point))
                       point (svg--apply-transform x y transform))
                 (insert (format "X%07dY%07dD02*\n"
                                 (* (car point) mult)
                                 (* (cdr point) mult))))
               (split-string (dom-attr node 'points)))
         (insert "\n")
         (if fill
             (insert "G37*\n"))
         ))
      ('g (unless pad
            (mapc (lambda (a)
                    (gerber--plot a transform layer))
                  (dom-children node)))
          (when pad
            (setq x (or (dom-attr node 'x) 0) y (or (dom-attr node 'y) 0))
            (setq point (svg--apply-transform x y transform))
            (insert (format "%%ADD%d%s*%%\n" gerber--aperture-count pad))
            (insert (format "D%d*\n" gerber--aperture-count))
            (setq gerber--aperture-count (1+ gerber--aperture-count))
            (insert (format "X%07dY%07dD03*\n" (* (car point) mult) (* (cdr point) mult)))
            (insert "D10*\n")
            ))
      ('path
       (let* ((tokens (if (dom-attr node 'd) (split-string (dom-attr node 'd))))
              (mult (* mult 0.1))
              cmd x y nl)
         (while tokens
           (setq cmd (pop tokens)
                 nl t
                 x (string-to-number (pop tokens))
                 y (string-to-number (pop tokens))
                 point (svg--apply-transform x y transform))
           (insert (format "X%07dY%07dD0%s* "
                           (* (car point) mult) (* (cdr point) mult)
                           (if (string= cmd "M") 2 1)))
           )
         (if nl (insert "\n"))
         ))
      ('text
       ;; Replace text with SVG stroke font glyphs
       (let* ((size (or (dom-attr node 'font-size) 15))
              (w 0)
              (cache gerber--font-cache))
         (mapc (lambda (a)
                 (gerber--plot `(path ((d . ,(dom-attr (aref cache a) 'd))
                                       (transform . ,(format "translate(%d)" w))
                                       )))
                 (setq w (+ w (string-to-number
                                     (dom-attr (aref cache a) 'horiz-adv-x))))
                 ;; (message "%s %s" (string a) (aref cache a))
                 )
               (car (dom-children node)))
       ))
      )
    )))

(defvar gerber-font-lock-keywords
  `(("[GMD][0-9]\\{2\\}" . font-lock-keyword-face)
    ("%\\(FSLA\\|MOMM\\|MOIN\\|LPD\\|LPC\\)"
     . font-lock-keyword-face)
    ("\\(X\\|Y\\)" . font-lock-variable-name-face)
    ("%\\(FS\\|AD\\|AM\\|AB\\|LM\\|LR\\|LS\\|SR\\|TA\\|TD\\|TF\\|TO\\)"
     . font-lock-keyword-face)))

;;;###autoload
(define-derived-mode gerber-mode fundamental-mode "Gerber"
  "Major mode for editing Gerber file."
  (setq-local abbrev-mode t)
  (setq-local comment-start "G04")
  (setq-local comment-end "*")

  (font-lock-add-keywords nil gerber-font-lock-keywords)
  )

(add-to-list 'auto-mode-alist '("\\.gbr$" . gerber-mode))

(defvar pcb--3d-pcb "
# DEF EntryView Viewpoint {
#    position 0 2 15
#    description \"Entry View\"
#    jump TRUE
#    orientation 0 0 1 0.2}

Transform {
    translation %0.1f %0.1f %0.1f
    children Shape {
   geometry Box {
      size %0.1f %0.1f %0.1f
   }
   appearance Appearance {
      material Material {
	  emissiveColor 1 1 0
	  shininess 1
      }
   }
 }
}
")

(defvar pcb--3d-component "
Transform {
    # rotation 1 0 0 3.14
    translation %0.1f %0.1f %0.1f
    children [
	Inline {
	    url \"%s\"
        }
    ]
}
")

(defun pcb--3d (svg)
  (let* ((z 0)
         x y w h point url type bbox)
    (with-current-buffer (get-buffer-create "b.wrl")
      (erase-buffer)
      ;; (insert-file "~/a.wrl")
      (insert "#VRML V2.0 utf8")
      (dolist (node (dom-children svg))
        (setq type (dom-attr node 'eda:type))
        (cond ((string= type "board")
               (setq bbox (svg-bbox node)
                     x (+ (/ (- (nth 2 bbox) (nth 0 bbox)) 2.0)
                          (dom-attr node 'x))
                     y (+ (/ (- (nth 3 bbox) (nth 1 bbox)) 2.0)
                          (dom-attr node 'y))
                     w (dom-attr node 'width)
                     h (dom-attr node 'height))
               (insert (format pcb--3d-pcb x y z w h 0.1)))

              (type
               (canvas--move-init node)
               (setq url (dom-attr node 'eda:model)
                     point (cdr canvas--temp-points)
                     x (car point)
                     y (cdr point))
               (insert (format pcb--3d-component x y z url)))))

      (shell-command-on-region (point-min) (point-max) "view3dscene -")
      )))

(defun pcb--circle (svg)
  (pcb--add-namespace svg)
  (let* ((r (read-number "Radius: "))
         (rh (read-number "Hole Radius: " 0))
         (id canvas--id)
         (g (svg-group svg nil :eda:type "pad"
                       :eda:pad (if (> rh 0)
                                    (format "C,%dX%d" (* 2 r) (* 2 rh))
                                  (format "C,%d" (* 2 r))))))
    (svg-circle g 0 0 r :id (number-to-string id)
                :stroke canvas--stroke-color :fill "darkred")
    (if (> rh 0)
        (svg-circle g 0 0 rh :id (number-to-string (1+ id)) :fill "white"))
    ;; (message "%s" g)
    (canvas--move-init g)
    (setq canvas--undo-marker (list g)
          canvas--id (+ canvas--id 2)
          canvas--mode 'place)
    ))

(defun pcb--rectangle (svg)
  (pcb--add-namespace svg)
  (let* ((w (read-number "Width: "))
         (h (read-number "Height: " 0))
         (rh (read-number "Hole Radius: " 0))
         (id canvas--id)
         (g (svg-group svg nil :eda:type "pad"
                       :eda:pad (if (> rh 0)
                                    (format "R,%dX%dX%d" w h (* 2 rh))
                                  (format "R,%dX%d" w h)))))
    (svg-rectangle g (/ w -2) (/ h -2) w h :id (number-to-string id)
                :stroke canvas--stroke-color :fill "darkred")
    (if (> rh 0)
        (svg-circle g 0 0 rh :id (number-to-string (1+ id))
                    :fill "white"))
    ;; (message "%s" g)
    (canvas--move-init g)
    (setq canvas--undo-marker (list g)
          canvas--id (+ canvas--id 2)
          canvas--mode 'place)
    ))

(defun pcb--obround (svg)
  (pcb--add-namespace svg)
  (let* ((w (read-number "Width: "))
         (h (read-number "Height: " 0))
         (r (/ h 2))
         (rh (read-number "Hole Radius: " 0))
         (id canvas--id)
         (g (svg-group svg nil :eda:type "pad"
                       :eda:pad (if (> rh 0)
                                    (format "O,%dX%dX%d" w h (* 2 rh))
                                  (format "O,%dX%d" w h))))
         (node (svg-path g nil :id (number-to-string id)
                         :stroke canvas--stroke-color :fill "darkred"))
         (path (concat
                (format "M %d,%d a %d %d 0 1 1 %d,%d"
                        (/ (- w h) 2) (- r) r r 0 h)
                (format " h %d a %d %d 0 1 1 %d,%d z"
                        (- (- w h)) r r 0 (- h))
                )))
    ;; (message "%s" path)
    (dom-set-attribute node 'd path)
    (if (> rh 0)
        (svg-circle g 0 0 rh :id (number-to-string (1+ id))
                    :fill "white"))
    (message "%s" g)
    (canvas--move-init g)
    (setq canvas--undo-marker (list g)
          canvas--id (+ canvas--id 2)
          canvas--mode 'place)
    ))

(defun pcb--polygon (svg)
  (pcb--add-namespace svg)
  (let* (points p
         (r (read-number "Radius: "))
         (n (read-number "Sides: " 3))
         (deg (read-number "Rotation: " 0))
         (rh (read-number "Hole Radius: " 0))
         (id canvas--id)
         (g (svg-group svg nil :eda:type "pad"
                       :eda:pad
                       (cond ((> rh 0)
                              (format "P,%dX%dX%dX%d" (* 2 r) n deg (* 2 rh)))
                             ((> deg 0)
                              (format "P,%dX%dX%d" (* 2 r) n deg))
                             (t
                              (format "P,%dX%d" (* 2 r) n)))))
         ;; (r 10)
         ;; (n 3)
         ;; ;; (g (dom-node 'g))
         ;; (g (svg-group svg nil))
         ;; (id 2)
         ;; (rh 0)
         )
    (unless (and (>= n 3) (<= n 12))
        (error "Number of sides must be between 3 and 12"))
    (dotimes (i n)
      (setq p (cons (* r (cos (* 2 pi (/ i n 1.0))))
                    (* r (sin (* 2 pi (/ i n 1.0))))))
      (push p points))
    (svg-polygon g points :id (number-to-string id)
                 :transform (format "rotate(%s)" deg)
                 :stroke canvas--stroke-color :fill "darkred")
    (if (> rh 0)
        (svg-circle g 0 0 rh :id (number-to-string (1+ id))
                    :fill "white"))
    (message "%s" g)
    (canvas--move-init g)
    (setq canvas--undo-marker (list g)
          canvas--id (+ canvas--id 2)
          canvas--mode 'place)
    ))

(defun pcb--add-namespace (svg)
  ;; Add eda namespace
  (unless (dom-attr svg 'xmlns:eda)
    (dom-set-attribute svg 'xmlns:eda
                       "http://www.gnu.org/eda")))

(defun pcb--export (svg)
  (gerber--export svg nil))

;;;###autoload
(defvar pcb-canvas-map
  (let* ((key-mode (make-sparse-keymap)))
    ;; mode function docstring
    (define-key key-mode "c" '(pcb pcb--circle "Circle"))
    (define-key key-mode "r" '(pcb pcb--rectangle "Rectangle"))
    (define-key key-mode "o" '(pcb pcb--obround "Obround"))
    (define-key key-mode "p" '(pcb pcb--polygon "Polygon"))

    (define-key key-mode "e" '(pcb pcb--export "Export to Gerber"))
    (define-key key-mode "v" '(pcb pcb--3d "3D view"))
    key-mode)
  "Map to be used for canvas plugin.")

(provide 'gerber)
