;;; comics-mode.el --- Comic Book Reader mode

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: comics, comic book

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)

(defvar comics--magnifier-offset 20)
(defvar comics--magnifier-width 200)
(defvar comics--magnifier-height 120)

(defvar comics--view-thumb nil)
(make-variable-buffer-local 'comics--view-thumb)

(defvar comics--zoom-enabled nil)
(defvar comics--magnifier-point nil)
(defvar comics--magnifier-pos nil)
(defvar comics--magnifier-point-width nil)
(make-variable-buffer-local 'comics--magnifier-point-width)

(defvar comics--image-width nil)
(make-variable-buffer-local 'comics--image-width)

(defvar comics--image-height nil)
(make-variable-buffer-local 'comics--image-height)

(defvar comics-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map special-mode-map)
    (define-key map "p" 'comics-previous)
    (define-key map "n" 'comics-next)
    (define-key map " " 'comics--open-page)
    (define-key map "q" 'comics-mode)
    (define-key map "z" 'comics-zoom)
    (define-key map "\C-m" 'comics--open-page)
    map))

(defun comics--open-page ()
  (interactive)
  (let ((w comics--view-thumb))
    ;; Move to thumb view if we are on a page
    (select-window w)
    (archive-extract-other-window)
    (setq comics--view-thumb w)
    (use-local-map comics-mode-map)
    (select-window w)))

(defun comics--change-viewbox (node x y)
  (let* ((w comics--magnifier-point-width)
         (half-point (/ w 2))
         (half-view (+ half-point comics--magnifier-offset))
         (limit-x 0)
         (limit-y 0))
    ;; Keep point in the center of viewBox
    (when (and (> x half-point)
               (< (+ x half-point) comics--image-width))
      (setcar comics--magnifier-point (- x half-point)))
    (when (and (> y half-point)
               (< (+ y half-point) comics--image-height))
      (setcdr comics--magnifier-point (- y half-point)))

    ;; Keep magnifier inside the view port
    (when (and (> x half-view)
               (< (setq limit-x (+ x half-view comics--magnifier-width))
                  comics--image-width))
      (setcar comics--magnifier-pos (+ x half-view)))
    (when (and (> y half-view)
               (< (setq limit-y (+ y half-view comics--magnifier-height))
                  comics--image-height)
      (setcdr comics--magnifier-pos (+ y half-view))))
    (when (and (> limit-x comics--image-width)
               (> limit-y comics--image-height))
        (setcdr comics--magnifier-pos (- y half-view comics--magnifier-height)))

    (when (string= (dom-attr node 'id) "point")
      (dom-set-attribute node 'x (car comics--magnifier-point))
      (dom-set-attribute node 'y (cdr comics--magnifier-point)))
    (when (string-prefix-p "tool" (dom-attr node 'id))
      (dom-set-attribute node 'x (car comics--magnifier-pos))
      (dom-set-attribute node 'y (cdr comics--magnifier-pos))
      (when (eq (dom-tag node) 'svg)
        (dom-set-attribute node 'viewBox
                           (format "%d %d %d %d"
                                   (car comics--magnifier-point)
                                   (cdr comics--magnifier-point)
                                   w w))))
    ))

(defun comics--zoom-exit ()
  (set-buffer-modified-p nil)
  (kill-buffer))

(defun comics--zoom-on ()
  (unless (derived-mode-p 'image-mode)
    (error "Not an image"))
  (let* ((dir (concat temporary-file-directory "comics"))
         (bbox (window-body-pixel-edges))
         (w (- (nth 2 bbox) (nth 0 bbox)))
         (h (- (nth 3 bbox) (nth 1 bbox)))
         (w-tool comics--magnifier-width)
         (h-tool comics--magnifier-height)
         (w-zoom (/ w w-tool 0.1))
         (svg (svg-create w h :stroke-color "black"))
         (img (svg-create w-tool h-tool
                           :id "tool-img"
                           :viewBox (format "0 0 %d %d" w-zoom w-zoom)))
         ;; (canvas--hide-prompt t)
         (canvas--move-to-fn 'comics--change-viewbox)
         (canvas-exit-fn 'comics--zoom-exit)
         (file (file-name-base (buffer-file-name)))
         rect point)
    (make-directory dir t)
    (write-file (expand-file-name file dir))

    (svg-use img "img")

    (svg-embed-href svg file
               :id "img"
               :width w
               :height h)
    (svg--append svg img)
    (setq rect (svg-rectangle svg 0 0 w-tool h-tool
                              :id "tool-rect"
                              :fill "none"
                              :stroke "grey"
                              :stroke-width "5"
                              :rx "5"
                              :ry "5")
          point (svg-rectangle svg 0 0 w-zoom w-zoom
                               :id "point"
                               :fill "none"))

    (set-window-buffer nil (find-file-literally (concat dir "/main.svg")))
    (setq inhibit-read-only t)
    (rename-buffer "*Magnifier*")
    (erase-buffer)
    (svg-insert-image svg)

    (setq comics--magnifier-point-width w-zoom
          comics--magnifier-point '(0 . 0)
          comics--magnifier-pos   '(0 . 0)
          comics--image-width w
          comics--image-height h)
    (setq canvas--svg (list svg)
          canvas--mode 'move
          canvas--undo-marker (list point rect img)
          canvas--id (length (dom-children svg)))
    ;; (canvas--move-init canvas--undo-marker)
    (ewp-crop-image-1 svg)
    ))

(defun comics-previous ()
  (interactive)
  (forward-line -1)
  (comics--open-page))

(defun comics-next ()
  (interactive)
  (forward-line)
  (comics--open-page))

(defun comics-zoom ()
  "Toggle comics zoom mode."
  (interactive)
  (comics--zoom-on))
  ;; (if comics--zoom-enabled
  ;;     (setq comics--zoom-enabled nil)
  ;;   (comics--zoom-on)
  ;;   (setq comics--zoom-enabled t)))

(defun comics--setup ()
  (interactive)
  (let* ()
    (setq comics--view-thumb (selected-window))
    (if (= (length (window-list)) 1)
        (split-window-right 25))
    (comics--open-page)
    ))

;;;###autoload
(define-minor-mode comics-mode
  "Minor mode for Comics Book Reader."
  :lighter "comics"
  (if comics-mode
      (progn
        (image-font-lock-mode 1)
        (comics--setup))
    (image-font-lock-mode -1)))

(provide 'comics-mode)
