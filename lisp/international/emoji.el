;;; emoji.el --- Inserting emojis  -*- lexical-binding:t -*-

;; Copyright (C) 2021-2022 Free Software Foundation, Inc.

;; Author: Lars Ingebrigtsen <larsi@gnus.org>
;; Keywords: fun

;; Package-Requires: ((emacs "28.0") (transient "0.3.7"))
;; Package-Version: 0.1

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'cl-lib)
(require 'cl-extra)
(require 'generate-lisp-file)
(require 'emoji-zwj)

(defgroup emoji nil
  "Inserting Emojis."
  :version "29.1"
  :group 'play)

(defface emoji-list-header
  '((default :weight bold :inherit variable-pitch))
  "Face for emoji list headers."
  :version "29.1")

(defface emoji
  '((t :height 2.0))
  "Face used when displaying an emoji."
  :version "29.1")

(defface emoji-with-derivations
  '((((background dark))
     (:background "#202020" :inherit emoji))
    (((background light))
     (:background "#e0e0e0" :inherit emoji)))
  "Face for emojis that have derivations."
  :version "29.1")

(defvar emoji-alternate-names nil
  "Alist of emojis and lists of alternate names for the emojis.
Each element in the alist should have the emoji (as a string) as
the first element, and the rest of the elements should be strings
representing names.  For instance:

  (\"🤗\" \"hug\" \"hugging\" \"kind\")")

(defvar emoji--labels nil)
(defvar emoji--all-bases nil)
(defvar emoji--derived nil)
(defvar emoji--names (make-hash-table :test #'equal))
(defvar emoji--done-derived nil)
(defvar emoji--insert-buffer)
(defvar emoji--font nil)

(defun emoji--name (glyph)
  (or (gethash glyph emoji--names)
      (get-char-code-property (aref glyph 0) 'name)))

(defun emoji--init (&optional force inhibit-adjust)
  (when (or (not emoji--labels)
            force)
    (unless force
      (ignore-errors (require 'emoji-labels)))
    ;; The require should define the variable, but in case the .el
    ;; file doesn't exist (yet), parse the file now.
    (when (or force
              (not emoji--labels))
      (setq emoji--derived (make-hash-table :test #'equal))
      (emoji--parse-emoji-test)))
  (when (and (not inhibit-adjust)
             (not emoji--all-bases))
    (setq emoji--all-bases (make-hash-table :test #'equal))
    (emoji--adjust-displayable (cons "Emoji" emoji--labels))))

(defun emoji--adjust-displayable (alist)
  "Remove glyphs we don't have fonts for."
  (let ((emoji--font nil))
    (emoji--adjust-displayable-1 alist)))

(defun emoji--adjust-displayable-1 (alist)
  (if (consp (caddr alist))
      (dolist (child (cdr alist))
        (emoji--adjust-displayable-1 child))
    (while (cdr alist)
      (let ((glyph (cadr alist)))
        ;; Store all the emojis for later retrieval by
        ;; the search feature.
        (when-let ((name (emoji--name glyph)))
          (setf (gethash (downcase name) emoji--all-bases) glyph))
        (if (display-graphic-p)
            ;; Remove glyphs we don't have in graphical displays.
            (if (let ((char (elt glyph 0)))
                  (if emoji--font
                      (char-displayable-p char)
                      ;; (font-has-char-p emoji--font char)
                    (when-let ((font (car (internal-char-font nil char))))
                      (setq emoji--font font))))
                (setq alist (cdr alist))
              ;; Remove the element.
              (setcdr alist (cddr alist)))
          ;; We don't have font info on non-graphical displays.
          (if (let ((char (elt glyph 0)))
                ;; FIXME.  Some grapheme clusters display more or less
                ;; correctly in the terminal, but we don't really know
                ;; which ones.  None of these display totally
                ;; correctly, though, so should they be filtered out?
                (char-displayable-p char))
              (setq alist (cdr alist))
            ;; Remove the element.
            (setcdr alist (cddr alist))))))))

(defun emoji--parse-emoji-test ()
  (setq emoji--labels nil)
  (with-temp-buffer
    (insert-file-contents (expand-file-name "../admin/unidata/emoji-test.txt"
                                            data-directory))
    (unless (re-search-forward "^# +group:" nil t)
      (error "Can't find start of data"))
    (beginning-of-line)
    (setq emoji--names (make-hash-table :test #'equal))
    (let ((derivations (make-hash-table :test #'equal))
          (case-fold-search t)
          (glyphs nil)
          group subgroup)
      (while (not (eobp))
        (cond
         ((looking-at "# +group: \\(.*\\)")
          (setq group (match-string 1)
                subgroup nil))
         ((looking-at "# +subgroup: \\(.*\\)")
          (setq subgroup (match-string 1)))
         ((looking-at
           "\\([[:xdigit:] \t]+\\); *\\([^ \t]+\\)[ \t]+#.*?E[.0-9]+ +\\(.*\\)")
          (let* ((codes (match-string 1))
                 (qualification (match-string 2))
                 (name (match-string 3))
                 (glyph (mapconcat
                         (lambda (code)
                           (string (string-to-number code 16)))
                         (split-string codes) "")))
            (push (list name qualification group subgroup glyph) glyphs))))
        (forward-line 1))
      ;; We sort the data so that the "person foo" variant comes
      ;; first, so that that becomes the key.
      (setq glyphs
            (sort (nreverse glyphs)
                  (lambda (g1 g2)
                    (and (equal (nth 2 g1) (nth 2 g2))
                         (equal (nth 3 g1) (nth 3 g2))
                         (< (emoji--score (car g1))
                            (emoji--score (car g2)))))))
      ;; Get the derivations.
      (cl-loop for (name qualification group subgroup glyph) in glyphs
               for base = (emoji--base-name name derivations)
               do
               ;; Special-case flags.
               (when (equal base "flag")
                 (setq base name))
               ;; Register all glyphs to that we can look up their names
               ;; later.
               (setf (gethash glyph emoji--names) name)
               ;; For the interface, we only care about the fully qualified
               ;; emojis.
               (when (equal qualification "fully-qualified")
                 (when (equal base name)
                   (emoji--add-to-group group subgroup glyph))
                 ;; Create mapping from base glyph name to name of
                 ;; derived glyphs.
                 (setf (gethash base derivations)
                       (nconc (gethash base derivations) (list glyph)))))
      ;; Finally create the mapping from the base glyphs to derived ones.
      (setq emoji--derived (make-hash-table :test #'equal))
      (maphash (lambda (_k v)
                 (setf (gethash (car v) emoji--derived)
                       (cdr v)))
               derivations))))

(defun emoji--score (string)
  (if (string-match-p "person\\|people"
                      (replace-regexp-in-string ":.*" "" string))
      0
    1))

(defun emoji--add-to-group (group subgroup glyph)
  ;; "People & Body" is very large; split it up.
  (cond
   ((equal group "People & Body")
    (if (or (string-match "\\`person" subgroup)
            (equal subgroup "family"))
        (emoji--add-glyph glyph "People"
                          (if (equal subgroup "family")
                              (list subgroup)
                            ;; Avoid "Person person".
                            (cdr (emoji--split-subgroup subgroup))))
      (emoji--add-glyph glyph "Body" (emoji--split-subgroup subgroup))))
   ;; "Smileys & Emotion" also seems sub-optimal.
   ((equal group "Smileys & Emotion")
    (if (equal subgroup "emotion")
        (emoji--add-glyph glyph "Emotion" nil)
      (let ((subs (emoji--split-subgroup subgroup)))
        ;; Remove one level of menus in the face case.
        (when (equal (car subs) "face")
          (pop subs))
        (emoji--add-glyph glyph "Smileys" subs))))
   ;; Don't modify the rest.
   (t
    (emoji--add-glyph glyph group (emoji--split-subgroup subgroup)))))

(defun emoji--generate-file (&optional file)
  "Generate an .el file with emoji mapping data and write it to FILE."
  ;; Running from Makefile.
  (unless file
    (setq file (pop command-line-args-left)))
  (emoji--init t t)
  ;; Weed out the elements that are empty.
  (let ((glyphs nil))
    (maphash (lambda (k v)
               (unless v
                 (push k glyphs)))
             emoji--derived)
    (dolist (glyph glyphs)
      (remhash glyph emoji--derived)))
  (with-temp-buffer
    (generate-lisp-file-heading file 'emoji--generate-file)
    (insert ";; Copyright © 1991-2021 Unicode, Inc.
;; Generated from Unicode data files by emoji.el.
;; The source for this file is found in the admin/unidata/emoji-test.txt
;; file in the Emacs sources.  The Unicode data files are used under the
;; Unicode Terms of Use, as contained in the file copyright.html in that
;; same directory.\n\n")
    (dolist (var '(emoji--labels emoji--derived emoji--names))
      (insert (format "(defconst %s '" var))
      (pp (symbol-value var) (current-buffer))
      (insert (format "\n) ;; End %s\n\n" var)))
    (generate-lisp-file-trailer file)
    (write-region (point-min) (point-max) file)))

(defun emoji--base-name (name derivations)
  (let* ((base (replace-regexp-in-string ":.*" "" name)))
    (catch 'found
      ;; If we have (for instance) "person golfing", and we're adding
      ;; "man golfing", make the latter a derivation of the former.
      (let ((non-binary (replace-regexp-in-string
                         "\\`\\(m[ae]n\\|wom[ae]n\\) " "" base)))
        (dolist (prefix '("person " "people " ""))
          (let ((key (concat prefix non-binary)))
            (when (gethash key derivations)
              (throw 'found key)))))
      ;; We can also have the gender at the end of the string, like
      ;; "merman" and "pregnant woman".
      (let ((non-binary (replace-regexp-in-string
                         "\\(m[ae]n\\|wom[ae]n\\|maid\\)\\'" "" base)))
        (dolist (suffix '(" person" "person" ""))
          (let ((key (concat non-binary suffix)))
            (when (gethash key derivations)
              (throw 'found key)))))
      ;; Just return the base.
      base)))

(defun emoji--split-subgroup (subgroup)
  (let ((prefixes '("face" "hand" "person" "animal" "plant"
                    "food" "place")))
    (cond
     ((string-match (concat "\\`" (regexp-opt prefixes) "-") subgroup)
      ;; Split these subgroups into hierarchies.
      (list (substring subgroup 0 (1- (match-end 0)))
            (substring subgroup (match-end 0))))
     ((equal subgroup "person")
      (list "person" "age"))
     (t
      (list subgroup)))))

(defun emoji--add-glyph (glyph main subs)
  (let (parent elem)
    ;; Useless category.
    (unless (member main '("Component"))
      (unless (setq parent (assoc main emoji--labels))
        (setq emoji--labels (append emoji--labels
                                    (list (setq parent (list main))))))
      (setq elem parent)
      (while subs
        (unless (setq elem (assoc (car subs) parent))
          (nconc parent (list (setq elem (list (car subs))))))
        (pop subs)
        (setq parent elem))
      (nconc elem (list glyph)))))

(provide 'emoji)

;;; emoji.el ends here
