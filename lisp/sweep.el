;;; sweep.el --- Swype like input method

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: input method

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)

(defvar sweep--keyboard (concat data-directory "/images/keyboard.svg"))
(defvar sweep--shift nil)

(defun sweep--select-mode (key svg)
  (let (target desktop)
    (unless (memq key '(?u ?m ?g ?G ?R ?\M-w ?> escape ?\C-x ?A))
      ;; All operations which don't use canvas--undo-marker should
      ;; deselect objects
      (canvas--deselect))

    (setq canvas--mode
          (pcase key
            ;; Object operations
            (?p 'polyline)
            ('escape (when (eq canvas--mode 'move)
                       (canvas--move-cancel))
                     (when (eq canvas--mode 'place)
                       (canvas--undo svg))
                     (when (and (eq canvas--mode 'conn)
                                canvas--temp-points)
                       (svg-polyline svg canvas--temp-points
                                     :stroke-width canvas--stroke-width
                                     :id (number-to-string canvas--id)
                                     :stroke-color (foreground-color-at-point)
                                     :fill "none")
                       (setq canvas--temp-points nil
                             canvas--id (1+ canvas--id)))
                     (canvas--deselect)
                     (svg-possibly-update-image svg)
                     nil)
            ('double-mouse-1
             ;; (message "key1 %s %s" key canvas--nearest-objects)
             (setq target (car canvas--nearest-objects))

             canvas--mode)

            ;; Retain mode even if mouse moves outside the image
            (_ canvas--mode)
            ))
    key))

(defun sweep--act-in-region (svg area)
  (let* ((res (canvas--transform svg area))
         (x (nth 0 res))
         (y (nth 1 res))
         (w (nth 2 res))
         (h (nth 3 res))
         str x1 y1 points point prev cur next dt1 dt2 flag key
         )
  (pcase canvas--mode
    ((guard (eq canvas--mode 'polyline))
     (pp (dom-attr (car (last svg)) 'points))
     (setq points (split-string (dom-attr (car (last svg)) 'points)))
     (while points
       (when (cdr points)
         (setq point (split-string (cadr points) ",")
               x1 (string-to-number (car point))
               y1 (string-to-number (cadr point))
               next (cons x1 y1)))
       (setq point (split-string (car points) ",")
             x1 (string-to-number (car point))
             y1 (string-to-number (cadr point))
             cur (cons x1 y1))

       (when (and prev next)
         (setq dt1 (/ (- (cdr cur) (cdr prev))
                      (- (car cur) (car prev)))
               dt2 (/ (- (cdr next) (cdr cur))
                      (- (car next) (car cur)))
               flag (or (> (abs (- dt2 dt1)) 1)
                        (not (and (>= dt1 0) (>= dt2 0)))))
         (message "%s %s %s %s" dt1 dt2 (> (abs (- dt2 dt1)) 1)
                  (not (and (>= dt1 0) (>= dt2 0))))
         )
       ;; Skip points till slope changes drastically
       (when (or flag (not (and prev next)))
         (canvas--adjacent-obj svg x1 y1)
         (message "%s %s %s" x1 y1 canvas--nearest-objects)
         (setq key (dom-attr (car canvas--nearest-objects) 'id))
         (or (null canvas--nearest-objects)
             (when (cdr points)
               ;; Skip special keys during word formation
               (member key '(nil "shift" "del" "num" "comma" "sp" "dot" "ret")))
             (memq key str)
             (push key str)))
       (setq prev cur
             flag nil
             points (cdr points))
       )

     (pp str)
     (if (member (car str) '(nil "shift" "del" "num" "comma" "sp" "dot" "ret"))
         (sweep--execute key)

       ;; Insert the word and call ispell
       (setq str (reverse str))
       (if sweep--shift
           (setq str (apply 'concat str)
                 str (upcase str)))
       (mapc 'insert str)
       (ispell-word)
       (insert " "))

     (canvas--undo svg)
     ;; Remove selection box
     (dom-remove-node svg (car (dom-by-id svg "_selection")))

     ;; (message "%s" canvas--undo-marker)
     (svg-possibly-update-image svg))

    ;; (_ (setq canvas--id (1+ canvas--id)))
    )))

(defun sweep--execute (cmd)
  (pcase cmd
    ("shift" (setq sweep--shift (not sweep--shift)))
    ("del" (delete-char -1))
    ("num" ;; display numbers
     )
    ("comma" (insert ","))
    ("sp" (insert " "))
    ("dot" (insert "."))
    ("ret" (insert "\n"))
    ))

;;;###autoload
(defun sweep-mode ()
  "Use swipe patterns for text input."
  (interactive)
  (let* ((svg (svg-load sweep--keyboard))
         (buf (get-buffer-create "*Sweep*"))
         (canvas-plugin-fn 'sweep--select-mode)
         (canvas-act-in-region-fn 'sweep--act-in-region)
         ;; (canvas--hide-prompt t)
         (win (selected-window))
         bbox
         )
    (pop-to-buffer buf)
    (select-window win)
    (setq bbox (svg-bbox svg))
    (setf (nth 1 bbox) (- (nth 1 bbox) 100))
    (setf (nth 2 bbox) (- (nth 2 bbox) 100))
    (canvas--zoom svg bbox)

    (with-current-buffer buf
      (erase-buffer)
      (svg-insert-image svg))

    (setq canvas--svg (list svg)
          canvas--mode 'polyline
          canvas--fill-color "none"
          canvas--stroke-color "red"
          canvas--stroke-width 20
          canvas--outline-style 'solid
          sweep--shift nil
          canvas--id (length (dom-children svg)))
    (ewp-crop-image-1 svg)
    ))

(provide 'sweep)
