;;; media.el --- Simple multimedia editor  -*- lexical-binding: t -*-
;;
;; Copyright (C) 2022 Free Software Foundation, Inc.
;;
;; Author: Anand Tamariya (atamariya@gmail.com)
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
;;
;; --------------------------------------------------------------------

(require 'svg)
(require 'xwidget)

(defvar media-viewer-mode-map
  (let ((map (make-sparse-keymap)))
    (suppress-keymap map)
    (define-key map "o" 'media-open-file)
    (define-key map "c" 'media-change-scale)
    (define-key map "e" 'media-export)
    (define-key map "q" 'media-edit-quit)
    (define-key map (kbd "SPC") 'media-toggle-play)
    (define-key map (kbd "RET") 'media-split-track)

    (define-key map [remap forward-char]        'media-forward)
    (define-key map [remap backward-char]       'media-backward)
    (define-key map [remap right-char]        'media-forward)
    (define-key map [remap left-char]       'media-backward)
    map)
  "Keymap for `media-viewer-mode'.")

(defvar media-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "\C-cm" media-viewer-mode-map)
    (define-key map (kbd "\C-c r") 'media-refresh)
    map)
  "Keymap for `media-mode'.")

(define-derived-mode media-mode
  fundamental-mode "media" "Multimedia editor mode."
  (setq-local window-selection-change-functions 'media--refresh-buffer)
  (make-variable-buffer-local 'text-property-default-nonsticky)
  (push '(time . t) text-property-default-nonsticky)
  )

(define-derived-mode media-viewer-mode
    media-mode "media" "Multimedia viewer mode."
    ;; Avoid flickering due to blinking cursor
    (blink-cursor-mode 0)
    (setq-local resize-mini-windows nil)
    )

(defvar media--viewer-win nil)
(defvar media--track-win nil)
(defvar media--viewer-buf nil)
(defvar media--track-buf nil)
(defvar media--ffmpeg-buf nil)

(defvar media--svg nil)
(defvar media--vlc nil)

(defvar media--tracks nil)
(defvar media--current-track 0)
(defvar-local media--file nil)
(defvar media--scale 1)
(defvar media--scale-options '(60 10 5 1))
(defvar media--current-time 0)
(defvar media--time-obj nil)
(defvar media--timer-fn nil)

;;;###autoload
(defun media-edit-quit ()
  (interactive)
  (kill-buffer "media-main")
  (kill-buffer media--ffmpeg-buf)
  (kill-buffer media--track-buf)
  (kill-buffer media--viewer-buf))

;;;###autoload
(defun media-edit ()
  "Start multimedia editing session.
It sets up viewer window on the left and track window on the right."
  (interactive)
  (let* (w h
           (svg (svg-load (concat data-directory "images/media-editor.svg"))))
    ;; Set up buffer and windows
    (setq media--track-buf (get-buffer-create "media-main")
          media--current-track 0
          media--viewer-buf (get-buffer-create "media-viewer"))

    (delete-other-windows)
    (switch-to-buffer media--track-buf)
    (media-mode)
    (setq media--viewer-win (selected-window))
    (setq media--track-win (split-window-right))
    (select-window media--viewer-win t)
    (switch-to-buffer media--viewer-buf)
    (media-viewer-mode)

    ;; Insert widgets
    (erase-buffer)
    (setq w (window-pixel-width)
          h (window-pixel-height))
    (dom-set-attribute svg 'viewBox (format "0 0 400 100"))
    (dom-set-attribute svg 'width w)
    (dom-set-attribute svg 'height (round (* 0.1 h)))
    (svg-insert-image svg)
    (insert "\n")
    ;; (svg-possibly-update-image svg)
    (if (and media--vlc
             (buffer-live-p (xwidget-buffer media--vlc)))
        (progn
          (insert " ")
          (forward-char -1)
          (put-text-property (point) (+ 1 (point))
                             'display (list 'xwidget ':xwidget media--vlc)))
      (setq media--vlc (xwidget-vlc "media" w (round (* 0.8 h))))
      ;; Read two lines of output
      (xwidget-socket-read-output media--vlc)
      (xwidget-socket-read-output media--vlc))
    (setq media--svg svg)
    ;; Allow the buffer to be reused
    ;; (setq buffer-read-only t)
    ))

(defun media-open-file (file)
  (interactive "fFile: ")
  (let* (duration dur-str dur-arr tmp)
    (setq file (expand-file-name file))
    (if (member file media--tracks)
        (setq media--current-track (length (member file media--tracks)))
      (push file media--tracks)
      (setq media--current-track (length media--tracks)))

    (select-window media--track-win)
    (setq media--track-buf (get-buffer-create
                            (format "media-track-%d"
                                    media--current-track)))
    (switch-to-buffer media--track-buf)
    (erase-buffer)
    (media-mode)
    (setq media--file file)
    (xwidget-vlc-open-url media--vlc media--file)

    (media--update-dashboard file media--current-track nil)

    ;; Get duration in seconds using ffmpeg
    (setq media--ffmpeg-buf (get-buffer-create "ffmpeg"))
    (with-current-buffer media--ffmpeg-buf
      (erase-buffer)
      (call-process-shell-command
       (concat "ffmpeg -i " file) nil t)
      (goto-char 1)
      (re-search-forward "Duration: \\([0-9:\\.]*\\),")
      (setq dur-str (match-string 1)
            dur-arr (split-string dur-str ":")
            duration (+ (* (string-to-number (nth 0 dur-arr)) 60 60)
                        (* (string-to-number (nth 1 dur-arr)) 60)
                        (* (string-to-number (nth 2 dur-arr)))))
      (pp duration)

      ;; Find an appropriate scale
      (setq tmp (/ duration 1000)
            media--scale (or (find tmp media--scale-options :test '>=) 1))
      )
    (xwidget-vlc-pause media--vlc)
    (media--propertize 0 duration)
    (goto-char 1)
    ))

(defun media--propertize (start end)
  (let* ((str (format "%s" media--current-track))
         (time start)
         (diff 0))
    (setq time (ceiling start)
          diff (- time start))
    (if (> diff 0)
        (insert (propertize str
                            'time start
                            'fraction diff
                            'category 'fraction
                            'face 'error)))

    (while (< time (floor end))
      (insert (propertize str 'time time))
      (setq time (+ time media--scale)))

    (setq diff (- end time))
    (if (> diff 0)
        (insert (propertize str
                            'time time
                            'fraction diff
                            'category 'fraction
                            'face 'error)))
  ))

(defun media-toggle-play ()
  (interactive)
  (let ()
    (when (and (eq major-mode 'media-mode)
               (not (xwidget-vlc-is-playing media--vlc)))
      ;; Play current track buffer
      (setq media--track-buf (current-buffer)))

    (catch 'off-limit
      (media--refresh-buffer 0))

    (when media--current-time
      (when media--timer-fn
        (cancel-timer media--timer-fn)
        (setq media--timer-fn nil))
      (if (not (xwidget-vlc-is-playing media--vlc))
          (progn
            (xwidget-vlc-play media--vlc)
            (setq media--timer-fn (run-with-timer 0 media--scale #'media--timer)))
        (xwidget-vlc-pause media--vlc))
      )))

(defun media-forward ()
  (interactive)
  (setq media--current-time (+ media--current-time media--scale))
  (xwidget-vlc-pause media--vlc)
  (catch 'off-limit
    (media--refresh-buffer 1)))

(defun media-backward ()
  (interactive)
  (setq media--current-time (- media--current-time media--scale))
  (xwidget-vlc-pause media--vlc)
  (catch 'off-limit
    (media--refresh-buffer -1)))

(defun media-refresh ()
  (interactive)
  (when (and (eq major-mode 'media-mode)
             (not (xwidget-vlc-is-playing media--vlc)))
    ;; Play current track buffer
    (setq media--track-buf (current-buffer)))
  (catch 'off-limit
    (media--refresh-buffer 0)))

(defun media-split-track ()
  (interactive)
  (with-current-buffer media--track-buf
    (let* (time time-next time-prev)
      (save-excursion
        (goto-char (point))
        (setq time (get-text-property (point) 'time))
        (skip-chars-forward "[:space:]")
        (setq time-next (get-text-property (1+ (point)) 'time))
        (if (= time-next (+ time media--scale))
            (insert "\n")
          (setq time-prev (get-text-property (1- (point)) 'time)
                time (+ time-prev (- (/ media--current-time media--scale)
                                     (point))))
          (media--propertize time-prev time)
          (insert "\n")
          (media--propertize time time-next)
          )))))

(defun media--timer ()
  (setq media--current-time (+ media--current-time media--scale))
  (catch 'off-limit
    (media--refresh-buffer)))

(defun media--refresh-buffer (&optional seek)
  "SEEK is -1, 0 or 1.
Throws `off-limit' if seeking is out of bounds."
  ;; TIME on the global timeline. Track might contain reordered clips.
  (let ((time (or seek 0))
        pos track track-time file)
    (unless (xwidget-vlc-is-playing media--vlc)
      (set-window-buffer media--viewer-win media--viewer-buf)
      (set-window-buffer media--track-win media--track-buf)

      ;; Media is not playing. Find out the start time.
      ;; Time starts from 0 while pos starts at 1.
      (when (eq (current-buffer) media--track-buf)
        ;; Time = point - whitespaces
        (save-excursion
          (while (re-search-forward "[[:space:]]" nil t -1)
            (setq time (1- time))))
        (setq time (* (+ time -1 (point)) media--scale)
              media--current-time time
              pos (point)))

      (unless (eq (current-buffer) media--track-buf)
        (setq time media--current-time
              pos (floor (/ media--current-time media--scale)))
          (setq pos (1+ pos))
          (with-current-buffer media--track-buf
            (goto-char pos)
            (while (re-search-forward "[[:space:]]" nil t -1)
              (setq pos (1+ pos))))))

    (with-current-buffer media--track-buf
      (if pos
          (progn
            ;; (xwidget-vlc-play media--vlc)
            ;; (xwidget-vlc-pause media--vlc)
            )
        (setq pos (1+ (point))
              time media--current-time))
      (when (or (<= pos 0) (>= pos (buffer-size)))
        (message "Cannot seek %d" time)
        ;; Restore valid current time
        (if (< pos 0) (setq media--current-time 0))
        (if (> pos (buffer-size))
            (setq media--current-time (* (1- (buffer-size)) media--scale)))

        (if media--timer-fn (cancel-timer media--timer-fn))
        (xwidget-vlc-pause media--vlc)
        (throw 'off-limit media--current-time))

      ;; Move point to next valid position in track buffer
      ;; Seek only works if the media is playing (not stopped).
      ;; (setq pos (+ pos (skip-chars-forward "[:space:]")))
      (while (not (setq track-time
                        (get-text-property pos 'time)))
        (setq pos (1+ pos)))
      ;; Set both buffer and window points
      (goto-char pos)
      (set-window-point media--track-win pos)

      (setq track (string-to-number (string (char-after pos))))
      (unless (eq track media--current-track)
        ;; Find the clip and it's start time
        (setq file (nth (- (length media--tracks) track) media--tracks)
              media--current-track track)
        (xwidget-vlc-open-url media--vlc file)
        (if seek (xwidget-vlc-pause media--vlc))))

    (message "%d" media--current-time)
    (if (or seek (/= media--current-time track-time))
        (xwidget-vlc-seek media--vlc track-time))
    (message "%d %d %d" time track track-time)
    ))

(defun media-export (out)
  (interactive "sOutput file name: ")
  (let ((first t)
        str time time-next file n fraction)
    (if (= media--current-track 0)
        (user-error "No track selected"))
    (with-current-buffer media--track-buf
      (setq file media--file)
      (goto-char 1)
      (while (<= (1+ (point)) (buffer-size))
        (skip-chars-forward "[:space:]")
        (setq time (get-text-property (point) 'time)
              time-next (get-text-property (1+ (point)) 'time))
        (setq fraction (or (get-text-property (point) 'fraction)
                           media--scale))
        (cond ((null time))
              (first
               (setq str (concat str "\nfile " file
                                 "\ninpoint " (number-to-string time)))
               (setq first nil))
              ((eq (char-after (point)) (char-after (1+ (point))))
               ;; Same file fragment
               (if (< time-next (+ time fraction))
                   ;; (forward-char)
                   (setq str (concat str
                                     "\noutpoint "
                                     (number-to-string (+ time fraction))
                                     "file " file
                                     "\ninpoint " (number-to-string time-next))))
               )
              ((eq (char-after (point)) ?.)
               ;; Gap fragment
               )
              ((or (not (char-after (1+ (point))))
                   (not time-next))
               ;; EOF - necessary for fragment end
               (setq str (concat str
                                 (format "\noutpoint %d" (+ time fraction))
                                 )
                     first t))
              ((numberp (char-after (point)))
               ;; Fragment from different file
               (setq n (char-after (point))
                     file (nth (- (length media--tracks) n) media--tracks))
               )
              )
        (forward-char)
        ))

    (with-current-buffer (find-file-noselect "/tmp/ffconcat")
      (erase-buffer)
      (insert "ffconcat version 1.0\n")
      (insert str)
      (save-buffer))

    (message (concat "ffmpeg -safe 0 -f concat -i /tmp/ffconcat " out))
    (with-current-buffer media--ffmpeg-buf
      (erase-buffer)
      (call-process-shell-command
       (concat "ffmpeg -safe 0 -y -f concat -i /tmp/ffconcat " out) nil t))
    ))

(defun media-change-scale (scale)
  (interactive "nScale (in seconds): ")
  (media--update-dashboard nil nil scale)
  (setq media--scale scale))

(defun media--update-dashboard (file no scale)
  (let (id node tmp)
    ;; (setq media--svg (svg-load (concat data-directory "images/media-editor.svg")))
    ;; Update file
    (when file
      (setq id (format "node%d" no)
            node (dom-by-id media--svg id))
      (unless node
        (setq node (copy-tree (car (dom-by-id media--svg "temp"))))
        (dom-set-attribute node 'id id)
        (dom-set-attribute node 'transform (format "translate(%d)" (+ (* no 100))))
        (dom-append-child media--svg node))
      ;; Set filename and file number
      (setq tmp (car (dom-by-id node "file_name")))
      (setf (nth 2 tmp) (file-name-nondirectory file))
      (setq tmp (car (dom-by-id node "file_no")))
      (setf (nth 2 tmp) (number-to-string no)))

    ;; Update scale
    (when scale
      (setq id "scale"
            node (car (dom-by-id media--svg id)))
      (setf (nth 2 node) (number-to-string scale)))

    (svg-possibly-update-image media--svg)
    ;; (svg-insert-image media--svg)
    ))

(provide 'media)
;;; media.el ends here
