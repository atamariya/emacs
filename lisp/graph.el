;;; graph.el --- SVG graph creation functions -*- lexical-binding: t -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com> 2021
;; Keywords: image graph
;; Version: 1.0
;; Package-Requires: ((emacs "25"))

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)

(defun graph ()
  (interactive)
  (let* ((file "~/in.data")
         (cols 4)
         (svg (svg-create 400 400))
         (points nil)
         (xpos 0)
         (ypos 1)
         (i 0)
         x y lpoints
         (b (find-file-noselect (expand-file-name file))))
    (with-current-buffer b
      (setq points (split-string (buffer-substring-no-properties
                                  (point-min) (point-max)))))
    (dolist (num points)
      (if (= i xpos)
          (setq x num))
      (if (= i ypos)
          (setq y num))
      (if (< i (1- cols))
                (setq i (1+ i))
        (push (cons x y) lpoints)
        (setq i 0)))
    (svg-polyline svg lpoints :stroke "red")
    ;; (pp svg)
    (svg-insert-image svg)
    ))

(defun graph-gnuplot ()
  (interactive)
  (let* ((file "~/d.svg")
         (svg (svg-load file)))
    (mapc (lambda (a)
            (push (dom-attr a 'id) canvas--layers))
          (dom-by-id svg "gnuplot_plot"))
    ))

(provide 'graph)
