;; Copyright (C) 1999-2020 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

(require 'semantic/bovine/c)
(require 'semantic/wisent/c-wy)

;;;###autoload
(defun semantic-default-c-setup ()
  "Set up a buffer for semantic parsing of the C language."
  ;; (semantic-c-by--install-parser)
  (wisent-c-wy--install-parser)
  (setq semantic-lex-syntax-modifications '(
                                            ;; (?> ")<")
                                            ;; (?< "(>")
                                            ;; (?\\ ".")
                                            )
        semantic-lex-analyzer 'wisent-c-lexer
        )

  ;; (setq semantic-lex-analyzer #'semantic-c-lexer)
  (add-hook 'semantic-lex-reset-functions 'semantic-lex-spp-reset-hook nil t)
  (when (eq major-mode 'c++-mode)
    (add-to-list 'semantic-lex-c-preprocessor-symbol-map '("__cplusplus" . "")))
  )

(provide 'semantic/c)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "semantic/c"
;; End:

;;; semantic/c.el ends here
