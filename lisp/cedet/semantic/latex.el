;;; semantic/latex.el --- Semantic details for latex files

;; Copyright (C) 2004-2005, 2007-2019 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Basic semantic infrastructre for LATEX code completion.
;;

;;; Code:

(require 'semantic)
(require 'semantic/format)
(require 'semantic/analyze/complete)
(require 'semantic/wisent/latex-wy)

(defvar semantic-command-separation-character)
;; Used for code completion
(defvar semanticdb-implicit-tags
  '(("documentclass" command (:arguments ("class")
                              :options ("option")))
    ("usepackage" command (:arguments ("package")
                           :options ("option")))
    ("begin" command (:arguments ("document")))
    ("end" command (:arguments ("document")))
    ("document" environment)
    ))

(define-mode-local-override semantic-parse-region
  latex-mode (start end &optional nonterminal depth returnonerror)
  "Combines tags from buffer with `latex-semantic-tags'."
  (let* ((res (semantic-parse-region-default start end nonterminal depth returnonerror))
	(next res)
        (depth 0)
        class options tree sibling d)
    (unless (or nonterminal (not res))
      ;; Create a hierarchy for sections
      ;; options are returned as member of class_declaration.
      (setq res nil)
      (while next
        (cond ((semantic-tag-of-class-p (car next) 'option)
               (push (car next) options))
              ((semantic-tag-of-class-p (car next) 'type)
               (setq class (car next)))
              ((semantic-tag-of-class-p (car next) 'section)
               (setq d (semantic-tag-get-attribute (car next) :level))
               (cond ((> d depth)
                      (push sibling tree)
                      (setq  depth d sibling nil)
                      (push (car next) sibling)
                      )
                     ((= d depth)
                      (push (car next) sibling)
                      )
                     (t
                      (while (< d depth)
                        (setq depth (1- depth))
                        (semantic-tag-put-attribute (car (car tree)) :members sibling)
                        (setq sibling (pop tree)))
                      (push (car next) sibling)
                      )))
              (t (setq res (cons (car next) res))))
        (setq next (cdr next)))

      (while tree
        (semantic-tag-put-attribute (car (car tree)) :members sibling)
        (setq sibling (pop tree)))
      (setq res (append res sibling))

      (setq res (nreverse res))
      (when class
        (semantic-tag-put-attribute class :options (nreverse options))
        (push class res)))
    res))

;; (define-mode-local-override semantic-parse-changes
;;   latex-mode (&rest ignore)
;;   "Reparse complete buffer."
;;   (semantic-parse-tree-set-needs-rebuild))

(define-mode-local-override semantic-format-tag-prototype
  latex-mode (tag &optional parent color)
  "Return a prototype for TOKEN.
Optional argument PARENT is a parent (containing) item.
Optional argument COLOR indicates that color should be mixed in."
  (let ((name (semantic-tag-name tag))
        (option (semantic-tag-get-attribute tag :options))
        (arg (semantic-tag-get-attribute tag :arguments)))
    (concat name
            (when option "[options]")
            (when arg "{arguments}"))
    ))

(define-mode-local-override semantic-tag-components
  latex-mode (tag)
  "Return option tags."
  (if (semantic-tag-of-class-p tag 'type)
      (semantic-tag-get-attribute tag :options)
    (semantic-tag-get-attribute tag :members)))

(define-mode-local-override semantic-tag-include-filename latex-mode (tag)
  "Return a suitable filename with extension."
  (let ((name (semantic-tag-name tag)))
    (concat name (semantic-tag-get-attribute tag :ext))))

(define-mode-local-override semantic-ctxt-current-class-list
  latex-mode (&optional point)
  "Return a list of tag classes that are allowed at POINT.
Assume a functional typed language.  Uses very simple rules."
  (save-excursion
    (skip-syntax-backward "\\w ")
    (forward-char -1)
    (cond ((looking-at-p "\\\\")
           '(command))
          ((looking-at-p "[[]")
           '(option))
          ((looking-at-p "[{]")
           (if (semantic-tag-of-class-p (semantic-current-tag) 'code)
               (cond ((semantic-tag-of-type-p (semantic-current-tag) "ref")
                      '(label))
                     ((semantic-tag-of-type-p (semantic-current-tag) "cite")
                      '(bibtex-entry))
                     (t
                      '(environment)))
           '(class package)))
      )))

(define-mode-local-override semantic-analyze-current-context
  latex-mode (point)
  "Provide a semantic analysis object describing context."
  (let* ((context-return nil)
	 (prefixandbounds (semantic-ctxt-current-symbol-and-bounds))
	 (prefix (car prefixandbounds))
	 (bounds (nth 2 prefixandbounds))
	 (prefixtypes nil)
	 (prefixclass (semantic-ctxt-current-class-list))
	 (scope (semantic-scope-cache))
         (ctag (semantic-current-tag))
         tables tablelist alias tag name
	 )
    ;; Find class in scope
    (when (and (semantic-tag-of-class-p ctag 'include)
               (memq 'option prefixclass))
      (setq name (semantic-tag-name ctag)
            tag (or (semanticdb-typecache-find name)
                    ;(car (semantic-find-tags-by-name name (current-buffer)))
                    )))
    (when tag
      (setq tablelist (list tag))
      )

    (oset scope scopetypes tablelist)
    ;; (oset scope typescope tablelist)
    (oset scope fullscope (append tablelist
                                  semanticdb-implicit-tags
                                  (semantic-analyze-scoped-tags tablelist scope)))
    ;; (oset scope localvar alias)
          ;; '(("a" alias (:type "table1"))))
    ;; (data-debug-show scope)

    ;; Search all tables
    ;; (when prefix
    ;;   (catch 'throwsym
    ;;     (semantic-analyze-find-tag-sequence prefix scope 'prefixtypes 'throwsym)))

    (setq context-return
	  (semantic-analyze-context
	   :buffer (current-buffer)
	   :scope scope
	   :bounds bounds
	   :prefix prefix
	   :prefixtypes tablelist
	   :prefixclass prefixclass
           :errors semantic-analyze-error-stack
	   ))

    context-return))

;;;###autoload
(defun wisent-default-latex-setup ()
  "Set up a buffer for parsing of LATEX files."
  (wisent-latex-wy--install-parser)
  (setq semantic-symbol->name-assoc-list (append semantic-symbol->name-assoc-list
                                                 '((section . "Contents"))))
  (setq-mode-local latex-mode
        semantic-lex-analyzer 'wisent-latex-lexer
	semantic-command-separation-character nil
	semantic-type-relation-separator-character nil
        semantic-typesafe-lang nil
        semantic-lex-comment-regex "%"

        company-minimum-prefix-length 1
	))

(provide 'semantic/latex)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "semantic/latex"
;; End:

;;; semantic/latex.el ends here
