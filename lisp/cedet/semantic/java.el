;;; semantic/java.el --- Semantic functions for Java

;;; Copyright (C) 1999-2020 Free Software Foundation, Inc.

;; Author: David Ponce <david@dponce.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Common function for Java parsers.

;;; Code:
(require 'semantic)
(require 'semantic/ctxt)
(require 'semantic/doc)
(require 'semantic/db-typecache)
(require 'semantic/format)
(require 'ede/locate)
(require 'ede/config)
;; (require 'srecode/semantic)
(require 'semantic/symref/grep)
(require 'subr-x)

(eval-when-compile
  (require 'semantic/find)
  (require 'semantic/dep))


;;; Lexical analysis
;;
(defconst semantic-java-number-regexp
  (eval-when-compile
    (concat "\\("
            "\\<[0-9]+[.][0-9]+\\([eE][-+]?[0-9]+\\)?[fFdD]?\\>"
            "\\|"
            "\\<[0-9]+[.][eE][-+]?[0-9]+[fFdD]?\\>"
            "\\|"
            "\\<[0-9]+[.][fFdD]\\>"
            "\\|"
            "\\<[0-9]+[.]"
            "\\|"
            "[.][0-9]+\\([eE][-+]?[0-9]+\\)?[fFdD]?\\>"
            "\\|"
            "\\<[0-9]+[eE][-+]?[0-9]+[fFdD]?\\>"
            "\\|"
            "\\<0[xX][[:xdigit:]]+[lL]?\\>"
            "\\|"
            "\\<[0-9]+[lLfFdD]?\\>"
            "\\)"
            ))
  "Lexer regexp to match Java number terminals.
Following is the specification of Java number literals.

DECIMAL_LITERAL:
    [1-9][0-9]*
  ;
HEX_LITERAL:
    0[xX][[:xdigit:]]+
  ;
OCTAL_LITERAL:
    0[0-7]*
  ;
INTEGER_LITERAL:
    <DECIMAL_LITERAL>[lL]?
  | <HEX_LITERAL>[lL]?
  | <OCTAL_LITERAL>[lL]?
  ;
EXPONENT:
    [eE][+-]?[09]+
  ;
FLOATING_POINT_LITERAL:
    [0-9]+[.][0-9]*<EXPONENT>?[fFdD]?
  | [.][0-9]+<EXPONENT>?[fFdD]?
  | [0-9]+<EXPONENT>[fFdD]?
  | [0-9]+<EXPONENT>?[fFdD]
  ;")

;;; Parsing
;;
(defsubst semantic-java-dim (id)
  "Split ID string into a pair (NAME . DIM).
NAME is ID without trailing brackets: \"[]\".
DIM is the dimension of NAME deduced from the number of trailing
brackets, or 0 if there is no trailing brackets."
  (let ((dim (when id (string-match "\\(\\[]\\)+\\'" id))))
    (if dim
        (cons (substring id 0 dim)
              (/ (length (match-string 0 id)) 2))
      (cons id 0))))

(defsubst semantic-java-type (tag)
  "Return the type of TAG, taking care of array notation."
  (let ((type (semantic-tag-type tag))
        (dim  (semantic-tag-get-attribute tag :dereference)))
    (when dim
      (while (> dim 0)
        (setq type (concat type "[]")
              dim (1- dim))))
    type))

(defun semantic-java-expand-tag (tag)
  "Expand compound declarations found in TAG into separate tags.
TAG contains compound declarations when its class is `variable', and
its name is a list of elements (NAME START . END), where NAME is a
compound variable name, and START/END are the bounds of the
corresponding compound declaration."
  (let* ((class (semantic-tag-class tag))
         (elts (semantic-tag-name tag))
         dim type dim0 elt clone start end xpand)
    (cond
     ((and (eq class 'function)
           (> (cdr (setq dim (semantic-java-dim elts))) 0))
      (setq clone (semantic-tag-clone tag (car dim))
            xpand (cons clone xpand))
      (semantic-tag-put-attribute clone :dereference (cdr dim)))

     ((eq class 'variable)
      (or (consp elts) (setq elts (list (list elts))))
      (setq dim  (semantic-java-dim (semantic-tag-get-attribute tag :type))
            type (car dim)
            dim0 (cdr dim))
      (while elts
        ;; For each compound element, clone the initial tag with the
        ;; name and bounds of the compound variable declaration.
        (setq elt   (car elts)
              elts  (cdr elts)
              start (if elts  (cadr elt) (semantic-tag-start tag))
              end   (if xpand (cddr elt) (semantic-tag-end   tag))
              dim   (semantic-java-dim (car elt))
              clone (semantic-tag-clone tag (car dim))
              xpand (cons clone xpand))
        (semantic-tag-put-attribute clone :type type)
        (semantic-tag-put-attribute clone :dereference (+ dim0 (cdr dim)))
        (semantic-tag-set-bounds clone start end)))

     ((and (eq class 'type) (string-match "\\." (semantic-tag-name tag)))
      ;; javap outputs files where the package name is stuck onto the class or interface
      ;; name.  To make this more regular, we extract the package name into a package statement,
      ;; then make the class name regular.
      (let* ((name (semantic-tag-name tag))
	     (rsplit (nreverse (split-string name "\\." t)))
	     (newclassname (car rsplit))
	     (newpkg (mapconcat 'identity (reverse (cdr rsplit)) ".")))
	(semantic-tag-set-name tag newclassname)
	(setq xpand
	      (list tag
		    (semantic-tag-new-package newpkg nil))))
      ))
    xpand))

;;; Environment
;;
(defcustom-mode-local-semantic-dependency-system-include-path
  java-mode semantic-java-dependency-system-include-path
  ;; @todo - Use JDEE to get at the include path, or something else?
  nil
  "The system include path used by Java language.")

(defvar-local semantic-java-classpath nil
  "Project classpath.")

(defvar-local semantic-java--new-classes nil
  "List of classes found during parsing. This list is used to generate
include tags after parsing. FOR INTERNAL USE ONLY.")

(defvar-local semantic-java--pending-include nil
  "List of required include tags which are not present in code. FOR INTERNAL USE ONLY.")

(defvar-local semantic-java--imported-classes nil
  "Alist of (NAME . TAG) of classes that are already
  disambiguated and imported. FOR INTERNAL USE ONLY.")

(defvar-local semantic-javap-temp-folder nil
  "Temporary folder containing javap output")

;; Local context
;;
(define-mode-local-override semantic-ctxt-scoped-types
  java-mode (&optional point)
  "Return a list of type names currently in scope at POINT."
  (mapcar 'semantic-tag-name
          (append (semantic-find-tags-by-class
                   'type (current-buffer))
                  (semantic-find-tags-by-class
                   'include (current-buffer)))))

;; Tag Protection
;;
(define-mode-local-override semantic-tag-protection
  java-mode (tag &optional parent)
  "Return the protection of TAG in PARENT.
Override function for `semantic-tag-protection'."
  (let ((prot (semantic-tag-protection-default tag parent)))
    (or prot 'package)))

;; Prototype handler
;;
(defun semantic-java-prototype-function (tag &optional parent color)
  "Return a function (method) prototype for TAG.
Optional argument PARENT is a parent (containing) item.
Optional argument COLOR indicates that color should be mixed in.
See also `semantic-format-tag-prototype'."
  (let ((name (semantic-tag-name tag))
        (type (semantic-java-type tag))
        (tmpl (semantic-tag-get-attribute tag :template-specifier))
        (args (semantic-tag-function-arguments tag))
        (argp "")
        arg argt)
    (while args
      (setq arg  (car args)
            args (cdr args))
      (if (semantic-tag-p arg)
          (setq argt (if color
                         (semantic--format-colorize-text
                          (semantic-java-type arg) 'type)
                       (semantic-java-type arg))
                argp (concat argp argt (if args ", " "")))))
    (when color
      (when type
        (setq type (semantic--format-colorize-text type 'type)))
      (setq name (semantic--format-colorize-text name 'function)))
    (concat (or tmpl "") (if tmpl " " "")
            (or type "") (if type " " "")
            name "(" argp ")")))

(defun semantic-java-prototype-variable (tag &optional parent color)
  "Return a variable (field) prototype for TAG.
Optional argument PARENT is a parent (containing) item.
Optional argument COLOR indicates that color should be mixed in.
See also `semantic-format-tag-prototype'."
  (let ((name (semantic-tag-name tag))
        (type (semantic-java-type tag)))
    (concat (if color
                (semantic--format-colorize-text type 'type)
              type)
            " "
            (if color
                (semantic--format-colorize-text name 'variable)
              name))))

(defun semantic-java-prototype-type (tag &optional parent color)
  "Return a type (class/interface) prototype for TAG.
Optional argument PARENT is a parent (containing) item.
Optional argument COLOR indicates that color should be mixed in.
See also `semantic-format-tag-prototype'."
  (let ((name (semantic-tag-name tag))
        (type (semantic-tag-type tag))
        (tmpl (semantic-tag-get-attribute tag :template-specifier)))
    (concat type " "
            (if color
                (semantic--format-colorize-text name 'type)
              name)
            (or tmpl ""))))

(define-mode-local-override semantic-format-tag-prototype
  java-mode (tag &optional parent color)
  "Return a prototype for TOKEN.
Optional argument PARENT is a parent (containing) item.
Optional argument COLOR indicates that color should be mixed in."
  (let ((f (intern-soft (format "semantic-java-prototype-%s"
                                (semantic-tag-class tag)))))
    (funcall (if (fboundp f)
                 f
               'semantic-format-tag-prototype-default)
             tag parent color)))

(semantic-alias-obsolete 'semantic-java-prototype-nonterminal
                         'semantic-format-tag-prototype-java-mode "23.2")

;; Include Tag Name
;;

;; Thanks Bruce Stephens
(define-mode-local-override semantic-tag-include-filename java-mode (tag)
  "Return a suitable path for (some) Java imports."
  (let ((name (semantic-tag-name tag)))
    (concat (mapconcat 'identity (split-string name "\\.") "/")
            "\\.(class|java)")))

(defun semantic-java-post-inc-parser-hook (tags)
  (semantic-java-post-parser-hook tags t))

(defun semantic-java-post-parser-hook (tags &optional partial)
  "Add include tags based on class usages found during parsing."
  ;; Update res and semantic--buffer-cache after removing duplicates
  (when (and tags
             ;; This ensures system files don't ask for import resolution eg. Map
             ;;(buffer-file-name)
             (derived-mode-p 'java-mode)
             (ede-current-project))
    (let (tag
          wildcard
          name
          flag
          types
          tmp
          (type-face (cdr (assoc 'type semantic-format-face-alist)))
          (type (semantic-tag-name
                 (car (semantic-find-tags-by-class 'type tags)))))
      (unless partial
        (setq semantic--buffer-cache tags
              semantic-java--pending-include nil)
      ;; Package tag
      (setq tag (car
                 (semantic-find-tags-by-class 'package tags)))
      (unless tag
        (setq tag
              (car (semantic-java--add-include (buffer-name) nil (buffer-file-name))))
        (push tag semantic--buffer-cache))
      (when tag
        ;; During incremental parsing, package tag might be missing
        (push (concat (semantic-tag-name tag) ".*") wildcard)))

      ;; Add commonly used tags. This is not needed if static usage
      ;; can be parsed and classes added to semantic-java--new-classes.
      ;; Use semanticdb-typecache-complete-flush to see the changes
      ;; reflect when semanticdb-implied-include-tags is changed.
      (dolist (tag ());'("java.lang.System" "java.util.Arrays" "java.util.Collections"))
        (unless (assoc tag semanticdb-implied-include-tags)
          (setq semanticdb-implied-include-tags
                (append semanticdb-implied-include-tags
                        (semantic-java--add-include tag)))))

      (when semantic-java--new-classes
        (setq semantic-java--new-classes
              (remove type (delete-dups semantic-java--new-classes)))

        ;; Generate list of wildcard imports. Classes in these packages would be auto-imported.
        (dolist (import (semantic-find-tags-by-class 'include tags))
          (setq name (semantic-tag-name import))
          (setq semantic-java--new-classes
                (remove (file-name-extension name) semantic-java--new-classes))
          (when (string= (file-name-extension name) "*")
            (push name wildcard)))

        (dolist (tag semantic-java--new-classes)
          (setq name (if (listp tag) (car tag) tag)
                tmp (split-string name "\\.")
                tags (cdr (assoc name semantic-java--imported-classes)))
          (if (= (length tmp) 2)
              ;; Include Map in case of Map.Entry
              ;; Search java.lang.String as is
              (setq tmp (car tmp))
              (setq tmp name))
          ;; Disambiguate and populate tag in semantic-java--imported-classes
          (unless tags
            (setq tags (semantic-java--add-include tmp wildcard))
            (when tags
              (setq flag t)
              (push (cons name tags) semantic-java--imported-classes)))

          ;; Is the disambiguated tag present in semantic-java--pending-include?
          (unless (assoc (semantic-tag-name (car tags))
                         semantic-java--pending-include)
            (setq flag t
                  semantic-java--pending-include
                  (append tags
                          semantic-java--pending-include)))
          )

        ;; Need to reset includestream so that tags from newly
        ;; included classes are available
        (when (and flag semanticdb-current-table)
          (oset (semanticdb-get-typecache semanticdb-current-table)
                includestream
                nil)))

      (setq semantic-java--new-classes nil)
      ))
  ;; (semantic-fl-update-keywords)

  (when (and semantic--buffer-cache
             (not partial))
    (setq semantic--buffer-cache
          (append semantic--buffer-cache semantic-java--pending-include))))

(defvar semantic-font-lock-types nil
  "Variable used to track types before edit so that same may be
  removed from `font-lock-keywords'")
(make-variable-buffer-local 'semantic-font-lock-types)

(define-overloadable-function semantic-fl-update-keywords (&optional rest)
  "Update `font-lock-keywords' post parsing.")

(defun semantic-fl-update-keywords-default (&optional rest)
  "Do nothing.")

(define-mode-local-override semantic-fl-update-keywords java-mode (&optional rest)
  "Add all included classes as font-lock keywords."
  ;;  Adding it individually so that keywords are replaced maintaining
  ;; the length somewhat constant. Also the reason why removal MUST
  ;; happen before we add new keywords.
  (let* ((types nil)
         (type-face (cdr (assoc 'type semantic-format-face-alist))))
    (mapc #'(lambda (a)
              (push (cons a type-face)
                    types))
          semantic-font-lock-types)
    (font-lock-remove-keywords nil types)

    (setq semantic-font-lock-types nil)
    (mapc #'(lambda (a)
              ;; Don't highlight type if it's not imported or is incorrect
              (when (or (semantic--tag-get-property a 'included)
                        (semantic-tag-include-system-p a)
                        (and (semantic-dependency-tag-file a)
                             (semantic-tag-with-position-p a)))
                (setq semantic-font-lock-types
                      (append (last
                               (split-string
                                (semantic-tag-name a) "\\."))
                              semantic-font-lock-types))
                ))
          (semantic-find-tags-by-class 'include (semantic-fetch-available-tags)))

    (setq types nil)
    (mapc #'(lambda (a)
              (push (cons a type-face)
                    types))
          semantic-font-lock-types)
    (font-lock-add-keywords nil types)
    ))

;; (advice-add 'semanticdb-synchronize-table :filter-args #'semantic-java-post-parser-hook)

(define-mode-local-override semantic-parse-region java-mode
  (start end &optional nonterminal depth returnonerror)
  "Calls `semantic-parse-region-default'. Adds package level classes as include.
START, END, NONTERMINAL, DEPTH, and RETURNONERRORS are the same
as for the parent."

  ;; Do the parse
  (let* ((res (semantic-parse-region-default start end nonterminal
				             depth returnonerror))
         (tags (semantic-find-tags-by-class 'code res)))
    (when tags
      (setq semantic-java--new-classes
            (append (apply #'append
                           (mapcar (lambda (tag)
                                     (semantic-tag-get-attribute tag :detail))
                                   tags))
                    semantic-java--new-classes)
            res (semantic-filter-tags-by-class '(code) res)))

    ;; (cond ((and res (null nonterminal))
    ;;        ;; Top level parsing
    ;;        (setq res
    ;;         (semantic-java-post-parser-hook (list res))
    ;;         ))
    ;;       ((null depth)
    ;;        ;; During incremental parsing, only imports should be updated
    ;;        (semantic-java-post-parser-hook (list res) t)
    ;;        (setq semantic--buffer-cache (append semantic-java--pending-include
    ;;                                             semantic--buffer-cache))
    ;;         ))
    res))

(defvar semantic-java-exclude-pattern "\\(classes\\|target\\|bin\\)"
  "Pattern to exclude while searching for files in Java project")

(defun semantic-java--strip-project-path(fname dirs)
  "Utility function to strip project paths in DIRS from
FNAME. Sequence of path in DIRS is important."
  (setq dirs (sort (copy-sequence dirs) #'(lambda (a b) (> (length a) (length b)))))
  (while dirs
    (setq fname (string-trim fname (file-name-as-directory (car dirs))))
    (setq dirs (cdr dirs)))
  fname)

;;;###autoload
(defun semantic-java--add-include (type &optional wildcard package fuzzy)
  "Utility function to create an include tag for TYPE.
Classes matching WILDCARD are included.
If PACKAGE is t, package tag is inserted instead of include tag.
If FUZZY is t, multiple tags might be returned."
  ;; Find file directory and convert to package
  (let* ((seq (split-string type "\\."))
         (fname type);(mapconcat 'identity seq "/"))
         (proj (ede-current-project))
         (root (ede-project-root-directory proj))
         (sys (> (length seq) 2)) ;; System include - not displayed in source
         found choices ext tmp filename tag files
         process ;; We only process java and class files
         (match nil)
         (dirs nil))

    (if package
        (setq files (list package))

      ;; No need to import type in itself
      (unless (string= type
                       (semantic-tag-name
                        (car (semantic-find-tags-by-class 'type semantic--buffer-cache))))
        ;; We are searching Map, Map.Entry or java/util/Map
        (unless fuzzy
        (setq fname (concat (if sys
                                (concat "^" fname)
                              (concat "/" fname)) "\\.(class|java)")))
        (setq files (ede-expand-filename proj fname))
        (when (stringp files)
          (setq files (list files)))))

    ;; Create choice list
    (while files
      (setq filename (car files))
      (setq fname (file-name-sans-extension (car files)))
      (setq ext (file-name-extension (car files)))
      (setq process nil)

      (cond
       ((and (derived-mode-p 'java-mode)
             wildcard
             (not (string-match-p semantic-java-exclude-pattern fname)))
        ;; Import wildcards
        ;; Auto-include java.lang.*
        ;; Check for Double, System and Collections
        (setq tmp (cons "java.lang" wildcard))
        (while (and files tmp)
          (when (string-match-p (car tmp) fname)
            (if (string= (file-name-extension (car tmp)) "*")
                (setq match t files nil)
              (setq sys t files nil))
            (unless fuzzy
                (setq  choices nil))
            (setq process t
                  fname (if (string= "java" ext)
                            (file-name-base filename)
                          (file-name-nondirectory filename)))
            (push `(,fname ,filename) choices))
          (setq tmp (cdr tmp)))

        ;; (unless process
        ;;   (push `(,(file-name-nondirectory filename) ,filename) choices))
        )

        ((string= "java" ext)
        ;; Remove directory prefixes
        (setq dirs (cons root (ede-include-path proj)))
        (setq fname (semantic-java--strip-project-path fname dirs))

        ;; If one of the options is already included, we should use that.
        (setq tmp (mapconcat 'identity  (split-string fname "/") "."))
        (when package
          (setq tmp (file-name-sans-extension tmp)))

        (if (assoc tmp semantic--buffer-cache)
            (setq files nil found t)
          (add-to-list 'choices `(,tmp ,filename))))

       ((string= "h" ext)
        ;; Remove directory prefixes
        (setq dirs (cons root (ede-include-path proj)))
        (setq tmp (semantic-java--strip-project-path filename dirs))

        ;; If one of the options is already included, we should use that.
        (when (string-match-p (concat "^" type) tmp)
          ;; stdio.h should not match bsd/stdio.h
          (if (and (not fuzzy)
                   (assoc tmp semantic--buffer-cache))
              (setq files nil found t)
            (add-to-list 'choices `(,tmp ,filename)))))
       )

      (setq files (cdr files)))

    (unless (or found (not choices))
      ;;(setq choices (nreverse choices))
      (if (and (null fuzzy)
               (> (length choices) 1))
          ;; Else, let user select one
          (setq tag (completing-read
                               (format "Import %s (in %s): " type (buffer-name))
                               (mapcar #'car choices))
                choices (list (assoc tag choices))))
      (mapcar (lambda (tuple)
                (setq type (car tuple))
                (cond ((null package)
                       ;; Add include tag
                       (setq tag (semantic-tag-new-include type sys))
                       (when match
                         ;; Mark included via wildcard
                         (semantic--tag-put-property tag 'included t))
                       (semantic--tag-put-property tag 'dependency-file
                                                   (format "%s" (cadr (assoc type choices))))
                       ;; (semantic-dependency-tag-file tag)
                       tag)

                      (t
                       ;; Add package tag
                       (setq tag (semantic-tag-new-package type nil))))

                tag)
              choices)
      )))

(define-mode-local-override semanticdb-find-load-unloaded
  java-mode (filename)
  "Use javap to load table for class files. For java files, use the default semanticdb loader."
  (let ((ext (file-name-extension filename))
        (proj (ede-current-project))
        config
        table)
    (when (and proj (string= "class" ext))
      ;; Class files can't be loaded without project info.
    (unless semantic-java-classpath
      (setq config (ede-config-get-configuration proj))
      (setq semantic-java-classpath (mapconcat 'identity (oref config classpath) ":")))

    (setq semantic-javap-temp-folder
          (concat (ede-project-root-directory proj)
                  ".emacs/javap"))
    (unless (file-exists-p semantic-javap-temp-folder)
      (mkdir semantic-javap-temp-folder t))

    (unless (string= "java" ext)
      (let* ((db (semanticdb-get-database semantic-javap-temp-folder))
             (class (mapconcat 'identity
                               (split-string (file-name-nondirectory filename) "/")
                               "."))
             (tbl (semanticdb-file-table db class))
             tags)
        (unless tbl
          (setq tags (semanticdb-javap-extract-tag-table class))
          (setq tbl (semanticdb-create-table db class))
          (semanticdb-synchronize tbl tags)
          (semanticdb-save-db db))

        (setq table tbl))))

  (when (string= "java" ext)
    (setq table (semanticdb-file-table-object filename t)))

  table))

;;; Javap command Support
;;
(defun javap-get-class (class)
  "In JAR, get a javap dump of CLASS, return the buffer."
  (let* ((dir semantic-javap-temp-folder)
         (classpath semantic-java-classpath)
         (file (expand-file-name class dir))
         (b (find-file-noselect file))
         (size (buffer-size b))
         flags
	 )
    (when (zerop size)
    ;; (with-current-buffer b
    ;;   (setq default-directory dir)
    ;;   (erase-buffer))
    (setq flags (list "-classpath"
		      classpath
                      "-protected"
		      class))
    (apply 'call-process "javap"
	   nil b nil
	   flags))
    b))

;; Given a .jar file, and a class file name, retrieve a detailed tag list
;; for the public and protected API.  (Assuming the private API is never accessible anyway.
(defun semanticdb-javap-extract-tag-table (qualifiedclassfile)
  "Within classpath, get QUALIFIEDCLASSFILE's tag table.
QUALIFIEDCLASSFILE is a filename with package qualifiers
to some class in classpath."
  (let ((javapbuff (javap-get-class qualifiedclassfile)))
    (with-current-buffer javapbuff
      ;; (set-buffer javapbuff)
      (when (buffer-modified-p javapbuff)
      (goto-char (point-min))
      (unless (search-forward "Error:" nil t)
        ;; The first line can say "Compiled from ..." or some-such.
        (let* ((case-fold-search nil)
	       (p (search-forward "Compiled from" nil t)))
	  (when (and p (numberp p))
	    (goto-char p)
	    (beginning-of-line)
	    (insert "// ")))

        ;; strip out fully qualified part of class- and interface names - not required
        (save-excursion
	;;   (goto-char (point-min))
	;;   (while (re-search-forward "\\(class\\|interface\\) \\([^\\. ]*\\.\\)+" nil t)
	;;     (replace-match "\\1 " nil nil))
	  (while (re-search-forward "\\(\\w\\)\\$\\(\\w\\)" nil t)
	    (replace-match "\\1.\\2" nil nil))
	  )
        ;; (write-region nil nil qualifiedclassfile)
        (save-buffer)
        ))

        ;; Enable java mode and semantic parsing.
        (java-mode)
        ;; (ede-minor-mode)
        ;; (semantic-new-buffer-fcn)
        ;;Get the tags, and strip out buffer information.
        (let ((tagsout (semantic-fetch-tags)))
	  ;; Changes tags in place.
	  (semantic--tag-unlink-list-from-buffer tagsout)
	  ;; Return the tag table
	  tagsout))))

;;;###autoload
(defun semantic-java-get-classpath ()
  "Update classpath configuration and locate DB."
  ;; Read classpath from saved configuration
  (let* ((proj (ede-current-project))
	 (config (ede-config-get-configuration proj))
         (classpath (mapconcat 'identity (oref config classpath) ":")))
    (setq semantic-java-classpath classpath)))

(defun semantic-java-get-current-class ()
  "Return FQCN for current buffer."
  (let* ((tags (semantic-find-tags-by-class 'package (current-buffer))))
    (concat (when tags
              (concat (semantic-tag-name (car tags))
                      "."))
            (file-name-base (buffer-file-name (current-buffer))))))

(cl-defmethod ede-expand-filename-impl ((this ede-project-with-config-java)
                                        filename &optional force)
  "For class files, return paths with `semantic-javap-temp-folder' prefix.
Source files are handled by base class."
  (let ((found (cl-call-next-method))
        (path (ede-project-root-directory this)))
    (mapcar (lambda (file)
              (if (and file (string= (file-name-extension file) "class"))
                  (concat path ".emacs/javap/"
                          (mapconcat 'identity  (split-string
                                                 (file-name-sans-extension file) "/") "."))
                file))
            found)))

;;;###autoload
(defun semantic-java-update-classpath ()
  "Update classpath configuration and locate DB."
  (interactive)
    ;; Read classpath from saved configuration
    ;; Regenerate DB only if classpath has changed
    (let* ((proj (ede-current-project))
	   (config (ede-config-get-configuration proj))
           (javahome (oref config javahome))
           (compilation-directory (ede-project-root-directory proj))
           (tmp-classpath (mapconcat 'identity (oref config classpath) ":")))
      (setq semantic-java-classpath tmp-classpath)
      (setq semantic-java-classpath
            (read-string "Enter classpath: " semantic-java-classpath))
      (unless (string-equal tmp-classpath semantic-java-classpath)
        (oset config classpath
              (split-string semantic-java-classpath ":"))
        (ede-commit config)
        (when (and javahome
                   (not (string-empty-p javahome)))
          (setq semantic-java-classpath
                (concat (expand-file-name javahome)
                        "/jre/lib/rt.jar:"
                        semantic-java-classpath)))
        ;; Create required temp directories
        (mkdir (concat compilation-directory (file-name-directory ede-locate-db)) t)
        (semantic-java-update-locate-db))))

(defun semantic-java-update-locate-db ()
  "Update locate database with latest JARs.
A java editor needs a list of fully qualified names of all the classes in the classpath.
Storing these names in frcode format is more efficient in terms of storage."
  (let* ((jar "jar tf ")
         ;; Skip JDK internal classes
         (grp "grep -P '^(?:(?!sun).)*\\.(class|java)$'")
         ;; Conversion for static inner classses which appear as $1
         (sed "sed -E 's/(\\$[0-9\\.]+)/\\./g ; s/\\$/\\//g'")
         (frcode "/usr/libexec/frcode")
         (file (concat compilation-directory ede-locate-db))
         (tmp (concat compilation-directory ".tmp"))
         cmd
         process
         (i 0))

    ;; Add source files
    (when ede-minor-mode
      (ede-flush-directory-hash))
    (start-process-shell-command
     "shell" "*Messages*" (concat "find " compilation-directory " -name \"*.java\" > " tmp))

    ;; Generate file list from all JARs
    (dolist (f (and semantic-java-classpath (split-string semantic-java-classpath ":")))
      (unless (string= "" f)
        (setq cmd (concat jar f ">" tmp (number-to-string i)))
        (setq i (1+ i))
        (setq process
              (start-process-shell-command "shell" "*Messages*" cmd))))

    ;; Generate frcode file
    ;; Must select unique here as static classes will result in duplicate entries
    ;; We want project files on top
    (setq cmd (concat "cat " tmp "* | " grp " | " sed " | sort | uniq | " frcode  " > " file))
    ;; Wait for all the files to be created
    (while (process-live-p process)
      (sleep-for 1))
    (call-process-shell-command cmd)

    ;; Using multiple tmp files for parallel processing
    (call-process-shell-command (concat "rm " tmp "*"))))

(defun semantic-c-update-include-path ()
  "Update classpath configuration and locate DB."
  (interactive)
  ;; Read classpath from saved configuration
  ;; Regenerate DB only if classpath has changed
  (let* ((proj (ede-current-project))
	 (config (ede-config-get-configuration proj))
         (compilation-directory (ede-project-root-directory proj))
         (tmp-classpath (mapconcat 'identity semantic-dependency-system-include-path ":"))
         )
    (setq semantic-java-classpath tmp-classpath)
    (setq semantic-java-classpath
          (read-string "Enter classpath: " semantic-java-classpath))
    (unless (string-equal tmp-classpath semantic-java-classpath)
      ;; (oset config include-path
      ;;       (split-string semantic-java-classpath ":"))
      ;; (ede-commit config)
      ;; Create required temp directories
      (mkdir (concat compilation-directory (file-name-directory ede-locate-db)) t)
      (semantic-c-update-locate-db))))

(defun semantic-c-update-locate-db ()
  "Update locate database with latest JARs.
A java editor needs a list of fully qualified names of all the classes in the classpath.
Storing these names in frcode format is more efficient in terms of storage."
  (let* ((frcode "/usr/lib/x86_64-linux-gnu/locate/frcode")
         (file (concat compilation-directory ede-locate-db))
         (tmp (concat compilation-directory ".tmp"))
         cmd
         process
         (i 0))

    ;; Add source files
    (when ede-minor-mode
      (ede-flush-directory-hash))
    (start-process-shell-command
     "shell" "*Messages*" (concat "find " compilation-directory " -name \"*.h\" >" tmp))

    ;; Generate file list from all JARs
    (dolist (f (split-string semantic-java-classpath ":"))
      (unless (string= "" f)
        (setq cmd (concat "find " f " -name \"*.h\" >" tmp (number-to-string i)))
        (setq i (1+ i))
        (setq process
              (start-process-shell-command "shell" "*Messages*" cmd))))

    ;; Generate frcode file
    ;; Must select unique here as static classes will result in duplicate entries
    ;; We want project files on top
    (setq cmd (concat "cat " tmp "* | sort | uniq |" frcode  ">" file))
    ;; Wait for all the files to be created
    (while (process-live-p process)
      (sleep-for 1))
    (call-process-shell-command cmd)

    ;; Using multiple tmp files for parallel processing
    (call-process-shell-command (concat "rm " tmp "*"))))

;;;###autoload
(defun semantic-java-organize-imports ()
  "Organize imports based on `srecode-java-import-order'."
  (interactive)
  (semantic-force-refresh)
  (let ((inhibit-modification-hooks t)
        start tag)
    (unwind-protect
        (progn
          ;; Delete all imports in the buffer. Be careful to not delete anything else e.g. comments.
          (dolist (tag (semantic-find-tags-by-class 'include (semantic-fetch-tags-fast)))
            (when (semantic-tag-with-position-p tag)
              (setq start (semantic-tag-start tag))
              (delete-region start (semantic-tag-end tag))
              (delete-overlay
               (semantic-tag-overlay tag))))
          (delete-blank-lines))

      (progn
        (save-excursion
          ;; Insert after package tag
          (setq tag (semantic-find-tags-by-class 'package (semantic-fetch-tags-fast)))
          (setq start (and tag (semantic-tag-overlay (car tag)) (semantic-tag-end (car tag))))
          (unless start
            (setq start (point-min)))

          ;; srecode insert template
          (goto-char start)
          (delete-blank-lines)
          (srecode-insert "declaration:import")
          (save-buffer)
          )))))

;;;###autoload
(defun semantic-java-extract-method (&optional name)
  "Extract method with name as NAME or 'extracted'"
  (interactive "sMethod Name: ")
  (semantic-fetch-tags)
  (let* ((start (region-beginning))
         (end (region-end))
         (region (string-trim (buffer-substring-no-properties start end)))
         (tag (semantic-current-tag))
         (local-vars (semantic-get-all-local-variables))
         (name (or name "extracted"))
         (dict (srecode-create-dictionary))
	 (templ nil)
         (mod "private")
         sec arglist ta tmp)
    (when (semantic-tag-of-class-p tag 'function)
      (save-excursion
        ;; Generate arglist
        (dolist (ta local-vars)
          (when (and (srecode--var-in-region-p ta start end)
                     (or (not (eq major-mode 'java-mode))
                         ;; Avoid "this" variable
                         (and (semantic-tag-with-position-p ta)
                              ;; If var is defined before the region, include in args
                              (> start (semantic-tag-start ta)))))
            (setq tmp (cons (semantic-tag-type ta) (semantic-tag-name ta)))
            (push tmp arglist)
            (setq sec (srecode-dictionary-add-section-dictionary dict "ARGS"))
            (srecode-dictionary-set-value sec "TYPE" (car tmp))
            (srecode-dictionary-set-value sec "NAME" (cdr tmp))))

        (when (semantic-tag-static-p tag)
          (setq mod (concat mod " static")))
        (srecode-dictionary-set-value dict "PROTECTION" mod)
        (srecode-dictionary-set-value dict "TYPE" "void")
        (srecode-dictionary-set-value dict "NAME" name)
        (srecode-dictionary-set-value dict "CODE" region)

        ;; Replace existing code with method name
        ;; Format params according to mode
        (save-excursion
          (goto-char (point-min))
          (while (semantic-region-match region);(re-search-forward region nil t)
            (delete-active-region)
            ;; (replace-match "")
            (setq templ (srecode-template-get-table (srecode-table) "misc:funcall"))
            (srecode-insert-fcn templ dict)
            (indent-for-tab-command)
            ))

        ;; Insert new method at the end of current one
        (setq start (goto-char (semantic-tag-end tag)))
        (newline)
        (setq templ (srecode-template-get-table (srecode-table) "classdecl:function"))
        (srecode-insert-fcn templ dict)
        (indent-region start (point))
        (save-buffer)
        ))))

(defun semantic-region-match (region)
  "Match a region of text without whitespaces."
  (let ((lines (split-string region))
        (first t)
        found line bound)
    (while
        (and lines
             (setq line (car lines)
                   lines (cdr lines)
                   bound (save-excursion (forward-line) (end-of-line) (point))
                   found (re-search-forward (string-trim line) bound t)))
      (when first
        (setq first nil)
        (set-mark (match-beginning 0))))
    (unless found
      (set-mark nil))
    found))

;;;###autoload
(defun semantic-java-extract-interface (&optional name pkg)
  "Extract interface with name as NAME or 'extracted'"
  (interactive)
  (let* ((tag (semantic-current-tag))
         (type tag)
         buf file interfaces templ tags style mark start
         (dict (srecode-create-dictionary)))
    ;; tag must be a function
    (when (semantic-tag-of-class-p tag 'function)
      ;; Create options from existing interfaces
      (save-excursion
        ;; Find parent type
        (while (not (semantic-tag-of-type-p type "class"))
          (forward-sexp -1)
          (semantic-up-context (point) 'type)
          (setq mark (point))
          (setq type (semantic-current-tag))))
      (setq interfaces (semantic-tag-type-interfaces type))
      (setq name (completing-read "Interface name: " interfaces))
      (when (string= name "")
        (error "Please enter a valid name"))

      (setq file (concat (file-name-directory (buffer-file-name))
                         name ".java"))

      ;; Add method in other file (class or interface)
      (setq buf (find-file-noselect file))
      (unwind-protect
          (with-current-buffer buf
            (unless (file-exists-p file)
              (setq ede--refresh t)
              (java-mode)
              ;; Buffer might still be open even when file is deleted
              (erase-buffer)
              (setq tag
                    (semantic-tag-new-type name "interface"
                                           (list tag)
                                           nil)))
            (when (file-exists-p file)
              ;; Avoid duplicating the tag
              (setq tags (semantic-find-tags-by-name name buf))
              (setq semantic-tag-similar-ignorable-attributes '( :members))
              (setq style (list 'prototype))
              (when (cl-member tag
                               (semantic-tag-type-members (car tags))
                               :test #'semantic-tag-similar-p)
                (error "Tag already exists"))
              (goto-char (1- (semantic-tag-end (car tags)))))

            (setq tag (semantic-tag-copy tag))
            (semantic-tag-put-attribute tag :typemodifiers '("public"))
            (srecode-semantic-insert-tag tag style)
            (save-buffer))

        (when (member name interfaces)
          (error "Nothing more to do"))

        (save-excursion
          ;; Add to implements list and re-insert class tag
          (push name interfaces)
          (setq interfaces
                (sort (delete-dups interfaces) #'string<))
          (semantic-tag-put-attribute type :interfaces interfaces)

          (setq start (goto-char (semantic-tag-start type)))
          (delete-region start mark)
          (srecode-semantic-apply-tag-to-dict
           (srecode-semantic-tag (semantic-tag-name type) :prime type) dict)
          (setq templ (srecode-template-get-table (srecode-table) "declaration:class-signature"))
          (srecode-insert-fcn templ dict nil t)
          (insert " ")
          (indent-region start (point))
          (save-buffer)
          (semantic-force-refresh)
          )))))

;;;###autoload
(defun semantic-java-pull-up (&optional name pkg)
  "Extract interface with name as NAME or 'extracted'"
  (interactive)
  (let* ((tag (semantic-current-tag))
         (type tag)
         buf file interfaces templ tags style mark start
         cstart cend region args mod
         (dict (srecode-create-dictionary)))
    ;; tag must be a function
    (when (semantic-tag-of-class-p tag 'function)
      ;; Create options from existing super classes
      (save-excursion
        ;; Find parent type
        (while (not (semantic-tag-of-type-p type "class"))
          ;; Store tag content start and end points
          (when (looking-at "\\s(")
            (setq cstart (point))
            (forward-sexp 1)
            (setq cend (point)))
          (forward-sexp -1)
          (semantic-up-context (point) 'type)
          (setq mark (point))
          (setq type (semantic-current-tag))))
      (setq interfaces (semantic-tag-type-superclasses type))
      (setq name (completing-read "Class name: " interfaces))
      (when (string= name "")
        (error "Please enter a valid name"))

      (setq region (buffer-substring-no-properties (1+ cstart) (1- cend)))
      (senator-kill-tag)
      (setq file (concat (file-name-directory (buffer-file-name))
                         name ".java"))

      ;; Add method in other file (class or interface)
      (setq buf (find-file-noselect file))
      (with-current-buffer buf
        (unless (file-exists-p file)
          (setq ede--refresh t)
          (java-mode)
          ;; Buffer might still be open even when file is deleted
          (erase-buffer)
          (srecode-semantic-insert-tag (semantic-tag-new-type name "class" nil nil)))

        (when (file-exists-p file)
          ;; Avoid duplicating the tag
          (setq tags (semantic-find-tags-by-name name buf))
          (setq semantic-tag-similar-ignorable-attributes '( :members))
          (when (cl-member tag
                           (semantic-tag-type-members (car tags))
                           :test #'semantic-tag-similar-p)
            (error "Tag already exists"))
          ;; TODO: Need to move all referenced attributes and methods
          (goto-char (- (semantic-tag-end (car tags)) 2)))
        (setq start (point))

        ;; Can't insert tag directly as we need to add BODY and change private to protected
        (when (eq (semantic-tag-protection tag) 'private)
          (setq mod "protected"))
        (when (semantic-tag-static-p tag)
          (setq mod (concat mod " static")))
        (srecode-dictionary-set-value dict "PROTECTION" mod)
        (srecode-dictionary-set-value dict "TYPE" (semantic-tag-type tag))
        (srecode-dictionary-set-value dict "NAME" (semantic-tag-name tag))
        (setq args (srecode-dictionary-add-section-dictionary dict "ARGS"))
        (dolist (a (semantic-tag-function-arguments tag))
          (srecode-dictionary-set-value args "NAME" (semantic-tag-name a))
          (srecode-dictionary-set-value args "TYPE" (semantic-tag-type a)))
        (srecode-dictionary-set-value dict "CODE" (string-trim region))
        (setq templ (srecode-template-get-table (srecode-table) "classdecl:function"))
        (srecode-insert-fcn templ dict)

        ;; (newline)
        ;; (indent-region start (point))
        (save-buffer))

      (when (member name interfaces)
        (error "Nothing more to do"))

      (save-excursion
        ;; Add to extends list and re-insert class tag
        (push name interfaces)
        (setq interfaces
              (sort (delete-dups interfaces) #'string<))
        (semantic-tag-put-attribute type :superclasses interfaces)

        (setq start (goto-char (semantic-tag-start type)))
        (delete-region start mark)
        (srecode-semantic-apply-tag-to-dict
         (srecode-semantic-tag (semantic-tag-name type) :prime type) dict)
        (setq templ (srecode-template-get-table (srecode-table) "declaration:class-signature"))
        (srecode-insert-fcn templ dict nil t)
        (insert " ")
        (indent-region start mark)
        (save-buffer)
        (semantic-force-refresh)
        ))))

;;;###autoload
(defun semantic-java-implement-interface ()
  "Implement all interface methods."
  (interactive)
  (let* ((tag (semantic-current-tag))
         (type tag)
         buf file interfaces templ
         impl unimpl tags tmp
         (dict (srecode-create-dictionary)))
    ;; Find the class
    (while (not (semantic-tag-of-type-p type "class"))
      (semantic-up-context (point) 'type)
      (forward-sexp -1)
      (setq type (semantic-current-tag)))

    ;; Find interface methods
    (setq interfaces (semantic-tag-type-interfaces type))
    (dolist (ta interfaces)
      (setq tags (semantic-tag-type-members (semanticdb-typecache-find ta)))
      (setq tmp (semantic-find-tags-by-class 'function tags))
      (setq unimpl (cl-union unimpl tmp :test #'semantic-tag-similar-p)))

    ;; Find implemented methods
    ;; TODO: Abstract methods are umimplemented methods
    (setq interfaces (semantic-tag-type-superclasses type))
    (setq impl (semantic-tag-type-members type))
    (dolist (ta interfaces)
      (setq tags (semantic-tag-type-members (semanticdb-typecache-find ta)))
      (setq impl (append (semantic-find-tags-by-class 'function tags) impl)))

    ;; Find unimplemented methods
    (setq semantic-tag-similar-ignorable-attributes '( :members))
    (setq unimpl (cl-set-difference unimpl impl :test #'semantic-tag-similar-p))

    (dolist (ta unimpl)
      ;; Find insertion point
      (goto-char (- (semantic-tag-end type) 1))
      ;; Interface methods are public
      (semantic-tag-put-attribute ta :typemodifiers '("public"))
      ;; Insert unimplemented methods
      (srecode-semantic-insert-tag ta))
    (save-buffer)
    (semantic-force-refresh)
    ))

;;;###autoload
(defun semantic-java-declare-variable(name)
  "Declare the symbol under point as a local variable."
  (interactive (list (symbol-name (symbol-at-point))))
  (when (or (fboundp (symbol-at-point))
            (nth 3 (syntax-ppss))
            (nth 4 (syntax-ppss)))
    (error "Place point on a variable symbol"))

  (semantic-java-extract-variable name))

;;;###autoload
(defun semantic-java-extract-variable(&optional name)
  "Extract VALUE or selected expression into a local variable."
  (interactive)
  (let* ((type (semantic-current-tag))
         (start (region-beginning))
         (end (region-end))
         (region (when (region-active-p)
                     (string-trim (buffer-substring-no-properties start end))))
         templ pos tname data nl
         (dict (srecode-create-dictionary)))
    (setq templ (srecode-template-get-table (srecode-table) "misc:variable-set"))
    (when (or (and (not region)
                   (not name))
              (not templ))
      (error "Nothing to do"))

    ;; Validate the expression
    (condition-case nil
        (when region
          (read region))
      (error (error "Invalid selection")))

    (unless name
      (setq name (completing-read "Variable name: " nil)))
    (when (string= name "")
      (error "Please enter a valid name"))

    ;;(deactivate-mark)
    (setq data (semantic--extract-var-helper name region)
          pos (car data)
          tname (nth 1 data)
          nl (nth 2 data))

    (when pos
      (when tname
        (setq templ (srecode-template-get-table (srecode-table) tname)))

    (srecode-dictionary-set-value dict "NAME" name)
    (srecode-dictionary-set-value dict "BODY" (or region "nil"))

    ;; Replace all occurences within local scope
    (when (region-active-p)
      (semantic-end-of-context)
      (setq end (point))
      (semantic-up-context)
      (while (re-search-forward region end t)
        (unless (or (nth 3 (syntax-ppss))
                    (nth 4 (syntax-ppss)))
          (replace-match name)
        )))

    ;; Insert before the containing block
    (goto-char pos)
    (srecode-insert-fcn templ dict nil t)
    (if nl
        (newline-and-indent)))

    (save-buffer)
    (semantic-force-refresh)
    ))

(define-overloadable-function semantic--extract-var-helper (name value)
  "Determine the containing block and the template to insert. Returns a
(POS TNAME NL).
NL denotes if a new line is required after insert."
  )

(defun semantic--extract-var-helper-default (name value)
  "Determine the containing block and the template to insert."
  (let (found)
    (save-excursion
      (while (not found)
        (if (looking-at-p ")")
            (progn
              (forward-char)
              (forward-sexp -1)
              (setq found (point)))
          (forward-sexp))))
    (cons found nil)))

(define-mode-local-override semantic--extract-var-helper emacs-lisp-mode (name value)
  "Determine the containing block and the template to insert."
  ;; No let, inside let and inside set
  (let ((pos (region-beginning))
        found vars str arg-end nl last region templ end
        (result nil)
        (is-set nil)
        (dict nil))
    (save-excursion
      (while (not (or (looking-at "(\\(defun\\|let\\)")
                      (when value (looking-at "(set"))))
        (up-list -1))
      (progn
        ;;(semantic-up-context)
        (forward-char)
        (when (looking-at "defun")
          ;; Surround body with let. Avoid (interactive) statement.
          (up-list)
          (setq end (1- (point)))
          (goto-char pos)
          (while (not last)
            (push (point) found)
            (setq last (semantic-up-context-default)))
          (pop found)
          (setq found (pop found))
          (goto-char found)
          (setq region (delete-and-extract-region found end)
                found nil
                dict (srecode-create-dictionary)
                templ (srecode-template-get-table (srecode-table) "misc:surround"))

          (srecode-dictionary-set-value dict "TAG" "let*")
          (srecode-dictionary-set-value dict "ARGS" "()")
          (srecode-dictionary-set-value dict "BODY" region)
          (srecode-insert-fcn templ dict)
          (indent-according-to-mode)
          (forward-sexp -1)
          (forward-char))

        (when (looking-at "let")
          ;; If point is inside let binding, vars is nil. So move to end of arglist.
          (forward-sexp 2)
          (setq arg-end (1- (point)))
          (setq vars (semantic-get-all-local-variables))

          ;; Some variables are only declared and have no default value
          (while (and vars (not found))
            (setq str (semantic-tag-variable-default (car vars)))
            (when (and str
                       value
                       (string-match value str))
              (setq found (car vars)))
            (when (and name
                       (string= name (semantic-tag-name (car vars))))
              (setq found (car vars)
                    is-set t))
            (setq vars (cdr vars)))
          (if found
              (if (not is-set)
                  (setq found (search-backward
                               (concat "(" (semantic-tag-name found) " "))
                        nl t)
                (setq found nil))
            (goto-char arg-end)
            (unless (looking-back "(" (1- arg-end))
              (newline-and-indent))
            (setq found (point)))
          (setq result
                (list found "misc:variable-let" nl)))

        (when (looking-at "set")
          (while (< (point) pos)
            (push (point) found)
            (forward-sexp))
          (pop found)
          (setq result
                (list (1+ (pop found)) "misc:variable-set" t)))
        ))
    result))

(defun srecode--var-in-region-p (tag beg end)
  "Check if a local variable TAG is in a region from BEG to END."
  (save-excursion
    (goto-char beg)
    (search-forward-regexp (srecode--local-var-regexp tag)
                           end t)))

(defun srecode--local-var-regexp (tag)
  "Return regexp for searching local variable TAG."
  (concat "\\_<" (regexp-quote (semantic-tag-name tag)) "\\_>"))

;; Completion at point functions
(add-hook 'completion-at-point-functions 'ede-completion-at-point-function)

(defvar-local semanticdb-implied-include-tags nil)

(define-overloadable-function ede-completion-at-point-function ()
  "Use `ede-expand-filename' to provide header include filename completions."
  (when (and (semantic-active-p)
             ;; Not inside comment
             (not (nth 4 (syntax-ppss)))
             (ede-minor-mode))
    (:override)))

(defun ede-completion-at-point-function-default ()
  "Do nothing."
 nil)

(define-mode-local-override ede-completion-at-point-function java-mode ()
  "Return possible package and class name completions at point.
The completions provided are via `ede-expand-filename'. Non nil TEST skips extra processing.
This function can be used by `completion-at-point-functions'."
  ;; Ideally, if package is parsed as hierarchy of namespace tags,
  ;; this method won't be necessary. However, that would require
  ;; upfront decompiling and parsing of all the classes. Logic below
  ;; allows us to use just in time decompilation and parsing.
    (let* ((prefixandbounds (semantic-ctxt-current-symbol-and-bounds (point)))
           (tl (car prefixandbounds))
	   (prefix (nth 1 prefixandbounds))
	   (bounds (nth 2 prefixandbounds))
           (tag1 (string-join tl "/"))
           (tag2 (string-join tl "\\."))
           (case-fold-search nil)
           tags possible b)
      (setq tags (semantic-java--add-include (concat tag1 ".*") nil nil t))
      (mapc
       #'(lambda(a)
           (setq a (semantic-tag-name a))
           (cond ((null tag1)
                  ;; Either package or class
                  (setq b (split-string a "\\."))
                  (push (car b) possible)
                  (setq possible (append (last b) possible)))
                 ((string-match (concat "^" tag2) a)
                  ;; Package match
                  (setq a (concat prefix (car (split-string (substring a (match-end 0)) "\\."))))
                  (unless (= (length a) (length tag1))
                    (push a possible)))
                 ((and (file-name-extension a)
                       (= (length tl) 1)
                       (string-match (concat "^" prefix) (file-name-extension a)))
                  ;; Classname match
                  (push (file-name-extension a) possible))
                 ))
       tags)
      (setq possible (remove nil (delete-dups possible)))
      (when possible
        (list (car bounds)
              (cdr bounds)
              possible)))
    )

(define-mode-local-override ede-completion-at-point-function c-mode ()
  "Return possible header file name completions at point.
The completions provided are via `ede-expand-filename'.
This function can be used by `completion-at-point-functions'."
  ;; prefix = tag1 / tag2
  (catch 'invalid
    (let* ((case-fold-search nil)
           prefix bounds tags possible tag1 tag2)
      (save-excursion
        (setq bounds (point))
        (while (progn
                 ;; Move past file separator
                 (backward-word)
                 (not (or (bolp)
                          (not (looking-back "/" 1))
                          (looking-back "[[:punct:]]" 1)
                          ))))
        (unless (and (looking-back "[<\"]" 1)
                     (save-excursion
                       (backward-word)
                       (looking-at-p "include")))
          (throw 'invalid nil))
        (setq prefix (buffer-substring-no-properties (point) bounds)
              tag1 (split-string prefix "/")
              bounds (cons (point) bounds)))

      (when (> (length tag1) 1)
        (setq tag2 (car (reverse tag1))
              tag1 (butlast tag1)))
      (setq tag1 (string-join tag1 "/"))
      (setcar bounds (+ (car bounds)
                        (if tag2 (1+ (length tag1)) 0)
                        ))

      (setq tags (semantic-java--add-include (concat prefix ".*\\.h") nil nil t))
      (mapc
       #'(lambda(a)
           (setq a (semantic-tag-name a))
           ;; linux/a.out.h, linux/android/binder.h, linux/arm_sdei.h
           (string-match (concat "^" prefix) a)
           (setq a (concat tag2 (car (split-string
                                      (if tag2 (substring a (match-end 0)) a)
                                      "/"))))
           (push a possible))
       tags)
      (setq possible (remove nil (delete-dups possible)))
      (when possible
        (list (car bounds)
              (cdr bounds)
              possible)))
    ))

(define-mode-local-override semantic-dependency-tag-file
  c-mode (&optional tag)
  "Find the filepath represented from TAG."
  (or (semantic--tag-get-property tag 'dependency-file)
  (if (ede-current-project)
      (let* ((tag1 (car (semantic-java--add-include (semantic-tag-name tag) nil nil t)))
             (file (semantic--tag-get-property tag1 'dependency-file)))
        (semantic--tag-put-property tag 'dependency-file file)
        file)
    (semantic-dependency-tag-file-default tag)
    )))

;; company-semantic-java has extra post completion logic
(eval-after-load 'company
  `(progn
     (add-to-list 'company-backends 'company-semantic)
     (add-to-list 'company-backends 'company-semantic-inc)))

;;;###autoload
(defun company-semantic-inc (command &optional arg &rest ignored)
  "`company-mode' completion backend using CEDET Semantic."
  (interactive (list 'interactive))
  (cl-case command
    (interactive (company-begin-backend 'company-semantic-java))
    (prefix (and ;(derived-mode-p 'java-mode)
                     (caddr (ede-completion-at-point-function))
                     (company-semantic--prefix))
                );'stop))
    (candidates
     (caddr (ede-completion-at-point-function)))
    (no-cache nil)
    (duplicates nil)
    (sorted nil)
    ))

(defvar company-minimum-prefix-length 3)
(make-variable-buffer-local 'company-minimum-prefix-length)
(defvar company-i18n-input-hash nil)
(make-variable-buffer-local 'company-i18n-input-hash)

;;;###autoload
(defun company-i18n (command &optional arg _rest)
  "`company-mode' completion backend for transliteration."
  (interactive (list 'interactive))
  (setq company-minimum-prefix-length 1)
  (cl-case command
    (interactive (company-begin-backend 'company-i18n))
    (prefix (and company-i18n-input-hash
                 (featurep 'ind-util)
                 (featurep 'company-ispell)
                 (company-grab-word)))
    (candidates
     (let ((c (company-i18n 'trans arg)))
       (cons c (ispell-lookup-words c))
       ))
    (trans
     (let* ((hash company-i18n-input-hash)
            (len (length arg))
            txt mark res prev)
       (while (not (string-empty-p arg))
         (setq mark (min 3 (length arg)))
         (while (and (not (setq res (indian-getchar (substring arg 0 mark) prev)))
                     (> mark 1))
           (setq mark (1- mark)))
         ;; (message "1 %s" res)
         (when res
           (setq res (if (stringp res) res (vector res)))
           (setq txt (if txt (vconcat txt res) res)))
         (setq prev (substring arg 0 mark)
               arg (substring arg mark))
         )
       ;; (message "%s" txt)
       (if txt (mapconcat 'string txt ""))))
    (no-cache t)
    (duplicates nil)
    (sorted t)
    ))

(defun indian-getchar (char &optional prev)
  (let* ((hash (cdr company-i18n-input-hash))
        (trans (gethash char hash))
        (halant (gethash "halant" hash))
        (trans-prev (gethash prev hash)))
    (cond ((consp trans)
           ;; Vowels
           (if (and prev (not (consp trans-prev)))
               (cadr trans)
               ;; (if (cadr trans) (cadr trans) "")
             (car trans)))
          ((numberp trans)
           (string trans))
          (prev
           (if (not (or (consp trans-prev)
                              (numberp trans-prev)))
               (concat halant trans)
             trans))
          (t trans
           ;; Consonants
           ;; Don't combine if prev is a vowel. Vowels are stored as lists.
           ;; (if (and prev (not (consp trans-prev))) (concat halant trans) trans))
          ;; (string-as-multibyte (apply #'insert '(2358 2381 2352)))
          ;; (mapconcat #'string '(2358 2381 2352) "")
          ;; (mapcar 'append (encode-coding-string trans 'utf-8)))
          ))))

(defvar company-semantic-begin-after-member-access t)
(defun company-semantic--prefix ()
  (if company-semantic-begin-after-member-access
      (company-grab-symbol-cons "\\.\\|->\\|::\\|/" 2)
    (company-grab-symbol)))
(defun company--clean-string (str)
  ;; Overriding to avoid mangling unicode composed strings
  str)

(defun semantic-indent-region (&optional region-beg region-end col paren-block)
  (interactive)
  (let* ((region-beg (or region-beg (region-beginning)))
         (region-end (or region-end (region-end)))
         (col (or col 0))
         (before '(IDENTIFIER NUMBER_LITERAL STRING_LITERAL))
         (after  '(COMMA))
         (both   '(PLUS EQ LT GT CATCH FINALLY ELSE LDIAMOND RDIAMOND))
         (stream (semantic-lex region-beg region-end))
         (fit-line nil)
         (count 0)
         (first t)
         (inhibit-modification-hooks t)
         (fluent nil)
         pos beg end match bol dot id tok para temp prev stop space bef)

    ;; COMMA, SEMICOLON, PLUSPLUS, MINUSMINUS and DOT are treated differently from other punctuation chars
    (setq both (mapcar 'car (semantic-lex-type-value "punctuation")))
    (deactivate-mark)
    (undo-auto-amalgamate)

    ;; Empty block
    (unless stream
      (delete-region region-beg region-end)
      (setq count (- region-beg region-end)))

    (when (and stream
               paren-block
               )
      ;; Sum of token extents plus one space for each token
      ;; Consider BRACE_BLOCK as single char inside PAREN_BLOCK
      ;; If there's a comma, add one indent
      (let ((tmp stream)
            (val 0)
            elt stop-list)
        (pcase paren-block
          ('BRACE_BLOCK
           (setq stop-list '(COLON SEMICOLON)))
          (_
           (setq stop-list '(ARROW BRACE_BLOCK COMMA OROR ANDAND))))
        (while tmp
          (setq elt (car tmp)
                val (+ val 1 (if (and (eq paren-block 'PAREN_BLOCK)
                                      (eq (car elt) 'BRACE_BLOCK))
                                 1
                               (- (cddr elt) (cadr elt))))
                tmp (cdr tmp))
          (when (> val fill-column)
            (setq tmp nil))
          (when (memq (car elt) stop-list)
            (setq stop (memq (car elt) stop-list))))
        (when (eq (car stop) 'COLON)
          ;; JS key-value pair
          (setq stop '(COMMA)))
        (when (and (not (memq 'SEMICOLON stop))
                   (<= (+ (current-column) val) fill-column))
          (setq fit-line t)))

      (unless fit-line
        (setq col (+ col tab-width)))
      )

    (while stream
      (setq tok (car stream)
            stream (cdr stream))
      ;; If we identified paragraph in previous iteration, let's restore it now;
      ;; except at the end
      (when (and para stream)
        (goto-char end)
        (setq count (+ count 1))
        (insert "\n"))

      ;; When a space is added, count is incremented. This is added to
      ;; position returned by lexer to obtain current token position.
      (setq pos (cdr tok)
            beg (+ (car pos) count)
            end (+ (cdr pos) count))

      ;; Remove extra space
      (goto-char beg)
      (if (and (not col) first)
          (beginning-of-line))
      ;; First non-whitespace char is anchor. Hence skip it.
      (unless (or bol
                  para
                  (zerop (setq match (skip-chars-backward " \t\n" region-beg))))
        (delete-region (+ beg match) beg)
        (setq count (+ count match)
              beg (+ beg match)
              end (+ end match)))

      ;; Treat \n\n as paragraph end
      (setq para (when stream (<= 2 (count-matches "\n" end (+ count (cadar stream))))))

      ;; Leave whitespace after last token so that next line is not joined.
      (goto-char end)
      (unless (zerop (setq match (skip-chars-forward " \t\n"
                                                     (+ region-end
                                                        count
                                                        (if paren-block 0 -1)
                                                        ))))
        (delete-region end (+ end match))
        (setq count (+ (- count match))))
      (goto-char beg)

      (setq bef (or (memq (car tok) before) (memq (car tok) both))
            bol nil
            space nil)

      ;; Determine whether we need a space or a newline
      (cond ((memq (car tok) '(PAREN_BLOCK BRACE_BLOCK DIAMOND_BLOCK BRACK_BLOCK))
             ;; Single space before block - no newline
             ;; Content follows on pos + 1
             ;; Only function call begins with no space
             (unless (or first
                         (and id (memq (car tok) '(PAREN_BLOCK DIAMOND_BLOCK BRACK_BLOCK))))
               (setq beg (1+ beg)
                     count (1+ count)
                     end (1+ end))
               (insert " "))
             (setq match
                   (semantic-indent-region (1+ beg) (- end 1) col (car tok))
                   end (+ match end)
                   count (+ match count)
                   )
             ;; Content follows on next line with one additional indent
             (when (memq (car tok) '(BRACE_BLOCK))
               ;; Align ending bracket with beg tab position on a newline
               (save-excursion
                 (goto-char beg)
                 (setq bol (re-search-forward "\n" end 1))
                 )
               (goto-char (1- end))
               )

             ;; Restore point after end brace
             (unless bol
               (goto-char end))
             )
            (first
             ;; Indent first line of the code block according to col
             (unless col
               ;; Depth zero is anchor point. Align to a tab stop.
               (setq col (indent-next-tab-stop (1+ (current-column)) t)))

             (when (and (eq paren-block 'BRACE_BLOCK)
                        (not fit-line))
               (setq bol t))
             )
            ((and (not fit-line) (memq prev '(SEMICOLON)))
             ;; command separator
             (setq bol t)
             (when fluent
               (setq col (- col tab-width)
                     fluent nil))
             )
            ((and (not fit-line) (memq prev stop))
             ;; Arrow function
             (setq bol t)
             )
            ((or (memq prev '(DOT NOT AT))
                 (and (memq (car tok) '(MINUSMINUS PLUSPLUS))
                      (memq prev '(IDENTIFIER)))
                 (and (memq prev '(MINUSMINUS PLUSPLUS))
                      (memq (car tok) '(IDENTIFIER)))
                 (memq (car tok) '(COMMA SEMICOLON)))
             nil)
            ((semantic-lex-keyword-symbol (downcase (symbol-name (car tok))))
             ;; Keywords usually start on a new line unless following
             ;; another keyword.
             (cond ((memq prev '(DOT))
                    ;; File.class has no space
                    )
                   ((memq prev '(PAREN_BLOCK BRACE_BLOCK))
                    ;; New line after brace and paren block
                    (if (memq (car tok) '(CATCH FINALLY ELSE))
                        (setq space t)
                      (setq bol (not fit-line))))
                   (t
                    (setq space t)))
             )
            ((and (memq (car tok) '(DOT))
                  (semantic-lex-keyword-symbol (downcase (symbol-name (caar stream))))
                  )
             ;; .then() on the same line
             )
            ((eq (car tok) 'DOT)
             (when (memq prev '(PAREN_BLOCK))
               (setq bol t)
               (when (not fluent)
                 ;; Fluent style - one extra indent
                 (setq col (+ col tab-width)
                       fluent t)
                 ))
             )
            ((and (or (memq (car tok) '(OROR ANDAND))
                      (not (memq (car tok) both)))
                  ;; (not (memq (car tok) '(ARROW QUESTION)))
                  (memq prev '(BRACE_BLOCK PAREN_BLOCK comment)))
             ;; If condition block
             (setq bol t)
             )
            ((and stream (not (memq prev both)))
             ;; Add space before except after punctuation eg !=
             (if (and (not (memq (caar stream) '(BRACE_BLOCK STRING_LITERAL)))
                      (>= (+ (current-column) (cddar stream) (- (cadar stream)))
                          fill-column)
                      )
                 ;; If next token can't fit in current line, break at punctuation
                 (setq bol t
                       fluent t
                       col (+ col tab-width))
               (setq space t))
             )
            (bef (setq space t))
            )

      ;; Insert space or newline
      (cond (bol
             ;; Don't insert newline if it's the last token
             (setq count (+ 1 count (/ col tab-width)))
             (insert "\n")
             (indent-to col)
             )
            (space
             (setq count (1+ count))
             (insert " ")
             )
            )
      (setq prev (car tok)
            first nil
            ;; If id is non nil, space is not inserted before next keyword
            id (memq prev '(IDENTIFIER NOT BRACE_BLOCK PAREN_BLOCK))
            ;; If dot is non nil, space is not inserted before next identifier
            dot (or bol (memq prev '(DOT NOT AT))))
      )
    (goto-char region-beg)
    count))

(defun java-type-hierarchy-insert-tags (buf)
  "Insert Java Type hierarchy in speedbar for BUF."
  (let* (sym tags tag ctxt)
    (with-current-buffer buf
      (setq sym (symbol-name (symbol-at-point))
            ctxt (semantic-analyze-current-context)
            tags (java-type-hierarchy-create sym))

      (unless (or tags (not ctxt))
        ;; Check if point is on something other than type
        (setq tag (oref ctxt prefix))
        (when (semantic-tag-p (car tag))
          ;; Type has no children
          (setq tags (list (semantic-tag-new-type sym 'class nil nil)))))
      )

    (erase-buffer)
    (when tags
      (semantic-sb-insert-tag-table
       0
       (list (cons sym
                   (list (cons sym tags))
                   ))
       )
      (forward-line -1)
      ;; Open it.
      (speedbar-expand-line))))

(defun java-type-hierarchy-create (sym)
  "Create Java Type hierarchy for SYM."
  (mapcar
   (lambda (a)
     (let* ((tag (copy-tree (cadr a)))
            (name (semantic-tag-name tag))
            (child (java-type-hierarchy-create name))
            (file (semanticdb-full-filename (car a))))
       ;; (message "%s %s %s" name file (length (cdr a)))
       (semantic--tag-put-property tag :filename file)
       (semantic-tag-put-attribute tag :members child)
       tag))
   (semanticdb-find-tags-subclasses-of-type sym)
   ))

;;;###autoload
(defun java-type-hierarchy ()
  "Display Java Type hierarchy in speedbar."
  (interactive)
  (unless (symbol-at-point)
    (error "Point is not on a type"))
  (setq speedbar-mode-specific-contents-flag t
        speedbar-tag-hierarchy-method '(semantic-sort-tags-by-name-increasing)
        speedbar-special-mode-key-map speedbar-file-key-map
        speedbar-special-mode-expansion-list
        '(java-type-hierarchy-insert-tags))

  (speedbar-window-mode t t))

(defun semantic-fontify-region1 (beg end &optional edit loudly)
  "Fontify region from BEG to END."
  ;; We need to fontify current symbol. We will parse current command
  ;; for the case when a new variable is being defined or it's a
  ;; package/import statement. Else use current symbols available in
  ;; the context and fontify all the occurences within the context.
  ;; This function is also used for full buffer fontification as
  ;; well. However, try to maintain incremental style.
  (unless (and (semantic-active-p)
               font-lock-defaults)
    (signal 'quit "Not a semantic buffer"))
  (let* ((pos1 beg)
         (pos2 end)
         (pos (point))
         ;; (ctxt (semantic-analyze-current-context))
         ;; (scope (semantic-calculate-scope end))
         (len 0)
         ;; Since we are only changing text property and not text,
         ;; don't trigger hooks
         (inhibit-modification-hooks t)
         sym tag ltag ltag1 ltag2 command-bounds ctxt-bounds res vbounds)
    ;; (message "%d %d %d %s %s %s" pos beg end edit this-command last-command)
    (condition-case nil
        (save-excursion
          (goto-char pos1)
          ;; Special handling for comments
          (setq beg (point))
          (while (forward-comment 1)
            (setq sym t
                  end (point)))
          (when sym
            (signal 'quit '("Inside comment")))

          ;; Find command bounds
          ;; While editing we want previous command bounds. However,
          ;; while sequentially fontifying, we want next command
          ;; bounds. So select current and previous commands.

          ;; Find context bounds
          ;; Consider point inside a variable declaration in a method or outside type
          ;;(goto-char pos1)
          ;; Overlays provide easy mechanism to deduce bounds even while editing
          (if edit
              (semantic-fetch-tags))
          ;; JS variable tag doesn't include var keyword
          (setq tag (semantic-find-tag-by-overlay-in-region beg (line-end-position)))
                ;; (semantic-find-tag-by-overlay))
          ;; (message "%d %d %s" pos1 pos2 tag)
          (if tag
              (setq ltag1 (car (last tag))
                    command-bounds (cons (min beg (semantic-tag-start ltag1))
                                         (semantic-tag-end ltag1))
                    ctxt-bounds (when (> (length tag) 1)
                                  (cons (semantic-tag-start (car tag))
                                        (semantic-tag-end (car tag)))))
            (semantic-beginning-of-command)
            (setq beg (point)
                  ltag1 (semantic-current-tag))
            (goto-char pos1)
            (if (looking-at semantic-command-separation-character)
                (forward-char))
            (semantic-end-of-command)
            (if (eobp)
                (setq end (point))
              ;; Include ending semi-colon
              (setq end (1+ (point))))
            ;; Tag is one char before ;
            (forward-char -1)
            (setq command-bounds (cons beg end)
                  ltag2 (semantic-current-tag)))


          ;; (message "Tag: %s %s" ctxt-bounds tag)
          ;; Parse only for edit scenarios
          (when (or ltag1 ltag2)
            ;; (if edit
            ;;     ;; New tag being inserted. At times, it's expected to fail with error.
            ;;   (setq ltag (semantic-parse-region (car command-bounds)
            ;;                                     (cdr command-bounds)
            ;;                                     (semantic--tag-get-property
            ;;                                      (or ltag1 ltag2)
            ;;                                      'reparse-symbol)
            ;;                                     nil t))
              (setq ltag (if (and ltag1 ltag2)
                             (list ltag1 ltag2)
                             (list (or ltag1 ltag2)))))
          ;; (message "LTag: %s %s" command-bounds ltag)
          (when (and ltag ctxt-bounds)
            (if (> (length tag) 1)
                (progn
                  ;; We need to update the member within the context since
                  ;; tags will be updated in a separate hook
                  (dolist (m (append (semantic-tag-type-members (car tag))
		                     ;;	       (semantic-tag-function-arguments tag))
                                     ))
                    (if (eq (semantic-tag-start (car ltag))
                            (semantic-tag-start m))
                        (push (car ltag) res)
                      (push m res)))
                  ;; (message "%s" res)
                  (setq tag (semantic-tag-clone (car tag)))
                  (semantic-tag-put-attribute tag :members res)
                  (setq tag (list tag)))
              ;; We have commented a tag
              (setq tag ltag)))
          )
      (error (setq end pos2))
      (quit nil))
    (if ctxt-bounds
        (setq beg (car ctxt-bounds)
              end (cdr ctxt-bounds))
      (when command-bounds
        (setq beg (car command-bounds))
        (if (and ltag
                 (not edit))
            (setq tag ltag
                  end (cdr command-bounds))
          ;; Global context
          (setq tag (semantic-fetch-tags-fast)
                ;;(when ltag1 (list ltag1))
                end (window-end)))
        ))
    ;; (message "%d %d" beg end)
    ;; Don't limit by initial values. Limits don't work in case of edits.
    ;; (setq beg (max beg pos1)
    ;;       end (min end pos2))
    ;; (when (and (or tag sym)
               ;; (>= pos (car vbounds))
               ;; (<= pos (cdr vbounds))
               ;; )
      ;; Restrict to viewport
    (with-silent-modifications
      (save-excursion
        ;; Unfontify region
        (font-lock-unfontify-region beg end)
        (font-lock-fontify-syntactically-region beg end loudly)

        ;; (while ltag
        ;;   (semantic-fontify-tag (car ltag) beg end)
        ;;   (setq ltag (cdr ltag)))
        (while tag
          (semantic-fontify-tag (car tag) beg end)
          (setq tag (cdr tag)))

        ;; Fontify keywords
        (font-lock-fontify-keywords-region beg end loudly)))
      ;; )
    end))

(defvar semantic-lex-spp-fontify-depth 1)

(defun semantic-fontify-region (beg end start-win end-win)
  "Fontify region from BEG to END."
  ;; We need to fontify current symbol. We will parse current command
  ;; for the case when a new variable is being defined or it's a
  ;; package/import statement. Else use current symbols available in
  ;; the context and fontify all the occurences within the context.
  ;; This function is also used for full buffer fontification as
  ;; well. However, try to maintain incremental style.
  (let* (tag ltag c1 c2)
    ;; (message "%d %d %d %s %s %s" 0 beg end edit this-command last-command)
    ;; (message "%s" (semantic-current-tag))

    ;; Uncommenting affects whole line or multiple lines for multiline comments
    ;; Changing string or comment doesn't affect semantic context
    (save-excursion
      (goto-char beg)
      (if (looking-at "\\w") (forward-word -1))
      (and (derived-mode-p 'java-mode)
           (semantic-current-tag)
           ;; Imports are usually longer than just a word. Use tag start instead.
           (goto-char (semantic-tag-start (semantic-current-tag))))
      (setq c1 (point))

      (goto-char end)
      (or (semantic-end-of-context)
          (goto-char (point-max)))
      (setq c2 (point)))

    ;; Since this is expected to run in idle time, we can afford a
    ;; reparse if required
    (setq tag (semantic-fetch-tags))

    (when (derived-mode-p 'java-mode)
      ;; Collect properly included types from imports
      (unless semantic-font-lock-types
        ;; File has only been loaded. Force this once. Edit mode will
        ;; keep this updated via hooks.
        (semantic-fl-update-keywords)))

    ;; Since we are only changing text property and not text,
    ;; don't trigger hooks
    (with-silent-modifications
      (undo-boundary)
      (save-excursion
        ;; Restrict to viewport
        (setq beg (max c1 start-win)
              end (min c2 end-win))
        ;; (message "Semantic context %s - %s, %s - %s" c1 c2 start-win end-win)
        ;; (message "Fontifying region %s - %s" beg end)
        ;; (pp semantic-font-lock-types)
        (if (> c2 end-win)
            (put-text-property end-win (- c2 end-win) 'fontified 'defer))
        ;; Unfontify region
        (font-lock-unfontify-region beg end)
        (font-lock-fontify-syntactically-region beg end)

        (while tag
          ;; Marking package first will prevent other imports from
          ;; same hierarchy to be painted correctly
          (if (semantic-tag-of-class-p (car tag) 'package)
              (push (car tag) ltag)
            (semantic-fontify-tag (car tag) beg end))
          (setq tag (cdr tag)))

        ;; Paint leftover tags now
        (setq tag ltag)
        (while tag
          (semantic-fontify-tag (car tag) beg end)
          (setq tag (cdr tag)))

        ;; Fontify keywords
        (when font-lock-keywords
          (font-lock-fontify-keywords-region beg end)))
      (undo-boundary))
    end))

;;;###autoload
(define-minor-mode semantic-font-lock-mode1
  "Semantic font lock minor mode"
  :lighter "sflm"
  (if (and (featurep 'semantic) (semantic-active-p)
           ;; define-minor-mode highlighting doesn't work because it's
           ;; identified as code class
           ;(not (derived-mode-p 'emacs-lisp-mode 'lisp-mode))
           semantic-font-lock-mode
           )
      (progn
        (semantic-jit-common-activate)
        (add-hook 'semantic-jit-functions 'semantic-fontify-region nil t))
    (remove-hook 'semantic-jit-functions 'semantic-fontify-region t)
    (semantic-jit-common-deactivate)))

;;;###autoload
(define-minor-mode image-font-lock-mode1
  "Image font lock minor mode"
  :lighter "img"
  (if image-font-lock-mode
      (progn
        (semantic-jit-common-activate)
        (add-hook 'semantic-jit-functions 'font-lock-image nil t))
    (remove-hook 'semantic-jit-functions 'font-lock-image t)
    (semantic-jit-common-deactivate)))

(defun semantic-jit-common-activate ()
  (add-hook 'after-change-functions 'semantic-after-change-function nil t)
  ;; (add-hook 'semantic-jit-functions 'font-lock-image nil t)
  ;; (add-hook 'semantic-jit-functions 'semantic-fontify-region nil t)
  (semantic-fontify-timer)
  ;; (semantic-fontify-timer-function)
  )

(defun semantic-jit-common-deactivate ()
  (remove-hook 'after-change-functions 'semantic-after-change-function t)
  (remove-hook 'semantic-jit-functions 'font-lock-image t)
  ;; (remove-hook 'semantic-jit-functions 'semantic-fontify-region t)
  )

;; (semantic-add-minor-mode 'semantic-font-lock-mode "f")
;; (semantic-toggle-minor-mode-globally 'semantic-font-lock-mode 1)

;;;###autoload
(defun semantic-font-lock-activate ()
  "Set up keywords and turn on `font-lock-mode'."
  (interactive)
  ;; Add keywords from language grammar
  (let (face keywords)
    (setq face (cdr-safe (assoc 'keyword semantic-format-face-alist))
          ;; font-lock-keywords nil
          keywords (mapcar #'(lambda (k)
                               ;; (cons (symbol-name k)
                               (cons (regexp-opt `(,(symbol-name k)) 'symbols)
                                     face))
                           (semantic-lex-keywords)))
    ;; Unless font-lock-keywords font-lock-defaults is set by major
    ;; mode, this will trigger font-lock-mode
    (font-lock-add-keywords nil keywords)
    (when (derived-mode-p 'java-mode)
      (font-lock-add-keywords nil javadoc-font-lock-keywords))
    (setq-local font-lock-fontify-buffer-function 'semantic-fontify-buffer)
    ;; (setq-local font-lock-fontify-region-function 'semantic-fontify-region)
    (setq-local font-lock-extend-region-functions nil)
    (remove-hook 'before-change-functions 'c-before-change t)
    (remove-hook 'after-change-functions 'jit-lock-after-change t)
    (remove-hook 'after-change-functions 'c-after-change t)
    (remove-hook 'after-change-functions 'font-lock-after-change-function t)
    (remove-hook 'fontification-functions 'jit-lock-function t)

    (add-hook 'after-change-functions 'semantic-after-change-function nil t)
    (add-hook 'fontification-functions 'semantic-fontify nil t)
    ;; Required in case idle scheduler mode is off
    (add-hook 'window-scroll-functions 'semantic-window-update-function nil t)
    (semantic-idle-scheduler-add 'semantic-fontify-timer)
    ;; Post parsing hooks
    ;; (add-hook 'semantic-after-toplevel-cache-change-hook 'semantic-fl-update-keywords nil t)
    ;; (add-hook 'semantic-after-partial-cache-change-hook 'semantic-fl-update-keywords nil t)

    ;; (font-lock-unfontify-buffer)
    ;; Need to re-enable font lock as it doesn't function without keywords
    ;; (font-lock-mode 1)
    ;; (current-buffer)
    ;; (semantic-fontify (window-end))
    ))

(defun semantic-font-lock-deactivate ()
    (remove-hook 'after-change-functions 'semantic-after-change-function t)
    (remove-hook 'fontification-functions 'semantic-fontify t)
    (remove-hook 'window-scroll-functions 'semantic-window-update-function t)
    (semantic-idle-scheduler-remove 'semantic-fontify-timer)
    (font-lock-mode -1))

;; semantic--create-overlay handles disabling. Hence
;; semantic--spp-highlight-ifdef and semantic--spp-highlight-ifdef-jit
;; are not used
(defun semantic--spp-highlight-ifdef (end)
  ;; Fontify disabled section. Overrides existing fontification.
  (let ((start (point))
        overlays o found)
    (set-match-data nil)
    (while (and (< start end)
                (not (eobp))
                (not found))
      (goto-char start)
      (setq overlays (overlays-at (point)))
      (setq start (next-overlay-change (point)))
      (while overlays
        (when (and (setq o (car overlays))
                   (overlay-get o 'spp-ifdef)
                   (overlay-get o :disabled))
          (setq found t)
          (set-match-data (append (match-data)
                                  (list (overlay-start o)
				        (min (overlay-end o) end))))
          (goto-char (min (overlay-end o) end)))
        (setq overlays (cdr overlays))))
    (match-data)
    ))

(defun semantic--spp-highlight-ifdef-jit (start end)
  ;; Fontify disabled section. Overrides existing fontification.
  (let ((r1 start)
        (r2 end)
        overlays o found c1 c2)
    (setq c1 start c2 start)
    (while (and (< start end)
                (not found))
      (goto-char start)
      (setq overlays (overlays-at (point)))
      (setq start (next-overlay-change (point)))
      (while (and overlays
                  (not found))
        (when (and (setq o (car overlays))
                   (overlay-get o 'spp-ifdef)
                   (overlay-get o :disabled))
          (setq c1 (min c1 (overlay-start o))
                c2 (max c2 (overlay-end o))
                found (and (<= c1 r1) (>= c2 r2)))
          (goto-char c2))
        (setq overlays (cdr overlays)))
      (if (/= c1 c2)
          (put-text-property c1 c2 'face 'font-lock-disabled-face)))
    ))

;; (add-hook
;;  'c-mode-hook
;;  (lambda ()
;;    (add-hook 'jit-functions #'semantic--spp-highlight-ifdef-jit nil t)))

;; (add-hook
;; 'c-mode-hook
;; (lambda ()
;;    (font-lock-add-keywords
;;     nil
;;     `(,(cons 'semantic--spp-highlight-ifdef
;;              '(0 font-lock-disabled-face t))))))

(defvar semantic-jit-functions nil
  "Abnormal hook for JIT functions.")

(defvar-local semantic--change-start nil
  "Internal variable for tracking change bounds.")

(defvar-local semantic--change-end nil
  "Internal variable for tracking change bounds.")

(defvar semantic--fontify-timer nil
  "Internal variable for scheduling fontification.")

(defvar semantic--fontify-delay 0.1
  "Internal variable for fontification delay.")

(defun semantic-window-update-function (window pos)
  "Used for `window-scroll-functions'"
  ;; (message "Win start %d" pos)
  ;; (setq semantic--change-end (window-end window t))
  (semantic-fontify-timer))

(defun semantic-after-change-function (beg end len)
  "Used for `after-change-functions'"
  ;; Undo actions typically result in multiple invocations. Let's club
  ;; them together.
  (save-current-buffer
    ;; (message "After change %s %d %d %d" (buffer-name) beg end len)
    (setq semantic--change-start
          (if semantic--change-start (min semantic--change-start beg) beg)
          semantic--change-end
          (if semantic--change-end (max semantic--change-end end) end))
    ;; (put-text-property beg end 'fontified 'defer)

    ;; (when semantic--fontify-timer
    ;;   (cancel-timer semantic--fontify-timer))
    ;; (setq semantic--fontify-timer
    ;;       (run-with-timer semantic--fontify-delay nil #'semantic-fontify-region beg end t)
    ;;       )
    ;; (semantic-fontify-timer)
    ))

(defun semantic-fontify-timer ()
  "Trigger to be used by window resize and update functions which
change window dimensions. Ensure `semantic--change-end' is
updated with latest value before calling this."
  ;; Run a single instance of timer function
  (when semantic--fontify-timer
    (cancel-timer semantic--fontify-timer))
  (setq semantic--fontify-timer
        (run-with-idle-timer semantic--fontify-delay t #'semantic-fontify-timer-function)
        ))

(defun semantic-fontify-timer-function ()
  "Mark the unfontified region in the visible window."
  (let (end-change start-change start-win end-win)
    (and semantic--change-start semantic--change-end
         ;; (> semantic--change-start (point-min))
         ;; Text deleted
         (setq semantic--change-end (min semantic--change-end (point-max)))
         (message "Fontify mark changes %s - %s, %s"
                  semantic--change-start semantic--change-end (buffer-name))
         (with-silent-modifications
           (put-text-property semantic--change-start semantic--change-end
                              'fontified 'defer)))
    (setq semantic--change-start nil
          semantic--change-end   nil)

    ;; If the last redisplay of window was preempted, and did not
    ;; finish, Emacs does not know the position of the end of display
    ;; in that window. In that case, (window-end) returns nil.
    (and (boundp 'semantic-jit-functions)
         semantic-jit-functions
         (setq end-win (window-end))
         (setq start-win (window-start))
         (setq start-change (or (text-property-any start-win end-win 'fontified nil)
                                (text-property-any start-win end-win 'fontified 'defer)))
         (setq end-change (or (next-single-property-change
                               start-change 'fontified nil end-win)
                              end-win))
         ;; (message "Fontify JIT %s - %s, %s" start-change end-change (buffer-name))
         (save-excursion
           (with-silent-modifications
             ;; Refontify till the end of screen. This takes care of single
             ;; line tags like imports on subsequent screens.
             (if (run-hook-with-args-until-failure
                  'semantic-jit-functions start-change end-change)
                 (put-text-property start-change end-change 'fontified t)
                 ;; (put-text-property (max (window-start) (point-min))
                 ;;                    (min (window-end) (point-max))
                 ;;                    'fontified t)
               )))
         )))

(defun semantic-fontify (pos)
  "Fontification function for fontifying using semantic."
  (let* ((end (or (window-end)
                  (1+ pos)))
         ;; Uncommenting this will flood the undo tree. However,
         ;; keeping this for easier debugging.
         ;; (inhibit-modification-hooks t)
         ;; (inhibit-read-only t)
         (max (point-max))
         (state 'defer)
         (mark end))
    ;; (message "Fontify %s %d %d" (buffer-name) pos end)
    ;; (message "Prop: %s" (text-properties-at pos))

    (with-silent-modifications
      (ignore-errors
        (save-excursion
          (setq mark (funcall font-lock-fontify-region-function pos end)
                state t)))
      ;; (message "Fontify 1: %d %d" mark end)
      ;; Mark non-visible and non-essential text as fontified
      ;; (when (> mark end)
      (put-text-property mark (point-max) 'fontified 'defer)
      ;;(setq mark end))

      (put-text-property pos mark 'fontified state))
    ))

(defun archive-get-image ()
  (let* ((name (get-text-property (point) 'file))
         (j (archive-get-lineno))
         (buf (archive-extract-by-index j))
         (img (create-image (with-current-buffer buf
                              (encode-coding-string (buffer-string) 'utf-8))
                            (image-type name)
                            t
                            :max-width 100)))
    img))

(defun font-lock-image (start end &optional loudly)
  (while (< start end)
    (let* (fn img next)
      (setq next (next-single-property-change start 'image-function))
      (if (or (not next) (> next end))
          (setq start end)
        (setq start next)
        (setq fn (get-text-property start 'image-function))
        (goto-char start)
        (setq img (funcall fn))
        (add-text-properties (point) (1+ (point))
                             `(display ,img image-function nil))
        )))
  start)

(defun semantic-fontify-buffer ()
  "Fontify semantic buffer.
By default, only visible text is fontified. This function ensures
complete buffer is fontified."
  ;;  (semantic-fontify (window-start)))
  (with-silent-modifications
    ;; Refontify till the end of screen. This takes care of single
    ;; line tags like imports on subsequent screens.
    (put-text-property (point-min) (point-max) 'fontified nil)))
  ;; (let* ((pos (point-min)))
  ;;   (while (< pos (point-max))
  ;;     (setq pos (semantic-fontify-region pos (point-max))))))

(defun semantic-fontify-tag (&optional tag start end prefix)
  "Fontify semantic TAG.
START and END indicate viewport extent."
  (let* ((tag (or tag (semantic-current-tag)))
         (inhibit-modification-hooks t)
         (case-fold-search nil)
         (lisp (derived-mode-p 'emacs-lisp-mode))
	 pos1 pos2 len comp face str)
    (save-excursion
      (if (and (semantic-tag-p tag)
               (semantic-tag-with-position-p tag))
          (let* ((class (semantic-tag-class tag))
	         (type (semantic-tag-type tag))
	         (name (semantic-tag-name tag))
                 (members nil)
                 (elist nil))
	    (setq start (or start
                            (semantic-tag-start tag))
	          end (or end
                          (semantic-tag-end tag)))
            (goto-char start)

	    (when (and type (not semantic-font-lock-types))
              (cond ((semantic-tag-p type)
	             (push (cons (semantic-tag-type type) 'keyword) comp))
                    ;; ((member type '("class" "interface" "enum" "struct" "union"))
                    ((semantic-lex-keyword-p type)
                     nil)
                    (t
	             (push (cons type 'type) comp))))

            (push class face)
            (when (and (semantic-tag-static-p tag)
                       (not (eq class 'type)))
              (push 'static face))
            (when (and (semantic-tag-constant-p tag)
                       (eq class 'variable))
              (push 'constant face))
            ;; Don't fontify include if file is not found
            (when (or (not (eq class 'include))
                      (semantic-dependency-tag-file tag))
	      (push (cons name face) comp))
            (setq comp (nreverse comp))

            (setq elist (append (semantic-tag-type-superclasses tag)
                                (semantic-tag-type-parameters tag)
                                (semantic-tag-function-throws tag)
                                (semantic-tag-type-interfaces tag)))
            (mapc #'(lambda (e)
                      (push (cons e 'type) comp))
                  elist)
            (mapc #'(lambda (e)
                      (if (or (not semantic-font-lock-types)
                              (member e semantic-font-lock-types))
                          (push (cons (concat "@" e) 'annotation) comp)))
                  (semantic-tag-type-annotations tag))

	    ;; Fontify tag members
            (setq members (append (semantic-tag-type-members tag)
                                  ;; Performance: This triggers parsing
                                  ;; which can be slow for large
                                  ;; functions
                                  ;; (when (semantic-tag-of-class-p tag 'function)
                                  ;;   (semantic-get-local-variables (1- end)))
			          (semantic-tag-function-arguments tag)))
            ;; Sort is destructive - create a copy
            (setq members (copy-sequence members))
            ;; Sort by name in descending order of length so that
            ;; exact match takes precedence
            (setq members (sort members #'(lambda(a b)
                                            (if (semantic-tag-p a)
                                              (> (length (semantic-tag-name a))
                                                 (length (semantic-tag-name b)))
                                              (> (length a)
                                                 (length b))))))
	    (dolist (m members)
              (if (or (member (semantic-tag-type tag)
                              '("class" "interface" "struct" "union"))
                      (semantic-tag-of-class-p tag 'function))
                  ;; Members are restricted to tag start and end
                  (unless (or (< end (semantic-tag-start tag))
                              (> start (semantic-tag-end tag)))
	            (semantic-fontify-tag m
                                          (max start (semantic-tag-start tag))
                                          (min end (semantic-tag-end tag))
                                          name))
	        (semantic-fontify-tag m
                                      start end
                                      name))))
        (unless (semantic-tag-p tag)
          (setq comp (list (cons tag 'variable)))))

      ;; Fontify tag
      (dolist (m comp)
	(setq pos2 (goto-char start)
              face (if (listp (cdr m))
                       ;; Superimpose modifiers eg. static constant
                       (mapcar #'(lambda (f)
                                   (cdr-safe (assoc f semantic-format-face-alist)))
                               (cdr m))
                     (cdr-safe (assoc (cdr m) semantic-format-face-alist)))
	      len (length (car m)))
        ;; Fontify tag occurences from start to end
        ;; Don't fontify if it's already fontified
        ;; Don't add these to keywords as scope is important here
        ;; Avoid substring match. But take care of special cases like
        ;; File[], same named variable and functions, static ref eg. T.fun()
	(setq str (concat "\\(" prefix "\\.\\)?"
                          "\\_<\\("
                          ;; Handle multi-level generics
                          (regexp-quote (if (listp (car m))
                                            (caar m)
                                          (car m)))
                          "\\)\\_>"
                          (when (and (not lisp)
                                     (or (and (listp (cdr m))
                                              (member 'function (cdr m)))
                                         (eq (cdr m) 'function)))
                            "\\s-*(")))
        ;; (message "%s" str)
        (while (and pos2 (< pos2 end))
          (setq pos2 (re-search-forward str end t))
	  (when (and pos2
                     (not (get-text-property (match-beginning 2) 'face)))
            (setq pos1 (match-beginning 2)
                  pos2 (match-end 2))
	    (put-text-property
             pos1 pos2 'face face))
	  ))
      )))

;; Documentation handler
;;
(defsubst semantic-java-skip-spaces-backward ()
  "Move point backward, skipping Java whitespaces."
  (skip-chars-backward " \n\r\t"))

(defsubst semantic-java-skip-spaces-forward ()
  "Move point forward, skipping Java whitespaces."
  (skip-chars-forward " \n\r\t"))

(define-mode-local-override semantic-documentation-for-tag
  java-mode (&optional tag nosnarf)
  "Find documentation from TAG and return it as a clean string.
Java have documentation set in a comment preceding TAG's definition.
Attempt to strip out comment syntactic sugar, unless optional argument
NOSNARF is non-nil.
If NOSNARF is 'lex, then return the semantic lex token."
  (when (and (or tag (setq tag (semantic-current-tag)))
             (not (semantic-tag-faux-p tag)))
    (with-current-buffer (semantic-tag-buffer tag)
      (save-excursion
        ;; Move the point at token start
        (goto-char (semantic-tag-start tag))
        (semantic-java-skip-spaces-forward)
        ;; If the point already at "/**" (this occurs after a doc fix)
        (if (looking-at "/\\*\\*")
            nil
          ;; Skip previous spaces
          (semantic-java-skip-spaces-backward)
          ;; Ensure point is after "*/" (javadoc block comment end)
          (condition-case nil
              (backward-char 2)
            (error nil))
          (when (looking-at "\\*/")
            ;; Move the point backward across the comment
            (forward-char 2)              ; return just after "*/"
            (forward-comment -1)          ; to skip the entire block
            ))
        ;; Verify the point is at "/**" (javadoc block comment start)
        (if (looking-at "/\\*\\*")
            (let ((p (point))
                  (c (semantic-doc-snarf-comment-for-tag 'lex)))
              (when c
                ;; Verify that the token just following the doc
                ;; comment is the current one!
                (goto-char (semantic-lex-token-end c))
                (semantic-java-skip-spaces-forward)
                (when (eq tag (semantic-current-tag))
                  (goto-char p)
                  (semantic-doc-snarf-comment-for-tag nosnarf)))))
        ))))

;;; Javadoc facilities
;;

;; Javadoc elements
;;
(defvar semantic-java-doc-line-tags nil
  "Valid javadoc line tags.
Ordered following Sun's Tag Convention at
<http://java.sun.com/products/jdk/javadoc/writingdoccomments/index.html>")

(defvar semantic-java-doc-with-name-tags nil
  "Javadoc tags which have a name.")

(defvar semantic-java-doc-with-ref-tags nil
  "Javadoc tags which have a reference.")

;; Optional javadoc tags by classes of semantic tag
;;
(defvar semantic-java-doc-extra-type-tags nil
  "Optional tags used in class/interface documentation.
Ordered following Sun's Tag Convention.")

(defvar semantic-java-doc-extra-function-tags nil
  "Optional tags used in method/constructor documentation.
Ordered following Sun's Tag Convention.")

(defvar semantic-java-doc-extra-variable-tags nil
  "Optional tags used in field documentation.
Ordered following Sun's Tag Convention.")

;; All javadoc tags by classes of semantic tag
;;
(defvar semantic-java-doc-type-tags nil
  "Tags allowed in class/interface documentation.
Ordered following Sun's Tag Convention.")

(defvar semantic-java-doc-function-tags nil
  "Tags allowed in method/constructor documentation.
Ordered following Sun's Tag Convention.")

(defvar semantic-java-doc-variable-tags nil
  "Tags allowed in field documentation.
Ordered following Sun's Tag Convention.")

;; Access to Javadoc elements
;;
(defmacro semantic-java-doc-tag (name)
  "Return doc tag from NAME.
That is @NAME."
  `(concat "@" ,name))

(defsubst semantic-java-doc-tag-name (tag)
  "Return name of the doc TAG symbol.
That is TAG `symbol-name' without the leading `@'."
  (substring (symbol-name tag) 1))

(defun semantic-java-doc-keyword-before-p (k1 k2)
  "Return non-nil if javadoc keyword K1 is before K2."
  (let* ((t1   (semantic-java-doc-tag k1))
         (t2   (semantic-java-doc-tag k2))
         (seq1 (and (semantic-lex-keyword-p t1)
                    (plist-get (semantic-lex-keyword-get t1 'javadoc)
                               'seq)))
         (seq2 (and (semantic-lex-keyword-p t2)
                    (plist-get (semantic-lex-keyword-get t2 'javadoc)
                               'seq))))
    (if (and (numberp seq1) (numberp seq2))
        (<= seq1 seq2)
      ;; Unknown tags (probably custom ones) are always after official
      ;; ones and are not themselves ordered.
      (or (numberp seq1)
          (and (not seq1) (not seq2))))))

(defun semantic-java-doc-keywords-map (fun &optional property)
  "Run function FUN for each javadoc keyword.
Return the list of FUN results.  If optional PROPERTY is non-nil only
call FUN for javadoc keywords which have a value for PROPERTY.  FUN
receives two arguments: the javadoc keyword and its associated
'javadoc property list.  It can return any value.  All nil values are
removed from the result list."
  (delq nil
        (mapcar
         #'(lambda (k)
             (let* ((tag   (semantic-java-doc-tag k))
                    (plist (semantic-lex-keyword-get tag 'javadoc)))
               (if (or (not property) (plist-get plist property))
                   (funcall fun k plist))))
         semantic-java-doc-line-tags)))


;;; Mode setup
;;

(defun semantic-java-doc-setup ()
  "Lazy initialization of javadoc elements."
  (or semantic-java-doc-line-tags
      (setq semantic-java-doc-line-tags
            (sort (mapcar #'semantic-java-doc-tag-name
                          (semantic-lex-keywords 'javadoc))
                  #'semantic-java-doc-keyword-before-p)))

  (or semantic-java-doc-with-name-tags
      (setq semantic-java-doc-with-name-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 k)
             'with-name)))

  (or semantic-java-doc-with-ref-tags
      (setq semantic-java-doc-with-ref-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 k)
             'with-ref)))

  (or semantic-java-doc-extra-type-tags
      (setq semantic-java-doc-extra-type-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 (if (memq 'type (plist-get p 'usage))
                     k))
             'opt)))

  (or semantic-java-doc-extra-function-tags
      (setq semantic-java-doc-extra-function-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 (if (memq 'function (plist-get p 'usage))
                     k))
             'opt)))

  (or semantic-java-doc-extra-variable-tags
      (setq semantic-java-doc-extra-variable-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 (if (memq 'variable (plist-get p 'usage))
                     k))
             'opt)))

  (or semantic-java-doc-type-tags
      (setq semantic-java-doc-type-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 (if (memq 'type (plist-get p 'usage))
                     k)))))

  (or semantic-java-doc-function-tags
      (setq semantic-java-doc-function-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 (if (memq 'function (plist-get p 'usage))
                     k)))))

  (or semantic-java-doc-variable-tags
      (setq semantic-java-doc-variable-tags
            (semantic-java-doc-keywords-map
             #'(lambda (k p)
                 (if (memq 'variable (plist-get p 'usage))
                     k)))))

  )

(provide 'semantic/java)

;; Local variables:
;; generated-autoload-load-name: "semantic/java"
;; End:

;;; semantic/java.el ends here
