;;; jit.el --- JIT support for Emacs  -*- lexical-binding:t -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: jit

;; This file is not part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; JIT or lazy loading support for Emacs.
;; Add a (lambda (start end)) to `jit-functions' hook.

(require 'shr)

(defvar jit-functions nil
  "Abnormal hook for JIT functions.")

(defvar jit-delay 0.5
  "Idle Timer delay.")


(defvar-local jit--change-start nil
  "Internal variable for tracking change bounds.")

(defvar-local jit--change-end nil
  "Internal variable for tracking change bounds.")

(defvar jit--timer nil
  "Internal variable for scheduling fontification.")

(defvar jit--pending-functions nil
  "Internal variable for tracking pending functions.")


(defun jit-after-change-function (beg end _len)
  "Used for `after-change-functions'"
  ;; Undo actions typically result in multiple invocations. Let's club
  ;; them together.
  (save-current-buffer
    ;; (message "After change %s %d %d %d" (buffer-name) beg end len)
    (setq jit--change-start
          (if jit--change-start (min jit--change-start beg) beg)
          jit--change-end
          (if jit--change-end (max jit--change-end end) end))
    ))

(defun jit-timer ()
  "Run a single instance of timer function.
Restart timer after changing `jit-delay'."
  (interactive)
  (when jit--timer
    (cancel-timer jit--timer))
  (setq jit--timer
        (run-with-idle-timer jit-delay t #'jit--timer-function)
        ))

(defun jit--timer-function ()
  "Mark the unfontified region in the visible window."
  (let (end-change start-change start-win end-win fn)
    (and jit--change-start jit--change-end
         (or (/= jit--change-start jit--change-end)
             ;; Make sure we change at least one char
             (setq jit--change-end (1+ jit--change-end)))
         (setq jit--change-end (min jit--change-end (point-max)))
         ;; (message "Fontify mark changes %s - %s, %s"
         ;;          jit--change-start jit--change-end (buffer-name))
         (with-silent-modifications
           (put-text-property jit--change-start jit--change-end
                              'fontified 'defer)))
    (setq jit--change-start nil
          jit--change-end   nil)

    ;; If the last redisplay of window was preempted, and did not
    ;; finish, Emacs does not know the position of the end of display
    ;; in that window. In that case, (window-end) returns nil.
    (and (boundp 'jit-functions)
         jit-functions
         (setq end-win (window-end))
         (setq start-win (window-start))
         (setq start-change (or (text-property-any start-win end-win 'fontified nil)
                                (text-property-any start-win end-win 'fontified 'defer)))
         (setq end-change (next-single-property-change
                           start-change 'fontified nil end-win))
         ;; (message "Fontify JIT %s - %s, %s" start-change end-change (buffer-name))
         (save-excursion
           (with-silent-modifications
             ;; Refontify till the end of screen. This takes care of single
             ;; line tags like imports on subsequent screens.
             (unless jit--pending-functions
               (setq jit--pending-functions jit-functions))
             (while (and jit--pending-functions
                         (not (input-pending-p)))
               (setq fn (pop jit--pending-functions))
               ;; No need to run global hooks (t)
               (if (and fn (not (eq fn t)))
                   (condition-case err
                       (funcall fn start-change end-change start-win end-win)
                     (error (message "Fontify failed: %s %s" fn  err)))))
             (unless jit--pending-functions
               ;; (message "Fontify JIT done %s - %s, %s" start-change end-change
               ;;          (buffer-name))
               ;; Moving buffers like *Messages* resizes on the fly
               (put-text-property start-change
                                  (min end-change (point-max))
                                  'fontified t))
             ))
         )))

(defun jit--common-activate ()
  ;; Disable Emacs fontification via fontification-functions
  (setq fontification-functions nil)
  (with-silent-modifications
    (put-text-property (point-min) (point-max) 'fontified 'defer))

  (add-hook 'after-change-functions 'jit-after-change-function nil t)
  (add-hook 'jit-functions 'jit-font-lock-face nil t)
  (jit-timer))

(defun jit--common-deactivate ()
  (remove-hook 'after-change-functions 'jit-after-change-function t)
  (remove-hook 'jit-functions 'jit-font-lock-face t)
  (with-silent-modifications
    (remove-text-properties (point-min) (point-max) '(face)))

  ;; Need to keep timer running for other buffers
  ;; (when (and jit--timer
  ;;            (not jit-functions))
  ;;   (cancel-timer jit--timer))
  )


;; Image Functions
(defun jit--archive-get-image ()
  "Replace placeholders with file extracted from an archive."
  (let* ((name (get-text-property (point) 'file))
         (w (get-text-property (point) 'width))
         (j (archive-get-lineno))
         (buf (archive-extract-by-index j))
         (img (create-image (with-current-buffer buf
                              (encode-coding-string (buffer-string) 'utf-8))
                            (image-type name)
                            t
                            :max-width w)))
    img))

(defun jit--file-get-image ()
  "Replace placeholders with `file' property value."
  (let* ((name (get-text-property (point) 'file))
         (buf (find-file-noselect name))
         (w (get-text-property (point) 'width))
         (img (create-image (with-current-buffer buf
                              (encode-coding-string (buffer-string) 'utf-8))
                            (image-type name)
                            t
                            :max-width w)))
    img))

(defun jit--url-get-image ()
  "Replace placeholders with `url' property value."
  (let* ((url (get-text-property (point) 'url))
         (img-data (shr-get-image-data url))
         (data (car img-data))
         (type (cadr img-data))
         (w (get-text-property (point) 'width))
         img)
    (unless data
      ;; Image not available in cache. Fetch synchronously.
      (with-current-buffer (url-retrieve-synchronously url)
        (goto-char (point-min))
        (when (re-search-forward "\r?\n\r?\n" nil t)
	  (setq img-data (shr-parse-image-data)
                data (car img-data)
                type (cadr img-data))
          )))
    ;; Return nil when there's an error in fetching. This will keep
    ;; the placeholder and avoid image error by Emacs.
    (if data
        (setq img (create-image data
                                (shr--image-type)
                                t
                                :margin 5
                                :format type
                                :max-width w)))
    img))

(defun jit-image-load (start end _start-win _end-win)
  "Replace placeholders with `image-function' property value."
  (while (< start end)
    (let* (fn img next w)
      (setq fn (get-text-property start 'image-function))
      (setq next (next-single-property-change start 'image-function))
      (if (or (not next) (> next end))
          (setq next end))
      (goto-char start)
      (when fn
        (setq img (or (funcall fn)
                      ;; Failure indicator
                      (progn
                        (setq w (get-text-property (point) 'width))
                        (image-svg-icon '("bi file-x") w w))))
        (add-text-properties (point) (1+ (point))
                             `(display ,img image-function nil)))
      (setq start next)
      ))
  start)

(defun jit-font-lock-face (start end _start-win _end-win)
  "Replace `face' with `font-lock-face' property value."
  (while (< start end)
    (let* (fn next)
      (setq fn (get-text-property start 'font-lock-face))
      (setq next (next-single-property-change start 'font-lock-face))
      (if (or (not next) (> next end))
          (setq next end))
      (goto-char start)
      (when fn
        (add-text-properties (point) next `(face ,fn)))
      (setq start next)
      ))
  start)

;; Helper API functions
(defun jit--insert-image (val &optional img fn key w)
  (if (not (fboundp 'jit-image-mode))
      (insert-image img)
    ;; JIT image - use placeholder
    (setq w (or w 100))
    (insert-image (image-svg-icon '("bi image") w w))
    (add-text-properties (1- (point)) (point)
			 `(image-function ,fn ,key ,val width ,w))
    ))

(defun jit--image-fetched (status buffer)
  "Cache successful results."
  (when (and (setq buffer (current-buffer))
             (buffer-name buffer)
	     (not (plist-get status :error)))
    (url-store-in-cache buffer)
    (kill-buffer)))

(defun jit-insert-image-from-file (file &optional img w)
  (jit--insert-image file img 'jit--file-get-image 'file w))

(defun jit-insert-image-from-url (url &optional img w)
  (jit--insert-image url img 'jit--url-get-image 'url w)
  (url-queue-retrieve
   (shr-encode-url url) 'jit--image-fetched
   (list (current-buffer))
   t
   (not (shr--use-cookies-p url shr-base))))

(defun jit-insert-image-from-archive (file &optional img w)
  (jit--insert-image file img 'jit--archive-get-image 'file w))

;;;###autoload
(define-minor-mode semantic-font-lock-mode
  "Semantic font lock minor mode"
  :lighter ""
  (if (and (featurep 'semantic) (semantic-active-p)
           ;; define-minor-mode highlighting doesn't work because it's
           ;; identified as code class
           ;(not (derived-mode-p 'emacs-lisp-mode 'lisp-mode))
           )
      (progn
        ;; Add keywords from language grammar
        (let (face keywords)
          (setq face (cdr-safe (assoc 'keyword semantic-format-face-alist))
                keywords (mapcar 'symbol-name (semantic-lex-keywords))
                font-lock-keywords (list (cons
                                          (regexp-opt keywords 'symbols)
                                          face)))
          ;; Unless font-lock-keywords or font-lock-defaults is set by major
          ;; mode, this will trigger font-lock-mode
          (font-lock-add-keywords nil font-lock-keywords)
          (when (derived-mode-p 'java-mode)
            (font-lock-add-keywords nil javadoc-font-lock-keywords)))
        ;; Post parsing hooks
        (add-hook 'semantic-after-toplevel-cache-change-hook 'semantic-fl-update-keywords nil t)
        (add-hook 'semantic-after-partial-cache-change-hook 'semantic-fl-update-keywords nil t)
        ;; semantic-fontify-region is added as a local hook. It cannot
        ;; work as a global hook. It'll give spurious errors in non
        ;; semantic buffers.
        (add-hook 'jit-functions 'semantic-fontify-region nil t))
    (remove-hook 'jit-functions 'semantic-fontify-region t)
    (remove-hook 'semantic-after-toplevel-cache-change-hook 'semantic-fl-update-keywords t)
    (remove-hook 'semantic-after-partial-cache-change-hook 'semantic-fl-update-keywords t)
    ))

;;;###autoload
(define-minor-mode jit-image-mode
  "JIT Image minor mode"
  :lighter ""
  (if jit-image-mode
      (add-hook 'jit-functions 'jit-image-load nil t)
    (remove-hook 'jit-functions 'jit-image-load t)
    ))

;;;###autoload
(define-minor-mode jit-mode
  "JIT minor mode"
  :lighter (:eval (format " JIT[%s%s]"
                          (if semantic-font-lock-mode "s" "")
                          (if jit-image-mode "i" "")))
  (if jit-mode
      (progn
        (jit--common-activate)
        (jit-image-mode 1)
        (semantic-font-lock-mode 1))
    (jit--common-deactivate)
    (jit-image-mode -1)
    (semantic-font-lock-mode -1)))

(provide 'jit)
