;; Copyright (C) 1999-2020 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

(require 'semantic/wisent/php-wy)

(defvar php-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; (modify-syntax-entry ?. "w" table)
    (modify-syntax-entry ?= "." table)
    (modify-syntax-entry ?- "." table)
    (modify-syntax-entry ?> "." table)
    table)
  "Syntax table for PHP files.")

(define-derived-mode php-mode prog-mode "PHP"
  "Major mode for editing PHP code."
  ;; (set (make-local-variable 'comment-start) "# ")
  ;; (set (make-local-variable 'comment-start-skip) "#+\\s-*")
  (set (make-local-variable 'semantic-typesafe-lang) nil)
)

;;;###autoload
(defun semantic-default-php-setup ()
  "Set up a buffer for semantic parsing of the C language."
  ;; (semantic-c-by--install-parser)
  (wisent-php-wy--install-parser)
  (setq semantic-lex-syntax-modifications '(
                                            ;; (?> ")<")
                                            ;; (?< "(>")
                                            (?\\ ".")
                                            )
        semantic-lex-analyzer 'wisent-php-lexer
        ;; semantic-lex-syntax-table (syntax-table)
        semantic-lex-comment-regex "(//|#|/*|*/)"
        semantic-type-relation-separator-character '("->" "::")
        semantic-typesafe-lang nil
        )
  )

(define-mode-local-override semantic-ctxt-current-symbol php-mode (&optional point)
  "List the symbol under point."
  (let ((res (semantic-ctxt-current-symbol-default point)))
    (setcar res (string-remove-prefix "$" (car res)))
    res))

(provide 'semantic/php)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "semantic/c"
;; End:
