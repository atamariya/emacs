;;; semantic/html.el --- Semantic details for html files

;; Copyright (C) 2004-2005, 2007-2020 Free Software Foundation, Inc.

;; Author: Eric M. Ludlam <zappo@gnu.org>
;;         Anand Tamariya <atamariya@gmail.com>
;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Parse HTML files and organize them in a nice way.
;; Pay attention to anchors, including them in the tag list.
;;
;; Copied from the original semantic-texi.el.
;;
;; ToDo: Find <script> tags, and parse the contents in other
;; parsers, such as javascript, php, shtml, or others.

;;; Code:

(require 'semantic)
(require 'semantic/format)
(require 'sgml-mode)

(defvar semantic-command-separation-character)

(defvar semantic-html-super-regex
  "<\\(h[1-9]\\|title\\|script\\|body\\|a +href\\)\\>"
  "Regular expression used to find special sections in an HTML file.")

(defvar semantic-html-section-list
  '(("title" 1)
    ("script" 1)
    ("body" 1)
    ("a" 11)
    ("h1" 2)
    ("h2" 3)
    ("h3" 4)
    ("h4" 5)
    ("h5" 6)
    ("h6" 7)
    ("h7" 8)
    ("h8" 9)
    ("h9" 10)
    )
  "Alist of sectioning commands and their relative level.")

(define-mode-local-override semantic-parse-region
  html-mode (&rest ignore)
  "Parse the current html buffer for semantic tags.
IGNORE any arguments.  Always parse the whole buffer.
Each tag returned is of the form:
 (\"NAME\" section (:members CHILDREN))
or
 (\"NAME\" anchor)"
  (let ((entry (cl-assoc-if #'derived-mode-p semantic-new-buffer-setup-functions))
        tag)
    (save-excursion
      (setq tag (apply 'append
                       (mapcar 'semantic-html-expand-tag
	                       (semantic-html-parse-headings)))))
    ;; Need to reset html setup
    (when entry
      (funcall (cdr entry)))
    tag))

(defun semantic-html-expand-tag (tag)
  "Expand the HTML tag TAG."
  (let ((type (semantic-tag-class tag))
        (tag (car (semantic--tag-expand tag)))
        (start (semantic-tag-start tag))
        (end (semantic-tag-end tag))
        (semantic-tag-expand-function nil)
        match)
    (cond ((setq match (assoc type '((script . js-mode)
                                     (style . css-mode)
                                     )))
           (save-excursion
             ;; Skip HTML comment
             (forward-comment 1)
             (setq tag (semantic-parse-region (point) end))))
          ;; ((eq type 'class)
          ;;  ;; Split multiple classes into individual tags
          ;;  (setq tag
          ;;        (mapcar #'(lambda (n)
          ;;                    (car
          ;;                     (semantic--tag-expand
          ;;                      (semantic-html-new-tag n 'class
          ;;                                             nil
          ;;                                             start
          ;;                                             end))))
          ;;                (split-string (semantic-tag-name tag) " "))))
          (t
           (setq tag (list tag))))
    ))

(defun semantic-html-expand-tag1 (tag)
  "Expand the HTML tag TAG."
  (let ((chil (semantic-html-components tag)))
    (if chil
        (semantic-tag-put-attribute
         tag :members (mapcar 'semantic-html-expand-tag chil)))
    (car (semantic--tag-expand tag))))

(defun semantic-html-components (tag)
  "Return components belonging to TAG."
  (semantic-tag-get-attribute tag :members))

(defun semantic-html-gen-tag (dom)
  (let (pos tag tags content src start move)
    (setq pos (point))
    (forward-sexp)
    ;; For HTML comments, we end up here. Skip one char.
    (if (looking-at ">") (forward-char))
    (setq content (nth 2 dom)
          tag (dom-tag dom)
          move t
          src (dom-attr dom 'src))
    (cond
     ((eq tag 'comment)
      (setq move nil)
      (re-search-forward "-->" nil t)
      ;; (forward-sexp -1)
      )
     ((or (and (eq tag 'script) src)
          (and (eq tag 'link)
               (string= (dom-attr dom 'rel) "stylesheet")
               (setq src (dom-attr dom 'href))))
      (setq start pos)
      (sgml-skip-tag-forward 1)
      (if (not (sgml-looking-back-at (concat (symbol-name tag) ">")))
          (sgml-skip-tag-backward 1))
      (setq pos (point)
            move nil
            tag (semantic-html-new-tag src 'include nil
                                       start pos
                                       ))
      (push tag tags)
      (semantic--tag-put-property tag
                                  'dependency-file
                                  src))

      ((and content (memq tag '(script style)))
       (setq start (point))
       (semantic-cache-data-to-buffer
        (current-buffer)
        start (+ start (length content))
        (if (eq tag 'style) 'css-mode 'js-mode)
        'mode)
       (push (semantic-html-new-tag "" tag content
                                    start
                                    (+ start (length content)))
             tags))
      ((memq tag '(top html head))
       (dolist (node (dom-children dom))
         (setq tags (append tags (semantic-html-gen-tag node))))
       )
      ((eq tag 'body)
       (while (re-search-forward " \\(id\\|class\\)=\"\\([[:alnum:]-_ ]*\\)\""
                                 nil t)
         (let* ((class (match-string 1))
                (names (match-string 2)))
           (setq start (- (point) (length names) 1))
           (goto-char start)
           (setq tags (append tags
                              (mapcar (lambda (name)
                                        (search-forward name)
                                        (setq start (point))
                                        (semantic-html-new-tag
                                         name
                                         (if (string= class "id")
                                             'identifier 'class)
                                         nil
                                         (- start (length name)) start))
                                      (split-string names))))
           ))
       ))
    (when move
      ;; Move to the end of the tag
      (goto-char pos)
      (sgml-skip-tag-forward 1))
    ;; (message "%s" tags)
    tags))

(defun semantic-html-parse-headings ()
  "Parse the current html buffer for all semantic tags."
  (let* ((dom (libxml-parse-html-region (point-min) (point-max)))
         )
    (goto-char (point-min))
    (if (looking-at "<!doctype")
        (forward-line))
    (semantic-html-gen-tag dom)))

(defun semantic-html-parse-headings2 ()
  "Parse the current html buffer for all semantic tags."
  (let* ((dom (libxml-parse-html-region (point-min) (point-max)))
         (style (dom-by-tag dom 'style))
         (script (dom-by-tag dom 'script))
         (link (dom-by-tag dom 'link))
         (class (dom-by-class dom ""))
         (ids (dom-by-id dom ""))
         pos tags
         )
    (setq pos (point-min))
    (setq tags
          (mapcar #'(lambda (n)
                      (goto-char pos)
                      (let* ((name (nth 2 n)))
                        (setq pos (re-search-forward name))
                        (semantic-cache-data-to-buffer
                         (current-buffer)
                         (- pos (length name)) pos 'css-mode 'mode)
                        (semantic-html-new-tag "" 'style (nth 2 n)
                                               (- pos (length name))
                                               pos)))
                  style))

    (setq pos (point-min))
    (setq tags
          (append tags
                  (mapcar #'(lambda (n)
                              (goto-char pos)
                              (let* ((name (nth 2 n))
                                     (src (cdr (assoc 'src (nth 1 n))))
                                     tag start)
                                (if src
                                    (progn
                                      (re-search-forward src)
                                      (sgml-skip-tag-backward 1)
                                      (setq start (point))
                                      (sgml-skip-tag-forward 1)
                                      (setq pos (point)
                                            tag (semantic-html-new-tag src 'include nil
                                                                       start pos
                                                                       ))
                                      (semantic--tag-put-property tag
                                                                  'dependency-file
                                                                  src))

                                (setq pos (re-search-forward (regexp-quote name)))
                                (semantic-cache-data-to-buffer
                                 (current-buffer)
                                 (- pos (length name)) pos 'js-mode 'mode)
                                (semantic-html-new-tag "" 'script (nth 2 n)
                                                       (- pos (length name))
                                                       pos))))
                          script)))
    (setq pos (point-min))
    (setq tags
          (append tags
                  (mapcar #'(lambda (n)
                              (goto-char pos)
                              (let* ((name (nth 2 n))
                                     (src (cdr (assoc 'href (nth 1 n))))
                                     tag start)
                                (when src
                                  (progn
                                    (re-search-forward src)
                                    (sgml-skip-tag-backward 1)
                                    (setq start (point))
                                    (sgml-skip-tag-forward 1)
                                    (setq pos (point)
                                          tag (semantic-html-new-tag src 'include nil
                                                                     start pos
                                                                     ))
                                    (semantic--tag-put-property tag
                                                                'dependency-file
                                                                src))
                                  )))
                          link)))

    (setq pos (point-min))
    (setq tags
          (append tags
                  (mapcar #'(lambda (n)
                              (goto-char pos)
                              (let* ((name (cdr (assoc 'class (nth 1 n)))))
                                (when (not (string-empty-p name))
                                ;; classes exist within a tag
                                (while (not (and
                                             (setq pos (re-search-forward name))
                                             (sgml-beginning-of-tag))))
                                (semantic-html-new-tag name
                                                       'class
                                                       nil
                                                       (- pos (length name))
                                                       pos))))
                          class)))
    (setq pos (point-min))
    (setq tags
          (append tags
                  (mapcar #'(lambda (n)
                              (goto-char pos)
                              (let* ((name (cdr (assoc 'id (nth 1 n)))))
                                (when (not (string-empty-p name))
                                ;; ids exist within a tag
                                (while (not (and
                                             (setq pos (re-search-forward name))
                                             (sgml-beginning-of-tag))))
                                (semantic-html-new-tag name
                                                       'identifier
                                                       nil
                                                       (- pos (length name))
                                                       pos))))
                          ids)))
    (remove nil tags)))

(defsubst semantic-html-new-tag (name type &optional code start end)
  "Create a semantic tag of class section.
NAME is the name of this section.
MEMBERS is a list of semantic tags representing the elements that make
up this section.
LEVEL is the leveling level.
START and END define the location of data described by the tag."
  (append (semantic-tag name type :detail code)
	  (list start end)))

(define-mode-local-override semantic-ctxt-current-class-list
  html-mode (&optional point)
  "Return a list of tag classes that are allowed at POINT.
Assume a functional typed language.  Uses very simple rules."
  (save-excursion
    (if point (goto-char point))

    (let* ((tag (semantic-current-tag))
          (sym (car (nreverse (semantic-ctxt-current-symbol))))
          (point (or point (point)))
          (mode (semantic-get-cache-data 'mode point)))
      ;; CSS section
      ;; JS section
      ;; HTML class
      (cond ((string= "id" (car (semantic-ctxt-current-assignment)))
	     '(css-identifier))
            ((string= "class" (car (semantic-ctxt-current-assignment)))
	     '(css-class))
           ((looking-back (concat "<" sym) (- point (length sym) 1))
	     '(tag))
	   ((sgml-beginning-of-tag)
            '(attribute))
           ((and (looking-back (concat "\\." sym) (- point (length sym) 1))
                 (eq mode 'css-mode))
	    '(class))
           ((and (looking-back (concat "#" sym) (- point (length sym) 1))
                 (eq mode 'css-mode))
	    '(identifier))
           ((eq mode 'js-mode)
            '(function variable type))
           (t nil))
      )))

(defun semantic-html-parse-headings1 ()
  "Parse the current html buffer for all semantic tags."
  (let ((pass1 nil))
    ;; First search and snarf.
    (save-excursion
      (goto-char (point-min))

      (let ((semantic--progress-reporter
	     (make-progress-reporter
	      (format "Parsing %s..."
		      (file-name-nondirectory buffer-file-name))
	      (point-min) (point-max))))
	(while (re-search-forward semantic-html-super-regex nil t)
	  (setq pass1 (cons (match-beginning 0) pass1))
	  (progress-reporter-update semantic--progress-reporter (point)))
	(progress-reporter-done semantic--progress-reporter)))

    (setq pass1 (nreverse pass1))
    ;; Now, make some tags while creating a set of children.
    (car (semantic-html-recursive-combobulate-list pass1 0))
    ))

(defun semantic-html-set-endpoint (metataglist pnt)
  "Set the end point of the first section tag in METATAGLIST to PNT.
METATAGLIST is a list of tags in the intermediate tag format used by the
html parser.  PNT is the new point to set."
  (let ((metatag nil))
    (while (and metataglist
		(not (eq (semantic-tag-class (car metataglist)) 'section)))
      (setq metataglist (cdr metataglist)))
    (setq metatag (car metataglist))
    (when metatag
      (setcar (nthcdr (1- (length metatag)) metatag) pnt)
      metatag)))

(defsubst semantic-html-new-section-tag (name members level start end)
  "Create a semantic tag of class section.
NAME is the name of this section.
MEMBERS is a list of semantic tags representing the elements that make
up this section.
LEVEL is the leveling level.
START and END define the location of data described by the tag."
  (let ((anchorp (eq level 11)))
    (append (semantic-tag name
			  (cond (anchorp 'anchor)
				(t 'section))
			  :members members)
	    (list start (if anchorp (point) end)) )))

(defun semantic-html-extract-section-name ()
  "Extract a section name from the current buffer and point.
Assume the cursor is in the tag representing the section we
need the name from."
  (save-excursion
    ; Skip over the HTML tag.
    (forward-sexp -1)
    (forward-char -1)
    (forward-sexp 1)
    (skip-chars-forward "\n\t ")
    (while (looking-at "<")
      (forward-sexp 1)
      (skip-chars-forward "\n\t ")
      )
    (let ((start (point))
	  (end nil))
      (if (re-search-forward "</" nil t)
	  (progn
	    (goto-char (match-beginning 0))
	    (skip-chars-backward " \n\t")
	    (setq end (point))
	    (buffer-substring-no-properties start end))
	""))
    ))

(defun semantic-html-recursive-combobulate-list (sectionlist level)
  "Rearrange SECTIONLIST to be a hierarchical tag list starting at LEVEL.
Return the rearranged new list, with all remaining tags from
SECTIONLIST starting at ELT 2.  Sections not are not dealt with as soon as a
tag with greater section value than LEVEL is found."
  (let ((newl nil)
	(oldl sectionlist)
	(case-fold-search t)
        tag
	)
    (save-excursion
      (catch 'level-jump
	(while oldl
	  (goto-char (car oldl))
	  (if (looking-at "<\\(\\w+\\)")
	      (let* ((word (match-string 1))
		     (levelmatch (assoc-string
                                  word semantic-html-section-list t))
		     text begin tmp
		     )
		(when (not levelmatch)
		  (error "Tag %s matched in regexp but is not in list"
			 word))
		;; Set begin to the right location
		(setq begin (point))
		;; Get out of here if there if we made it that far.
		(if (and levelmatch (<= (car (cdr levelmatch)) level))
		    (progn
		      (when newl
			(semantic-html-set-endpoint newl begin))
		      (throw 'level-jump t)))
		;; When there is a match, the descriptive text
		;; consists of the rest of the line.
		(goto-char (match-end 1))
		(skip-chars-forward " \t")
		(setq text (semantic-html-extract-section-name))
		;; Next, recurse into the body to find the end.
		(setq tmp (semantic-html-recursive-combobulate-list
			   (cdr oldl) (car (cdr levelmatch))))
		;; Build a tag
		(setq tag (semantic-html-new-section-tag
			   text (car tmp) (car (cdr levelmatch)) begin (point-max)))
		;; Before appending the newtag, update the previous tag
		;; if it is a section tag.
		(when newl
		  (semantic-html-set-endpoint newl begin))
		;; Append new tag to our master list.
		(setq newl (cons tag newl))
		;; continue
		(setq oldl (cdr tmp))
		)
	    (error "Problem finding section in semantic/html parser"))
	  ;; (setq oldl (cdr oldl))
	  )))
    ;; Return the list
    (cons (nreverse newl) oldl)))

(define-mode-local-override semantic-sb-tag-children-to-expand
  html-mode (tag)
  "The children TAG expands to."
  (semantic-html-components tag))

;; In semantic/imenu.el, not part of Emacs.
(defvar semantic-imenu-expandable-tag-classes)
(defvar semantic-imenu-bucketize-file)
(defvar semantic-imenu-bucketize-type-members)

(defconst wisent-html-tags-wy--keyword-table
  (semantic-lex-make-keyword-table
   '(("!DOCTYPE" . ABSTRACT)
     ("html" . ABSTRACT)
     ("head" . ABSTRACT)
     ("title" . ABSTRACT)
     ("script" . ABSTRACT)
     ("link" . ABSTRACT)
     ("style" . ABSTRACT)
     ("body" . ABSTRACT)
     ("button" . ABSTRACT)
     ("input" . ABSTRACT)
     ("span" . ABSTRACT)
     )))

;;;###autoload
(defun semantic-default-html-setup ()
  "Set up a buffer for parsing of HTML files."
  ;; Initialize sub-mode parsers
  (dolist (major-mode '(js-mode css-mode))
        (semantic-new-buffer-fcn))
  ;; This will use our parser.
  (setq-mode-local html-mode
        semantic-parser-name "HTML"
        semantic--parse-table t
        semantic-flex-keywords-obarray wisent-html-tags-wy--keyword-table
        imenu-create-index-function 'semantic-create-imenu-index
	semantic-command-separation-character ">"
	semantic-type-relation-separator-character '(":" ".")
	semantic-imenu-expandable-tag-classes '(section)
	semantic-imenu-bucketize-file nil
	semantic-imenu-bucketize-type-members nil
	senator-step-at-start-end-tag-classes '(section)
	senator-step-at-tag-classes '(section)
	semantic-stickyfunc-sticky-classes '(section)
        semantic-typesafe-lang nil
	)
  (setq-mode-local js-mode comment-start "// "
                   comment-start-skip "\\(//+\\|/\\*+\\)\\s *"
                   comment-end "")
  (setq-mode-local css-mode comment-start "// "
                   comment-start-skip "\\(//+\\|/\\*+\\)\\s *"
                   comment-end "")
  (semantic-install-function-overrides
   '((semantic-tag-components . semantic-html-components)
     )
   t)
  (setq semantic-symbol->name-assoc-list '((selector . "CSS")
                                           (css-class . "CSS")
                                           (css-identifier . "CSS")
                                           (type . "JS")
                                           (variable . "JS")
                                           (function . "JS")
                                           (include . "Include")
                                           (identifier . "Identifier")
                                           (class . "Class")
					   ))

  )

;; `html-helper-mode' hasn't been updated since 2004, so it's not very
;; relevant nowadays.
;;(define-child-mode html-helper-mode html-mode
;;  "`html-helper-mode' needs the same semantic support as `html-mode'.")

(provide 'semantic/html)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "semantic/html"
;; End:

;;; semantic/html.el ends here
