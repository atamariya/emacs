;;; semantic/sql.el --- Semantic details for sql files

;; Copyright (C) 2004-2005, 2007-2019 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Basic semantic infrastructre for SQL code completion.
;;

;;; Code:

(require 'semantic)
(require 'semantic/format)
(require 'semantic/analyze/complete)
(require 'semantic/wisent/sql-wy)

(defvar semantic-command-separation-character)
;; Used for code completion
(defvar-mode-local sql-mode semanticdb-implied-include-tags
  '(("insert" statement (:arguments ("table" "columns" "values")))
    ("select" statement (:arguments ("tables")))
    ("update" statement (:arguments ("table" "column = value")))
    ("delete" statement (:arguments ("table")))
    ("drop"   statement (:arguments ("table")))
    ))

(define-mode-local-override semantic-parse-region
  sql-mode (start end &optional nonterminal depth returnonerror)
  "Combines tags from buffer with `sql-semantic-tags'."
  ;; Parse till end of line or semi-colon
  ;; Use sql-semantic-tags for table information and
  ;; semantic--buffer-cache for variable definition
  (let ((res (semantic-parse-region-default start end nonterminal depth returnonerror)))
	;; buf)
    ;; (unless nonterminal
    ;;   (setq buf (sql-find-sqli-buffer)
    ;;         res (append res
    ;;     		(when buf
    ;;     		  (with-current-buffer buf
    ;;     		    (unless sql-completion-object
    ;;     		      (sql-build-completions nil))
    ;;     		    sql-semantic-tags)))))
    res))

;; (define-mode-local-override semantic-parse-changes
;;   sql-mode (&rest ignore)
;;   "Reparse complete buffer."
;;   (semantic-parse-tree-set-needs-rebuild))

(define-mode-local-override semantic-format-tag-prototype
  sql-mode (tag &optional parent color)
  "Return a prototype for TOKEN.
Optional argument PARENT is a parent (containing) item.
Optional argument COLOR indicates that color should be mixed in."
  (let ((name (semantic-tag-name tag))
        (size (semantic-tag-type tag))
        (class (semantic-tag-class tag)))
    (format "%s %s" (upcase (concat name
                                     (when size
                                       (concat " : " size))))
             class)
    ))

(define-mode-local-override semantic-format-tag-type
  sql-mode (tag color)
  "Upcase TAG type name.
Optional argument COLOR indicates that color should be mixed in."
  (upcase (semantic-format-tag-type-default tag color)))

(define-mode-local-override semantic-ctxt-current-class-list
  sql-mode (&optional point)
  "Return a list of tag classes that are allowed at POINT.
Assume a functional typed language.  Uses very simple rules."
  (save-excursion
    (let ((paren (nth 9 (syntax-ppss))))
      (while (not (or (bobp)
                      (bolp)
                      (looking-at-p
                       "\\(from\\|into\\|update\\|select\\|where\\|set\\|values\\)")))
        (forward-word -1))
      ;; (skip-chars-backward " ")
      ;; (forward-char -1)
      (cond ((looking-at-p "\\(into\\|update\\)")
             (if paren
                 '(column)
               '(table)))
            ((looking-at-p "from")
             '(table alias))
            ((looking-at-p "select")
             '(table column alias))
            ((looking-at-p "where")
             '(column alias))
            ((looking-at-p "set")
             '(column))
            ((looking-back "," (- (point) 2))
             '(column table))
            ((looking-back "(" (- (point) 2))
             '(column))
            ((looking-back "[=']" (- (point) 2)) nil)
            ((looking-at-p "values") nil)
            (t
             '(statement)))
      )))

(defvar sematic-sql--statement-regexp
  '("from \\(.*\\)\\( where\\)?"
    "into \\(\\w+\\)[ ]*\\((\\|values\\)?"
    "update \\(\\w+\\)[ ]+\\(set\\)?")
  "Regex matching table name in a statement")

(define-mode-local-override semantic-analyze-current-context
  sql-mode (point)
  "Provide a semantic analysis object describing context."
  (let* ((context-return nil)
	 (prefixandbounds (semantic-ctxt-current-symbol-and-bounds))
	 (prefix (car prefixandbounds))
	 (bounds (nth 2 prefixandbounds))
	 (prefixtypes nil)
	 (prefixclass (semantic-ctxt-current-class-list))
	 (scope (semantic-scope-cache))
         tables tablelist alias tag
	 )
    ;; Find table in scope
    (save-excursion
      (forward-word -1)
      (setq tag (semantic-current-tag)))
    (unless sql-completion-object
      (sql-build-completions nil))
    (setq tables (semantic-tag-get-attribute tag :tables)
          tablelist
	  (remove nil
                  (apply #'append
                         sql-semantic-tags
		         (mapcar #'(lambda(n)
                                     (when (consp n)
                                       (push (semantic-tag (cdr n) 'alias :type (car n)) alias)
                                       (setq n (car n)))
                                     ;; Check DB
                                     (dolist (s sql-completion-object)
                                       (when (and (string-match-p (concat "^" n) s)
                                                  (not (assoc s sql-semantic-tags)))
                                         (sql-build-completions nil s)))

                                     ;; Check local definition
			             (semantic-find-tags-by-class 'table (current-buffer)))
			         tables
			         ))))
    (oset scope scopetypes tablelist)
    (oset scope typescope tablelist)
    (oset scope fullscope (apply #'append
                                 tablelist
                                 (mapcar #'semantic-tag-type-members tablelist)
                                 ;; (semantic-analyze-scoped-tags tablelist scope)
                                 ))
    (oset scope localvar alias)
          ;; '(("a" alias (:type "table1"))))
    ;; (data-debug-show scope)

    ;; Search all tables
    (when prefix
      (catch 'throwsym
        (semantic-analyze-find-tag-sequence prefix scope 'prefixtypes 'throwsym)))

    (setq context-return
	  (semantic-analyze-context
	   :buffer (current-buffer)
	   :scope scope
	   :bounds bounds
	   :prefix prefix
	   :prefixtypes prefixtypes
	   :prefixclass prefixclass
           :errors semantic-analyze-error-stack
	   ))

    context-return))

;; In semantic/imenu.el, not part of Emacs.
(defvar semantic-imenu-expandable-tag-classes)
(defvar semantic-imenu-bucketize-file)
(defvar semantic-imenu-bucketize-type-members)

;;;###autoload
(defun semantic-default-sql-setup ()
  "Set up a buffer for parsing of SQL files."
  (wisent-sql-wy--install-parser)
  (setq-mode-local sql-mode
        semantic-lex-analyzer 'wisent-sql-lexer
	semantic-command-separation-character ";"
	semantic-type-relation-separator-character '(".")
        semantic-typesafe-lang nil
        semantic-lex-comment-regex "--"
	semantic-case-fold t
        semantic-grammar-case-insensitive t

        imenu-create-index-function 'semantic-create-imenu-index
	semantic-symbol->name-assoc-list '((section . "Section")
					   )
	semantic-imenu-expandable-tag-classes '(section)
	semantic-imenu-bucketize-file nil
	semantic-imenu-bucketize-type-members nil
	senator-step-at-start-end-tag-classes '(section)
	senator-step-at-tag-classes '(section)
	semantic-stickyfunc-sticky-classes '(section)
	)
  )

(provide 'semantic/sql)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "semantic/sql"
;; End:

;;; semantic/sql.el ends here
