;;; semantic/css.el --- Semantic details for css files

;; Copyright (C) 2004-2005, 2007-2019 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamariya@gmail.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Parse CSS files and organize them in a nice way.
;; Pay attention to ids and classes.
;;

;;; Code:

(require 'semantic)
(require 'semantic/format)
(require 'css-mode)

(defvar semantic-command-separation-character)

(defvar semantic-css-super-regex
  ;; "\\([#\\.]?\\w+\\)[ \t\n]*{[^}]*}"
  "\\(.*[()]*\\){"
  "Regular expression used to find special sections in an CSS file.")

(define-mode-local-override semantic-parse-region
  css-mode (start end &rest ignore)
  "Parse the current css buffer for semantic tags.
IGNORE any arguments.  Always parse the whole buffer.
Each tag returned is of the form:
 (\"NAME\" section (:members CHILDREN))
or
 (\"NAME\" anchor)"
  (setq semantic-region-start start
        semantic-region-end end)
  (apply 'append
         (mapcar 'semantic-css-expand-tag
	        (semantic-css-parse-headings))))

(defun semantic-css-expand-tag (tag)
  "Expand the CSS tag TAG."
  (let ((type (semantic-tag-class tag))
        (semantic-tag-expand-function nil)
        (tag (car (semantic--tag-expand tag)))
        (start (semantic-tag-start tag))
        (end (semantic-tag-end tag))
        match class name)
    (cond ((setq match (assoc type '((script . js-mode)
                                     ;;(style . css-mode)
                                     )))
           (let ((major-mode (cdr match))
                 ;; (start 33)
                 ;; (end 91)
                 )
             (semantic-new-buffer-fcn)
             (setq tag (semantic-parse-region start end))))
          ((eq type 'selector)
           (setq name (semantic-tag-name tag)
                 class (cond ((string-match "[ >+~]" name)
                              'selector)
                             ((string-match "#\\(\\w+\\)" name)
                              'css-identifier)
                             ((string-match "\\.\\(\\w+\\)" name)
                              'css-class)
                             (t 'selector))
                 tag
                 (mapcar #'(lambda (n)
                             (unless (string-empty-p n)
                               (car
                                (semantic--tag-expand
                                 (semantic-css-new-tag n class nil start end)))))
                         (split-string name ","))
                 tag (remove nil tag)))
          (t
           (setq tag (semantic--tag-expand tag))))
    ;;(mapcar #'semantic--tag-expand tag)
    ))

(defun semantic-css-components (tag)
  "Return components belonging to TAG."
  (semantic-tag-get-attribute tag :members))

(defsubst semantic-css-new-tag (name type &optional code start end)
  "Create a semantic tag of class section.
NAME is the name of this section.
MEMBERS is a list of semantic tags representing the elements that make
up this section.
LEVEL is the leveling level.
START and END define the location of data described by the tag."
  (append (semantic-tag name type :detail code)
	  (list start end)))

(defun semantic-css-parse-headings ()
  "Parse the current css buffer for all semantic tags."
  (let ((pass1 nil))
    ;; First search and snarf.
    (save-excursion
      (goto-char semantic-region-start)

      (let ((semantic--progress-reporter
	     (make-progress-reporter
	      (format "Parsing %s..."
		      (file-name-nondirectory buffer-file-name))
              semantic-region-start semantic-region-end
	      )))
	(while (and (re-search-forward semantic-css-super-regex nil t)
                    (not (nth 4 (syntax-ppss)))
                    (< (point) semantic-region-end))
	;; (while (re-search-forward "\\w[ \t\n]*{[^}]*}" nil t)
	  (setq pass1 (cons (match-beginning 0) pass1))
	  (progress-reporter-update semantic--progress-reporter (point)))
	(progress-reporter-done semantic--progress-reporter)))

    (setq pass1 (nreverse pass1))
    ;; Now, make some tags while creating a set of children.
    (car (semantic-css-recursive-combobulate-list pass1 0))
    ))

(defun semantic-css-set-endpoint (metataglist pnt)
  "Set the end point of the first section tag in METATAGLIST to PNT.
METATAGLIST is a list of tags in the intermediate tag format used by the
css parser.  PNT is the new point to set."
  (let ((metatag nil))
    (while (and metataglist
		(not (eq (semantic-tag-class (car metataglist)) 'section)))
      (setq metataglist (cdr metataglist)))
    (setq metatag (car metataglist))
    (when metatag
      (setcar (nthcdr (1- (length metatag)) metatag) pnt)
      metatag)))

(defsubst semantic-css-new-section-tag (name members level start end)
  "Create a semantic tag of class section.
NAME is the name of this section.
MEMBERS is a list of semantic tags representing the elements that make
up this section.
LEVEL is the leveling level.
START and END define the location of data described by the tag."
  (let ((anchorp (eq level 11)))
    (append (semantic-tag name
			  (cond (anchorp 'class)
                                (anchorp 'identifier)
				(t 'selector))
			  :members members)
	    (list start (if anchorp (point) end)) )))

(defun semantic-css-extract-section-name ()
  "Extract a section name from the current buffer and point.
Assume the cursor is in the tag representing the section we
need the name from."
  (save-excursion
    ; Skip over the CSS tag.
    (forward-sexp -1)
    (forward-char -1)
    (forward-sexp 1)
    (skip-chars-forward "\n\t ")
    (while (looking-at "<")
      (forward-sexp 1)
      (skip-chars-forward "\n\t ")
      )
    (let ((start (point))
	  (end nil))
      (if (re-search-forward "</" nil t)
	  (progn
	    (goto-char (match-beginning 0))
	    (skip-chars-backward " \n\t")
	    (setq end (point))
	    (buffer-substring-no-properties start end))
	""))
    ))

(defun semantic-css-recursive-combobulate-list (sectionlist level)
  "Rearrange SECTIONLIST to be a hierarchical tag list starting at LEVEL.
Return the rearranged new list, with all remaining tags from
SECTIONLIST starting at ELT 2.  Sections not are not dealt with as soon as a
tag with greater section value than LEVEL is found."
  (let ((newl nil)
	(oldl sectionlist)
	(case-fold-search t)
        tag
	)
    (save-excursion
      (catch 'level-jump
	(while oldl
	  (goto-char (car oldl))
	  (if (looking-at semantic-css-super-regex)
	      (let* ((word (match-string 1))
		     (levelmatch nil)
                     ;; (assoc-string
                     ;;              word semantic-css-section-list t))
		     text begin tmp
		     )
                (set-text-properties 0 (length word) nil word)
		;; Set begin to the right location
		(setq begin (point))
		;; Get out of here if there if we made it that far.
		(if (and levelmatch (<= (car (cdr levelmatch)) level))
		    (progn
		      (when newl
			(semantic-css-set-endpoint newl begin))
		      (throw 'level-jump t)))
		;; When there is a match, the descriptive text
		;; consists of the rest of the line.
		(goto-char (match-end 1))
		(skip-chars-forward " \t")
		;; Build a tag
		(setq tag (semantic-css-new-section-tag
			   (string-trim word) (car tmp) (car (cdr levelmatch))
                           begin (+ begin (match-end 0))))
		;; Before appending the newtag, update the previous tag
		;; if it is a section tag.
		;; (when newl
		;;   (semantic-css-set-endpoint newl begin))
		;; Append new tag to our master list.
		(setq newl (cons tag newl))
		;; continue
		(setq oldl (cdr oldl))
		)
	    (error "Problem finding section in semantic/css parser"))
	  ;; (setq oldl (cdr oldl))
	  )))
    ;; Return the list
    (cons (nreverse newl) oldl)))

(define-mode-local-override semantic-sb-tag-children-to-expand
  css-mode (tag)
  "The children TAG expands to."
  (semantic-css-components tag))

(define-mode-local-override semantic-ctxt-current-class-list
  css-mode (&optional point)
  "Return a list of tag classes that are allowed at POINT.
Assume a functional typed language.  Uses very simple rules."
  (save-excursion
    (if point (goto-char point))
    (forward-word -1)
    (let ((tag (semantic-current-tag))
          (point (or point (point))))
      (cond ((looking-back "#" (1- point))
	     '(identifier))
            ((looking-back "\\." (- point 1))
	     '(class))
            ((looking-back "[ <>]" (- point 1))
	     '(tag))
            ((looking-back "[,{]" (- point 1))
	     '(attribute))
            ((looking-back ":" (- point 1))
	     '(attrval))
	    (t nil))
      )))

;; In semantic/imenu.el, not part of Emacs.
(defvar semantic-imenu-expandable-tag-classes)
(defvar semantic-imenu-bucketize-file)
(defvar semantic-imenu-bucketize-type-members)

;;;###autoload
(defun semantic-default-css-setup ()
  "Set up a buffer for parsing of CSS files."
  ;; This will use our parser.
  (setq-mode-local css-mode
        semantic-parser-name "CSS"
        semantic--parse-table t
        semantic-flex-keywords-obarray nil
        imenu-create-index-function 'semantic-create-imenu-index
	semantic-command-separation-character ","
	semantic-type-relation-separator-character '(":")
	semantic-symbol->name-assoc-list '((section . "Section")

					   )
	semantic-imenu-expandable-tag-classes '(section)
	semantic-imenu-bucketize-file nil
	semantic-imenu-bucketize-type-members nil
	senator-step-at-start-end-tag-classes '(section)
	senator-step-at-tag-classes '(section)
	semantic-stickyfunc-sticky-classes '(section)
        completion-at-point-functions '(css-completion-at-point)
	)
  (semantic-install-function-overrides
   '((tag-components . semantic-css-components)
     )
   t)
  )

(provide 'semantic/css)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "semantic/css"
;; End:

;;; semantic/css.el ends here
