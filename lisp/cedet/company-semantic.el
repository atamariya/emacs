;;; company-semantic.el --- company-mode completion backend using Semantic

;; Copyright (C) 2009-2011, 2013-2016  Free Software Foundation, Inc.

;; Author: Nikolaj Schumacher

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:
;;

;;; Code:

(with-eval-after-load
    (require 'company))
(require 'company-template)
(require 'cl-lib)

(defvar semantic-idle-summary-function)
(defvar semantic-typesafe-lang)
(declare-function semantic-documentation-for-tag "semantic/doc" )
(declare-function semantic-analyze-current-context "semantic/analyze")
(declare-function semantic-analyze-possible-completions "semantic/analyzze/complete")
(declare-function semantic-analyze-find-tags-by-prefix "semantic/analyze/fcn")
(declare-function semantic-tag-class "semantic/tag")
(declare-function semantic-tag-name "semantic/tag")
(declare-function semantic-tag-start "semantic/tag")
(declare-function semantic-tag-buffer "semantic/tag")
(declare-function semantic-active-p "semantic")
(declare-function semantic-format-tag-type "semantic/format")
(declare-function semantic-format-tag-prototype "semantic/format")

(defgroup company-semantic nil
  "Completion backend using Semantic."
  :group 'company)

(defcustom company-semantic-metadata-function 'company-semantic-summary-and-doc
  "The function turning a semantic tag into doc information."
  :type 'function)

(defcustom company-semantic-begin-after-member-access t
  "When non-nil, automatic completion will start whenever the current
symbol is preceded by \".\", \"->\" or \"::\", ignoring
`company-minimum-prefix-length'.

If `company-begin-commands' is a list, it should include `c-electric-lt-gt'
and `c-electric-colon', for automatic completion right after \">\" and
\":\"."
  :type 'boolean)
(make-variable-buffer-local 'company-semantic-begin-after-member-access)

(defcustom company-semantic-insert-arguments t
  "When non-nil, insert function arguments as a template after completion."
  :type 'boolean
  :package-version '(company . "0.9.0"))

(defvar company-semantic-modes
  '(c-mode c++-mode jde-mode java-mode js-mode python-mode emacs-lisp-mode
           lisp-interaction-mode html-mode asm-mode sql-mode latex-mode))

(defvar-local company-semantic--current-tags nil
  "Tags for the current context.")

(defun company-semantic-documentation-for-tag (tag)
  (when (semantic-tag-buffer tag)
    ;; When TAG's buffer is unknown, the function below raises an error.
    (semantic-documentation-for-tag tag)))

(defun company-semantic-doc-or-summary (tag)
  (or (company-semantic-documentation-for-tag tag)
      (and (require 'semantic-idle nil t)
           (require 'semantic/idle nil t)
           (funcall semantic-idle-summary-function tag nil t))))

(defun company-semantic-summary-and-doc (tag)
  (let ((doc (company-semantic-documentation-for-tag tag))
        (summary (if tag
                     (funcall semantic-idle-summary-function tag nil t)
                   "Keyword")))
    (and (stringp doc)
         (string-match "\n*\\(.*\\)$" doc)
         (setq doc (match-string 1 doc)))
    (concat summary
            (when doc
                  (if (< (+ (length doc) (length summary) 4) (window-width))
                      " -- "
                    "\n"))
            doc)))

(defun company-semantic-doc-buffer (tag)
  (let ((doc (company-semantic-documentation-for-tag tag)))
    (when doc
      (company-doc-buffer
       (concat (funcall semantic-idle-summary-function tag nil t)
               "\n"
               doc)))))

(defun company-semantic-completions (prefix)
  (ignore-errors
    (let ((completion-ignore-case semantic-grammar-case-insensitive)
          (context (semantic-analyze-current-context)))
      (setq company-semantic--current-tags
            (semantic-analyze-possible-completions context 'no-unique)
            ;; For non-typesafe languages, annotations are aligned to the right
            company-tooltip-align-annotations (not semantic-typesafe-lang))
      (all-completions prefix company-semantic--current-tags))))

(defun company-semantic-completions-raw (prefix)
  (setq company-semantic--current-tags nil)
  (dolist (tag (semantic-analyze-find-tags-by-prefix prefix))
    (unless (eq (semantic-tag-class tag) 'include)
      (push tag company-semantic--current-tags)))
  (delete "" (mapcar 'semantic-tag-name company-semantic--current-tags)))

(defun company-semantic-args (tag &optional annotate)
  (let* ((kind (when tag (elt tag 1))))
    (cl-case kind
      ((function command)
       (let* ((prototype (semantic-format-tag-prototype tag nil nil))
              (par-pos (string-match "[[{(]" prototype)))
         (when (and par-pos
                    (not annotate))
           (substring prototype par-pos)))))))

(defun company-semantic-annotation (argument tags)
  (let* ((tag (assq argument tags))
         (type (semantic-tag-class tag)))
    (concat (company-semantic-args tag (not semantic-typesafe-lang))
            (cond ((stringp (semantic-tag-type tag))
                   (concat " : "
                           (semantic-format-tag-type tag nil)
                           ;; (semantic-tag-type tag)
                           ;; C pointers
                           ;; (when (semantic-tag-get-attribute tag :pointer) "*")
                           ))
                  ((eq type 'type)  "object")
                  ((eq type 'variable) "var")
                  ((eq type 'function) "f()")
                  ((eq type 'statement) "<sql>")
                  ;; Latex - start
                  ((eq type 'command) "cmd")
                  ((eq type 'class) "cls")
                  ((eq type 'environment) "env")
                  ((eq type 'package) "pkg")
                  ((eq type 'option) "opt")
                  ;; Latex - end
                  ))))

(defun company-semantic--prefix ()
  (if company-semantic-begin-after-member-access
      ;; Latex commands start with \
      (company-grab-symbol-cons "\\.\\|->\\|::\\|\\\\" 2)
    (company-grab-symbol)))

(defun company-template-lisp-templatify (args)
  ;; Avoid beg and end overlap when eobp is true
  (let* ((beg (1- (point-marker)))
         (end (point))
         (templ (company-template-declare-template beg end)))
    (dolist (field args)
      (unless (or (string= field "&optional")
                  (string= field "&rest"))
        (insert " ")
        (company-template-add-field templ (point)
                                    (progn
                                      (insert (symbol-name field))
                                      (point))
                                    nil
                                    #'company-template--after-clear-c-like-field)))
    (insert ")")
    (company-template-move-to-first templ)))

(defun company-template-tex-templatify (tag)
  ;; Avoid beg and end overlap when eobp is true
  (let* ((beg (1- (point-marker)))
         (end (point))
         (name (semantic-tag-name tag))
         (option (semantic-tag-get-attribute tag :options))
         (arg (semantic-tag-get-attribute tag :arguments))
         (templ (company-template-declare-template beg end)))
    (when option
      (insert "[")
      (company-template-add-field templ (point)
                                  (progn
                                    (insert "options")
                                    (point))
                                  nil
                                  #'company-template--after-clear-c-like-field)
      (insert "]"))
    (when arg
      (insert "{")
      (company-template-add-field templ (point)
                                  (progn
                                    (insert (car arg))
                                    (point))
                                  nil
                                  #'company-template--after-clear-c-like-field)
      (insert "}"))

    (company-template-move-to-first templ)))

(defun company-template-sql-templatify (tag)
  ;; Avoid beg and end overlap when eobp is true
  (let* ((beg (1- (point-marker)))
         (end (point))
         (name (semantic-tag-name tag))
         (args (semantic-tag-function-arguments tag))
         text pos
         (templ (company-template-declare-template beg end)))
    (setq beg 0
          text (pcase name
                 ("insert" " into ? (?) values (?)")
                 ("select" " * from ?")
                 ("update" " ? set ?")
                 ("delete" " from ?")
                 ("drop"   " table ?")))
    (dolist (field args)
      (when (setq end (string-match "\\?" text beg))
        (insert (substring text beg end))
        (company-template-add-field templ (point)
                                    (progn
                                      (insert field)
                                      (point))
                                    nil
                                    #'company-template--after-clear-c-like-field))
      (setq beg (1+ end)))
    (insert (substring text beg (length text)))
    (company-template-move-to-first templ)))

(defun company-template--after-clear-c-like-field ()
  "Function that can be called after deleting a field of a c-like template.
For c-like templates it is set as `after-post-fn' property on fields in
`company-template-add-field'.  If there is a next field, delete everything
from point to it.  If there is no field after point, remove preceding comma
if present."
  (let* ((pos (point))
         (last-field-p (not (company-template-field-at pos)))
         (next-field-start (if last-field-p
                               pos
                             (company-template-find-next-field))))
    (cond ((and (not last-field-p)
                (< pos next-field-start)
                ;; (string-match "^[ ]*,+[ ]*$" (buffer-substring-no-properties
                ;;                               pos next-field-start))
		)
           (delete-region pos next-field-start))
          ((and last-field-p
                (looking-back "[, ]+" (line-beginning-position)))
           (delete-region (match-beginning 0) pos)))
    ;; Position point after closing parenthesis
    (forward-char 1)))

;;;###autoload
(defun company-semantic (command &optional arg &rest ignored)
  "`company-mode' completion backend using CEDET Semantic."
  (interactive (list 'interactive))
  (cl-case command
    (interactive (company-begin-backend 'company-semantic))
    (prefix (and (featurep 'semantic)
                 (semantic-active-p)
                 ;; We use css capf for css submode
                 (not (memq (semantic-get-cache-data 'mode)
                            '(css-mode)))
                 (memq major-mode company-semantic-modes)
                 ;; Elisp mode allows symbol names inside comments
                 ;; (not (company-in-string-or-comment))
                 ;; (or (company-semantic--prefix) 'stop)
                 (when (if (consp (company-semantic--prefix))
                           (company-semantic-completions (car (company-semantic--prefix)))
                         (company-semantic-completions (company-semantic--prefix)))
                   (company-semantic--prefix))
                 ))
    (candidates (company-semantic-completions arg))
     ;; (if (and (equal arg "")
     ;;                     (not (looking-back "->\\|\\.\\|::" (- (point) 2))))
     ;;                (company-semantic-completions-raw arg)
     ;;              (company-semantic-completions arg)))
    (meta (funcall company-semantic-metadata-function
                   (assoc arg company-semantic--current-tags)))
    (annotation (company-semantic-annotation arg
                                             company-semantic--current-tags))
    (doc-buffer (company-semantic-doc-buffer
                 (assoc arg company-semantic--current-tags)))
    ;; Because "" is an empty context and doesn't return local variables.
    ;; Don't cache to account for new elements being introduced while editing
    (no-cache (equal arg ""))
    (duplicates t)
    (ignore-case semantic-grammar-case-insensitive)
    (location (let ((tag (assoc arg company-semantic--current-tags)))
                (when (buffer-live-p (semantic-tag-buffer tag))
                  (cons (semantic-tag-buffer tag)
                        (semantic-tag-start tag)))))
    (post-completion (company-semantic-post-completion arg))
    ))

(defun company-semantic-post-completion (arg)
  "Post completion hook."
  (let* ((tag (assoc arg company-semantic--current-tags))
         (params nil))
    (when (semantic-tag-of-class-p tag 'statement)
      (company-template-sql-templatify tag))
    (when (semantic-tag-of-class-p tag 'command)
      (company-template-tex-templatify tag))
    (when (and company-semantic-insert-arguments
               (semantic-tag-of-class-p tag 'function))
      (if (derived-mode-p 'emacs-lisp-mode)
          ;; No need to complete args if we are only completing symbols
          (when (= (semantic-ctxt-current-argument) 1)
            (setq params
                  (help-function-arglist (intern arg) t))
            ;; (semantic-tag-function-arguments tag))
            (company-template-lisp-templatify params))
        (setq params (company-semantic-args tag))
        (insert params)
        (company-template-c-like-templatify (concat arg params)))))
  )

(provide 'company-semantic)
;;; company-semantic.el ends here
