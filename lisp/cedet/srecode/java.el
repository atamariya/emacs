;;; srecode/java.el --- Srecode Java support

;; Copyright (C) 2009-2020 Free Software Foundation, Inc.

;; Author: Eric M. Ludlam <zappo@gnu.org>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Special support for the Java language.

;;; Code:

(require 'srecode/dictionary)
(require 'semantic/find)
(require 'ede)

(defcustom srecode-java-import-order
  '("java" "javax" "org" "com")
  "List order of import groups."
  :group 'srecode-java
  :type  '(repeat string))

;;;###autoload
(defun srecode-semantic-handle-:java (dict)
  "Add macros into the dictionary DICT based on the current java file.
Adds the following:
TYPE - class
FILENAME_AS_PACKAGE - file/dir converted into a java package name.
FILENAME_AS_CLASS - file converted to a Java class name."
  ;; Symbols needed by empty files.
  (let* ((fsym (file-name-nondirectory (buffer-file-name)))
	 (fnox (file-name-sans-extension fsym))
	 (dir (file-name-directory (buffer-file-name)))
	 (fpak fsym)
	 (proj (ede-current-project))
         (root (if proj (ede-project-root-directory proj)))
	 (pths (if proj (ede-source-paths proj 'java-mode)))
         (tags (semantic-fetch-tags))
         (pkgname nil) ;; current package
         dirs
	 )
    (while (string-match "\\.\\| " fpak)
      (setq fpak (replace-match "_" t t fpak)))
    ;; We can extract package from:
    ;; 1) a java EDE project source paths,
    (cond ((and proj pths)
           (let* ((pth) (res))
             (while (and (not res)
                         (setq pth (expand-file-name (car pths))))
               (when (string-match pth dir)
                 (setq res (substring dir (match-end 0))))
               (setq pths (cdr pths)))
             (setq dir res)))
          ;; 2) a simple heuristic
          ((string-match "src/" dir)
           (setq dirs (cons root (ede-include-path proj)))
           (setq dir (semantic-java--strip-project-path dir dirs)))
          ;; 3) outer directory as a fallback
          (t (setq dir (file-name-nondirectory (directory-file-name dir)))))
    (setq dir (directory-file-name dir))
    (while (string-match "/" dir)
      (setq dir (replace-match "." t t dir)))
    (srecode-dictionary-set-value dict "CURRENT_PACKAGE" dir)
    (srecode-dictionary-set-value dict "FILENAME_AS_CLASS" fnox)
    (srecode-dictionary-set-value dict "TYPE" "class")
    (setq pkgname dir)

    ;; Symbols needed for most other files with stuff in them.
    ;; (let ((pkg (semantic-find-tags-by-class 'package tags)))
    ;;   (setq pkgname (semantic-tag-name (car pkg)))
    ;;   (when pkg
    ;;     (srecode-dictionary-set-value dict "CURRENT_PACKAGE" pkgname)
    ;;     ))

    (let ((pkg (semantic-find-tags-by-class 'include tags))
          res group name el section)
      ;; Remove same package or java.lang imports
      (setq pkg (seq-filter
                 #'(lambda (a)
                     (let ((n (semantic-tag-name a)))
                       (not (or (string= pkgname (file-name-sans-extension n))
                                (semantic-tag-include-system-p a)
                                ))))
                 pkg))
      ;; sort
      (setq pkg (semantic-sort-tags-by-name-increasing pkg))

      ;; Create buckets for sort order
      (dolist (el srecode-java-import-order)
        (push (cons el nil) res))

      ;; Group imports
      (while pkg
        (setq name (semantic-tag-name (car pkg)))
        (setq group (car (split-string name "\\.")))
        (setq el (assoc group res))
        (if el
	    (setcdr el (cons (car pkg) (cdr el)))
          (push (list group (car pkg)) res))
        (setq pkg (cdr pkg)))
      ;; Since we pushed earlier, need to revert it now
      (setq res (nreverse res))

      ;; Add to srecode dictionary
      (while res
        (setq pkg (cdar res))
        (if (stringp (car pkg))
            (setq pkg (list (car pkg)))
          (setq pkg (nreverse pkg))
          )
        (setq section (srecode-dictionary-add-section-dictionary dict "IMPORTS"))

        ;; Identify groups to insert blank line after each
        (while pkg
          (when (not (semantic-tag-in-buffer-p (car pkg)))
            (srecode-dictionary-set-value
             (srecode-dictionary-add-section-dictionary section "GROUP")
             "IMPORT" (semantic-tag-name (car pkg))))
          (setq pkg (cdr pkg)))

        (setq res (cdr res))))))

(provide 'srecode/java)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "srecode/java"
;; End:

;;; srecode/java.el ends here
