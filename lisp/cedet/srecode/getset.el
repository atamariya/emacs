;;; srecode/getset.el --- Package for inserting new get/set methods.

;; Copyright (C) 2007-2020 Free Software Foundation, Inc.

;; Author: Eric M. Ludlam <zappo@gnu.org>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; SRecoder application for inserting new get/set methods into a class.

(require 'semantic)
(require 'semantic/analyze)
(require 'semantic/find)
(require 'srecode/insert)
(require 'srecode/dictionary)
(require 'srecode/semantic)

;;; Code:
(defvar srecode-insert-getset-fully-automatic-flag nil
  "Non-nil means accept choices srecode comes up with without asking.")

(defvar srecode--dialog-tags nil)
(defvar srecode--dialog-modifier "public")

(defun srecode-insert-getset (&optional class-in field-in methods)
  "Insert get/set methods for the current class.
CLASS-IN is the semantic tag of the class to update.
FIELD-IN is the semantic tag, or string name, of the field to add.
If you do not specify CLASS-IN or FIELD-IN then a class and field
will be derived."
  (interactive)

  (srecode-load-tables-for-mode major-mode)
  (srecode-load-tables-for-mode major-mode 'getset)

  (if (not (srecode-table))
      (error "No template table found for mode %s" major-mode))

  (if (not (srecode-template-get-table (srecode-table)
				       "getset-in-class"
				       "declaration"
				       'getset))
      (error "No templates for inserting get/set"))

  ;; Step 1: Try to derive the tag for the class we will use
  (semantic-fetch-tags)
  (let* ((class (or class-in (srecode-auto-choose-class (point))))
	 )

    (when (not class)
      (error "Move point to a class and try again"))

    (if methods
        (srecode-insert-getset-by-method class methods)
      (srecode-insert-getset-by-field class field-in))))

(defun srecode-insert-getset-by-field (&optional class field-in)
  (let* ((tagstart (when class (semantic-tag-start class)))
	 (inclass (eq (semantic-current-tag-of-class 'type) class))
	 (field nil)
	 )

    (when (not class)
      (error "Move point to a class and try again"))

    ;; Step 2: Select a name for the field we will use.
    (when field-in
      (setq field field-in))

    (when (and inclass (not field))
      (setq field (srecode-auto-choose-field (point))))

    (when (not field)
      (setq field (srecode-query-for-field class)))

    ;; Step 3: Insert a new field if needed
    (when (stringp field)

      (goto-char (point))
      (srecode-position-new-field class inclass)

      (let* ((dict (srecode-create-dictionary))
	     (temp (srecode-template-get-table (srecode-table)
					       "getset-field"
					       "declaration"
					       'getset))
	     )
	(when (not temp)
	  (error "Getset templates for %s not loaded!" major-mode))
	(srecode-resolve-arguments temp dict)
	(srecode-dictionary-set-value dict "NAME" field)
	(when srecode-insert-getset-fully-automatic-flag
	  (srecode-dictionary-set-value dict "TYPE" "int"))
	(srecode-insert-fcn temp dict)

	(semantic-fetch-tags)
	(save-excursion
	  (goto-char tagstart)
	  ;; Refresh our class tag.
	  (setq class (srecode-auto-choose-class (point)))
	  )

	(let ((tmptag (semantic-deep-find-tags-by-name-regexp
		       field (current-buffer))))
	  (setq tmptag (semantic-find-tags-by-class 'variable tmptag))

	  (if tmptag
	      (setq field (car tmptag))
	    (error "Could not find new field %s" field)))
	)

      ;; Step 3.5: Insert an initializer if needed.
      ;; ...


      ;; Set up for the rest.
      )

    (if (not (semantic-tag-p field))
	(error "Must specify field for get/set.  (parts may not be impl'd yet.)"))

    ;; Set 4: Position for insertion of methods
    (srecode-position-new-methods class field)

    ;; Step 5: Insert the get/set methods
    ;; (if (not (eq (semantic-current-tag) class))
    ;;     ;; We are positioned on top of something else.
    ;;     ;; insert a /n
    ;;     (insert "\n"))

    (let* ((dict (srecode-create-dictionary))
	   (srecode-semantic-selected-tag field)
	   (temp (srecode-template-get-table (srecode-table)
					     "getset-in-class"
					     "declaration"
					     'getset))
	   )
      (if (not temp)
	  (error "Getset templates for %s not loaded!" major-mode))
      (srecode-resolve-arguments temp dict)
      (srecode-dictionary-set-value dict "GROUPNAME"
				    (concat (semantic-tag-name field)
					    " Accessors"))
      (srecode-dictionary-set-value dict "NICENAME"
				    (srecode-strip-fieldname
				     (semantic-tag-name field)))
      (srecode-dictionary-set-value dict "NAME"
                                    (semantic-tag-name field))
      (srecode-dictionary-set-value dict "TYPE"
                                    (semantic-tag-type field))
      (srecode-dictionary-set-value dict "ACCESS"
                                    srecode--dialog-modifier)
      (srecode-insert-fcn temp dict)
      )))

(defun srecode-insert-getset-by-method (&optional class methods)
    (let* ((dict (srecode-create-dictionary))
	   (temp-get (srecode-template-get-table (srecode-table)
					     "get-in-class"
					     "declaration"
					     'getset))
	   (temp-set (srecode-template-get-table (srecode-table)
					     "set-in-class"
					     "declaration"
					     'getset))
	   all-fields fields field name temp)
      (if (not (and temp-get temp-set))
	  (error "Getset templates for %s not loaded!" major-mode))

      ;; Set 4: Position for insertion of methods
      (srecode-position-new-methods class nil)
      (if (not (listp methods))
          (setq methods (list methods)))
      (setq all-fields (semantic-find-tags-by-class
                    'variable (semantic-tag-type-members class)))

      (while methods
        (setq fields all-fields
              temp nil)
        (while (and fields (not temp))
          (setq field (car fields)
                name (srecode-strip-fieldname (semantic-tag-name field))
                temp (if (string= (car methods) (concat "get" name))
                         temp-get
                       (if (string= (car methods) (concat "set" name))
                           temp-set))
                fields (cdr fields)))

        (srecode-resolve-arguments temp dict)
        (srecode-dictionary-set-value dict "GROUPNAME"
				      (concat (semantic-tag-name field)
					      " Accessors"))
        (srecode-dictionary-set-value dict "NICENAME"
				      (srecode-strip-fieldname
				       (semantic-tag-name field)))
        (srecode-dictionary-set-value dict "NAME"
                                      (semantic-tag-name field))
        (srecode-dictionary-set-value dict "TYPE"
                                      (semantic-tag-type field))
        (srecode-dictionary-set-value dict "ACCESS"
                                      srecode--dialog-modifier)
        (srecode-insert-fcn temp dict)
        (setq methods (cdr methods)))
      ))

(defun srecode-strip-fieldname (name)
  "Strip the fieldname NAME of polish notation things."
  (cond ((string-match "\\.h$" name)
         ;; c header guard
	 (replace-match "_h" nil nil name))
	;; Add more rules here.
	(t
	 (upcase-initials name))))

;;;###autoload
(defun srecode-getset-dialog ()
  (interactive)
  (let* ((class (srecode-auto-choose-class (point)))
         (existing (semantic-tag-type-members class))
         (fields (semantic-find-tags-by-class
		  'variable existing))
         (buf (get-buffer-create "*minibuffer*"))
         tags name names)
    (when (not class)
      (error "Move point to a class and try again"))

    (semantic-force-refresh)
    (setq srecode--dialog-tags nil
          srecode--dialog-modifier "public"
          tags
          (mapcar (lambda (field)
                    (setq names nil
                          name (concat "set" (srecode-strip-fieldname
				              (semantic-tag-name field))
                                       ;; (format "(%s)" (semantic-tag-type field))
                                       ))
                    (unless (semantic-find-tags-by-name name existing)
                        (push name names))
                    (setq name (concat "get" (srecode-strip-fieldname
				              (semantic-tag-name field))
                                       ;; "()"
                                       ))
                    (unless (semantic-find-tags-by-name name existing)
                        (push name names))
                    (if names
                        (list (semantic-tag-name field) names))
                    )
                  fields)
          tags (delq nil tags))
    ;; (message "%s" tags)

    (set-window-buffer nil buf)
    (with-current-buffer buf
      (erase-buffer)
      (let ((ezimage-expand-image-button-alist speedbar-svg-checkbox-alist)
            (speedbar-checkbox t)
            (speedbar-generic-list-tag-button-type nil))
        (insert "Select getters and setters to create\n")
        (widget-create
         (widget-tree--convert tags)
         :checkbox t
         :value (list t nil nil)
         :notify (lambda (w &rest ignore)
                   (setq srecode--dialog-tags
                         (widget-tree-flat-value w)))
         :name (semantic-tag-name class))
        )

      (let ((ezimage-expand-image-button-alist speedbar-svg-checkbox-alist)
            (speedbar-checkbox 'radio)
            (speedbar-tag-hierarchy-method nil)
            (speedbar-generic-list-tag-button-type nil))
        (insert "\nAccess modifier\n")
        (widget-create 'radio
                       :format "%v"
                       :indent 1
                       :value srecode--dialog-modifier
                       :notify (lambda (w &rest ignore)
                                 (setq srecode--dialog-modifier
                                       (widget-value w)))
                       "public" "private" "protected" "package")
        )

      (widget-insert "\n ")
      (widget-create 'push-button
                     :notify (lambda (&rest ignore)
                               (kill-buffer)
                               (if srecode--dialog-tags
                                   (srecode-insert-getset
                                    nil nil srecode--dialog-tags)))
                     "OK")
      (widget-insert " ")
      (widget-create 'push-button
                     :notify (lambda (&rest ignore)
                               (kill-buffer))
                     "Cancel")
      (widget-insert "\n")
      (widget-setup)
      (use-local-map widget-keymap)
      )))

(defun srecode-position-new-methods (class field)
  "Position the cursor in CLASS where new getset methods should go.
FIELD is the field for the get sets.
INCLASS specifies if the cursor is already in CLASS or not."
  (goto-char (1- (semantic-tag-end class))))

(defun srecode-position-new-methods1 (class field)
  (semantic-go-to-tag field)

  (let ((prev (semantic-find-tag-by-overlay-prev))
	(next (semantic-find-tag-by-overlay-next))
	(setname nil)
	(aftertag nil)
	)
    (cond
     ((and prev (semantic-tag-of-class-p prev 'variable))
      (setq setname
	    (concat "set"
		    (srecode-strip-fieldname (semantic-tag-name prev))))
      )
     ((and next (semantic-tag-of-class-p next 'variable))
      (setq setname
	    (concat "set"
		    (srecode-strip-fieldname (semantic-tag-name prev)))))
     (t nil))

    (setq aftertag (semantic-find-first-tag-by-name
		    setname (semantic-tag-type-members class)))

    (when (not aftertag)
      (setq aftertag (car-safe
		      (semantic--find-tags-by-macro
		       (semantic-tag-get-attribute (car tags) :destructor-flag)
		       (semantic-tag-type-members class))))
      ;; Make sure the tag is public
      (when (not (eq (semantic-tag-protection aftertag class) 'public))
	(setq aftertag nil))
      )

    (if (not aftertag)
	(setq aftertag (car-safe
			(semantic--find-tags-by-macro
			 (semantic-tag-get-attribute (car tags) :constructor-flag)
			 (semantic-tag-type-members class))))
      ;; Make sure the tag is public
      (when (not (eq (semantic-tag-protection aftertag class) 'public))
	(setq aftertag nil))
      )

    (when (not aftertag)
      (setq aftertag (semantic-find-first-tag-by-name
		      "public" (semantic-tag-type-members class))))

    (when (not aftertag)
      (setq aftertag (car (semantic-tag-type-members class))))

    (if aftertag
	(let ((te (semantic-tag-end aftertag)))
	  (when (not te)
	    (message "Unknown location for tag-end in %s:" (semantic-tag-name aftertag)))
	  (goto-char te)
	  ;; If there is a comment immediately after aftertag, skip over it.
	  (when (looking-at (concat "\\s-*\n?\\s-*" semantic-lex-comment-regex))
	    (let ((pos (point))
		  (rnext (semantic-find-tag-by-overlay-next (point))))
	      (forward-comment 1)
	      ;; Make sure the comment we skipped didn't say anything about
	      ;; the rnext tag.
	      (when (and rnext
			 (re-search-backward
			  (regexp-quote (semantic-tag-name rnext)) pos t))
		;; It did mention rnext, so go back to our starting position.
		(goto-char pos)
		)
	      ))
	  )

      ;; At the very beginning of the class.
      (goto-char (semantic-tag-end class))
      (forward-sexp -1)
      (forward-char 1)

      )

    (end-of-line)
    (forward-char 1)
    ))

(defun srecode-position-new-field (class inclass)
  "Select a position for a new field for CLASS.
If INCLASS is non-nil, then the cursor is already in the class
and should not be moved during point selection."

  ;; If we aren't in the class, get the cursor there, pronto!
  (when (not inclass)

    (error "You must position the cursor where to insert the new field")

    (let ((kids (semantic-find-tags-by-class
		 'variable (semantic-tag-type-members class))))
      (cond (kids
	     (semantic-go-to-tag (car kids) class))
	    (t
	     (semantic-go-to-tag class)))
      )

    (switch-to-buffer (current-buffer))

    ;; Once the cursor is in our class, ask the user to position
    ;; the cursor to keep going.
    )

  (if (or srecode-insert-getset-fully-automatic-flag
	  (y-or-n-p "Insert new field here? "))
      nil
    (error "You must position the cursor where to insert the new field first"))
  )



(defun srecode-auto-choose-field (point)
  "Choose a field for the get/set methods.
Base selection on the field related to POINT."
  (save-excursion
    (when point
      (goto-char point))

    (let ((field (semantic-current-tag-of-class 'variable)))

      ;; If we get a field, make sure the user gets a chance to choose.
      (when field
	(if srecode-insert-getset-fully-automatic-flag
	    nil
	  (when (not (y-or-n-p
		      (format "Use field %s? " (semantic-tag-name field))))
	    (setq field nil))
	  ))
      field)))

(defun srecode-query-for-field (class)
  "Query for a field in CLASS."
  (let* ((kids (semantic-find-tags-by-class
		'variable (semantic-tag-type-members class)))
	 (sel (completing-read "Use Field: " kids))
	 (fields (semantic-find-tags-by-name sel kids)))
    (if fields
        (car fields)
      sel)
    ))

(defun srecode-auto-choose-class (point)
  "Choose a class based on location of POINT."
  (save-excursion
    (when point
      (goto-char point))

    (let ((tag (semantic-current-tag-of-class 'type)))

      (when (or (not tag)
		(not (string= (semantic-tag-type tag) "class")))
	;; The current tag is not a class.  Are we in a fcn
	;; that is a method?
	(setq tag (semantic-current-tag-of-class 'function))

	(when (and tag
		   (semantic-tag-function-parent tag))
	  (let ((p (semantic-tag-function-parent tag)))
	    ;; @TODO : Copied below out of semantic-analyze
	    ;;         Turn into a routine.

	    (let* ((searchname (cond ((stringp p) p)
				     ((semantic-tag-p p)
				      (semantic-tag-name p))
				     ((and (listp p) (stringp (car p)))
				      (car p))))
		   (ptag (semantic-analyze-find-tag searchname
						    'type nil)))
	      (when ptag (setq tag ptag ))
	      ))))

      (when (or (not tag)
		(not (semantic-tag-of-class-p tag 'type))
		(not (string= (semantic-tag-type tag) "class")))
	;; We are not in a class that needs a get/set method.
	;; Analyze the current context, and derive a class name.
	(let* ((ctxt (semantic-analyze-current-context))
	       (pfix nil)
	       (ans nil))
	  (when ctxt
	    (setq pfix (reverse (oref ctxt prefix)))
	    (while (and (not ans) pfix)
	      ;; Start at the end and back up to the first class.
	      (when (and (semantic-tag-p (car pfix))
			 (semantic-tag-of-class-p (car pfix) 'type)
			 (string= (semantic-tag-type (car pfix))
				  "class"))
		(setq ans (car pfix)))
	      (setq pfix (cdr pfix))))
	  (setq tag ans)))

      tag)))

;;;###autoload
(defun srecode-gen-header ()
  (interactive)
  (let* ((class (file-name-nondirectory (buffer-file-name)))
         (existing (current-buffer))
         (fields (semantic-filter-tags-by-class
                  '(include variable) existing))
         (buf (get-buffer-create "*srecode*"))
         (map (copy-keymap widget-keymap))
         tags name names)

    (semantic-force-refresh)
    (setq srecode--dialog-tags nil
          tags (mapcar 'semantic-tag-name fields)
          )
    ;; (message "%s" tags)

    (set-window-buffer nil buf)
    (with-current-buffer buf
      (setq inhibit-read-only t)
      (erase-buffer)
      (let ((ezimage-expand-image-button-alist speedbar-svg-checkbox-alist)
            (speedbar-checkbox t)
            (speedbar-generic-list-tag-button-type nil))
        (insert "Select functions\n")
        (widget-create
         (widget-tree--convert tags)
         :checkbox t
         :value (list t nil nil)
         :notify (lambda (w &rest ignore)
                   (setq srecode--dialog-tags
                         (widget-tree-flat-value w)))
         :name class)
        )

      (widget-insert "\n ")
      (widget-create 'push-button
                     :notify (lambda (&rest ignore)
                               (kill-buffer)
                               (if srecode--dialog-tags
                                   (srecode-insert-header
                                    srecode--dialog-tags)))
                     "OK")
      (widget-insert " ")
      (widget-create 'push-button
                     :notify (lambda (&rest ignore)
                               (kill-buffer))
                     "Cancel")
      (widget-insert "\n")
      (widget-setup)
      (use-local-map widget-keymap)
      )))

(defun srecode-insert-header (methods)
  "Extract C header file."
  (interactive)
  (let* ((base (current-buffer))
         (style (list 'prototype))
         tag buf file tags name templ dict)

    ;; Add method in other file
    (setq name (file-name-base (buffer-file-name))
          file (concat name ".h"))
    (setq buf (find-file-noselect file))
    (with-current-buffer buf
      (unless (and (file-exists-p file)
                   (not (zerop (buffer-size))))
        (setq ede--refresh t)
        (c-mode)
        ;; Buffer might still be open even when file is deleted
        (erase-buffer)
        (setq templ (srecode-template-get-table (srecode-table) "file:header_guard")
              dict (srecode-create-dictionary))
        (srecode-dictionary-set-value
         dict "FILENAME_SYMBOL" (srecode-strip-fieldname file))
        (srecode-insert-fcn templ dict nil t)
        (save-buffer))

      (when (file-exists-p file)
        (semantic-fetch-tags)
        (beginning-of-line)
        (if (stringp methods)
            (setq methods (list methods)))
        (unwind-protect
            (while (setq name (car methods)
                              methods (cdr methods)
                              tag (semantic-find-first-tag-by-name name base))
              ;; Avoid duplicating the tag
              (setq tags (semantic-find-tags-by-name name buf))
              (when (not tags)
                (if (semantic-tag-of-class-p tag 'type)
                    (progn
                      (with-current-buffer base
                        (kill-ring-save (semantic-tag-start tag)
                                        (semantic-tag-end tag)))
                      (yank)
                      (insert ";\n"))
                  (srecode-semantic-insert-tag tag style))))
          (save-buffer)
          )))))

(provide 'srecode/getset)

;; Local variables:
;; generated-autoload-file: "loaddefs.el"
;; generated-autoload-load-name: "srecode/getset"
;; End:

;;; srecode/getset.el ends here
