;;; fsm.el --- Finite State Machine implementation

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: fsm

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;; Derived from https://www.emacswiki.org/emacs/StateMachine

;; Usage:
;; (define-state start
;;   (princ "I am starting...")
;;   (terpri)
;;   (setf number 1)
;;   (state 'count))
;; (define-state count
;;   (cond
;;    ((<= number 10)
;;     (princ number)
;;     (incf number)
;;     (state 'count))
;;    (t (state 'end))))
;; (define-state end
;;   (terpri)
;;   (princ "I am ending...")
;;   (terpri)
;;   (state 'halt))

;; (define-state-machine counter start end count)
;; Defines a function
;; (counter start-state end-state)
;; (counter 'start)

(require 'cl)
(defvar state-hash (make-hash-table))

(defmacro define-state (name &rest body)
  (let ((thunk `(lambda () ,@body)))
    `(setf (gethash ',name state-hash) ',thunk)))

(defmacro define-state-machine (name &rest states)
  (let ((-start-state (gensym "start-state--"))
	(-end-state (gensym "end-state--"))
        (-state (gensym "state--"))
	(-state-machine (gensym "state-machine--"))
	(-state-alist (gensym "state-alist--"))
	(state-alist (loop for state in (cons 'halt states)
			   collect (cons state (gensym (symbol-name state))))))
    `(defun ,name (,-start-state &optional ,-end-state)
       (block ,name
         (let* ((,-state-alist ',state-alist))
           (flet ((state (name)
	                 (let ((sym (cdr (assoc name ,-state-alist))))
	                   (throw (quote ,-state-machine) (symbol-value sym)))))
             (let* ((,(cdr (assoc 'halt state-alist)) (lambda () (return-from ,name)))
	            ,@(loop for state-name in states
		            collect `(,(cdr (assoc state-name state-alist))
				      ',(gethash state-name state-hash)))
                    (,-state (symbol-value
			      (cdr (assoc ,-start-state ,-state-alist)))))
	       (while (or (null ,-end-state)
                          (eq ,-state ,-end-state))
	         (setf ,-state (catch ',-state-machine
			         (funcall ,-state)))))))))))

(provide 'fsm)
