;;; comics.el --- create comics strips

;; Copyright (C) 2022 Free Software Foundation, Inc.

;; Author: Anand Tamariya <atamarya@gmail.com>
;; Keywords: comics generator

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
(require 'svg)

(defvar comics-artwork "/home/anand/work/spittoon/examples/artwork/"
  "Folder containing artwork")
(defvar comics-face "faces" "Folder containing faces")
(defvar comics-body "poses" "Folder containing bodies")
(defvar comics-background "backgrounds" "Folder containing backgrounds")
(defvar comics-col-max 2 "Maximum no. of columns")
(defvar comics-font-size 15 "Font size for text")

(defvar comics-emotions
  '((positive "positive" "grinning" "happy" "exclaiming" "surprised" "happy")
    (negative "negative" "annoyed" "angry" "bored" "grossed" "shocked" "question")
    (neutral "neutral" "standing" "explaining" "pointing" "question" "speaking"
             "listening")
    ))

(defvar comics-avatars
  '(("bucket" . "bucket")
    ("melon" . "melon")
    ("nibbles" . "nibbles")
    ("bucket" . "easy")
    ("bucket" . "melon")
    ("bucket" . "nibbles")
    ("bucket" . "taff")
    ))

(defun comics--get-image (type name)
  (let* (dir file flag)
    (setq dir
          (concat comics-artwork
                  (pcase type
                    ('face comics-face)
                    ('body comics-body)
                    ('background comics-background)
                    ))
          file (car (directory-files dir t
                                     (concat name ".*\\.\\(gif\\|jpg\\|png\\)"))))
    file))

(defun comics--create (svg w h)
  (let* ((padding 10)
         (m 0)
         (j 0)
         (n comics-col-max)
         (canvas--id 0)
         (w1 (/ (- w (* (1+ n) padding)) n))
         start end line tok actor emotion txt h1
         actors dialog narration background panel panels)
    ;; (setq narration "narration"
    ;;       background "002"
    ;;       canvas--id 0
    ;;       dialog '((1 . "hi")
    ;;                (2 . "hi")))
    ;; (dotimes (i n)
    ;;   (comics--create-panel svg (+ padding (* i (+ w1 padding)))
    ;;                         padding
    ;;                         w1 h dialog narration background)
    ;;   )
    ;; Calculate panel height
    (goto-char (point-min))
    (while (re-search-forward "\n\n" nil t)
      (setq m (1+ m)))
    (setq m (ceiling (/ m n 1.0))
          h1 (/ (- h (* (1+ m) padding)) m))

    (goto-char (point-min))
    (while (not (eobp))
      (setq start (point)
            end (line-end-position))
      (setq line (string-trim (buffer-substring-no-properties start end))
            tok (split-string line ":"))
      (when (looking-at "[[]")
        (setq background (string-trim (substring line 1 (1- (length line))))))

      (when (string-empty-p line)
        ;; (message "create panel")
        (when (and (or narration dialog)
                   (/= (length panels) comics-col-max))
          (push (list narration (nreverse dialog) background) panels))

        (when (= (length panels) comics-col-max)
          (setq panels (reverse panels))
          (dotimes (i (length panels))
            (setq panel (nth i panels)
                  dialog (nth 1 panel)
                  narration (nth 0 panel)
                  background (nth 2 panel))
            (comics--create-panel svg
                                  (+ padding (* i (+ w1 padding)))
                                  (+ padding (* j (+ h1 padding)))
                                  w1 h1 dialog (length actors) narration background)
            )
          (setq panels nil
                j (1+ j)))

        (setq narration nil
              dialog nil)
        )

      (unless (string-empty-p line)
        (if (= (length tok) 1)
            (setq narration (car tok))
          (setq actor (car tok)
                emotion nil
                txt (cadr tok))
          (if (string-match "\\(.*\\)(\\(.*\\))" txt)
              (setq emotion (match-string 2 txt)
                    txt (match-string 1 txt)))
          (unless (member actor actors)
            (push actor actors))
          (push (list (1- (length (member actor actors))) txt emotion)
                dialog)
          )
        ;; (message "%s %s %s" narration actor txt)
        )
      (forward-line))

    (when dialog
      (push (list narration (nreverse dialog) background) panels))
    (setq panels (reverse panels))
    ;; (message "%s" panels)

    (dotimes (i (length panels))
      (setq panel (nth i panels)
            dialog (nth 1 panel)
            narration (nth 0 panel)
            background (nth 2 panel))
      (comics--create-panel svg
                            (+ padding (* i (+ w1 padding)))
                            (+ padding (* j (+ h1 padding)))
                            w1 h1 dialog (length actors) narration background)
      )
    ))

(defun comics--create-panel (svg x1 y1 w h dialog n &optional narration background)
  (let* ((defs (car (dom-by-tag svg 'defs)))
         (g-actors (dom-node 'g))
         (g-balloons (dom-node 'g))
         (g-stage (dom-node 'g))
         (g-panel (dom-node 'g))
         (i 0)
         (padding 15)
         (text "some long text here are some of those")
         (tail-height (/ h 20))
         (xpos (make-list n nil))
         file x y w1 h1 dx bbox size1 size2 actor action repeat)
    (setq x 0
          y 0)
    (comics--create-background svg defs background x1 y1 w h)

    ;; Add text without actor as Narration balloon
    ;; Aligned to top left in the panel
    (when narration
      (setq size1 (comics--create-balloon g-panel x y narration 'narrate)
            y (+ y (cdr size1))))
    (setq y (+ y padding))

    (dotimes (i (length dialog))
      (setq repeat (nth (car (nth i dialog)) xpos)
            size1 '(0 . 0)
            size2 '(0 . 0))
      (unless repeat
        (setq size1 (comics--create-actor g-actors (car (nth i dialog))
                                         x (nth 2 (nth i dialog))
                                         (> i 0)))
        (setf (nth (car (nth i dialog)) xpos) (cons x (car size1))))

      (setq size2 (comics--create-balloon g-balloons
                                         (or (car repeat) x)
                                         y (nth 1 (nth i dialog))
                              ;; (mapconcat 'identity
                              ;;            (last (split-string
                              ;;                   text)
                              ;;                  (1+ (random 7)))
                              ;;            " ")
                              nil
                              ;; nil
                              (or (> i (/ i 2)))
                              (* (- 3 i) tail-height)
                              (+ (/ (or (cdr repeat) (car size1)) 2)))
            x (+ x (car size1))
            y (+ y (cdr size2))))

    (dom-set-attribute g-actors 'transform (format "translate(0,%d)"
                                                   (+ y tail-height)))
    (dom-append-child g-stage g-actors)
    (dom-append-child g-stage g-balloons)

    ;; Stage is center aligned in panel
    (dom-set-attribute g-stage 'transform (format "translate(%d,0)" (/ (- w x) 2)))
    (dom-set-attribute g-stage 'clip-path (format "url(#%s)" "clip"))
    (dom-append-child g-panel g-stage)
    (dom-set-attribute g-panel 'transform (format "translate(%d,%d)" x1 y1))
    (dom-append-child svg g-panel)
    ))

(defun comics--create-background (svg defs background x y w h)
  (let* ((i canvas--id)
         (aspect-t (/ w h 1.0))
         file size aspect-s w1 h1 node)
    ;; Add image def for re-use
    (setq file (comics--get-image 'background background)
          size (image-size (create-image file) t)
          aspect-s (/ (car size) (cdr size) 1.0))
    (if (> aspect-s aspect-t)
        (setq w1 (* aspect-s h)
              h1 h)
      (setq w1 w
            h1 (/ w aspect-s)))

    ;; Clip panel to size
    (unless defs
      (setq defs (dom-node 'defs))
      (svg--append svg defs)
    (setq node (svg-node defs 'clipPath :id "clip"))
    (svg-rectangle node 0 0 w h :id (+ i 3)
                   :fill "none")
    (svg-embed defs file (image-mime-type file) nil
               :id "background"
               :clip-path (format "url(#%s)" "clip")
               :width w1
               :height h1)
    )
    (svg-use svg "background" :id i
             :x x :y y)

    ;; Box outline
    (svg-rectangle svg x y w h :id (+ i 1)
                   :fill "none")
    (setq canvas--id (+ i 4))
    ))

(defun comics--create-actor (svg actor x &optional emotion flip)
  (let* ((i canvas--id)
         (trans "translate(%d,0)")
         (node (dom-node 'g))
         (actor (nth actor comics-avatars))
         action opts file file-face file-body str w h w1 h1 size)
    (when emotion
      (setq str (split-string emotion "/"))
      (if (= (length str) 1)
          (push (car str) str))
      (setq file-face (comics--get-image 'face
                                         (format "%s-%s" (car actor) (car str)))
            file-body (comics--get-image 'body
                                         (format "%s-%s" (cdr actor) (cadr str)))
            opts (rassoc-if (lambda (a) (member emotion a))
                            comics-emotions)
            action (pop opts)))

    ;; If exact match not found, try equivalence
    (unless (or (and file-face file-body) opts)
      (setq action 'neutral
            opts (alist-get action comics-emotions)))

    (setq file file-body)
    (while (and (not file) opts)
      (setq file (comics--get-image 'body
                                    (format "%s-%s" (cdr actor) (car opts))))
      (setq opts (cdr opts)))
    (setq size (image-size (create-image file) t)
          file-body file
          w (car size)
          h (cdr size))

    (setq file file-face
          opts (alist-get action comics-emotions))
    (while (and (not file) opts)
      (setq file (comics--get-image 'face
                                    (format "%s-%s" (car actor) (car opts))))
      (setq opts (cdr opts)))
    (setq size (image-size (create-image file) t)
          file-face file
          w1 (car size)
          h1 (cdr size))

    (when flip
      (setq trans (concat trans " scale(-1,1)")
            x (+ x w)))
    (dom-set-attribute node 'transform (format trans x))

    (svg-embed node file-body (image-mime-type file-body) nil
               :id (+ i 0)
               :x 0 :y (* 0.7 h1)
               :width w
               :height h)
    (svg-embed node file-face (image-mime-type file-face) nil
               :id (+ i 1)
               :x (/ (- w w1) 2) :y 0
               :width w1
               :height h1)

    (dom-append-child svg node)

    (setq canvas--id (+ i 2))
    (cons w (+ (* 0.7 h1) h))
    ))

(defun comics--create-balloon (svg x1 y1 txt &optional type right size x2)
  ;; Talk, shout, think, whisper, narrate balloons
  (let* ((i canvas--id)
         (type (or type 'speak))
         (padding 15)
         (font-size comics-font-size)
         (node (dom-node 'g))
         (text-node (dom-node 'g))
         x y w1 h1 dx bbox)
    (svg-render-tag text-node txt
                    0 0 (- (* 18 font-size) (* 2 padding)) font-size)
    (setq i canvas--id)
    (setq bbox (svg-bbox (last text-node))
          w1 (+ (* 2 padding) (- (nth 2 bbox) (nth 0 bbox)))
          h1 (+ (* 2 padding) (- (nth 3 bbox) (nth 1 bbox))))
    (setq x (+ (/ w1 5))
          x2 (or x2 (/ w1 3))
          dx (min 10 (/ h1 2))
          y (+ (* h1 1.1)))
    (svg-rectangle node 0 0 w1 y :id (+ i 1)
                   :rx (unless (eq type 'narrate) padding)
                   :fill "white")

    (unless (eq type 'narrate)
      (if right
          (setq x (- w1 x)
                x2 (or x2 (- w1 x2))
                dx (- dx)))
      (svg-polyline node `((,x . ,y)
                          (,x2 . ,(+ y size))
                          (,(+ x dx) . ,y))
                    :fill "white"
                    :id (+ i 2))
      (svg-line node x y (+ x dx) y
                :stroke-color "white"
                :stroke-width 2
                :id (+ i 3)))
    (dom-set-attribute node 'transform (format "translate(%d,%d)" x1 y1))
    (dom-set-attribute text-node 'transform (format "translate(%d,%d)"
                                                    padding padding))
    (dom-append-child node text-node)
    (dom-append-child svg node)
    (setq canvas--id (+ i 4))
    (cons w1 y)
    ))

;;;###autoload
(defun comics (&optional prefix)
  "Create a comics strip (an SVG image)."
  (interactive "P")
  (let* ((buf (get-buffer-create "*Comics*"))
         (bbox (window-body-pixel-edges))
         (w (- (nth 2 bbox) (nth 0 bbox)))
         (h (- (nth 3 bbox) (nth 1 bbox)))
         (svg (svg-create w h :stroke-color "black"
                          :font-family "DejaVu Sans"))
         )
    (comics--create svg w h)
    (pop-to-buffer buf)

    (with-current-buffer buf
      (setq inhibit-read-only t)
      (erase-buffer)
      ;; (canvas-mode nil svg)
      ;; (insert-image (svg-image svg))
      (svg-print svg)
      (image-mode)

      )
    ))

(provide 'comics)
