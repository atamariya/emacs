;;; xwidget.el --- api functions for xwidgets  -*- lexical-binding: t -*-
;;
;; Copyright (C) 2011-2020 Free Software Foundation, Inc.
;;
;; Author: Joakim Verona (joakim@verona.se)
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
;;
;; --------------------------------------------------------------------

;;; Commentary:
;;
;; See xwidget.c for more api functions.

;; This breaks compilation when we don't have xwidgets.
;; And is pointless when we do, since it's in C and so preloaded.
;;(require 'xwidget-internal)

;;; Code:

(require 'cl-lib)
(require 'bookmark)
(require 'subr-x)
(require 'webkit-ace)

(defvar xwidget-webkit-block-file "~/block.txt")

;;Must begin with a protocol
(defvar xwidget-webkit-home-page "http://www.google.com")
(defvar xwidget-webkit-search-url "https://www.google.com/search?q=")
;; Defined in C
(defvar xwidget-webkit-block-list nil)
(defvar xwidget-webkit-user-fields
  '("username" "identifier" "login" "email" "email-or-phone" "user[login]"))
(defvar xwidget-webkit-password-fields
  '("password" "passwd" "pass" "user[password]"))

(declare-function make-xwidget "xwidget.c"
                  (type title width height arguments &optional buffer))
(declare-function xwidget-buffer "xwidget.c" (xwidget))
(declare-function xwidget-size-request "xwidget.c" (xwidget))
(declare-function xwidget-resize "xwidget.c" (xwidget new-width new-height))
(declare-function xwidget-webkit-execute-script "xwidget.c"
                  (xwidget script &optional callback))
(declare-function xwidget-webkit-goto-uri "xwidget.c" (xwidget uri))
(declare-function xwidget-webkit-zoom "xwidget.c" (xwidget factor))
(declare-function xwidget-plist "xwidget.c" (xwidget))
(declare-function set-xwidget-plist "xwidget.c" (xwidget plist))
(declare-function xwidget-view-window "xwidget.c" (xwidget-view))
(declare-function xwidget-view-model "xwidget.c" (xwidget-view))
(declare-function delete-xwidget-view "xwidget.c" (xwidget-view))
(declare-function get-buffer-xwidgets "xwidget.c" (buffer))
(declare-function xwidget-query-on-exit-flag "xwidget.c" (xwidget))

(defun xwidget-insert (pos type title width height &optional args)
  "Insert an xwidget at position POS.
If POS is nil, insert at point.
Supply the xwidget's TYPE, TITLE, WIDTH, and HEIGHT.
See `make-xwidget' for the possible TYPE values.
The usage of optional argument ARGS depends on the xwidget.
This returns the result of `make-xwidget'."
  (setq width (or width (window-pixel-width))
        height (or height (window-pixel-height)))
  (goto-char (or pos (point)))
  (when (eobp)
    (insert " ")
    (forward-char -1))
  (let ((id (make-xwidget type title width height args)))
    (put-text-property (point) (+ 1 (point))
                       'display (list 'xwidget ':xwidget id))
    (forward-char)
    id))

(defun xwidget-at (pos)
  "Return xwidget at POS."
  ;; TODO this function is a bit tedious because the C layer isn't well
  ;; protected yet and xwidgetp apparently doesn't work yet.
  (let* ((disp (get-text-property pos 'display))
         (xw (car (cdr (cdr  disp)))))
    ;;(if (xwidgetp  xw) xw nil)
    (if (equal 'xwidget (car disp)) xw)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; webkit support
(require 'browse-url)
(require 'image-mode);;for some image-mode alike functionality

;;;###autoload
(defun xwidget-webkit-browse-url-new-session (url &optional _new-session)
  "Ask xwidget-webkit to browse URL in a new xwidget-webkit session.
Interactively, URL defaults to the string looking like a url around point."
  (interactive (progn
                 (require 'browse-url)
                 (browse-url-interactive-arg "xwidget-webkit URL: "
                                             ;;(xwidget-webkit-current-url)
                                             )))
  (xwidget-webkit-browse-url url t))

;;;###autoload
(defun xwidget-webkit-browse-url (url &optional new-session)
  "Ask xwidget-webkit to browse URL.
NEW-SESSION specifies whether to create a new xwidget-webkit session.
Interactively, URL defaults to the string looking like a url around point."
  (interactive (progn
                 (require 'browse-url)
                 (browse-url-interactive-arg "xwidget-webkit URL: "
                                             ;;(xwidget-webkit-current-url)
                                             )))
  (or (featurep 'xwidget-internal)
      (user-error "Your Emacs was not compiled with xwidgets support"))
  (when (stringp url)
    (if (string-empty-p url)
        (setq url xwidget-webkit-home-page))
    (if (string-match-p "^\\(~\\|/\\)" url)
        (setq url (concat "file://" (expand-file-name url))))
    (if (and (string-match "\\.\\(com\\|org\\|net\\|edu\\|gov\\|mil\\)" url)
             (not (string-match "^http" url)))
        (setq url (concat "https://" url)))
    (if (and (not (string-match "^\\(http\\|file:\\|about:\\)" url))
             xwidget-webkit-search-url)
        ;; Search string
        (setq url (concat xwidget-webkit-search-url url)))
    (if new-session
        (xwidget-webkit-new-session url)
      (xwidget-webkit-goto-url url))))

;;todo.
;; - check that the webkit support is compiled in
(defvar xwidget-webkit-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "g" 'xwidget-webkit-browse-url)
    (define-key map "G" 'xwidget-webkit-browse-url-new-session)
    (define-key map "a" 'xwidget-webkit-adjust-size-dispatch)
    (define-key map "b" 'xwidget-webkit-back)
    (define-key map "f" 'xwidget-webkit-forward)
    (define-key map "r" 'xwidget-webkit-reload)
    (define-key map "o" 'webkit-ace)
    (define-key map "\C-m" 'xwidget-webkit-insert-string)
    (define-key map "\C-i" 'xwidget-webkit-insert-key)
    (define-key map "\C-p" 'xwidget-webkit-insert-cred)
    (define-key map "\C-j" 'xwidget-webkit-exec-js)
    (define-key map "w" 'xwidget-webkit-current-url)
    (define-key map "+" 'xwidget-webkit-zoom-in)
    (define-key map "-" 'xwidget-webkit-zoom-out)
    (define-key map "z" 'xwidget-webkit-zoom-reset)

    ;;similar to image mode bindings
    (define-key map (kbd "SPC")                 'xwidget-webkit-scroll-up)
    (define-key map (kbd "DEL")                 'xwidget-webkit-scroll-down)

    (define-key map [remap isearch-forward]           'webkit-search-forward)
    (define-key map [remap isearch-backward]          'webkit-search-forward)
    (define-key map [remap isearch-abort]             'webkit-search-finish)
    (define-key map [remap isearch-repeat-forward]    'webkit-search-next)
    (define-key map [remap isearch-repeat-backward]   'webkit-search-previous)

    (define-key map [remap scroll-up]           'xwidget-webkit-scroll-up)
    (define-key map [remap scroll-up-command]   'xwidget-webkit-scroll-up)

    (define-key map [remap scroll-down]         'xwidget-webkit-scroll-down)
    (define-key map [remap scroll-down-command] 'xwidget-webkit-scroll-down)

    (define-key map [remap forward-char]        'xwidget-webkit-scroll-right)
    (define-key map [remap backward-char]       'xwidget-webkit-scroll-left)
    (define-key map [remap right-char]          'xwidget-webkit-scroll-right)
    (define-key map [remap left-char]           'xwidget-webkit-scroll-left)
    (define-key map [remap previous-line]       'xwidget-webkit-scroll-down)
    (define-key map [remap next-line]           'xwidget-webkit-scroll-up)

    (define-key map [remap move-beginning-of-line]
      'xwidget-webkit-beginning-of-line)
    (define-key map [remap move-end-of-line]       'xwidget-webkit-end-of-line)
    (define-key map [remap beginning-of-buffer] 'xwidget-webkit-scroll-top)
    (define-key map [remap end-of-buffer]       'xwidget-webkit-scroll-bottom)
    (define-key map [remap bookmark-set]  'xwidget-webkit-bookmark-make-record)
    map)
  "Keymap for `xwidget-webkit-mode'.")

(defun xwidget-webkit-zoom-in ()
  "Increase webkit view zoom factor."
  (interactive)
  (xwidget-webkit-zoom (xwidget-webkit-current-session) 0.1))

(defun xwidget-webkit-zoom-out ()
  "Decrease webkit view zoom factor."
  (interactive)
  (xwidget-webkit-zoom (xwidget-webkit-current-session) -0.1))

(defun xwidget-webkit-zoom-reset ()
  "Reset webkit view zoom factor."
  (interactive)
  (xwidget-webkit-zoom (xwidget-webkit-current-session) 1.0))

(defun webkit-search ()
  "Search TEXT in webkit."
  (interactive)
  (xwidget-webkit-search (xwidget-webkit-current-session) isearch-string))

(defun webkit-search-forward ()
  "Search TEXT in webkit."
  (interactive)
  (isearch-mode t nil 'webkit-search t))

(defun webkit-search-finish ()
  "Finish search in webkit."
  (interactive)
  (xwidget-webkit-search-finish (xwidget-webkit-current-session)))

(defun webkit-search-next ()
  "Search next in webkit."
  (interactive)
  (xwidget-webkit-search-next (xwidget-webkit-current-session)))

(defun webkit-search-previous ()
  "Search previous in webkit."
  (interactive)
  (xwidget-webkit-search-previous (xwidget-webkit-current-session)))

(defun xwidget-webkit-scroll-up ()
  "Scroll webkit up."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollBy(0, 0.8 * window.innerHeight);"))

(defun xwidget-webkit-scroll-down ()
  "Scroll webkit down."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollBy(0, -0.8 * window.innerHeight);"))

(defun xwidget-webkit-scroll-right ()
  "Scroll webkit forwards."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollBy(50, 0);"))

(defun xwidget-webkit-scroll-left ()
  "Scroll webkit backwards."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollBy(-50, 0);"))

(defun xwidget-webkit-beginning-of-line ()
  "Scroll webkit to the beginning of line."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollTo(0, pageYOffset);"))

(defun xwidget-webkit-end-of-line ()
  "Scroll webkit to the end of line."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollTo(window.document.body.scrollWidth, pageYOffset);"))

(defun xwidget-webkit-scroll-top ()
  "Scroll webkit to the very top."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollTo(pageXOffset, 0);"))

(defun xwidget-webkit-scroll-bottom ()
  "Scroll webkit to the very bottom."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.scrollTo(pageXOffset, window.document.body.scrollHeight);"))

;; The xwidget event needs to go into a higher level handler
;; since the xwidget can generate an event even if it's offscreen.
;; TODO this needs to use callbacks and consider different xwidget event types.
(define-key (current-global-map) [xwidget-event] #'xwidget-event-handler)
(defun xwidget-log (&rest msg)
  "Log MSG to a buffer."
  (let ((buf (get-buffer-create " *xwidget-log*")))
    (with-current-buffer buf
      (insert (apply #'format msg))
      (insert "\n"))))

(defun xwidget-webkit-load-progress (progress)
  (message "Loading %d%%" progress))

(defun xwidget-event-handler ()
  "Receive xwidget event."
  (interactive)
  (xwidget-log "stuff happened to xwidget %S" last-input-event)
  (let*
      ((xwidget-event-type (nth 1 last-input-event))
       (xwidget (nth 2 last-input-event))
       ;;(xwidget-callback (xwidget-get xwidget 'callback))
       ;;TODO stopped working for some reason
       )
    ;;(funcall  xwidget-callback xwidget xwidget-event-type)
    (message "xw callback %s" xwidget)
    (funcall  'xwidget-webkit-callback xwidget xwidget-event-type)))

(defun xwidget-webkit-callback (xwidget xwidget-event-type)
  "Callback for xwidgets.
XWIDGET instance, XWIDGET-EVENT-TYPE depends on the originating xwidget."
  (if (not (buffer-live-p (xwidget-buffer xwidget)))
      (xwidget-log
       "error: callback called for xwidget with dead buffer")
    (with-current-buffer (xwidget-buffer xwidget)
      (cond ((eq xwidget-event-type 'load-changed)
             (xwidget-webkit-execute-script
              xwidget "document.title"
              (lambda (title)
                (xwidget-log "webkit finished loading: '%s'" title)
                ;;TODO - check the native/internal scroll
                ;;(xwidget-adjust-size-to-content xwidget)
                ;; (xwidget-webkit-adjust-size-to-window xwidget)
                (rename-buffer (format "*xwidget webkit: %s *" title))))
             (pop-to-buffer (current-buffer)))
            ((eq xwidget-event-type 'decide-policy)
               ;; (unless xwidget-webkit-block-list
               ;;   (with-temp-buffer
               ;;     (insert-file-contents
               ;;      (expand-file-name xwidget-webkit-block-file))
	       ;;     (setq xwidget-webkit-block-list
               ;;           (split-string (buffer-string)))
	       ;;     ))
               ;; (let ((strarg  (nth 3 last-input-event)))
               ;;   (if (and strarg (string-match-p ".*#\\(.*\\)" strarg))
               ;;       (xwidget-webkit-show-id-or-named-element
               ;;        xwidget
               ;;        (match-string 1 strarg))))
               )
            ((eq xwidget-event-type 'javascript-callback)
             (let ((proc (nth 3 last-input-event))
                   (arg  (nth 4 last-input-event)))
               (funcall proc arg)))
            (t (xwidget-log "unhandled event:%s" xwidget-event-type))))))

(defvar bookmark-make-record-function)
(define-derived-mode xwidget-webkit-mode
    special-mode "xwidget-webkit" "Xwidget webkit view mode."
    (setq buffer-read-only t)
    ;; Avoid flickering due to blinking cursor
    (blink-cursor-mode 0)
    (setq-local resize-mini-windows nil)
    ;; (toggle-scroll-bar -1)
    ;; (setq-local bookmark-make-record-function
    ;;             #'xwidget-webkit-bookmark-make-record)
    (setq-local browse-url-browser-function 'xwidget-webkit-browse-url)
    ;; Keep track of [vh]scroll when switching buffers
    (image-mode-setup-winprops))

(defun xwidget-webkit-bookmark-make-record ()
  "Integrate Emacs bookmarks with the webkit xwidget."
  (interactive)
  (xwidget-webkit-current-url t))

(defun xwidget-webkit-bookmark-record-finish (title url)
  "Integrate Emacs bookmarks with the webkit xwidget."
  (setq title (read-string "Name: " title))
  (bookmark-store
   title
   (nconc (bookmark-make-record-default t t)
          `((page     . ,url)
            (handler  . (lambda (bmk)
                          (browse-url (bookmark-prop-get bmk 'page))))))
   t))


(defvar xwidget-webkit-last-session-buffer nil)

(defun xwidget-webkit-last-session ()
  "Last active webkit, or nil."
  (if (buffer-live-p xwidget-webkit-last-session-buffer)
      (with-current-buffer xwidget-webkit-last-session-buffer
        (xwidget-at (point-min)))
    nil))

(defun xwidget-webkit-current-session ()
  "Either the webkit in the current buffer, or the last one used.
The latter might be nil."
  (or (xwidget-at (point-min)) (xwidget-webkit-last-session)))

(defun xwidget-adjust-size-to-content (xw)
  "Resize XW to content."
  ;; xwidgets doesn't support widgets that have their own opinions about
  ;; size well, yet this reads the desired size and resizes the Emacs
  ;; allocated area accordingly.
  (let ((size (xwidget-size-request xw)))
    (xwidget-resize xw (car size) (cadr size))))


(defvar xwidget-webkit-activeelement-js"
function findactiveelement(doc) {
  doc = doc || document;
//alert(doc.activeElement.value);
   if(doc.activeElement.value != undefined){
      return doc.activeElement;
   }else{
        // recurse over the child documents:
        var frames = doc.getElementsByTagName('frame');
        for (var i = 0; i < frames.length; i++)
        {
                var d = frames[i].contentDocument;
                 var rv = findactiveelement(d);
                 if(rv != undefined){
                    return rv;
                 }
        }
        frames = doc.getElementsByTagName('iframe');
        for (var i = 0; i < frames.length; i++)
        {
                var d = frames[i].contentWindow;
                 var rv = findactiveelement(d);
                 if(rv != undefined){
                    return rv;
                 }
        }
    }
    return undefined;
};


"

  "javascript that finds the active element."
  ;; Yes it's ugly, because:
  ;; - there is apparently no way to find the active frame other than recursion
  ;; - the js "for each" construct misbehaved on the "frames" collection
  ;; - a window with no frameset still has frames.length == 1, but
  ;; frames[0].document.activeElement != document.activeElement
  ;;TODO the activeelement type needs to be examined, for iframe, etc.
  )

(defun xwidget-webkit-exec-js (script)
  "Convenient wrapper for `xwidget-webkit-execute-script'."
  (interactive "sJS: ")
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   script
   'pp))

(defun xwidget-webkit-insert-cred (&optional step host port)
  (interactive)
  (let ((auth-source-creation-prompts
         '((password . "Enter password for %u@%h: ")))
        (step (or step 1))
        user secret found save-fn secret-fn)
    (pcase step
      (1
       ;; Find credentials for host
       (xwidget-webkit-execute-script
        (xwidget-webkit-current-session)
        "[window.location.host, window.location.protocol]"
        '(lambda (a)
           (setq host (aref a 0)
                 protocol (aref a 1))
           ;; port (if (string-match-p "https" protocol) 443 80))
           ;; (message "a %s" host)
           (if (string-match-p "https" protocol)
               ;; Don't enter credentials in insecure sites
               (xwidget-webkit-insert-cred 2 host)))))
      (2
       ;; Let the user select appropriate user
       (setq found (auth-source-search :host host
                                       :port port
                                       :max 10
                                       ))
       ;; (pp found)

       (when (> (length found) 1)
         (setq user (completing-read "Select user: "
                                     (mapcar (lambda (a)
                                               (plist-get a :user))
                                             found)))

         ;; Find the credentials for host:port:user
         (setq found (auth-source-search :host host
                                         :port port
                                         :user user
                                         )))
       (when found
         (setq save-fn (plist-get (car found) :save-function)
               secret-fn (plist-get (car found) :secret)
               user (plist-get (car found) :user)
               secret (if secret-fn (funcall secret-fn)))
         ;; (message "%s %s" user secret)
         (if save-fn (funcall save-fn))

         ;; Enter credentials in html form
         (xwidget-webkit-execute-script
          (xwidget-webkit-current-session)
          (format "
function fill(name, val) {
  var el = document.getElementsByName(name);
  if (el.length > 0) {
    el = el[0];
    el.value = val;
    // el.click();
    console.log(el);
  }
}
var user = '%s';
var password = '%s';
var user_fields = [%s];
var pass_fields = [%s];
user_fields.forEach(e => fill(e, user));
pass_fields.forEach(e => fill(e, password));
" user secret
(mapconcat #'(lambda (a) (format "\"%s\"" a)) xwidget-webkit-user-fields ", ")
(mapconcat #'(lambda (a) (format "\"%s\"" a)) xwidget-webkit-password-fields ", ")))
         )))))

(defun xwidget-webkit-insert-string ()
  "Prompt for a string and insert it in the active field in the
current webkit widget."
  ;; Read out the string in the field first and provide for edit.
  (interactive)
  (let ((xww (xwidget-webkit-current-session)))
    (xwidget-webkit-execute-script
     xww
     (concat xwidget-webkit-activeelement-js "
(function () {
  var res = findactiveelement();
  return [res.value, res.type];
})();")
     (lambda (field)
       (let ((str (pcase field
                    (`[,val "text"]
                     (read-string "Text: " val))
                    (`[,val "email"]
                     (read-string "Email: " val))
                    (`[,val "password"]
                     (read-passwd "Password: " nil val))
                    (`[,val "textarea"]
                     (xwidget-webkit-begin-edit-textarea xww val)))))
         (xwidget-webkit-execute-script
          xww
          (format "findactiveelement().value='%s'" str)))))))

(defun xwidget-webkit-insert-key (&optional key)
  "Insert one KEY at a time."
  (interactive)
  (let ((script nil))
    (setq key (read-key "Press RET to exit"))
    (setq script
          (pcase key
            (?\C-m nil) ;; Exit on RET
            ((guard (mouse-event-p key)) nil)
            (?\C-? "
var el = findactiveelement();
if (el && el.value.length > 0) {
  var s = el.value;
  s = s.substring(0, s.length - 1);
  el.value = s;
  el.click();
}
")
            (_ (format "
var el = findactiveelement();
if (el) {
  el.value +='%c';
  el.click();
}"  key))
            ))
    (when script
      ;; (message "insert %c" key)
      (xwidget-webkit-execute-script
       (xwidget-webkit-current-session)
       script
       'xwidget-webkit-insert-key))))

(defvar xwidget-xwbl)
(defun xwidget-webkit-begin-edit-textarea (xw text)
  "Start editing of a webkit text area.
XW is the xwidget identifier, TEXT is retrieved from the webkit."
  (switch-to-buffer
   (get-buffer-create "textarea"))
  (erase-buffer)
  (local-set-key "\C-c\ \C-c" 'xwidget-webkit-end-edit-textarea)
  (set (make-local-variable 'xwidget-xwbl) xw)
  (if text (insert text)))

(defun xwidget-webkit-end-edit-textarea ()
  "End editing of a webkit text area."
  (interactive)
  (goto-char (point-min))
  (while (search-forward "\n" nil t)
    (replace-match "\\n" nil t))
  (xwidget-webkit-execute-script
   xwidget-xwbl
   (format "findactiveelement().value='%s'"
           (buffer-substring (point-min) (point-max))))
  (kill-buffer))

(defun xwidget-webkit-show-element (xw element-selector)
  "Make webkit xwidget XW show a named element ELEMENT-SELECTOR.
The ELEMENT-SELECTOR must be a valid CSS selector.  For example,
use this to display an anchor."
  (interactive (list (xwidget-webkit-current-session)
                     (read-string "Element selector: ")))
  (xwidget-webkit-execute-script
   xw
   (format "
(function (query) {
  var el = document.querySelector(query);
  if (el !== null) {
    window.scrollTo(0, el.offsetTop);
  }
})('%s');"
    element-selector)))

(defun xwidget-webkit-show-named-element (xw element-name)
  "Make webkit xwidget XW show a named element ELEMENT-NAME.
For example, use this to display an anchor."
  (interactive (list (xwidget-webkit-current-session)
                     (read-string "Element name: ")))
  ;; TODO: This needs to be interfaced into browse-url somehow.  The
  ;; tricky part is that we need to do this in two steps: A: load the
  ;; base url, wait for load signal to arrive B: navigate to the
  ;; anchor when the base url is finished rendering
  (xwidget-webkit-execute-script
   xw
   (format "
(function (query) {
  var el = document.getElementsByName(query)[0];
  if (el !== undefined) {
    window.scrollTo(0, el.offsetTop);
  }
})('%s');"
    element-name)))

(defun xwidget-webkit-show-id-element (xw element-id)
  "Make webkit xwidget XW show an id-element ELEMENT-ID.
For example, use this to display an anchor."
  (interactive (list (xwidget-webkit-current-session)
                     (read-string "Element id: ")))
  (xwidget-webkit-execute-script
   xw
   (format "
(function (query) {
  var el = document.getElementById(query);
  if (el !== null) {
    window.scrollTo(0, el.offsetTop);
  }
})('%s');"
    element-id)))

(defun xwidget-webkit-show-id-or-named-element (xw element-id)
   "Make webkit xwidget XW show a name or element id ELEMENT-ID.
For example, use this to display an anchor."
  (interactive (list (xwidget-webkit-current-session)
                     (read-string "Name or element id: ")))
  (xwidget-webkit-execute-script
   xw
   (format "
(function (query) {
  var el = document.getElementById(query) ||
           document.getElementsByName(query)[0];
  if (el !== undefined) {
    window.scrollTo(0, el.offsetTop);
  }
})('%s');"
    element-id)))

(defun xwidget-webkit-adjust-size-to-content ()
  "Adjust webkit to content size."
  (interactive)
  (xwidget-adjust-size-to-content (xwidget-webkit-current-session)))

(defun xwidget-webkit-adjust-size-dispatch ()
  "Adjust size according to mode."
  (interactive)
  (xwidget-webkit-adjust-size-to-window (xwidget-webkit-current-session))
  ;; The recenter is intended to correct a visual glitch.
  ;; It errors out if the buffer isn't visible, but then we don't get
  ;; the glitch, so silence errors.
  (ignore-errors
    (recenter-top-bottom)))

(defun xwidget-webkit-adjust-size-to-window (xwidget &optional window)
  "Adjust the size of the webkit XWIDGET to fit the WINDOW."
  (xwidget-resize xwidget
                  (window-pixel-width window)
                  (window-pixel-height window)))

(defun xwidget-webkit-adjust-size (w h)
  "Manually set webkit size to width W, height H."
  ;; TODO shouldn't be tied to the webkit xwidget
  (interactive "nWidth:\nnHeight:\n")
  (xwidget-resize (xwidget-webkit-current-session) w h))

(defun xwidget-webkit-fit-width ()
  "Adjust width of webkit to window width."
  (interactive)
  (xwidget-webkit-adjust-size (- (nth 2 (window-inside-pixel-edges))
                                 (car (window-inside-pixel-edges)))
                              1000))

(defun xwidget-webkit-auto-adjust-size (window)
  "Adjust the size of the webkit widget in the given WINDOW."
  (with-current-buffer (window-buffer window)
    (when (eq major-mode 'xwidget-webkit-mode)
      (let ((xwidget (xwidget-webkit-current-session)))
        (xwidget-webkit-adjust-size-to-window xwidget window)))))

(defun xwidget-webkit-adjust-size-in-frame (frame)
  "Dynamically adjust webkit widget for all windows of the FRAME."
  (walk-windows 'xwidget-webkit-auto-adjust-size 'no-minibuf frame))

(eval-after-load 'xwidget-webkit-mode
  (add-to-list 'window-size-change-functions
               'xwidget-webkit-adjust-size-in-frame))

(defun xwidget-webkit-new-session (url)
  "Create a new webkit session buffer with URL."
  (let*
      ((bufname (generate-new-buffer-name "*xwidget-webkit*"))
       xw)
    (setq xwidget-webkit-last-session-buffer (switch-to-buffer
                                              (get-buffer-create bufname)))
    ;; The xwidget id is stored in a text property, so we need to have
    ;; at least character in this buffer.
    (insert " ")
    (setq xw (xwidget-insert 1 'webkit bufname
                             (window-pixel-width)
                             (window-pixel-height)))
    (xwidget-put xw 'callback 'xwidget-webkit-callback)
    (xwidget-webkit-mode)
    (webkit--ace-init)
    (xwidget-webkit-goto-uri (xwidget-webkit-last-session) url)))


(defun xwidget-webkit-goto-url (url)
  "Goto URL."
  (if (xwidget-webkit-current-session)
      (progn
        (xwidget-webkit-goto-uri (xwidget-webkit-current-session) url))
    (xwidget-webkit-new-session url)))

(defun xwidget-webkit-back ()
  "Go back in history."
  (interactive)
  (xwidget-webkit-execute-script (xwidget-webkit-current-session)
                                 "history.go(-1);"))

(defun xwidget-webkit-forward ()
  "Go forward in history."
  (interactive)
  (xwidget-webkit-execute-script (xwidget-webkit-current-session)
                                 "history.go(1);"))

(defun xwidget-webkit-reload ()
  "Reload current url."
  (interactive)
  (xwidget-webkit-execute-script (xwidget-webkit-current-session)
                                 "location.reload();"))

(defun xwidget-webkit-current-url (&optional record)
  "Get the webkit url and place it on the kill-ring."
  (interactive)
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "[document.title, document.URL]"
   (lambda (rv)
     (let* ((title (aref rv 0))
            (url (aref rv 1)))
       (kill-new (or url ""))
       (message "url: %s" url)
       (if record (xwidget-webkit-bookmark-record-finish title url))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun xwidget-webkit-get-selection (proc)
  "Get the webkit selection and pass it to PROC."
  (xwidget-webkit-execute-script
   (xwidget-webkit-current-session)
   "window.getSelection().toString();"
   proc))

(defun xwidget-webkit-copy-selection-as-kill ()
  "Get the webkit selection and put it on the kill-ring."
  (interactive)
  (xwidget-webkit-get-selection (lambda (selection) (kill-new selection))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Xwidget plist management (similar to the process plist functions)

(defun xwidget-get (xwidget propname)
  "Get an xwidget's property value.
XWIDGET is an xwidget, PROPNAME a property.
Returns the last value stored with `xwidget-put'."
  (plist-get (xwidget-plist xwidget) propname))

(defun xwidget-put (xwidget propname value)
  "Set an xwidget's property value.
XWIDGET is an xwidget, PROPNAME a property to be set to specified VALUE.
You can retrieve the value with `xwidget-get'."
  (set-xwidget-plist xwidget
                     (plist-put (xwidget-plist xwidget) propname value)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar xwidget-view-list)              ; xwidget.c
(defvar xwidget-list)                   ; xwidget.c

(defun xwidget-delete-zombies ()
  "Helper for `xwidget-cleanup'."
  (dolist (xwidget-view xwidget-view-list)
    (when (or (not (window-live-p (xwidget-view-window xwidget-view)))
              (not (memq (xwidget-view-model xwidget-view)
                         xwidget-list)))
      (delete-xwidget-view xwidget-view))))

(defun xwidget-cleanup ()
  "Delete zombie xwidgets."
  ;; During development it was sometimes easy to wind up with zombie
  ;; xwidget instances.
  ;; This function tries to implement a workaround should it occur again.
  (interactive)
  ;; Kill xviews that should have been deleted but still linger.
  (xwidget-delete-zombies)
  ;; Redraw display otherwise ghost of zombies will remain to haunt the screen
  (redraw-display))

(defun xwidget-kill-buffer-query-function ()
  "Ask before killing a buffer that has xwidgets."
  (let ((xwidgets (get-buffer-xwidgets (current-buffer))))
    (or (not xwidgets)
        (not (memq t (mapcar #'xwidget-query-on-exit-flag xwidgets)))
        (yes-or-no-p
         (format "Buffer %S has xwidgets; kill it? " (buffer-name))))))

;;;###autoload
(defun xwidget-vlc (&optional title width height pos args)
  "Insert an VLC xwidget at position POS.
Supply the xwidget's TYPE, TITLE, WIDTH, and HEIGHT.
See `make-xwidget' for the possible TYPE values.
The usage of optional argument ARGS depends on the xwidget.
This returns the result of `make-xwidget'."
  (interactive)
  (setq args (or args '(:init nil))
        width (or width (window-pixel-width))
        height (or height (window-pixel-height)))
  (plist-put args :init "/usr/bin/cvlc -I rc --drawable-xid %lu")
  (xwidget-insert pos 'socket title width height args))

;;;###autoload
(defun xwidget-mpv-yt (uri &optional title width height pos args)
  (interactive "sURI: ")
  (xwidget-mpv (concat "--ytdl-format='[height <=? 360]' ytdl://"
                       uri)
               title width height pos args))

;;;###autoload
(defun xwidget-mpv (uri &optional title width height pos args)
  "Insert an MPV xwidget at position POS.
Supply the xwidget's TYPE, TITLE, WIDTH, and HEIGHT.
See `make-xwidget' for the possible TYPE values.
The usage of optional argument ARGS depends on the xwidget.
This returns the result of `make-xwidget'."
  (interactive "sURI: ")
  (setq args (or args '(:init nil)))
  (plist-put args :init
             (concat "/usr/bin/mpv --wid=%lu " uri))
  (xwidget-insert pos 'socket title width height args))

;;;###autoload
(defun xwidget-vim (&optional title width height pos args)
  "Insert an VI xwidget at position POS.
Supply the xwidget's TYPE, TITLE, WIDTH, and HEIGHT.
See `make-xwidget' for the possible TYPE values.
The usage of optional argument ARGS depends on the xwidget.
This returns the result of `make-xwidget'."
  (interactive)
  (setq args (or args '(:init nil))
        width (or width (window-pixel-width))
        height (or height (window-pixel-height)))
  (plist-put args :init "/usr/bin/gvim --servername emacs --socketid %lu")
  (xwidget-insert pos 'socket title width height args))

;;;###autoload
(defun xwidget-xterm (&optional title width height pos args)
  "Insert an XTERM xwidget at position POS.
Supply the xwidget's TYPE, TITLE, WIDTH, and HEIGHT.
See `make-xwidget' for the possible TYPE values.
The usage of optional argument ARGS depends on the xwidget.
This returns the result of `make-xwidget'."
  (interactive)
  (setq args (or args '(:init nil))
        width (or width (window-pixel-width))
        height (or height (window-pixel-height)))
  (plist-put args :init "/usr/bin/xterm -into %lu")
  (xwidget-insert pos 'socket title width height args))


(defvar xwidget-vlc--playing nil)
(defun xwidget-vlc-open-url (widget url)
  "Play any VLC supported URL."
  (if (expand-file-name url)
      (xwidget-socket-command widget (concat "add " url))))

(defun xwidget-vlc-is-playing (widget)
  "Check if VLC WIDGET is playing media."
  (let (out)
    (xwidget-socket-command widget "status")
    (setq out (xwidget-socket-read-output widget))
    (if (and out (string-match-p "new input" out))
        (xwidget-socket-read-output widget))
    (setq out (xwidget-socket-read-output widget)
          xwidget-vlc--playing (and out (string-match-p "playing" out)))))

(defun xwidget-vlc-play (widget)
  "Play VLC WIDGET."
  (setq xwidget-vlc--playing t)
  (xwidget-socket-command widget "play"))

(defun xwidget-vlc-pause (widget)
  "Pause VLC WIDGET."
  ;; VLC pause command toggles playing status.
  (let (out)
    (xwidget-socket-command widget "status")
    (setq out (xwidget-socket-read-output widget))
    (if (and out (string-match-p "new input" out))
        (xwidget-socket-read-output widget))
    (setq out (xwidget-socket-read-output widget)
          xwidget-vlc--playing
          (and out (string-match-p "\\(paused\\|stopped\\)" out)))
    (unless xwidget-vlc--playing
      (xwidget-socket-command widget "pause"))))

(defun xwidget-vlc-toggle-play (widget)
  "Toggle play/pause in VLC WIDGET."
  ;; VLC pause command toggles playing status.
  (xwidget-vlc-pause widget))

(defun xwidget-vlc-seek (widget time)
  "Seek TIME in seconds in VLC WIDGET"
  (xwidget-socket-command widget (format "seek %d" time)))

(defun xwidget-vlc-get-time (widget)
  "Seek TIME in seconds in VLC WIDGET"
  (xwidget-socket-command widget "get_time"))

(when (featurep 'xwidget-internal)
  (add-hook 'kill-buffer-query-functions #'xwidget-kill-buffer-query-function)
  ;; This would have felt better in C, but this seems to work well in
  ;; practice though.
  (add-hook 'window-configuration-change-hook #'xwidget-delete-zombies))

(provide 'xwidget)
;;; xwidget.el ends here
